/*******************************************************************************
 * Copyright (c) 2017, 2020 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.execution.ui.simmenu.wizards;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Panel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collection;
import java.util.Iterator;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.ExecutionModel;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.misc.EMTimeType;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.MalformedModelException;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMAllocation;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMCore;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMEdge;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMMapping;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMRunnable;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMTask;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMUtil;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.ISystemProxy;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.SimException;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.SystemProxyFactory;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.SchedulerAlgorithmRegister;
import org.eclipse.app4mc.multicore.execution.ui.traceview.TraceViewUtil;
import org.eclipse.app4mc.multicore.execution.ui.widget.emtracewidget.IEMTraceWidget;
import org.eclipse.app4mc.multicore.execution.ui.widget.model.XContainer;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Display;

public class TaskVisuWizard extends Wizard {

	protected TaskVisuOverviewPage page1;
	protected TaskVisuDependencyPage page2;
	protected TaskVisuSchedulingPage page3;

	protected IEMTraceWidget tw;
	protected OMMapping omModel;
	protected ConstraintsModel cm;

	private ISystemProxy systemProxy;
	XContainer con;


	/**
	 * Create task visualization wizard
	 *
	 * @param omModel
	 * @param tw
	 *            TraceView have to be opened before
	 * @param path
	 */
	private final String pa;

	public TaskVisuWizard(final IEMTraceWidget tw, final OMMapping model, final ConstraintsModel cm,
			final String path) {
		this.tw = tw;
		this.omModel = model;
		this.cm = cm;
		this.pa = path;
		this.con = XContainer.newInstance();
	}


	public void vievModel(final OMMapping omM) {
		System.out.println("###############Start Inspection########################");
		final Iterator<OMAllocation> it = omM.getAllocationList().iterator();
		OMAllocation tmp;
		while (it.hasNext()) {
			tmp = it.next();
			final OMTask ttp = tmp.getTask();
			final Iterator<OMRunnable> list = ttp.getRunnableCallSequence().iterator();
			System.out.println(tmp.getCore() + " " + ttp);
			while (list.hasNext()) {
				System.out.println("### -> " + list.next().getRunnableRef().toString().split(" ")[2]);
			}
		}

		System.out.println("###############Start Inspection########################");


	}

	@Override
	public String getWindowTitle() {
		return "Task Visualization Wizard";
	}

	@Override
	public void addPages() {
		this.page1 = new TaskVisuOverviewPage(this.omModel);
		this.page2 = new TaskVisuDependencyPage(this.omModel, this.cm);
		this.page3 = new TaskVisuSchedulingPage(this.tw, this.omModel, this.con);

		addPage(this.page1);
		addPage(this.page2);
		addPage(this.page3);
	}


	@Override
	public boolean performFinish() {
		final String sched = this.page3.getScheduling();
		final long schedtime = this.page3.getSimuntil();
		final EMTimeType timeScale = this.page3.getTimeScale();

		final Collection<OMEdge> taskPrecedence = this.page2.getTaskPrecedences();
		final boolean considerPrecedence = this.page2.considerTaskPrecedence();

		final Job job = new Job("Task Visualisation Job") {
			@Override
			protected IStatus run(final IProgressMonitor monitor) {

				try {
					final ExecutionModel execResult = new ExecutionModel();
					System.out.println(TaskVisuWizard.this.pa);
					execResult.setPath(TaskVisuWizard.this.pa);
					execResult.setTimeScale(timeScale);
					TaskVisuWizard.this.systemProxy = SystemProxyFactory.createEMSystemProxyFromOMMapping(
							timeScale.picos(), SchedulerAlgorithmRegister.schedulerFactoryByName(sched),
							TaskVisuWizard.this.omModel, execResult);
					if (considerPrecedence) {
						for (final OMEdge edge : taskPrecedence) {
							final OMCore preCore = OMUtil.getCoreForTask(TaskVisuWizard.this.omModel, edge.getPre());
							final OMCore postCore = OMUtil.getCoreForTask(TaskVisuWizard.this.omModel, edge.getPost());
							final long releaseTime = OMUtil.getProcessingTime(preCore, edge.getReleaseInstruction());

							TaskVisuWizard.this.systemProxy.addTaskPrecedence(preCore.getCoreRef().getName(),
									edge.getPre().getTaskRef().getName(), releaseTime, postCore.getCoreRef().getName(),
									edge.getPost().getTaskRef().getName());
						}
					}


					TaskVisuWizard.this.tw.addExecutionModel(execResult);
					System.err.println("STARTING SIM");


					final long cur = System.currentTimeMillis();
					TaskVisuWizard.this.systemProxy.compute(schedtime);
					monitor.done();
					System.out.println("Finish");

					TaskVisuWizard.this.tw.setFinish();
					if (TaskVisuWizard.this.con.isDataFlow() && !TaskVisuWizard.this.con.isMergingArrow()) {
						JOptionPane.showMessageDialog(null, "Report:\n" + TaskVisuWizard.this.con.getReport());
					}
					if (TaskVisuWizard.this.con.isMergingArrow() && TaskVisuWizard.this.con.isDataFlow()) {
						renderBetterReport(TaskVisuWizard.this.con);
					}


					System.err.println("READY duration:" + (System.currentTimeMillis() - cur));
				}
				catch (SimException | MalformedModelException e) {
					e.printStackTrace();
					getDisplay().asyncExec(new Runnable() {
						@Override
						public void run() {
							MessageDialog.openInformation(TraceViewUtil.getTraceViewPart().getSite().getShell(),
									"Task Visualization", e.getMessage());
						}
					});
				}
				return Status.OK_STATUS;
			}

			@Override
			protected void canceling() {
				TaskVisuWizard.this.tw.setFinish();
				super.canceling();
				System.err.println("Canceling task visualization");
				if (TaskVisuWizard.this.systemProxy != null) {
					TaskVisuWizard.this.systemProxy.interruptComputation();
				}
			}
		};

		// Start the Job
		job.schedule();
		return true;
	}

	private void renderBetterReport(final XContainer con) {
		Object[][] data;
		try {
			data = con.generateReport();
		}
		catch (final NullPointerException e) {

			JOptionPane.showMessageDialog(null, "The data flow report could not be generated due to the missed goals",
					"Error", JOptionPane.ERROR_MESSAGE);
			return;
		}

		final Object[] columnNames = { "Name", "Total Execution", "Oversampling Exe.", "Undersampling Exe." };

		final JDialog dialog = new JDialog();
		final JPanel contentPanel = new JPanel();
		dialog.setAlwaysOnTop(true);
		dialog.setResizable(false);
		dialog.setBounds(100, 100, 700, 400);
		dialog.getContentPane().setLayout(new BorderLayout());
		// ############################################################################################################################
		final Panel panel2 = new Panel();
		dialog.getContentPane().add(panel2, BorderLayout.NORTH);

		final Box verticalBox1 = Box.createVerticalBox();
		panel2.add(verticalBox1);

		final Panel header = new Panel();
		verticalBox1.add(header);
		header.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		final JLabel lblNewLabel = new JLabel("Report");
		header.add(lblNewLabel);

		final Panel infoSpace = new Panel();
		verticalBox1.add(infoSpace);

		final JLabel core = new JLabel("Cores:");
		infoSpace.add(core);

		final JLabel coreCounter = new JLabel("" + con.getCores().size());
		coreCounter.setForeground(Color.RED);
		infoSpace.add(coreCounter);

		final JLabel runnable = new JLabel("Runnables:");
		infoSpace.add(runnable);

		final JLabel runnableCounter = new JLabel("" + con.getRunnables().size());
		runnableCounter.setForeground(Color.RED);
		infoSpace.add(runnableCounter);

		final JLabel labels = new JLabel("Labels:");
		infoSpace.add(labels);

		final JLabel label = new JLabel("" + con.getLabels().size());
		label.setForeground(Color.RED);
		infoSpace.add(label);

		final Panel panel = new Panel();
		verticalBox1.add(panel);

		final JLabel globalChain = new JLabel("Global Eventchain:");
		panel.add(globalChain);

		final JLabel lblGlobalChain = new JLabel("" + con.isHaveglobal());
		lblGlobalChain.setForeground(Color.RED);
		panel.add(lblGlobalChain);


		final JTable table = new JTable(data, columnNames) {
			@Override
			public Dimension getPreferredScrollableViewportSize() {
				final Dimension d = getPreferredSize();
				final int n = getRowHeight();
				return new Dimension(d.width * 2, (n * con.getCores().size()));
			}
		};
		final JScrollPane sp = new JScrollPane(table);
		verticalBox1.add(contentPanel);

		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.add(sp);


		final JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		dialog.getContentPane().add(buttonPane, BorderLayout.SOUTH);

		final JButton okButton = new JButton("OK");
		okButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				dialog.setVisible(false);
			}
		});
		okButton.setActionCommand("OK");
		buttonPane.add(okButton);
		dialog.getRootPane().setDefaultButton(okButton);


		// ############################################################################################################################

		//
		dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		dialog.setVisible(true);
	}

	public static Display getDisplay() {
		Display display = Display.getCurrent();
		// may be null if outside the UI thread
		if (display == null) {
			display = Display.getDefault();
		}
		return display;
	}


}
