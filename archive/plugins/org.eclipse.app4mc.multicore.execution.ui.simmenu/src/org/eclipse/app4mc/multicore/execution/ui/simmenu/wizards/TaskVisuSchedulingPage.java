/*******************************************************************************
 * Copyright (c) 2017, 2020 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.execution.ui.simmenu.wizards;

import java.text.DecimalFormat;
import java.util.stream.Collectors;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.misc.EMTimeType;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMAllocation;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMMapping;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMUtil;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.SchedulerAlgorithmRegister;
import org.eclipse.app4mc.multicore.execution.ui.widget.emtracewidget.IEMTraceWidget;
import org.eclipse.app4mc.multicore.execution.ui.widget.model.XContainer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;


public class TaskVisuSchedulingPage extends WizardPage {
	private static final String[] SIM_TIME_INCREMENT = { "ms", "us", "ns", "ps" };
	private static final EMTimeType[] SIM_TIME_SCALE = { EMTimeType.MS, EMTimeType.US, EMTimeType.NS, EMTimeType.PS };
	private Button button3;
	private Composite container;
	private Text txtSimuntil;
	private Combo cmbSchedulerSelection;

	private final OMMapping model;

	private long majorCycle = 0;
	private long simuntil = 0;

	private String scheduling = "";
	private EMTimeType timescale;
	private Combo cmbPrecessionSelection;
	private final XContainer con;

	protected TaskVisuSchedulingPage(final IEMTraceWidget tw, final OMMapping model, final XContainer con) {
		super("Task Visualisation: Scheduling");
		this.con = con;
		this.model = model;
		setTitle("Task Visualisation: Scheduling");
		setDescription("Setup the scheduling used for the visualisation.");
	}

	@Override
	public void createControl(final Composite parent) {
		this.container = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		this.container.setLayout(layout);
		layout.numColumns = 2;

		this.majorCycle = OMUtil.getMajorCycle(
				this.model.getAllocationList().stream().map(OMAllocation::getTask).collect(Collectors.toList()));
		final Label lblMajorCycle = new Label(this.container, SWT.NONE);
		lblMajorCycle.setText("Major-Cycle / Hyperperiod (ps)");

		final DecimalFormat decimalFormat = new DecimalFormat("#,##0");
		final String numberAsString = decimalFormat.format(this.majorCycle);
		final Label lbl_majorCycleVal = new Label(this.container, SWT.NONE);
		lbl_majorCycleVal.setText(numberAsString);
		lbl_majorCycleVal.addMouseListener(new MouseListener() {
			@Override
			public void mouseUp(final MouseEvent e) {
				// Empty on purpose
			}

			@Override
			public void mouseDown(final MouseEvent e) {
				// Empty on purpose
			}

			@Override
			public void mouseDoubleClick(final MouseEvent e) {
				if (TaskVisuSchedulingPage.this.txtSimuntil != null) {
					TaskVisuSchedulingPage.this.txtSimuntil.setText(TaskVisuSchedulingPage.this.majorCycle + "");
				}
			}
		});

		final Label lblSeconds = new Label(this.container, SWT.NONE);
		lblSeconds.setText(" ");

		final Label lblMajorCycleVal2 = new Label(this.container, SWT.NONE);
		lblMajorCycleVal2.setText(psToString(this.majorCycle));

		final Label label1 = new Label(this.container, SWT.NONE);
		label1.setText("Algorithm");

		this.cmbSchedulerSelection = new Combo(this.container, SWT.READ_ONLY);
		this.cmbSchedulerSelection.setItems(SchedulerAlgorithmRegister.schedulerList());
		this.cmbSchedulerSelection.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				final int i = TaskVisuSchedulingPage.this.cmbSchedulerSelection.getSelectionIndex();
				if (i != -1) {
					TaskVisuSchedulingPage.this.scheduling = SchedulerAlgorithmRegister.schedulerList()[i];
					setPageComplete(isReadyToSim());
				}
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
			}
		});

		final Label labeltimeinc = new Label(this.container, SWT.NONE);
		labeltimeinc.setText("Scale to");

		this.cmbPrecessionSelection = new Combo(this.container, SWT.READ_ONLY);
		this.cmbPrecessionSelection.setItems(SIM_TIME_INCREMENT);
		this.cmbPrecessionSelection.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(final SelectionEvent e) {
				final int i = TaskVisuSchedulingPage.this.cmbPrecessionSelection.getSelectionIndex();
				if (i != -1) {
					TaskVisuSchedulingPage.this.timescale = SIM_TIME_SCALE[i];
					setPageComplete(isReadyToSim());
				}
			}

			@Override
			public void widgetDefaultSelected(final SelectionEvent e) {
				// Empty on purpose
			}
		});


		final Label labelCheck = new Label(this.container, SWT.NONE);
		labelCheck.setText("Simulation-Time (ps)");
		this.txtSimuntil = new Text(this.container, SWT.NONE);
		this.simuntil = this.majorCycle;
		this.txtSimuntil.setText("" + this.majorCycle);

		this.txtSimuntil.addModifyListener(ev -> {
			final String text = ((Text) ev.getSource()).getText();
			try {
				TaskVisuSchedulingPage.this.simuntil = Long.parseLong(text);
			}
			catch (final NumberFormatException ex) {
				TaskVisuSchedulingPage.this.simuntil = 0;
			}
			setPageComplete(isReadyToSim());
		});


		final SelectionListener listenerDataFlow = new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Button button = ((Button) event.widget);
				System.out.print("DataFlow");
				System.out.println(" selected = " + button.getSelection());
				TaskVisuSchedulingPage.this.button3.setEnabled(button.getSelection());

				if (!button.getSelection()) {
					TaskVisuSchedulingPage.this.con.setMergingArrow(false);
					TaskVisuSchedulingPage.this.con.setAlternativeArrow(false);
					TaskVisuSchedulingPage.this.button3.setSelection(false);
				}

				TaskVisuSchedulingPage.this.con.setDataFlow(button.getSelection());

			}
		};
		final SelectionListener listenerMergeArrow = new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent event) {
				final Button button = ((Button) event.widget);
				TaskVisuSchedulingPage.this.con.setMergingArrow(button.getSelection());

			}
		};

		// # DataFlow Controls

		final Label labelDataflow = new Label(this.container, SWT.NONE);
		labelDataflow.setText("Show Dataflow in Simulation:");


		final Button button2 = new Button(this.container, SWT.CHECK);
		button2.setText("");
		button2.addSelectionListener(listenerDataFlow);

		// # ArrowMerging Controls

		final Label labelMergeArrow = new Label(this.container, SWT.NONE);
		labelMergeArrow.setText("Extended Dataflow Simulation");

		this.button3 = new Button(this.container, SWT.CHECK);
		this.button3.setEnabled(false);
		this.button3.setText("");
		this.button3.addSelectionListener(listenerMergeArrow);

		setControl(this.container);
		setPageComplete(isReadyToSim());
	}


	private boolean isReadyToSim() {
		return this.simuntil != 0 && // valid simulation time
				this.cmbSchedulerSelection != null && // combobox is
														// instantiated
				this.cmbSchedulerSelection.getSelectionIndex() != -1 && // combobox
				// item
				// selected
				this.cmbPrecessionSelection != null && this.cmbPrecessionSelection.getSelectionIndex() != -1;
	}


	private String psToString(final long ps) {
		if ((ps % 1000) != 0) {
			return ps + " ps";
		}
		else if ((ps % 1000000) != 0) {
			return ps / 1000 + " ns";
		}
		else if ((ps % 1000000000) != 0) {
			return ps / 1000000 + " us";
		}
		else {
			return ps / 1000000000 + " ms";
		}
	}

	/**
	 * Get the time until the simulation should run.
	 *
	 * @return
	 */
	public long getSimuntil() {
		return this.simuntil;
	}

	/**
	 * Get the key-name of the scheduling.
	 *
	 * @return
	 */
	public String getScheduling() {
		return this.scheduling;
	}

	public EMTimeType getTimeScale() {
		return this.timescale;
	}


}
