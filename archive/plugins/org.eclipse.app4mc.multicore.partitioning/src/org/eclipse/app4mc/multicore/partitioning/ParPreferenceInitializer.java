/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

/**
 * Class used to initialize default preference values.
 */
public class ParPreferenceInitializer extends AbstractPreferenceInitializer {

	@Override
	public void initializeDefaultPreferences() {
		Bundle bundle = FrameworkUtil.getBundle(getClass());
		if (bundle != null) {
			IEclipsePreferences node = DefaultScope.INSTANCE.getNode(bundle.getSymbolicName());
			if (node == null) {
				return;
			}
			
			node.putBoolean(IParConstants.PRE_ACTIVATION, true);
			node.putBoolean(IParConstants.PRE_GGP, false);
			node.putBoolean(IParConstants.PRE_MIN_EDGES, false);
			node.putBoolean(IParConstants.PRE_EFF_EDGE, false);
			node.putBoolean(IParConstants.PRE_GCP, false);
			node.putInt(IParConstants.PRE_INT, 4);
			node.putBoolean(IParConstants.PRE_TA, false);
			node.putBoolean(IParConstants.PRE_DEBUG, false);
			node.put(IParConstants.PRE_LOC_STRING, "output");
			node.put(IParConstants.PRE_LOC_RADIO, "0");
			node.put(IParConstants.PRE_PARTITIONING_ALG, "essp");
			node.putBoolean(IParConstants.PRE_ASIL, false);
			node.putBoolean(IParConstants.PRE_RPC, false);
			node.putBoolean(IParConstants.PRE_RCPC, false);
			node.putBoolean(IParConstants.PRE_TAGS, false);
		}
	}

}
