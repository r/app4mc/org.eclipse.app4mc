/*******************************************************************************
 * Copyright (c) 2017-2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.partitioning.workflow;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.workflow.core.Context;
import org.eclipse.app4mc.amalthea.workflow.core.WorkflowComponent;
import org.eclipse.app4mc.multicore.partitioning.IParConstants;
import org.eclipse.app4mc.multicore.partitioning.algorithms.PerformPartitioning;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.LoggerFactory;

public class GeneratePartitioning extends WorkflowComponent {

	public static final String PARTITIONING_CPP = "cpp";
	public static final String PARTITIONING_ESSP = "essp";
	private static final String RESULTSLOT = "partitioning";
	private String partitioningAlg;
	private Integer numberOfPartitions;
	private String modelLoc = "";


	public void setModelLoc(final String modelLoc) {
		this.modelLoc = modelLoc;
	}

	@Override
	protected void runInternal(final Context ctx) {

		final NullProgressMonitor monitor = new NullProgressMonitor();
		Amalthea modelCopy = getAmaltheaModelCopy(ctx);
		assert null != modelCopy.getSwModel() && null != modelCopy.getConstraintsModel();

		final Path path = new Path(this.modelLoc);
		final IFile file = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
		final PerformPartitioning partitioning = new PerformPartitioning();
		final IPreferenceStore store = new ScopedPreferenceStore(InstanceScope.INSTANCE,
				FrameworkUtil.getBundle(getClass()).getSymbolicName());
		store.setValue(IParConstants.PRE_PARTITIONING_ALG, getPartitioningAlg());
		store.setValue(IParConstants.PRE_INT, this.numberOfPartitions);
		partitioning.execute(file, monitor, store);

		modelCopy = partitioning.retainAffntyCnstrnts(file, modelCopy, monitor);

		LoggerFactory.getLogger(GeneratePartitioning.class).debug("Setting result model in slot: {}", getResultSlot());
		ctx.set(getResultSlot(), modelCopy);
	}

	/**
	 * @see org.eclipse.emf.mwe.core.lib.WorkflowComponentWithModelSlot#checkConfiguration(org.eclipse.emf.mwe.core.issues.Issues)
	 */
	@Override
	protected void checkInternal() {
		if (null == getPartitioningAlg() || getPartitioningAlg().isEmpty()) {
			LoggerFactory.getLogger(GeneratePartitioning.class).error(
					"No proper partitioning defined! Please define one of the following values: numberOfPartitions, globalCP, partitioningAlg");
		}
	}


	public String getPartitioningAlg() {
		return this.partitioningAlg;
	}

	public void setPartitioningAlg(final String partitioningAlg) {
		this.partitioningAlg = partitioningAlg;
	}

	public String getResultSlot() {
		return GeneratePartitioning.RESULTSLOT;
	}

	public String getNumberOfPartitions() {
		return this.numberOfPartitions != null ? this.numberOfPartitions.toString() : null;
	}

	public void setNumberOfPartitions(final String numberOfPartitions) {
		this.numberOfPartitions = numberOfPartitions != null ? Integer.parseInt(numberOfPartitions) : null;
	}

}
