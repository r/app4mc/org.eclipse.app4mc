/**
 ********************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.systemproxy.multicoresystem;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.ExecutionModel;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMCore;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTask;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskEvent;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState.EMTaskHistStateFactory;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskState;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.ISchedulerEventListener;

public class EMTracer implements ISchedulerEventListener {

	private final ExecutionModel executionModel;

	private final EMCore emCore;

	private final Map<String, EMTaskHistState> currentHist = new HashMap<>();

	public EMTracer(final String coreName, final ExecutionModel m) {
		this.executionModel = m;
		this.emCore = new EMCore(coreName);
		m.addCore(this.emCore);
	}

	@Override
	public void onTaskAdded(final String taskName) {
		this.executionModel.addTask(new EMTask(taskName));
	}

	@Override
	public void onStartTask(final String task, final long time) {
		final EMTaskHistState leaving = this.currentHist.get(task);
		if (leaving != null) {
			leaving.setEnd(time);
			this.executionModel.addTaskHistEntry(task, leaving);
		}

		final EMTaskHistState currentRunning = EMTaskHistStateFactory.create(EMTaskState.RUNNING, this.emCore);
		currentRunning.setStart(time);
		this.currentHist.put(task, currentRunning);
	}


	@Override
	public void onTerminateTask(final String task, final long time) {
		final EMTaskHistState leavingRunning = this.currentHist.get(task);
		if (leavingRunning != null) {
			leavingRunning.setEnd(time);
			this.executionModel.addTaskHistEntry(task, leavingRunning);
		}


		final EMTaskHistState suspendedTask = EMTaskHistStateFactory.create(EMTaskState.SUSPENDED, this.emCore);
		suspendedTask.setStart(time);
		suspendedTask.setEvent(EMTaskEvent.TERMINATE);
		this.currentHist.put(task, suspendedTask);
	}

	@Override
	public void onPreemptTask(final String task, final long time) {
		final EMTaskHistState leavingRunning = this.currentHist.get(task);
		if (leavingRunning != null) {
			leavingRunning.setEnd(time);
			this.executionModel.addTaskHistEntry(task, leavingRunning);
		}

		final EMTaskHistState readyTask = EMTaskHistStateFactory.create(EMTaskState.READY, this.emCore);
		readyTask.setStart(time);
		readyTask.setEvent(EMTaskEvent.PREEMPT);
		this.currentHist.put(task, readyTask);
	}

	@Override
	public void onStartIdleCore(final long time) {
		/**
		 * listening for this event does not apply here
		 */
	}

	@Override
	public void onStopIdleCore(final long time) {
		/**
		 * listening for this event does not apply here
		 */
	}

	@Override
	public void onActivateTask(final String task, final long time) {
		final EMTaskHistState leavingSuspend = this.currentHist.get(task);
		if (leavingSuspend != null) {
			leavingSuspend.setEnd(time);
			this.executionModel.addTaskHistEntry(task, leavingSuspend);
		}

		final EMTaskHistState readyTask = EMTaskHistStateFactory.create(EMTaskState.READY, this.emCore);
		readyTask.setStart(time);
		readyTask.setEvent(EMTaskEvent.ACTIVATE);
		this.currentHist.put(task, readyTask);
	}

	@Override
	public void onWaitTask(final String task, final long time, final String muxName, final String holder) {
		final EMTaskHistState leavingRunning = this.currentHist.get(task);
		if (leavingRunning != null) {
			leavingRunning.setEnd(time);
			this.executionModel.addTaskHistEntry(task, leavingRunning);
		}

		final EMTaskHistState waitingTask = EMTaskHistStateFactory.create(EMTaskState.WAITING, this.emCore);
		waitingTask.setStart(time);
		waitingTask.setEventCause(muxName);
		waitingTask.setNote(holder);
		this.currentHist.put(task, waitingTask);
	}

	@Override
	public void onReleaseTask(final String task, final long time) {
		final EMTaskHistState leavingWaiting = this.currentHist.get(task);
		if (leavingWaiting != null) {
			leavingWaiting.setEnd(time);
			this.executionModel.addTaskHistEntry(task, leavingWaiting);
		}

		final EMTaskHistState readyTask = EMTaskHistStateFactory.create(EMTaskState.READY, this.emCore);
		readyTask.setStart(time);
		readyTask.setEvent(EMTaskEvent.RELEASE);
		this.currentHist.put(task, readyTask);
	}

	@Override
	public void onTaskMissedDeadline(final String task, final long time, final long remainingExectime) {
		final EMTaskHistState before = this.currentHist.get(task);
		if (before != null) {
			before.setEnd(time);
			this.executionModel.addTaskHistEntry(task, before);
			this.currentHist.remove(task); // next will be activation
		}
		this.executionModel.addTaskDeadlineMissedEntry(task, this.emCore.getName(), time);
	}

	@Override
	public String getCoreName() {
		return this.emCore.getName();
	}


}
