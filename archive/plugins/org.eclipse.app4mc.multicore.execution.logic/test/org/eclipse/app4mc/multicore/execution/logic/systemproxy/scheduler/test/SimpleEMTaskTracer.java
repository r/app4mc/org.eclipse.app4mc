/**
 ********************************************************************************
 * Copyright (c) 2019 Dortmund University of Applied Sciences and Arts and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.test;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMCore;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTask;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState.EMTaskHistStateFactory;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskState;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.ISchedulerEventListener;

/**
 * Logs only running states into Execution Model tasks.
 */
public class SimpleEMTaskTracer implements ISchedulerEventListener {

	private final Map<String, EMTask> tasks;
	private final EMCore core;

	private EMTaskHistState currentRunning;

	public SimpleEMTaskTracer(final String corename) {
		this.core = new EMCore(corename);
		this.tasks = new HashMap<>();
	}

	public SimpleEMTaskTracer(final Map<String, EMTask> tasks, final EMCore c) {
		this.core = c;
		this.tasks = tasks;
	}


	@Override
	public void onTaskAdded(final String taskName) {
		this.tasks.put(taskName, new EMTask(taskName));
	}

	@Override
	public void onActivateTask(final String task, final long time) {
	}

	@Override
	public void onStartTask(final String task, final long time) {
		this.currentRunning = EMTaskHistStateFactory.create(EMTaskState.RUNNING, this.core);
		this.currentRunning.setEventSource(task);
		this.currentRunning.setStart(time);
	}

	@Override
	public void onTerminateTask(final String task, final long time) {
		this.currentRunning.setEnd(time);
		this.tasks.get(task).getStateHistory().add(this.currentRunning);
		this.currentRunning = null;
	}

	@Override
	public void onPreemptTask(final String task, final long time) {
		this.currentRunning.setEnd(time);
		this.tasks.get(task).getStateHistory().add(this.currentRunning);
		this.currentRunning = null;
	}


	@Override
	public void onStartIdleCore(final long time) {

	}

	@Override
	public void onStopIdleCore(final long time) {

	}

	public Map<String, EMTask> getEMTasks() {
		return this.tasks;
	}

	@Override
	public void onWaitTask(final String task, final long time, final String muxName, final String holder) {
		this.currentRunning.setEnd(time);
		this.tasks.get(task).getStateHistory().add(this.currentRunning);
		this.currentRunning = null;
	}

	@Override
	public void onReleaseTask(final String task, final long time) {
	}

	@Override
	public void onTaskMissedDeadline(final String task, final long time, final long remainingExectime) {
		if (this.currentRunning != null && this.currentRunning.getEventSource() == task) {
			this.currentRunning.setEnd(time);
			this.tasks.get(task).getStateHistory().add(this.currentRunning);
			this.currentRunning = null;
		}
	}

	@Override
	public String getCoreName() {
		return this.core.getName();
	}


}
