/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.editors.sirius.design.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventChain;
import org.eclipse.app4mc.amalthea.model.EventChainContainer;
import org.eclipse.app4mc.amalthea.model.EventChainItemType;
import org.eclipse.app4mc.amalthea.model.EventChainReference;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableEvent;
import org.eclipse.app4mc.amalthea.model.RunnableEventType;
import org.eclipse.app4mc.amalthea.model.SubEventChain;
import org.junit.Test;

/**
 * @author daniel.kunz@de.bosch.com
 *
 */
public class ConstraintServiceTest {

	private ConstraintService constServ = new ConstraintService();
	private EventChain ec = AmaltheaFactory.eINSTANCE.createEventChain();

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getEventsFromEventChain(org.eclipse.app4mc.amalthea.model.AbstractEventChain)}
	 * .
	 */
	@Test
	public void testGetEventsFromEventChainNull() {
		List<Event> result = this.constServ.getEventsFromEventChain(null);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getEventsFromEventChain(org.eclipse.app4mc.amalthea.model.AbstractEventChain)}
	 * .
	 */
	@Test
	public void testGetEventsFromEventChainStimulus() {
		Event stimulus = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		this.ec.setStimulus(stimulus);
		List<Event> result = this.constServ.getEventsFromEventChain(this.ec);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(stimulus, result.get(0));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getEventsFromEventChain(org.eclipse.app4mc.amalthea.model.AbstractEventChain)}
	 * .
	 */
	@Test
	public void testGetEventsFromEventChainResponse() {
		Event response = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		this.ec.setResponse(response);
		List<Event> result = this.constServ.getEventsFromEventChain(this.ec);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(response, result.get(0));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getEventsFromEventChain(org.eclipse.app4mc.amalthea.model.AbstractEventChain)}
	 * .
	 */
	@Test
	public void testGetEventsFromEventChainStimulusResponse() {
		Event response = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		this.ec.setResponse(response);
		Event stimulus = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		this.ec.setStimulus(stimulus);
		List<Event> result = this.constServ.getEventsFromEventChain(this.ec);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(2, result.size());
		assertSame(stimulus, result.get(0));
		assertSame(response, result.get(1));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getEventsFromEventChain(org.eclipse.app4mc.amalthea.model.AbstractEventChain)}
	 * .
	 */
	@Test
	public void testGetEventsFromEventChainSegmentsSubChain() {
		Event event1 = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		EventChainContainer eci1 = AmaltheaFactory.eINSTANCE.createEventChainContainer();
		SubEventChain sc1 = AmaltheaFactory.eINSTANCE.createSubEventChain();
		sc1.setStimulus(event1);
		eci1.setEventChain(sc1);
		this.ec.setItemType(EventChainItemType.SEQUENCE);
		this.ec.getItems().add(eci1);
		Event event2 = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		EventChainContainer eci2 = AmaltheaFactory.eINSTANCE.createEventChainContainer();
		SubEventChain sc2 = AmaltheaFactory.eINSTANCE.createSubEventChain();
		sc2.setStimulus(event2);
		eci2.setEventChain(sc2);
		this.ec.getItems().add(eci2);
		List<Event> result = this.constServ.getEventsFromEventChain(this.ec);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(2, result.size());
		assertSame(event1, result.get(0));
		assertSame(event2, result.get(1));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getEventsFromEventChain(org.eclipse.app4mc.amalthea.model.AbstractEventChain)}
	 * .
	 */
	@Test
	public void testGetEventsFromEventChainSegmentsReference() {
		EventChain ec2 = AmaltheaFactory.eINSTANCE.createEventChain();
		Event event1 = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		ec2.setStimulus(event1);
		Event event2 = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		ec2.setResponse(event2);
		EventChainReference eci1 = AmaltheaFactory.eINSTANCE.createEventChainReference();
		eci1.setEventChain(ec2);
		this.ec.setItemType(EventChainItemType.SEQUENCE);
		this.ec.getItems().add(eci1);
		List<Event> result = this.constServ.getEventsFromEventChain(this.ec);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(2, result.size());
		assertSame(event1, result.get(0));
		assertSame(event2, result.get(1));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnablesFromEventChain(org.eclipse.app4mc.amalthea.model.EventChain)}
	 * .
	 */
	@Test
	public void testGetRunnablesFromEventChainNull() {
		List<Runnable> result = this.constServ.getRunnablesFromEventChain(null);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnablesFromEventChain(org.eclipse.app4mc.amalthea.model.EventChain)}
	 * .
	 */
	@Test
	public void testGetRunnablesFromEventChainNoRunnableEvent() {
		Event response = AmaltheaFactory.eINSTANCE.createProcessEvent();
		this.ec.setResponse(response);
		List<Runnable> result = this.constServ.getRunnablesFromEventChain(this.ec);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnablesFromEventChain(org.eclipse.app4mc.amalthea.model.EventChain)}
	 * .
	 */
	@Test
	public void testGetRunnablesFromEventChainEmptyRunnableEvent() {
		RunnableEvent response = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		this.ec.setResponse(response);
		List<Runnable> result = this.constServ.getRunnablesFromEventChain(this.ec);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnablesFromEventChain(org.eclipse.app4mc.amalthea.model.EventChain)}
	 * .
	 */
	@Test
	public void testGetRunnablesFromEventChainRunnableEvent() {
		RunnableEvent response = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		Runnable runnable = AmaltheaFactory.eINSTANCE.createRunnable();
		response.setEntity(runnable);
		this.ec.setResponse(response);
		List<Runnable> result = this.constServ.getRunnablesFromEventChain(this.ec);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(runnable, result.get(0));
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnableEdgeForEvent(org.eclipse.app4mc.amalthea.model.Event)}
	 * .
	 */
	@Test
	public void testGetRunnableEdgeForEventNull() {
		List<Runnable> result = this.constServ.getRunnableEdgeForEvent(null);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnableEdgeForEventDescription(org.eclipse.app4mc.amalthea.model.Event)}
	 * .
	 */
	@Test
	public void testGetRunnableEdgeForEventDescriptionNull() {
		String result = this.constServ.getRunnableEdgeForEventDescription(null);
		assertNull(result);
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnableEdgeForEventDescription(org.eclipse.app4mc.amalthea.model.Event)}
	 * .
	 */
	@Test
	public void testGetRunnableEdgeForEventDescriptionNoRunnableEvent() {
		Event event = AmaltheaFactory.eINSTANCE.createProcessEvent();
		String result = this.constServ.getRunnableEdgeForEventDescription(event);
		assertNull(result);
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnableEdgeForEventDescription(org.eclipse.app4mc.amalthea.model.Event)}
	 * .
	 */
	@Test
	public void testGetRunnableEdgeForEventDescriptionRunnableEventDefault() {
		RunnableEvent event = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		String result = this.constServ.getRunnableEdgeForEventDescription(event);
		assertEquals("_all_", result);
	}

	/**
	 * Test method for
	 * {@link org.eclipse.app4mc.amalthea.editors.sirius.design.services.ConstraintService#getRunnableEdgeForEventDescription(org.eclipse.app4mc.amalthea.model.Event)}
	 * .
	 */
	@Test
	public void testGetRunnableEdgeForEventDescriptionRunnableEventStart() {
		RunnableEvent event = AmaltheaFactory.eINSTANCE.createRunnableEvent();
		event.setEventType(RunnableEventType.START);
		String result = this.constServ.getRunnableEdgeForEventDescription(event);
		assertEquals("start", result);
	}

}
