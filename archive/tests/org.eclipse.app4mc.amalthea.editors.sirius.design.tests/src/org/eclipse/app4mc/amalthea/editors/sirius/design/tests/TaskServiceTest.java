/*********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.editors.sirius.design.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.app4mc.amalthea.editors.sirius.design.services.TaskService;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ExecutionNeed;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.Preemption;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.util.InstructionsUtil;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.junit.Test;

/**
 *
 * @author daniel.kunz@de.bosch.com
 *
 */
public class TaskServiceTest {

	private TaskService taskS = new TaskService();

	@Test
	public void testCheckTaskPreemptiveNull() {
		boolean result = this.taskS.isTaskPreemptive(null);
		assertFalse(result);
	}

	@Test
	public void testCheckTaskPreemptiveUndefined() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		task.setPreemption(Preemption._UNDEFINED_);
		boolean result = this.taskS.isTaskPreemptive(task);
		assertFalse(result);
	}

	@Test
	public void testCheckTaskPreemptivePreepmtive() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		task.setPreemption(Preemption.PREEMPTIVE);
		boolean result = this.taskS.isTaskPreemptive(task);
		assertTrue(result);
	}

	@Test
	public void testCheckTaskPreemptiveCooperative() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		task.setPreemption(Preemption.COOPERATIVE);
		boolean result = this.taskS.isTaskPreemptive(task);
		assertFalse(result);
	}


	@Test
	public void testCheckTaskPreemptionUnknownNull() {
		boolean result = this.taskS.isTaskPreemptionUnknown(null);
		assertFalse(result);
	}

	@Test
	public void testCheckTaskPreemptionUnknownUnknown() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		task.setPreemption(Preemption._UNDEFINED_);
		boolean result = this.taskS.isTaskPreemptionUnknown(task);
		assertTrue(result);
	}

	@Test
	public void testCheckTaskPreemptionUnknownPremmptive() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		task.setPreemption(Preemption.PREEMPTIVE);
		boolean result = this.taskS.isTaskPreemptionUnknown(task);
		assertFalse(result);
	}

	@Test
	public void testGetRunnablesFromTaskNull() {
		List<Runnable> result = this.taskS.getRunnablesFromTask(null);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetRunnablesFromTaskEmptyTask() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		List<Runnable> result = this.taskS.getRunnablesFromTask(task);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetRunnablesFromTaskEmptyActivityGraph() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
		task.setActivityGraph(ag);
		List<Runnable> result = this.taskS.getRunnablesFromTask(task);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetRunnablesFromTaskActivityGraphOther() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
		task.setActivityGraph(ag);

		InterProcessTrigger ipt = AmaltheaFactory.eINSTANCE.createInterProcessTrigger();
		ag.getItems().add(ipt);
		List<Runnable> result = this.taskS.getRunnablesFromTask(task);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetRunnablesFromTaskActivityGraphEmptyRunnableCall() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
		task.setActivityGraph(ag);

		RunnableCall trc = AmaltheaFactory.eINSTANCE.createRunnableCall();
		ag.getItems().add(trc);
		List<Runnable> result = this.taskS.getRunnablesFromTask(task);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetRunnablesFromTaskActivityGraphRunnableCall() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
		task.setActivityGraph(ag);

		RunnableCall trc = AmaltheaFactory.eINSTANCE.createRunnableCall();
		ag.getItems().add(trc);
		Runnable runn = AmaltheaFactory.eINSTANCE.createRunnable();
		trc.setRunnable(runn);
		List<Runnable> result = this.taskS.getRunnablesFromTask(task);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(runn, result.get(0));
	}

	@Test
	public void testGetAccessedLabelsOfTaskNull() {
		List<Label> result = this.taskS.getAccessedLabelsOfTask(null);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetAccessedLabelsOfTaskEmptyTask() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		List<Label> result = this.taskS.getAccessedLabelsOfTask(task);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetAccessedLabelsOfTaskEmptyRunnable() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
		task.setActivityGraph(ag);

		RunnableCall trc = AmaltheaFactory.eINSTANCE.createRunnableCall();
		ag.getItems().add(trc);
		Runnable runn = AmaltheaFactory.eINSTANCE.createRunnable();
		trc.setRunnable(runn);
		List<Label> result = this.taskS.getAccessedLabelsOfTask(task);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetAccessedLabelsOfTaskEmptyRunnableOtherRunnabelItem() {
		Amalthea root = AmaltheaFactory.eINSTANCE.createAmalthea();
		SWModel sw = ModelUtil.getOrCreateSwModel(root);
		Runnable run = AmaltheaFactory.eINSTANCE.createRunnable();
		sw.getRunnables().add(run);
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		sw.getTasks().add(task);

		ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
		task.setActivityGraph(ag);

		RunnableCall trc = AmaltheaFactory.eINSTANCE.createRunnableCall();
		ag.getItems().add(trc);

		ExecutionNeed execNeed = InstructionsUtil.createExecutionNeedConstant(25);
		run.getRunnableItems().add(execNeed);
		trc.setRunnable(run);
		List<Label> result = this.taskS.getAccessedLabelsOfTask(task);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetAccessedLabelsOfTaskEmptyRunnableEmptyLabelAccess() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
		task.setActivityGraph(ag);

		RunnableCall trc = AmaltheaFactory.eINSTANCE.createRunnableCall();
		ag.getItems().add(trc);
		Runnable runn = AmaltheaFactory.eINSTANCE.createRunnable();
		LabelAccess la = AmaltheaFactory.eINSTANCE.createLabelAccess();
		runn.getRunnableItems().add(la);
		trc.setRunnable(runn);
		List<Label> result = this.taskS.getAccessedLabelsOfTask(task);
		assertNotNull(result);
		assertTrue(result.isEmpty());
	}

	@Test
	public void testGetAccessedLabelsOfTaskEmptyRunnableLabelAccess() {
		Task task = AmaltheaFactory.eINSTANCE.createTask();
		ActivityGraph ag = AmaltheaFactory.eINSTANCE.createActivityGraph();
		task.setActivityGraph(ag);

		RunnableCall trc = AmaltheaFactory.eINSTANCE.createRunnableCall();
		ag.getItems().add(trc);
		Runnable runn = AmaltheaFactory.eINSTANCE.createRunnable();
		LabelAccess la = AmaltheaFactory.eINSTANCE.createLabelAccess();
		runn.getRunnableItems().add(la);
		trc.setRunnable(runn);
		Label label = AmaltheaFactory.eINSTANCE.createLabel();
		la.setData(label);
		List<Label> result = this.taskS.getAccessedLabelsOfTask(task);
		assertNotNull(result);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		assertSame(label, result.get(0));
	}


}
