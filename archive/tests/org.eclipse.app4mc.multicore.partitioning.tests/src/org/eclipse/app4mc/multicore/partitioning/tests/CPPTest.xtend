/*******************************************************************************
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/
package org.eclipse.app4mc.multicore.partitioning.tests

import org.junit.Test
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.multicore.partitioning.algorithms.CPP
import org.eclipse.core.runtime.NullProgressMonitor

import static org.junit.Assert.assertTrue
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant
import java.math.BigInteger
import org.eclipse.app4mc.amalthea.model.Label
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum

class CPPTest {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension SoftwareBuilder b2 = new SoftwareBuilder
	
	@Test
	def void CPPTest(){
		val model = amalthea [
			constraintsModel[
				
			]
			softwareModel[
				label[
					name ="l1"
					size = createDS(5)
				]
				runnable[
					name="r1"
					activityGraph [
						ticks [^default = createDVC(20l)]
						labelAccess[
							data = _find(Label, "l1")
							access = LabelAccessEnum.WRITE
						]
					]
				]
				runnable[
					name="r2"
					activityGraph [
						ticks [^default = createDVC(30l)]
						labelAccess[
							data = _find(Label, "l1")
							access = LabelAccessEnum.WRITE
						]
					]
				]
				runnable[
					name="r3"
					activityGraph [
						ticks [^default = createDVC(10l)]
						labelAccess[
							data = _find(Label, "l1")
							access = LabelAccessEnum.READ
						]
					]
				]
			]
		]
		
		var CPP cpp = new CPP(model.swModel,model.constraintsModel)
		cpp.build(new NullProgressMonitor)
		assertTrue(cpp.swm.processPrototypes.size==2)
		assertTrue(cpp.swm.processPrototypes.get(0).runnableCalls.size==2)
		assertTrue(cpp.swm.processPrototypes.get(1).runnableCalls.get(0).runnable.name=="r1")
	}
	
	private def createDS(int i) {
		val ret = AmaltheaFactory.eINSTANCE.createDataSize
		ret.value = BigInteger.valueOf(i)
		ret
	}
	
	private def DiscreteValueConstant createDVC(long upper) {
		val ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant
		ret.value=upper
		ret
	}
}