/*******************************************************************************
 * Copyright (c) 2020 Dortmund University of Applied Sciences and Arts and others.
 *  
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *   
 *  SPDX-License-Identifier: EPL-2.0
 *  
 *  Contributors:
 *      Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.app4mc.multicore.partitioning.tests

import org.junit.Test
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.DiscreteValueConstant
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.multicore.partitioning.algorithms.TCbasedPart
import static org.junit.Assert.assertTrue
import org.junit.BeforeClass
import org.apache.log4j.Logger
import org.apache.log4j.Level

class TCpartTest {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	val String[] names = #["A","B","C","D","E","F","G","H","I","J"]
	val int[] loads = #[1, 3, 4, 9, 1, 2, 5, 3, 2, 4]
	val int NBTASKS = 3
	@BeforeClass
	def static void before() {
		org.apache.log4j.BasicConfigurator.configure()
		Logger.getRootLogger().setLevel(Level.INFO)
	}
	
	@Test
	def void simpleModelTest(){
		val model = amalthea [
			softwareModel[
				for (i : 0 ..<loads.length){
					runnables+=createRunnable(names.get(i),loads.get(i))
				}
			]
			constraintsModel[
			]
		]
		
		for (i : 0 ..<9){
			model.constraintsModel.runnableSequencingConstraints+=createRSC(i,model)
		}
		
		var TCbasedPart tcp = new TCbasedPart(model)
		tcp.run(NBTASKS,10,null)
		assertTrue(tcp.getAmaltheaModel.swModel.processPrototypes.size==NBTASKS)
	}
	
	def createRSC(Integer i, Amalthea model) {
		val rsc = AmaltheaFactory.eINSTANCE.createRunnableSequencingConstraint
		val rscg1 = AmaltheaFactory.eINSTANCE.createRunnableEntityGroup
		val rscg2 = AmaltheaFactory.eINSTANCE.createRunnableEntityGroup
		
		switch i{
			case 0: {rscg1.runnables.add(model.swModel.runnables.get(0))
					rscg2.runnables.add(model.swModel.runnables.get(4))
					rsc.name="AE"
					}
			case 1: {rscg1.runnables.add(model.swModel.runnables.get(1))
					rscg2.runnables.add(model.swModel.runnables.get(5))
					rsc.name="BF"
					}
			case 2: {rscg1.runnables.add(model.swModel.runnables.get(1))
					rscg2.runnables.add(model.swModel.runnables.get(6))
					rsc.name="BG"
					}
			case 3: {rscg1.runnables.add(model.swModel.runnables.get(1))
					rscg2.runnables.add(model.swModel.runnables.get(9))
					rsc.name="BJ"
					}
			case 4: {rscg1.runnables.add(model.swModel.runnables.get(2))
					rscg2.runnables.add(model.swModel.runnables.get(7))
					rsc.name="CH"
					}
			case 5: {rscg1.runnables.add(model.swModel.runnables.get(3))
					rscg2.runnables.add(model.swModel.runnables.get(9))
					rsc.name="DJ"
					}
			case 6: {rscg1.runnables.add(model.swModel.runnables.get(4))
					rscg2.runnables.add(model.swModel.runnables.get(7))
					rsc.name="EH"
					}
			case 7: {rscg1.runnables.add(model.swModel.runnables.get(7))
					rscg2.runnables.add(model.swModel.runnables.get(8))
					rsc.name="HI"
					}
			case 8: {rscg1.runnables.add(model.swModel.runnables.get(6))
					rscg2.runnables.add(model.swModel.runnables.get(8))
					rsc.name="GI"
					}
		}
		rsc.runnableGroups.add(rscg1)
		rsc.runnableGroups.add(rscg2)
		rsc
	}
	
	
	private def createRunnable(String id, int load) {
		val r = AmaltheaFactory.eINSTANCE.createRunnable
		r.name=id
		val ag = AmaltheaFactory.eINSTANCE.createActivityGraph
		val ticks = AmaltheaFactory.eINSTANCE.createTicks
		ticks.^default = createDVC(load)
		ag.items+=ticks
		r.activityGraph=ag
		r
	}
	
	private def DiscreteValueConstant createDVC(int upper) {
		val ret = AmaltheaFactory.eINSTANCE.createDiscreteValueConstant
		ret.value=upper
		ret
	}
}