
# Eclipse APP4MC

This is the main repository of Eclipse APP4MC. It contains the following components:

    Model           | Amalthea model
    Editor          | Standard Amalthea model editor
    Validation      | Framework and provided model validations
    Visualization   | Framework and standard model validations
    Workflow        | Model manipulation based on EASE or MWE2
    Tracing         | Amalthea trace database (ATDB) and converters
    Examples        | Modeling examples of the platform
    Help            | Eclipse APP4MC help

## Build

The project uses Maven/Tycho to build.

From the root:

```
$ mvn clean verify
```
You need Maven 3.6.3 or higher to perform the POM-less Tycho build.

## License

[Eclipse Public License (EPL) v2.0][1]

[1]: https://www.eclipse.org/legal/epl-2.0/