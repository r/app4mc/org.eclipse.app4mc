/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.ide.actions.processing;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.IAnnotatable;
import org.eclipse.app4mc.amalthea.model.ListObject;
import org.eclipse.app4mc.amalthea.model.StringObject;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.ProcessingService;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.osgi.service.component.annotations.Component;

@Component(
		property = {
			"name = Custom properties | Add Approval (Demo)",
			"description = Creates a custom property with timestamp and user name" })

public class AddApproval implements ProcessingService {

	private static final String APPROVED = "approved";
	private final SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@PostConstruct
	public void addTimestamp(IAnnotatable object) {
		// compute approval string
		String signature = timestampFormat.format(new Date()) + " by " + System.getProperty("user.name");
		StringObject approval = FactoryUtil.createStringObject(signature);

		Value oldValue = object.getCustomProperties().get(APPROVED);

		if (oldValue == null) {

			// first entry: add single signature
			object.getCustomProperties().put(APPROVED, approval);

		} else if (oldValue instanceof StringObject) {

			// additional entry: convert to list and add old and new signature
			ListObject list = AmaltheaFactory.eINSTANCE.createListObject();
			object.getCustomProperties().put(APPROVED, list);
			list.getValues().add(oldValue);
			list.getValues().add(approval);

		} else if (oldValue instanceof ListObject) {

			// add another signature
			((ListObject) oldValue).getValues().add(approval);
		}
	}

}
