/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.tool.validation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.amalthea.validations.standard.AmaltheaProfile;
import org.eclipse.app4mc.amalthea.validations.standard.EMFProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ProfileManager;
import org.eclipse.app4mc.validation.util.ValidationAggregator;
import org.eclipse.app4mc.validation.util.ValidationExecutor;

//@SuppressWarnings("java:S106") // Example code - Use of standard outputs is intended
public class NewValidationExample {

	public static void main(String[] args) {

		// example: absolute path
		// final File inputFile = new File("d:/temp/democar.amxmi");
		// final File outputFile = new File("d:/temp/democar_1.amxmi");

		// example: relative path
		final File inputFile = new File("model-input/democar.amxmi");
		final File resultFile = new File("output/validation/validation-results.txt");
		final File infoFile = new File("output/validation/validation-profile.txt");

		System.out.println("-----------------------------\n Amalthea Validation Example \n-----------------------------");

		// ***** Load model *****

		Amalthea model = AmaltheaLoader.loadFromFile(inputFile);

		if (model != null) {
			System.out.println("reading file: " + inputFile.getAbsolutePath());
		} else {
			System.err.println("Error: No model loaded!");
			return;
		}

		// ***** Create validation objects *****

		List<Class<? extends IProfile>> profileList =
				List.of(AmaltheaProfile.class, EMFProfile.class);

		ValidationAggregator aggregator = new ValidationAggregator();
		aggregator.addProfiles(profileList);

		// ***** Create output directories *****

		try {
			Files.createDirectories(Paths.get(infoFile.getAbsoluteFile().getParent()));
			Files.createDirectories(Paths.get(resultFile.getAbsoluteFile().getParent()));
		} catch (IOException e1) {
			System.err.println("Error: Output directories could not be created!");
			return;
		}

		// ***** Write to info file *****

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		try (PrintStream infoStream = new PrintStream(new FileOutputStream(infoFile.getAbsoluteFile(), false)))
		{
			System.out.println("writing file: " + infoFile.getAbsoluteFile());

			infoStream.println(dateFormat.format(new Date()));

			// ***** Show single profile resolution *****

			infoStream.println("\n\n*** Validation Profiles ***");

			ProfileManager man = new ProfileManager();

			infoStream.println();
			man.dumpProfile(AmaltheaProfile.class, infoStream);

			infoStream.println();
			man.dumpProfile(EMFProfile.class, infoStream);

			// ***** Print (complex) profile info *****

			infoStream.println("\n\n*** Validation info ***");

			infoStream.println();
			infoStream.println("Compact validation map (map1)");
			infoStream.println("-----------------------------");
			infoStream.println();
			aggregator.dumpValidationMap1(infoStream);

			infoStream.println("\n");
			infoStream.println("Expanded validation map (map2)");
			infoStream.println("------------------------------");
			infoStream.println();
			aggregator.dumpValidationMap2(infoStream);

		} catch (IOException e) {
			System.err.println("Error: Info file could not be created!");
		}

		// ***** Validate *****

		ValidationExecutor executor = new ValidationExecutor(profileList);
		executor.validate(model);
		List<ValidationDiagnostic> results = executor.getResults();

		// ***** Write to result file *****

		try (PrintStream resultStream = new PrintStream(new FileOutputStream(resultFile.getAbsoluteFile(), false)))
		{
			System.out.println("writing file: " + resultFile.getAbsoluteFile());

			resultStream.println("\nValidation results (" + dateFormat.format(new Date()) + ")");
			resultStream.println("\nNumber of results: " + results.size() + "\n");

			executor.dumpResultMap(resultStream);

		} catch (IOException e) {
			System.err.println("Error: Result file could not be created!");
		}

		System.out.println("\ndone.");
	}

}
