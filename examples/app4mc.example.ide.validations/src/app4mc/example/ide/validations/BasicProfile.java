/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.ide.validations;

import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

import app4mc.example.ide.validations.demo.AmDemoDataSize;
import app4mc.example.ide.validations.demo.AmDemoName;

@Profile(name = "Basic Demo Validations")

@ValidationGroup(
		severity = Severity.ERROR,
		validations =  {
				AmDemoDataSize.class
		}
)

@ValidationGroup(
		severity = Severity.WARNING,
		validations =  {
				AmDemoName.class
		}
)

public class BasicProfile implements IProfile {
    // Do nothing
}
