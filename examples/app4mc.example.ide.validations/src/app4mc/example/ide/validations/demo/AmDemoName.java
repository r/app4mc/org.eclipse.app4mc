/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package app4mc.example.ide.validations.demo;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the correctness of names
 *
 * <ul>
 * <li>Some names appear far down the alphabetical lists ;-)</li>
 * </ul>
 */

@Validation(
		id = "AM-Demo-Name",
		checks = { "Some names appear far down the alphabetical lists ;-)" })

public class AmDemoName extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getINamed();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof INamed) {
			final INamed namedObject = (INamed) eObject;
			String name = namedObject.getName();

			// name is null or empty -> checked by another validation
			if (name == null || name.isEmpty()) {
				return;
			}

			char firstChar = name.charAt(0);

			// Raise issue if name begins with letters S to Z
			if ((firstChar >= 's' && firstChar <= 'z') || (firstChar >= 'S' && firstChar <= 'Z')) {
				addIssue(results, namedObject, ePackage.getINamed_Name(),
						"INamed: " + name(namedObject) + " should begin with a letter smaller than \'S\'");
			}
		}
	}

}
