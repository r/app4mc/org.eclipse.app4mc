/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 ********************************************************************************
 */

package app4mc.example.ide.validations.demo;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.DataSizeUnit;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * Checks the correctness of data sizes
 *
 * <ul>
 * <li>Data should be of decent size (no odds and ends)</li>
 * </ul>
 */

@Validation(
		id = "AM-Demo-Data-Size",
		checks = { "Data should be of decent size (no odds and ends)" })

public class AmDemoDataSize extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getDataSize();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof DataSize) {
			final DataSize ds = (DataSize) eObject;
			// value is unset -> always correct
			if (ds.getValue() == null) {
				return;
			}

			final EStructuralFeature containingFeature = ds.eContainingFeature();
			// data size has no context
			if (containingFeature == null) {
				return;
			}

			// check for feature and data size limit
			int size = ds.getValue().intValue();
			DataSizeUnit unit = ds.getUnit();
			boolean isTooSmall = (size < 16 && unit == DataSizeUnit.BIT) || (size < 2 && unit == DataSizeUnit.B);

			if (containingFeature == ePackage.getAbstractMemoryElement_Size() && isTooSmall) {
				addIssue(results, ds, ePackage.getDataSize_Value(),
						"DataSize: use a decent size - " + containingFeature.getName() + " value should be at least 16 bit" + namedContainerInfo(ds));
			}
		}
	}

}
