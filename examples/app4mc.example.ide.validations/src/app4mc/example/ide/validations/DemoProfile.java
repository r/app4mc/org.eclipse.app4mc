/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package app4mc.example.ide.validations;

import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ProfileGroup;
import org.eclipse.app4mc.validation.core.IProfileConfiguration;
import org.osgi.service.component.annotations.Component;

/**
 * Demo validations for AMALTHEA
 * <p>
 * This is a top level profile and will appear in the selection dialog<br>
 * (assured by the @Component annotation)
 * <p>
 * It has only one sub profile called "BasicProfile"
 */

@Profile(
	name = "Amalthea Demo Validations",
	description = "Some example validations for AMALTHEA models."
)

@ProfileGroup(
	profiles = {
		BasicProfile.class
	}
)

@Component
public class DemoProfile implements IProfileConfiguration {
	// Do nothing
}
