/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.core.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.app4mc.validation.core.test.model.TestmodelPackage;
import org.eclipse.app4mc.validation.core.test.profiles.BaseProfile;
import org.eclipse.app4mc.validation.core.test.profiles.NormalProfile;
import org.eclipse.app4mc.validation.core.test.validations.TmBase;
import org.eclipse.app4mc.validation.util.CachedProfile;
import org.eclipse.app4mc.validation.util.CachedValidator;
import org.eclipse.app4mc.validation.util.ProfileManager;
import org.eclipse.app4mc.validation.util.ValidationAggregator;
import org.junit.Test;

public class ProfileTest {

	@Test
	public void testProfileManager() {

		ProfileManager man = new ProfileManager();

		//------------------------------------------------------------------------------
		// Expected result of man.dumpProfile(BaseProfile.class, System.out)
		//------------------------------------------------------------------------------
		// PROFILE BaseProfile
		// - name: Base Validations (Test)
		// - validations:
		//    TM-BaseObject(WARNING - BaseObject)
		//        * Name must not be null or empty
		//------------------------------------------------------------------------------

		CachedProfile cp1 = man.getCachedProfile(BaseProfile.class);

		assertTrue(cp1.getCachedProfiles().isEmpty()); // no sub profiles
		assertEquals(1, cp1.getCachedValidations().size()); // one validation

		//------------------------------------------------------------------------------
		// Expected result of man.dumpProfile(NormalProfile.class, System.out)
		//------------------------------------------------------------------------------
		// PROFILE NormalProfile
		//  - name: Normal Validations (Test)
		//  - validations:
		//     TM-NormalObject(ERROR - NormalObject)
		//         * Name length == Count
		//  - profiles:
		//     PROFILE BaseProfile
		//      - name: Base Validations (Test)
		//      - validations:
		//         TM-BaseObject(WARNING - BaseObject)
		//             * Name must not be null or empty
		//------------------------------------------------------------------------------

		CachedProfile cp2 = man.getCachedProfile(NormalProfile.class);

		assertEquals(1, cp2.getCachedProfiles().size()); // one profile
		assertEquals(1, cp2.getCachedValidations().size()); // one validation

		CachedProfile pro1 = cp2.getCachedProfiles().get(BaseProfile.class);
		if (pro1 == null) return;

		CachedValidator val1 = pro1.getCachedValidations().get(TmBase.class);
		if (val1 == null) return;

		assertEquals("TM-BaseObject", val1.getValidationID()); // one included validation
	}

	@Test
	public void testAggregator() {

		ValidationAggregator aggregator = new ValidationAggregator();
		aggregator.addProfiles(List.of(BaseProfile.class, NormalProfile.class));

		//------------------------------------------------------------------------------
		// Expected result of aggregator.dumpValidationMap1(System.out)
		//------------------------------------------------------------------------------
		// BaseObject:
		//     TM-BaseObject -> WARNING
		// NormalObject:
		//     TM-NormalObject -> ERROR
		//------------------------------------------------------------------------------

		assertEquals(8, aggregator.getConcurrentValidationMap().size());

		//------------------------------------------------------------------------------
		// Expected result of aggregator.dumpValidationMap2(System.out)
		//------------------------------------------------------------------------------
		// ElementB1:
		//     TM-BaseObject -> WARNING
		// ElementN1:
		//     TM-BaseObject -> WARNING
		//     TM-NormalObject -> ERROR
		// ElementN2:
		//     TM-BaseObject -> WARNING
		//     TM-NormalObject -> ERROR
		// ElementN3:
		//     TM-BaseObject -> WARNING
		//     TM-NormalObject -> ERROR
		// ElementS1:
		//     TM-BaseObject -> WARNING
		// ElementS2:
		//     TM-BaseObject -> WARNING
		// ElementS3:
		//     TM-BaseObject -> WARNING
		// Root:
		//     TM-BaseObject -> WARNING
		//------------------------------------------------------------------------------

		assertEquals(8, aggregator.getConcurrentValidationMap().size());
		assertEquals(1, aggregator.getValidations(TestmodelPackage.eINSTANCE.getElementB1()).size());
		assertEquals(2, aggregator.getValidations(TestmodelPackage.eINSTANCE.getElementN3()).size());
		assertEquals(1, aggregator.getValidations(TestmodelPackage.eINSTANCE.getRoot()).size());
	}

}
