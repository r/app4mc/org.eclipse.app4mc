/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model.impl;

import org.eclipse.app4mc.validation.core.test.model.NormalObject;
import org.eclipse.app4mc.validation.core.test.model.TestmodelPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Normal Object</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.impl.NormalObjectImpl#getNormalDescription <em>Normal Description</em>}</li>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.impl.NormalObjectImpl#getNormalCount <em>Normal Count</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class NormalObjectImpl extends BaseObjectImpl implements NormalObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The default value of the '{@link #getNormalDescription() <em>Normal Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String NORMAL_DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNormalDescription() <em>Normal Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalDescription()
	 * @generated
	 * @ordered
	 */
	protected String normalDescription = NORMAL_DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getNormalCount() <em>Normal Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalCount()
	 * @generated
	 * @ordered
	 */
	protected static final int NORMAL_COUNT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNormalCount() <em>Normal Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalCount()
	 * @generated
	 * @ordered
	 */
	protected int normalCount = NORMAL_COUNT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NormalObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestmodelPackage.Literals.NORMAL_OBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getNormalDescription() {
		return normalDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNormalDescription(String newNormalDescription) {
		String oldNormalDescription = normalDescription;
		normalDescription = newNormalDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestmodelPackage.NORMAL_OBJECT__NORMAL_DESCRIPTION, oldNormalDescription, normalDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int getNormalCount() {
		return normalCount;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setNormalCount(int newNormalCount) {
		int oldNormalCount = normalCount;
		normalCount = newNormalCount;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestmodelPackage.NORMAL_OBJECT__NORMAL_COUNT, oldNormalCount, normalCount));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestmodelPackage.NORMAL_OBJECT__NORMAL_DESCRIPTION:
				return getNormalDescription();
			case TestmodelPackage.NORMAL_OBJECT__NORMAL_COUNT:
				return getNormalCount();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestmodelPackage.NORMAL_OBJECT__NORMAL_DESCRIPTION:
				setNormalDescription((String)newValue);
				return;
			case TestmodelPackage.NORMAL_OBJECT__NORMAL_COUNT:
				setNormalCount((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestmodelPackage.NORMAL_OBJECT__NORMAL_DESCRIPTION:
				setNormalDescription(NORMAL_DESCRIPTION_EDEFAULT);
				return;
			case TestmodelPackage.NORMAL_OBJECT__NORMAL_COUNT:
				setNormalCount(NORMAL_COUNT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestmodelPackage.NORMAL_OBJECT__NORMAL_DESCRIPTION:
				return NORMAL_DESCRIPTION_EDEFAULT == null ? normalDescription != null : !NORMAL_DESCRIPTION_EDEFAULT.equals(normalDescription);
			case TestmodelPackage.NORMAL_OBJECT__NORMAL_COUNT:
				return normalCount != NORMAL_COUNT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (normalDescription: ");
		result.append(normalDescription);
		result.append(", normalCount: ");
		result.append(normalCount);
		result.append(')');
		return result.toString();
	}

} //NormalObjectImpl
