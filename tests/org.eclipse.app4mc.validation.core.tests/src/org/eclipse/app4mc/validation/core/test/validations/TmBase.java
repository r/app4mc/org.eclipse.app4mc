/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.core.test.validations;

import java.util.List;

import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.core.test.model.BaseObject;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(
		id = "TM-BaseObject",
		checks = { "Name must not be null or empty" })

public class TmBase extends TestmodelValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getBaseObject();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof BaseObject) {
			BaseObject obj = (BaseObject) eObject;
			if (obj.getName() == null) {
				addIssue(results, obj, ePackage.getBaseObject_Name(), "Base Object: Name is null");
			} else if (obj.getName().isEmpty()) {
				addIssue(results, obj, ePackage.getBaseObject_Name(), "Base Object: Name is empty");
			}
		}
	}

}
