/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.core.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.core.test.model.ElementB1;
import org.eclipse.app4mc.validation.core.test.model.NormalObject;
import org.eclipse.app4mc.validation.core.test.model.Root;
import org.eclipse.app4mc.validation.core.test.model.SpecialObject;
import org.eclipse.app4mc.validation.core.test.profiles.BaseProfile;
import org.eclipse.app4mc.validation.core.test.profiles.NormalProfile;
import org.eclipse.app4mc.validation.core.test.profiles.SpecialProfile;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.junit.Test;

public class ExecutorTest {

	@Test
	public void testSingleProfileExecution() {

		final Class<NormalProfile> profile = NormalProfile.class;
		ValidationExecutor executor = new ValidationExecutor(profile);

		Root model = TestModels.createModel1();

		executor.validate(model);
		List<ValidationDiagnostic> results = executor.getResults();

		// to verify the result use: executor.dumpResultMap(System.out)

		assertEquals(3, results.size());

		for (ValidationDiagnostic diagnostic : results) {
			if (diagnostic.getTargetObject() instanceof ElementB1) {
				assertEquals("TM-BaseObject", diagnostic.getValidationID());
				assertEquals(Severity.WARNING, diagnostic.getSeverityLevel());
				assertEquals("Base Object: Name is null", diagnostic.getMessage());
			} else if (diagnostic.getTargetObject() instanceof NormalObject) {
				assertEquals("TM-NormalObject", diagnostic.getValidationID());
				assertEquals(Severity.ERROR, diagnostic.getSeverityLevel());
				assertEquals("Normal Object condition failed: Name length != Count", diagnostic.getMessage());
			}
		}
	}

	@Test
	public void testMultipleProfileExecution() {

		final List<Class<? extends IProfile>> profileList = List.of(BaseProfile.class, SpecialProfile.class);
		ValidationExecutor executor = new ValidationExecutor(profileList);

		Root model = TestModels.createModel1();

		executor.validate(model);
		List<ValidationDiagnostic> results = executor.getResults();

		// to verify the result use: executor.dumpResultMap(System.out)

		assertEquals(2, results.size());

		for (ValidationDiagnostic diagnostic : results) {
			if (diagnostic.getTargetObject() instanceof ElementB1) {
				assertEquals("TM-BaseObject", diagnostic.getValidationID());
				assertEquals(Severity.ERROR, diagnostic.getSeverityLevel());
				assertEquals("Base Object: Name is null", diagnostic.getMessage());
			} else if (diagnostic.getTargetObject() instanceof SpecialObject) {
				assertEquals("TM-SpecialObject", diagnostic.getValidationID());
				assertEquals(Severity.ERROR, diagnostic.getSeverityLevel());
				assertEquals("Special Object condition failed: Occurrences of one special character != Count",
						diagnostic.getMessage());
			}
		}
	}

}
