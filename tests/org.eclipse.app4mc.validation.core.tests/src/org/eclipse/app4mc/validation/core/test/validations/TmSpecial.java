/**
 ********************************************************************************
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.core.test.validations;

import java.util.List;

import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.core.test.model.SpecialObject;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(
		id = "TM-SpecialObject",
		checks = { "Occurences of one character (x, y or z) == Count" })

public class TmSpecial extends TestmodelValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSpecialObject();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof SpecialObject) {
			final SpecialObject obj = (SpecialObject) eObject;
			final String name = obj.getName();
			final int count = obj.getSpecialCount();

			if (name == null) {
				addIssue(results, obj, ePackage.getBaseObject_Name(), "Special Object condition failed: Name is null");
			} else if (countChar(name, 'x') != count &&  countChar(name, 'y') != count && countChar(name, 'z') != count) {
				addIssue(results, obj, ePackage.getBaseObject_Name(), "Special Object condition failed: Occurrences of one special character != Count");
			}
		}
	}

	private long countChar(final String input, final Character c) {
		if (input == null) return 0;

		return input.chars().filter(ch -> ch == c).count();
	}

}
