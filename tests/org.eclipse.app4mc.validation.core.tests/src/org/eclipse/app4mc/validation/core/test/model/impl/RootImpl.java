/**
 * *******************************************************************************
 *  Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 *     Generated using Eclipse EMF
 *
 * *******************************************************************************
 */
package org.eclipse.app4mc.validation.core.test.model.impl;

import java.util.Collection;

import org.eclipse.app4mc.validation.core.test.model.BaseObject;
import org.eclipse.app4mc.validation.core.test.model.NormalObject;
import org.eclipse.app4mc.validation.core.test.model.Root;
import org.eclipse.app4mc.validation.core.test.model.SpecialObject;
import org.eclipse.app4mc.validation.core.test.model.TestmodelPackage;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Root</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.impl.RootImpl#getBaseObjects <em>Base Objects</em>}</li>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.impl.RootImpl#getNormalObjects <em>Normal Objects</em>}</li>
 *   <li>{@link org.eclipse.app4mc.validation.core.test.model.impl.RootImpl#getSpecialObjects <em>Special Objects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RootImpl extends BaseObjectImpl implements Root {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "*******************************************************************************\n Copyright (c) 2020 Robert Bosch GmbH and others.\n\n This program and the accompanying materials are made\n available under the terms of the Eclipse Public License 2.0\n which is available at https://www.eclipse.org/legal/epl-2.0/\n\n SPDX-License-Identifier: EPL-2.0\n\n    Generated using Eclipse EMF\n\n*******************************************************************************";

	/**
	 * The cached value of the '{@link #getBaseObjects() <em>Base Objects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<BaseObject> baseObjects;

	/**
	 * The cached value of the '{@link #getNormalObjects() <em>Normal Objects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormalObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<NormalObject> normalObjects;

	/**
	 * The cached value of the '{@link #getSpecialObjects() <em>Special Objects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<SpecialObject> specialObjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RootImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestmodelPackage.Literals.ROOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<BaseObject> getBaseObjects() {
		if (baseObjects == null) {
			baseObjects = new EObjectContainmentEList<>(BaseObject.class, this, TestmodelPackage.ROOT__BASE_OBJECTS);
		}
		return baseObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<NormalObject> getNormalObjects() {
		if (normalObjects == null) {
			normalObjects = new EObjectContainmentEList<>(NormalObject.class, this, TestmodelPackage.ROOT__NORMAL_OBJECTS);
		}
		return normalObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SpecialObject> getSpecialObjects() {
		if (specialObjects == null) {
			specialObjects = new EObjectContainmentEList<>(SpecialObject.class, this, TestmodelPackage.ROOT__SPECIAL_OBJECTS);
		}
		return specialObjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TestmodelPackage.ROOT__BASE_OBJECTS:
				return ((InternalEList<?>)getBaseObjects()).basicRemove(otherEnd, msgs);
			case TestmodelPackage.ROOT__NORMAL_OBJECTS:
				return ((InternalEList<?>)getNormalObjects()).basicRemove(otherEnd, msgs);
			case TestmodelPackage.ROOT__SPECIAL_OBJECTS:
				return ((InternalEList<?>)getSpecialObjects()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestmodelPackage.ROOT__BASE_OBJECTS:
				return getBaseObjects();
			case TestmodelPackage.ROOT__NORMAL_OBJECTS:
				return getNormalObjects();
			case TestmodelPackage.ROOT__SPECIAL_OBJECTS:
				return getSpecialObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestmodelPackage.ROOT__BASE_OBJECTS:
				getBaseObjects().clear();
				getBaseObjects().addAll((Collection<? extends BaseObject>)newValue);
				return;
			case TestmodelPackage.ROOT__NORMAL_OBJECTS:
				getNormalObjects().clear();
				getNormalObjects().addAll((Collection<? extends NormalObject>)newValue);
				return;
			case TestmodelPackage.ROOT__SPECIAL_OBJECTS:
				getSpecialObjects().clear();
				getSpecialObjects().addAll((Collection<? extends SpecialObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestmodelPackage.ROOT__BASE_OBJECTS:
				getBaseObjects().clear();
				return;
			case TestmodelPackage.ROOT__NORMAL_OBJECTS:
				getNormalObjects().clear();
				return;
			case TestmodelPackage.ROOT__SPECIAL_OBJECTS:
				getSpecialObjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestmodelPackage.ROOT__BASE_OBJECTS:
				return baseObjects != null && !baseObjects.isEmpty();
			case TestmodelPackage.ROOT__NORMAL_OBJECTS:
				return normalObjects != null && !normalObjects.isEmpty();
			case TestmodelPackage.ROOT__SPECIAL_OBJECTS:
				return specialObjects != null && !specialObjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RootImpl
