/**
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.sim.tests;

import com.google.common.base.Objects;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.VariableRateStimulus;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder;
import org.eclipse.app4mc.amalthea.validations.sim.SimBasicProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class SimBasicIdentifiersTests {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private StimuliBuilder b2 = new StimuliBuilder();

  @Extension
  private SoftwareBuilder b3 = new SoftwareBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(SimBasicProfile.class);

  @Test
  public void test_SimBasicIdentifiers() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<StimuliModel> _function_1 = (StimuliModel it_1) -> {
        final Consumer<PeriodicStimulus> _function_2 = (PeriodicStimulus it_2) -> {
          it_2.setName("ps_ok");
        };
        this.b2.periodicStimulus(it_1, _function_2);
        final Consumer<PeriodicStimulus> _function_3 = (PeriodicStimulus it_2) -> {
          it_2.setName("99");
        };
        this.b2.periodicStimulus(it_1, _function_3);
        final Consumer<VariableRateStimulus> _function_4 = (VariableRateStimulus it_2) -> {
          it_2.setName("while");
        };
        this.b2.variableRateStimulus(it_1, _function_4);
      };
      this.b1.stimuliModel(it, _function_1);
      final Consumer<SWModel> _function_2 = (SWModel it_1) -> {
        final Consumer<Task> _function_3 = (Task it_2) -> {
          it_2.setName("_BlaBla");
        };
        this.b3.task(it_1, _function_3);
        final Consumer<Task> _function_4 = (Task it_2) -> {
          it_2.setName("BLA_BLA");
        };
        this.b3.task(it_1, _function_4);
        final Consumer<Task> _function_5 = (Task it_2) -> {
          it_2.setName("not good");
        };
        this.b3.task(it_1, _function_5);
      };
      this.b1.softwareModel(it, _function_2);
    };
    final Amalthea model = this.b1.amalthea(_function);
    this.executor.validate(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> messages = this.executor.getResults().stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    int _size = messages.size();
    boolean _equals = (_size == 3);
    Assert.assertTrue(_equals);
    Assert.assertTrue(messages.contains("Task \"not good\": Invalid name (not a valid C++ identifier)"));
    Assert.assertTrue(messages.contains("Periodic Stimulus \"99\": Invalid name (not a valid C++ identifier)"));
    Assert.assertTrue(messages.contains("Variable Rate Stimulus \"while\": Invalid name (not a valid C++ identifier)"));
  }
}
