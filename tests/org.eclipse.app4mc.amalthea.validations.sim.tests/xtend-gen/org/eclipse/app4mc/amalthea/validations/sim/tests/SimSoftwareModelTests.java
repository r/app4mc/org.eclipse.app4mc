/**
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.sim.tests;

import com.google.common.base.Objects;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.AbstractMemoryElement;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.ChannelAccess;
import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.MemoryMapping;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaWriter;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.validations.sim.SimSoftwareProfile;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelAccessFeasibility;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelElements;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelMapped;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelAccessFeasibility;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelAccessType;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelMapped;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelAccessFeasibility;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelAccessType;
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelMapped;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class SimSoftwareModelTests {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private SoftwareBuilder b2 = new SoftwareBuilder();

  @Extension
  private OperatingSystemBuilder b3 = new OperatingSystemBuilder();

  @Extension
  private HardwareBuilder b4 = new HardwareBuilder();

  @Extension
  private MappingBuilder b5 = new MappingBuilder();

  @Extension
  private StimuliBuilder b6 = new StimuliBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(SimSoftwareProfile.class);

  private Amalthea createValidTestModel() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<StimuliModel> _function_1 = (StimuliModel it_1) -> {
        final Consumer<PeriodicStimulus> _function_2 = (PeriodicStimulus it_2) -> {
          it_2.setName("periodicStimulus");
          it_2.setOffset(FactoryUtil.createTime("100 ms"));
          it_2.setRecurrence(FactoryUtil.createTime("10 ms"));
        };
        this.b6.periodicStimulus(it_1, _function_2);
      };
      this.b1.stimuliModel(it, _function_1);
      final Consumer<SWModel> _function_2 = (SWModel it_1) -> {
        final Consumer<Label> _function_3 = (Label it_2) -> {
          it_2.setName("label");
        };
        this.b2.label(it_1, _function_3);
        final Consumer<ModeLabel> _function_4 = (ModeLabel it_2) -> {
          it_2.setName("modelabel");
        };
        this.b2.modeLabel(it_1, _function_4);
        final Consumer<Channel> _function_5 = (Channel it_2) -> {
          it_2.setName("channel");
        };
        this.b2.channel(it_1, _function_5);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_6 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("R_1");
          final Consumer<ActivityGraph> _function_7 = (ActivityGraph it_3) -> {
            final Consumer<LabelAccess> _function_8 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "label"));
              it_4.setAccess(LabelAccessEnum.WRITE);
            };
            this.b2.labelAccess(it_3, _function_8);
            final Consumer<ModeLabelAccess> _function_9 = (ModeLabelAccess it_4) -> {
              it_4.setData(this.b1.<ModeLabel>_find(it_4, ModeLabel.class, "modelabel"));
              it_4.setAccess(ModeLabelAccessEnum.INCREMENT);
            };
            this.b2.modeLabelAccess(it_3, _function_9);
            final Consumer<ChannelSend> _function_10 = (ChannelSend it_4) -> {
              it_4.setData(this.b1.<Channel>_find(it_4, Channel.class, "channel"));
              it_4.setElements(1);
            };
            this.b2.channelSend(it_3, _function_10);
            final Consumer<ChannelReceive> _function_11 = (ChannelReceive it_4) -> {
              it_4.setData(this.b1.<Channel>_find(it_4, Channel.class, "channel"));
              it_4.setElements(1);
            };
            this.b2.channelReceive(it_3, _function_11);
          };
          this.b2.activityGraph(it_2, _function_7);
        };
        this.b2.runnable(it_1, _function_6);
        final Consumer<Task> _function_7 = (Task it_2) -> {
          it_2.setName("Task_valid");
          final Consumer<ActivityGraph> _function_8 = (ActivityGraph it_3) -> {
            final Consumer<RunnableCall> _function_9 = (RunnableCall it_4) -> {
              it_4.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "R_1"));
            };
            this.b2.runnableCall(it_3, _function_9);
            final Consumer<LabelAccess> _function_10 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "label"));
              it_4.setAccess(LabelAccessEnum.WRITE);
            };
            this.b2.labelAccess(it_3, _function_10);
          };
          this.b2.activityGraph(it_2, _function_8);
          EList<Stimulus> _stimuli = it_2.getStimuli();
          PeriodicStimulus __find = this.b1.<PeriodicStimulus>_find(it_2, PeriodicStimulus.class, "periodicStimulus");
          _stimuli.add(__find);
        };
        this.b2.task(it_1, _function_7);
        final Consumer<ISR> _function_8 = (ISR it_2) -> {
          it_2.setName("ISR_valid");
          final Consumer<ActivityGraph> _function_9 = (ActivityGraph it_3) -> {
            final Consumer<RunnableCall> _function_10 = (RunnableCall it_4) -> {
              it_4.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "R_1"));
            };
            this.b2.runnableCall(it_3, _function_10);
            final Consumer<LabelAccess> _function_11 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "label"));
              it_4.setAccess(LabelAccessEnum.WRITE);
            };
            this.b2.labelAccess(it_3, _function_11);
          };
          this.b2.activityGraph(it_2, _function_9);
          EList<Stimulus> _stimuli = it_2.getStimuli();
          PeriodicStimulus __find = this.b1.<PeriodicStimulus>_find(it_2, PeriodicStimulus.class, "periodicStimulus");
          _stimuli.add(__find);
        };
        this.b2.isr(it_1, _function_8);
      };
      this.b1.softwareModel(it, _function_2);
      final Consumer<HWModel> _function_3 = (HWModel it_1) -> {
        final Consumer<ProcessingUnitDefinition> _function_4 = (ProcessingUnitDefinition it_2) -> {
          it_2.setName("TestCoreDef");
        };
        this.b4.definition_ProcessingUnit(it_1, _function_4);
        final Consumer<HwStructure> _function_5 = (HwStructure it_2) -> {
          it_2.setName("System");
          final Consumer<Memory> _function_6 = (Memory it_3) -> {
            it_3.setName("TestMemory");
          };
          this.b4.module_Memory(it_2, _function_6);
          final Consumer<ProcessingUnit> _function_7 = (ProcessingUnit it_3) -> {
            it_3.setName("TestCore");
            it_3.setDefinition(this.b1.<ProcessingUnitDefinition>_find(it_3, ProcessingUnitDefinition.class, "TestCoreDef"));
            final Consumer<HwAccessElement> _function_8 = (HwAccessElement it_4) -> {
              it_4.setDestination(this.b1.<Memory>_find(it_4, Memory.class, "TestMemory"));
            };
            this.b4.access(it_3, _function_8);
          };
          this.b4.module_ProcessingUnit(it_2, _function_7);
        };
        this.b4.structure(it_1, _function_5);
      };
      this.b1.hardwareModel(it, _function_3);
      final Consumer<OSModel> _function_4 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_5 = (OperatingSystem it_2) -> {
          it_2.setName("TestOS");
          final Consumer<TaskScheduler> _function_6 = (TaskScheduler it_3) -> {
            it_3.setName("TestScheduler");
          };
          this.b3.taskScheduler(it_2, _function_6);
          final Consumer<InterruptController> _function_7 = (InterruptController it_3) -> {
            it_3.setName("InterruptController");
          };
          this.b3.interruptController(it_2, _function_7);
        };
        this.b3.operatingSystem(it_1, _function_5);
      };
      this.b1.osModel(it, _function_4);
      final Consumer<MappingModel> _function_5 = (MappingModel it_1) -> {
        final Consumer<TaskAllocation> _function_6 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "Task_valid"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "TestScheduler"));
        };
        this.b5.taskAllocation(it_1, _function_6);
        final Consumer<ISRAllocation> _function_7 = (ISRAllocation it_2) -> {
          it_2.setIsr(this.b1.<ISR>_find(it_2, ISR.class, "ISR_valid"));
          it_2.setController(this.b1.<InterruptController>_find(it_2, InterruptController.class, "InterruptController"));
        };
        this.b5.isrAllocation(it_1, _function_7);
        final Consumer<SchedulerAllocation> _function_8 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "TestScheduler"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "TestCore");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_8);
        final Consumer<SchedulerAllocation> _function_9 = (SchedulerAllocation it_2) -> {
          it_2.setScheduler(this.b1.<InterruptController>_find(it_2, InterruptController.class, "InterruptController"));
          EList<ProcessingUnit> _responsibility = it_2.getResponsibility();
          ProcessingUnit __find = this.b1.<ProcessingUnit>_find(it_2, ProcessingUnit.class, "TestCore");
          _responsibility.add(__find);
        };
        this.b5.schedulerAllocation(it_1, _function_9);
        final Consumer<MemoryMapping> _function_10 = (MemoryMapping it_2) -> {
          it_2.setAbstractElement(this.b1.<Label>_find(it_2, Label.class, "label"));
          it_2.setMemory(this.b1.<Memory>_find(it_2, Memory.class, "TestMemory"));
        };
        this.b5.memoryMapping(it_1, _function_10);
        final Consumer<MemoryMapping> _function_11 = (MemoryMapping it_2) -> {
          it_2.setAbstractElement(this.b1.<ModeLabel>_find(it_2, ModeLabel.class, "modelabel"));
          it_2.setMemory(this.b1.<Memory>_find(it_2, Memory.class, "TestMemory"));
        };
        this.b5.memoryMapping(it_1, _function_11);
        final Consumer<MemoryMapping> _function_12 = (MemoryMapping it_2) -> {
          it_2.setAbstractElement(this.b1.<Channel>_find(it_2, Channel.class, "channel"));
          it_2.setMemory(this.b1.<Memory>_find(it_2, Memory.class, "TestMemory"));
        };
        this.b5.memoryMapping(it_1, _function_12);
      };
      this.b1.mappingModel(it, _function_5);
    };
    final Amalthea model = this.b1.amalthea(_function);
    AmaltheaWriter.writeToFileNamed(model, "test-data/software/SimSoftwareTestsModel.amxmi");
    return model;
  }

  @Test
  public void test_SimSoftwareLabelAccessFeasibility() {
    final Amalthea model = this.createValidTestModel();
    final Consumer<Label> _function = (Label it) -> {
      it.setName("label_notMapped");
    };
    this.b2.label(model.getSwModel(), _function);
    final Consumer<Task> _function_1 = (Task it) -> {
      it.setName("Task_labelNotMapped");
      final Consumer<ActivityGraph> _function_2 = (ActivityGraph it_1) -> {
        final Consumer<LabelAccess> _function_3 = (LabelAccess it_2) -> {
          it_2.setData(this.b1.<Label>_find(it_2, Label.class, "label_notMapped"));
          it_2.setAccess(LabelAccessEnum.WRITE);
        };
        this.b2.labelAccess(it_1, _function_3);
      };
      this.b2.activityGraph(it, _function_2);
      EList<Stimulus> _stimuli = it.getStimuli();
      PeriodicStimulus __find = this.b1.<PeriodicStimulus>_find(it, PeriodicStimulus.class, "periodicStimulus");
      _stimuli.add(__find);
    };
    this.b2.task(model.getSwModel(), _function_1);
    final Consumer<Task> _function_2 = (Task it) -> {
      it.setName("Task_notMapped");
      final Consumer<ActivityGraph> _function_3 = (ActivityGraph it_1) -> {
        final Consumer<RunnableCall> _function_4 = (RunnableCall it_2) -> {
          this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_2, org.eclipse.app4mc.amalthea.model.Runnable.class, "R_1");
        };
        this.b2.runnableCall(it_1, _function_4);
        final Consumer<LabelAccess> _function_5 = (LabelAccess it_2) -> {
          it_2.setData(this.b1.<Label>_find(it_2, Label.class, "label_valid"));
          it_2.setAccess(LabelAccessEnum.WRITE);
        };
        this.b2.labelAccess(it_1, _function_5);
      };
      this.b2.activityGraph(it, _function_3);
      EList<Stimulus> _stimuli = it.getStimuli();
      PeriodicStimulus __find = this.b1.<PeriodicStimulus>_find(it, PeriodicStimulus.class, "periodicStimulus");
      _stimuli.add(__find);
    };
    this.b2.task(model.getSwModel(), _function_2);
    final Consumer<ISR> _function_3 = (ISR it) -> {
      it.setName("ISR_notMapped");
      final Consumer<ActivityGraph> _function_4 = (ActivityGraph it_1) -> {
        final Consumer<RunnableCall> _function_5 = (RunnableCall it_2) -> {
          this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_2, org.eclipse.app4mc.amalthea.model.Runnable.class, "R_1");
        };
        this.b2.runnableCall(it_1, _function_5);
        final Consumer<LabelAccess> _function_6 = (LabelAccess it_2) -> {
          it_2.setData(this.b1.<Label>_find(it_2, Label.class, "label"));
          it_2.setAccess(LabelAccessEnum.WRITE);
        };
        this.b2.labelAccess(it_1, _function_6);
      };
      this.b2.activityGraph(it, _function_4);
      EList<Stimulus> _stimuli = it.getStimuli();
      PeriodicStimulus __find = this.b1.<PeriodicStimulus>_find(it, PeriodicStimulus.class, "periodicStimulus");
      _stimuli.add(__find);
    };
    this.b2.isr(model.getSwModel(), _function_3);
    AmaltheaIndex.buildIndex(model);
    final Set<HwAccessElement> hwAccessElements = AmaltheaIndex.<HwAccessElement>getElements(model, Pattern.compile(".*"), HwAccessElement.class);
    AmaltheaIndex.deleteAll(hwAccessElements);
    this.executor.validate(model);
    this.executor.getResults();
    final Predicate<ValidationDiagnostic> _function_4 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_5 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = this.executor.getResults().stream().filter(_function_4).<String>map(_function_5).collect(Collectors.<String>toList());
    int _size = errors.size();
    boolean _equals = (_size == 6);
    Assert.assertTrue(_equals);
    Assert.assertTrue(errors.contains(SimSoftwareLabelMapped.MESSAGE));
    Assert.assertTrue(errors.contains(SimSoftwareLabelAccessFeasibility.MESSAGE));
    Assert.assertTrue(errors.contains(SimSoftwareChannelAccessFeasibility.MESSAGE));
    final Predicate<ValidationDiagnostic> _function_6 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.WARNING);
    };
    final Function<ValidationDiagnostic, String> _function_7 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> warnings = this.executor.getResults().stream().filter(_function_6).<String>map(_function_7).collect(Collectors.<String>toList());
    int _size_1 = warnings.size();
    boolean _equals_1 = (_size_1 == 1);
    Assert.assertTrue(_equals_1);
    Assert.assertTrue(warnings.contains(SimSoftwareModeLabelAccessFeasibility.MESSAGE));
  }

  @Test
  public void test_SimSoftwareAbstractMemoryElementMapped() {
    final Amalthea model = this.createValidTestModel();
    final Function1<AbstractMemoryElement, Boolean> _function = (AbstractMemoryElement it) -> {
      return Boolean.valueOf((((it instanceof Label) || (it instanceof Channel)) || (it instanceof ModeLabel)));
    };
    final Iterable<AbstractMemoryElement> memoryElements = IterableExtensions.<AbstractMemoryElement>filter(AmaltheaIndex.<AbstractMemoryElement>getElements(model, Pattern.compile(".*"), AbstractMemoryElement.class), _function);
    final Consumer<AbstractMemoryElement> _function_1 = (AbstractMemoryElement element) -> {
      final Set<MemoryMapping> memoryMappings = AmaltheaIndex.<MemoryMapping>getReferringObjects(element, MemoryMapping.class);
      AmaltheaIndex.deleteAll(memoryMappings);
    };
    memoryElements.forEach(_function_1);
    this.executor.validate(model);
    this.executor.getResults();
    final Predicate<ValidationDiagnostic> _function_2 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = this.executor.getResults().stream().filter(_function_2).<String>map(_function_3).collect(Collectors.<String>toList());
    int _size = errors.size();
    boolean _equals = (_size == 2);
    Assert.assertTrue(_equals);
    Assert.assertTrue(errors.contains(SimSoftwareLabelMapped.MESSAGE));
    Assert.assertTrue(errors.contains(SimSoftwareChannelMapped.MESSAGE));
    final Predicate<ValidationDiagnostic> _function_4 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.WARNING);
    };
    final Function<ValidationDiagnostic, String> _function_5 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> warnings = this.executor.getResults().stream().filter(_function_4).<String>map(_function_5).collect(Collectors.<String>toList());
    int _size_1 = warnings.size();
    boolean _equals_1 = (_size_1 == 1);
    Assert.assertTrue(_equals_1);
    Assert.assertTrue(warnings.contains(SimSoftwareModeLabelMapped.MESSAGE));
  }

  @Test
  public void test_SimSoftwareLabelAccessType() {
    final Amalthea model = this.createValidTestModel();
    final Function1<AbstractMemoryElement, Boolean> _function = (AbstractMemoryElement it) -> {
      return Boolean.valueOf((it instanceof Label));
    };
    final Iterable<AbstractMemoryElement> memoryElements = IterableExtensions.<AbstractMemoryElement>filter(AmaltheaIndex.<AbstractMemoryElement>getElements(model, Pattern.compile(".*"), AbstractMemoryElement.class), _function);
    final Consumer<AbstractMemoryElement> _function_1 = (AbstractMemoryElement element) -> {
      final Set<LabelAccess> labelAccesses = AmaltheaIndex.<LabelAccess>getReferringObjects(element, LabelAccess.class);
      final Consumer<LabelAccess> _function_2 = (LabelAccess labelAccess) -> {
        labelAccess.setAccess(null);
      };
      labelAccesses.forEach(_function_2);
    };
    memoryElements.forEach(_function_1);
    this.executor.validate(model);
    this.executor.getResults();
    final Predicate<ValidationDiagnostic> _function_2 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = this.executor.getResults().stream().filter(_function_2).<String>map(_function_3).collect(Collectors.<String>toList());
    int _size = errors.size();
    boolean _equals = (_size == 3);
    Assert.assertTrue(_equals);
    Assert.assertTrue(errors.contains(SimSoftwareLabelAccessType.MESSAGE));
  }

  @Test
  public void test_SimSoftwareModelLabelAccessType() {
    final Amalthea model = this.createValidTestModel();
    final Function1<AbstractMemoryElement, Boolean> _function = (AbstractMemoryElement it) -> {
      return Boolean.valueOf((it instanceof ModeLabel));
    };
    final Iterable<AbstractMemoryElement> memoryElements = IterableExtensions.<AbstractMemoryElement>filter(AmaltheaIndex.<AbstractMemoryElement>getElements(model, Pattern.compile(".*"), AbstractMemoryElement.class), _function);
    final Consumer<AbstractMemoryElement> _function_1 = (AbstractMemoryElement element) -> {
      final Set<ModeLabelAccess> modeLabelAccesses = AmaltheaIndex.<ModeLabelAccess>getReferringObjects(element, ModeLabelAccess.class);
      final Consumer<ModeLabelAccess> _function_2 = (ModeLabelAccess modeLabelAccess) -> {
        modeLabelAccess.setAccess(null);
      };
      modeLabelAccesses.forEach(_function_2);
    };
    memoryElements.forEach(_function_1);
    this.executor.validate(model);
    this.executor.getResults();
    final Predicate<ValidationDiagnostic> _function_2 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = this.executor.getResults().stream().filter(_function_2).<String>map(_function_3).collect(Collectors.<String>toList());
    int _size = errors.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    Assert.assertTrue(errors.contains(SimSoftwareModeLabelAccessType.MESSAGE));
  }

  @Test
  public void test_SimSoftwareChannelElements() {
    final Amalthea model = this.createValidTestModel();
    final Function1<AbstractMemoryElement, Boolean> _function = (AbstractMemoryElement it) -> {
      return Boolean.valueOf((it instanceof Channel));
    };
    final Iterable<AbstractMemoryElement> memoryElements = IterableExtensions.<AbstractMemoryElement>filter(AmaltheaIndex.<AbstractMemoryElement>getElements(model, Pattern.compile(".*"), AbstractMemoryElement.class), _function);
    final Consumer<AbstractMemoryElement> _function_1 = (AbstractMemoryElement element) -> {
      final Set<ChannelAccess> channelAccesses = AmaltheaIndex.<ChannelAccess>getReferringObjects(element, ChannelAccess.class);
      final Consumer<ChannelAccess> _function_2 = (ChannelAccess channelAccess) -> {
        channelAccess.setElements(0);
      };
      channelAccesses.forEach(_function_2);
    };
    memoryElements.forEach(_function_1);
    this.executor.validate(model);
    this.executor.getResults();
    final Predicate<ValidationDiagnostic> _function_2 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_3 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> errors = this.executor.getResults().stream().filter(_function_2).<String>map(_function_3).collect(Collectors.<String>toList());
    int _size = errors.size();
    boolean _equals = (_size == 2);
    Assert.assertTrue(_equals);
    Assert.assertTrue(errors.contains(SimSoftwareChannelElements.MESSAGE));
  }
}
