/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.tests

import java.util.regex.Pattern
import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.AbstractMemoryElement
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex
import org.eclipse.app4mc.amalthea.model.Channel
import org.eclipse.app4mc.amalthea.model.HwAccessElement
import org.eclipse.app4mc.amalthea.model.ISR
import org.eclipse.app4mc.amalthea.model.InterruptController
import org.eclipse.app4mc.amalthea.model.Label
import org.eclipse.app4mc.amalthea.model.LabelAccess
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum
import org.eclipse.app4mc.amalthea.model.Memory
import org.eclipse.app4mc.amalthea.model.MemoryMapping
import org.eclipse.app4mc.amalthea.model.ModeLabel
import org.eclipse.app4mc.amalthea.model.ModeLabelAccessEnum
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition
import org.eclipse.app4mc.amalthea.model.Runnable
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder
import org.eclipse.app4mc.amalthea.model.io.AmaltheaWriter
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amalthea.validations.sim.SimSoftwareProfile
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelAccessFeasibility
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelMapped
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelAccessFeasibility
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelAccessType
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareLabelMapped
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelAccessFeasibility
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelMapped
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertTrue
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareModeLabelAccessType
import org.eclipse.app4mc.amalthea.model.ChannelAccess
import org.eclipse.app4mc.amalthea.validations.sim.software.SimSoftwareChannelElements

class SimSoftwareModelTests {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension SoftwareBuilder b2 = new SoftwareBuilder
	extension OperatingSystemBuilder b3 = new OperatingSystemBuilder
	extension HardwareBuilder b4 = new HardwareBuilder
	extension MappingBuilder b5 = new MappingBuilder
	extension StimuliBuilder b6 = new StimuliBuilder
	
	
	val executor = new ValidationExecutor(SimSoftwareProfile)
	
	private def createValidTestModel(){
		val model =
		amalthea [
			stimuliModel[
				periodicStimulus[
					name="periodicStimulus"
					offset= FactoryUtil.createTime("100 ms")
					recurrence = FactoryUtil.createTime("10 ms")
				]
			]
			softwareModel [
				label[name = "label"]
				modeLabel[name = "modelabel"]
				channel[name = "channel"]
				runnable[ 
					name="R_1"
					activityGraph [
						labelAccess[ 
							data = _find(Label, "label")
							access = LabelAccessEnum.WRITE
						]
						modeLabelAccess[ data = _find(ModeLabel, "modelabel")
							access = ModeLabelAccessEnum.INCREMENT
						]
						channelSend[ 
							data = _find(Channel, "channel")
							elements = 1
						]
						channelReceive[ 
							data = _find(Channel, "channel")
							elements= 1
						]
					]	
				]
				task [ 
					name = "Task_valid" 

					activityGraph [
						runnableCall[runnable = _find(Runnable, "R_1")]
						labelAccess[ 
							data = _find(Label, "label")
							access = LabelAccessEnum.WRITE
						]
					]
					stimuli += _find(PeriodicStimulus, "periodicStimulus")
				]			
				isr [ 
					name = "ISR_valid" 
					activityGraph[
						runnableCall[runnable = _find(Runnable, "R_1")]
						labelAccess[ 
							data = _find(Label, "label")
							access = LabelAccessEnum.WRITE
						]
					]
					stimuli += _find(PeriodicStimulus, "periodicStimulus")
				]
				
			]
			hardwareModel [
				definition_ProcessingUnit [ name = "TestCoreDef" ]
				structure [
					name = "System"
					module_Memory[
						name="TestMemory"
					]
					module_ProcessingUnit [
						name = "TestCore"
						definition = _find(ProcessingUnitDefinition, "TestCoreDef")
						access[
							destination = _find(Memory, "TestMemory")
						]
					]
					
				]
			]
			osModel [
				operatingSystem [ name = "TestOS"
					taskScheduler [ name = "TestScheduler" ]
					interruptController [name = "InterruptController"]
				] 
			]
			mappingModel [
				taskAllocation [
					task = _find(Task, "Task_valid")
					scheduler = _find(TaskScheduler, "TestScheduler")
				]
				isrAllocation [
					isr = _find(ISR, "ISR_valid")
					controller = _find(InterruptController, "InterruptController")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "TestScheduler")
					responsibility += _find(ProcessingUnit, "TestCore")
				]
				schedulerAllocation [
					scheduler = _find(InterruptController, "InterruptController")
					responsibility += _find(ProcessingUnit, "TestCore")
				]
				memoryMapping [
					abstractElement = _find(Label, "label")
					memory = _find(Memory, "TestMemory")
				]
				memoryMapping [
					abstractElement = _find(ModeLabel, "modelabel")
					memory = _find(Memory, "TestMemory")
				]
				memoryMapping [
					abstractElement = _find(Channel, "channel")
					memory = _find(Memory, "TestMemory")
				]
			]
			
		];
		AmaltheaWriter.writeToFileNamed(model, "test-data/software/SimSoftwareTestsModel.amxmi")
		return model;
	}

	@Test
	def void test_SimSoftwareLabelAccessFeasibility() {	
		val model = createValidTestModel();
		
		//unmapped task/isr should be filtered out and cause no issue within this validation (handled by standard validations)
		model.swModel.label[
			name = "label_notMapped"
		]
		model.swModel.task[
			name = "Task_labelNotMapped" 
			activityGraph [
				labelAccess[ data = _find(Label, "label_notMapped")
							access = LabelAccessEnum.WRITE
				]
			]
			stimuli += _find(PeriodicStimulus, "periodicStimulus")
		]
		model.swModel.task[
			name = "Task_notMapped" 
			activityGraph [
				runnableCall[_find(Runnable, "R_1")]
				labelAccess[ 
					data = _find(Label, "label_valid")
					access = LabelAccessEnum.WRITE
				]
			]
			stimuli += _find(PeriodicStimulus, "periodicStimulus")
		]
		model.swModel.isr[
			name = "ISR_notMapped" 
			activityGraph [
				runnableCall[_find(Runnable, "R_1")]
				labelAccess[ 
					data = _find(Label, "label")
					access = LabelAccessEnum.WRITE
				]
			]
			stimuli += _find(PeriodicStimulus, "periodicStimulus")
		]
		//remove hw access elements
		AmaltheaIndex.buildIndex(model)
		val hwAccessElements = AmaltheaIndex.getElements(model, Pattern.compile(".*"), HwAccessElement)
		AmaltheaIndex.deleteAll(hwAccessElements)
	
		executor.validate(model)
		executor.results
		
		val errors = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(errors.size == 6)
		 //from "label_notMapped" -> 1 message
		assertTrue(errors.contains(SimSoftwareLabelMapped.MESSAGE))
		// from missing hw access element (1 from ISR_valid, 1 from Task_valid, 1 from Runnable R1) -> 3 messages
		assertTrue(errors.contains(SimSoftwareLabelAccessFeasibility.MESSAGE)) 
		 // from missing hw access element (1 for channelsend, 1 for channelreceive, both in Runnable R1) -> 2 messages
		assertTrue(errors.contains(SimSoftwareChannelAccessFeasibility.MESSAGE))
		
		val warnings = executor.results.stream.filter[it.severityLevel == Severity.WARNING].map[it.message].collect(Collectors.toList)
		assertTrue(warnings.size == 1)
		assertTrue(warnings.contains(SimSoftwareModeLabelAccessFeasibility.MESSAGE)) // from missing hw access element
		
		
	}
	
	@Test
	def void test_SimSoftwareAbstractMemoryElementMapped() {	
		val model = createValidTestModel();
		
		val memoryElements = AmaltheaIndex.getElements(model, Pattern.compile(".*"), AbstractMemoryElement).filter[it instanceof Label || it instanceof Channel || it instanceof ModeLabel];
		memoryElements.forEach[element | { 
			val memoryMappings = AmaltheaIndex.getReferringObjects(element, MemoryMapping)
			AmaltheaIndex.deleteAll(memoryMappings)
		}]
		
		executor.validate(model)
		executor.results
		
		val errors = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(errors.size == 2)
		assertTrue(errors.contains(SimSoftwareLabelMapped.MESSAGE))
		assertTrue(errors.contains(SimSoftwareChannelMapped.MESSAGE))
		
		val warnings = executor.results.stream.filter[it.severityLevel == Severity.WARNING].map[it.message].collect(Collectors.toList)
		assertTrue(warnings.size == 1)
		assertTrue(warnings.contains(SimSoftwareModeLabelMapped.MESSAGE))
		
	}
	
		@Test
	def void test_SimSoftwareLabelAccessType() {	
		val model = createValidTestModel();
		
		val memoryElements = AmaltheaIndex.getElements(model, Pattern.compile(".*"), AbstractMemoryElement).filter[it instanceof Label];
		memoryElements.forEach[element | { 
			val labelAccesses = AmaltheaIndex.getReferringObjects(element, LabelAccess)
			labelAccesses.forEach[labelAccess | labelAccess.access=null]
		}]
		
		executor.validate(model)
		executor.results
		
		val errors = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(errors.size == 3)
		assertTrue(errors.contains(SimSoftwareLabelAccessType.MESSAGE))	
	}

	@Test
	def void test_SimSoftwareModelLabelAccessType() {	
		val model = createValidTestModel();
		
		val memoryElements = AmaltheaIndex.getElements(model, Pattern.compile(".*"), AbstractMemoryElement).filter[it instanceof ModeLabel];
		memoryElements.forEach[element | { 
			val modeLabelAccesses = AmaltheaIndex.getReferringObjects(element, ModeLabelAccess)
			modeLabelAccesses.forEach[modeLabelAccess | modeLabelAccess.access=null]
		}]
		
		executor.validate(model)
		executor.results
		
		val errors = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(errors.size == 1)
		assertTrue(errors.contains(SimSoftwareModeLabelAccessType.MESSAGE))	
	}
	
	@Test
	def void test_SimSoftwareChannelElements() {	
		val model = createValidTestModel();
		
		val memoryElements = AmaltheaIndex.getElements(model, Pattern.compile(".*"), AbstractMemoryElement).filter[it instanceof Channel];
		memoryElements.forEach[element | { 
			val channelAccesses = AmaltheaIndex.getReferringObjects(element, ChannelAccess)
			channelAccesses.forEach[channelAccess | channelAccess.elements = 0]
		}]
		
		executor.validate(model)
		executor.results
		
		val errors = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(errors.size == 2) //for channelSend and channelReceive
		assertTrue(errors.contains(SimSoftwareChannelElements.MESSAGE))	
	}

}
