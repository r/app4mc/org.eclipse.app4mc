/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.tests

import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.StimuliBuilder
import org.eclipse.app4mc.amalthea.validations.sim.SimBasicProfile
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertTrue

class SimBasicIdentifiersTests {
	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension StimuliBuilder b2 = new StimuliBuilder
	extension SoftwareBuilder b3 = new SoftwareBuilder
	val executor = new ValidationExecutor(SimBasicProfile)

	@Test
	def void test_SimBasicIdentifiers() {
		val model = amalthea [
			stimuliModel [
				periodicStimulus [ name = "ps_ok" ]
				periodicStimulus [ name = "99" ]
				variableRateStimulus [ name = "while" ]
			]
			softwareModel [
				task [ name = "_BlaBla" ]
				task [ name = "BLA_BLA" ]
				task [ name = "not good" ]
			]
		]

		executor.validate(model)

		val messages = executor.results.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(Collectors.toList)
		assertTrue(messages.size == 3)
		assertTrue(messages.contains("Task \"not good\": Invalid name (not a valid C++ identifier)"))
		assertTrue(messages.contains("Periodic Stimulus \"99\": Invalid name (not a valid C++ identifier)"))
		assertTrue(messages.contains("Variable Rate Stimulus \"while\": Invalid name (not a valid C++ identifier)"))
	}
}
