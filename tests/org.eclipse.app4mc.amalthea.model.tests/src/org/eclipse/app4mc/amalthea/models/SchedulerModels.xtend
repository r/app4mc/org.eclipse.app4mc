/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.models

import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.junit.Test
import org.eclipse.app4mc.amalthea.model.StructureType
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition
import org.eclipse.app4mc.amalthea.model.ParameterType
import org.eclipse.app4mc.amalthea.model.ISR
import org.eclipse.app4mc.amalthea.model.InterruptController
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amalthea.model.TimeUnit
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition

class SchedulerModels {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension HardwareBuilder b2 = new HardwareBuilder
	extension SoftwareBuilder b3 = new SoftwareBuilder
	extension OperatingSystemBuilder b5 = new OperatingSystemBuilder
	extension MappingBuilder b6 = new MappingBuilder
	
	@Test
	def void model1() {
		amalthea [
			hardwareModel [
				structure [
					name = "MC1"
					structureType = StructureType::MICROCONTROLLER
					module_ProcessingUnit [name = "core1"]
					module_ProcessingUnit [name = "core2"]
					module_ProcessingUnit [name = "core3"]
				]
			]
			softwareModel [
				task [name = "t1"]
				task [name = "t2"]
				task [name = "t3"]
				isr [name = "isr1"]
				isr [name = "isr2"]
				isr [name = "isr3"]
			]
			osModel [
				parameterDefinition [
					name = "hyperPeriod"
					type = ParameterType::TIME
				]
				parameterDefinition [
						name = "timeStamps"
						many = true
						type = ParameterType::TIME
				]
				parameterDefinition [
					name = "priority"
					type = ParameterType::INTEGER
				]
				parameterDefinition [
					name = "preemptible"
					type = ParameterType::BOOL
					defaultValue = FactoryUtil.createBooleanObject(false)
				]
				parameterDefinition [
					name = "taskGroup"
					type = ParameterType::INTEGER
				]
				schedulerDefinition [
					name = "TDMA"
					algorithmParameters += _find(SchedulingParameterDefinition, "hyperPeriod")
					processParameters += _find(SchedulingParameterDefinition, "timeStamps")
				]
				schedulerDefinition [
					name = "OSEK"
					processParameters += #[
						_find(SchedulingParameterDefinition, "priority"),
						_find(SchedulingParameterDefinition, "preemptible"),
						_find(SchedulingParameterDefinition, "taskGroup")
					]
				]
				schedulerDefinition [
					name = "FixedPriorityPreemptive"
					processParameters += #[
						_find(SchedulingParameterDefinition, "priority"),
						_find(SchedulingParameterDefinition, "preemptible")
					]
				]
				schedulerDefinition [
					name = "PriorityBased"
					processParameters += _find(SchedulingParameterDefinition, "priority")
				]
				operatingSystem [
					name = "Os1"
					taskScheduler [
						name = "sched1"
						definition = _find(SchedulerDefinition, "OSEK")
					]
					taskScheduler [
						name = "sched2"
						definition = _find(SchedulerDefinition, "FixedPriorityPreemptive")
					]
					taskScheduler [
						name = "sched3"
						definition = _find(SchedulerDefinition, "TDMA")
						schedulingParameter("hyperPeriod", FactoryUtil.createTime(5, TimeUnit.MS))
					]
					interruptController [
						name = "iCtrl1"
						definition = _find(SchedulerDefinition, "PriorityBased")
					]
					interruptController [
						name = "iCtrl2"
						definition = _find(SchedulerDefinition, "PriorityBased")
					]
				]
			]
			mappingModel [
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched2")
					responsibility += _find(ProcessingUnit, "core2")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched1")
					responsibility += _find(ProcessingUnit, "core1")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched3")
					responsibility += _find(ProcessingUnit, "core3")
				]
				taskAllocation [
					task = _find(Task, "t1")
					scheduler = _find(TaskScheduler, "sched1")
					schedulingParameter("priority", FactoryUtil.createIntegerObject(3))
				]
				taskAllocation [
					task = _find(Task, "t2")
					scheduler = _find(TaskScheduler, "sched2")
					schedulingParameter("priority", FactoryUtil.createIntegerObject(40))
					schedulingParameter("preemptible", FactoryUtil.createBooleanObject(false))
				]
				taskAllocation [
					task = _find(Task, "t3")
					scheduler = _find(TaskScheduler, "sched3")
					schedulingParameter("timeStamps", FactoryUtil.createListObject(#[
						FactoryUtil.createTime(0, TimeUnit.MS),
						FactoryUtil.createTime(2, TimeUnit.MS)
					]))
				]
				
				isrAllocation [
					isr = _find(ISR, "isr1")
					controller = _find(InterruptController, "iCtrl1")
				]
				schedulerAllocation [
					scheduler = _find(InterruptController, "iCtrl1")
					responsibility += _find(ProcessingUnit, "core1")
					executingPU = _find(ProcessingUnit, "core1")
				]
				isrAllocation [
					isr = _find(ISR, "isr2")
					controller = _find(InterruptController, "iCtrl2")
				]
				schedulerAllocation [
					scheduler = _find(InterruptController, "iCtrl2")
					responsibility += _find(ProcessingUnit, "core2")
				]
			]
		]
	}
	
}