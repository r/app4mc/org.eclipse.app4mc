/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.tests

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage
import org.eclipse.app4mc.amalthea.model.INamed
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition
import org.junit.Test

import static org.junit.Assert.assertTrue
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers
import org.eclipse.app4mc.amalthea.model.predefined.AmaltheaTemplates

class StandardSchedulerTests {
	
	@Test
	def void testStandardSchedulerCreation() {
		StandardSchedulers.Algorithm.values.forEach[
			val osModel = AmaltheaFactory.eINSTANCE.createOSModel
			val schedDef = AmaltheaTemplates.addStandardSchedulerDefinition(osModel, it)
			assertTrue(schedDef !== null)
			assertTrue(schedDef.eContainer === osModel)
			assertTrue(schedDef.name == it.algorithmName)
		]
	}
	
	@Test
	def void testStandardSchedulerCreationAlwaysNewCopy() {
		val checkSet = <INamed>newHashSet
		for(var i = 0; i < 10; i++) {
			StandardSchedulers.Algorithm.values.forEach[
				val osModel = AmaltheaFactory.eINSTANCE.createOSModel
				val schedDef = AmaltheaTemplates.addStandardSchedulerDefinition(osModel, it)
				assertTrue(checkSet += schedDef)
				schedDef.processParameters.forEach[assertTrue(checkSet += it)]
				schedDef.algorithmParameters.forEach[assertTrue(checkSet += it)]
			]
		}
	}
	
	@Test
	def void testAddStandardSchedulerToOSModelNamesUnique() {
		val osm = AmaltheaFactory.eINSTANCE.createOSModel
		for(var i = 0; i < 3; i++) StandardSchedulers.Algorithm.values.forEach[AmaltheaTemplates.addStandardSchedulerDefinition(osm, it)]
		// test: scheduler definition names are unique
		val checkSchedSet = newHashSet
		osm.schedulerDefinitions.map[name].forEach[assertTrue(checkSchedSet += it)]
		// test: parameter definition names are unique
		val checkParamSet = newHashSet
		osm.schedulingParameterDefinitions.map[name].forEach[assertTrue(checkParamSet += it)]
	}
	
	@Test
	def void testAddStandardSchedulerToOSModelCorrectReferences() {
		val osm = AmaltheaFactory.eINSTANCE.createOSModel
		StandardSchedulers.Algorithm.values.forEach[AmaltheaTemplates.addStandardSchedulerDefinition(osm, it)]
		// test: scheduler definitions only refer to parameters contained in osm
		osm.schedulerDefinitions.forEach[
			it.algorithmParameters.forEach[assertTrue(it.eContainer === osm)]
			it.processParameters.forEach[assertTrue(it.eContainer === osm)]
		]
		// test: parameter definitions are only referred by scheduler definitions that are contained in osm
		osm.schedulingParameterDefinitions.forEach[
			val algRefs = AmaltheaIndex.getReferringObjects(it, SchedulerDefinition, AmaltheaPackage.eINSTANCE.schedulerDefinition_AlgorithmParameters)
			algRefs.forEach[assertTrue(it.eContainer === osm)]
			val proRefs = AmaltheaIndex.getReferringObjects(it, SchedulerDefinition, AmaltheaPackage.eINSTANCE.schedulerDefinition_ProcessParameters)
			proRefs.forEach[assertTrue(it.eContainer === osm)]
		]
	}
}