/**
 ********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util.reports;

import java.io.PrintStream;
import java.util.List;

import org.apache.commons.math3.distribution.WeibullDistribution;
import org.eclipse.app4mc.amalthea.model.util.WeibullUtil;
import org.eclipse.app4mc.amalthea.model.util.WeibullUtil.Parameters;

public class WeibullUtilsReports {

	public static void main(String[] args) {
		WeibullUtilsReports test = new WeibullUtilsReports();

		test.printParameterEstimation1(System.out);
		test.printParameterEstimation2(System.out);
	}

	private List<List<Double>> testData_min_avg_max_pRemainPromille() {
		return List.of(
			List.of( 12.0, 18.0, 34.0, 0.05),
			List.of(-12.0, 18.0, 20.5, 0.05),
			List.of( 0.05, 10.0, 20.5, 1.0),
			List.of( 0.05, 10.0, 20.5, 125.0),
			List.of( 12.0, 14.0, 34.0, 0.5),
			List.of( 12.0, 14.0, 34.0, 1.0),
			List.of( 12.0, 14.0, 34.0, 5.0),
			List.of( 12.0, 14.0, 34.0, 50.0),
			List.of( 12.0, 14.0, 34.0, 100.0),
			List.of( 12.0, 14.0, 34.0, 500.0)
		);
	}

	public void printParameterEstimation1(PrintStream out) {
		List<List<Double>> testData;

		testData = testData_min_avg_max_pRemainPromille();

		out.println("\n\n***** Testing parameter estimation (based on the average) *****");

		for (List<Double> list : testData) {
			Double min = list.get(0);
			Double avg = list.get(1);
			Double max = list.get(2);
			Double rem = list.get(3);

			out.println("\nInput: [" + min + ", " + avg + ", " + max + "], " + rem);

			Parameters result = WeibullUtil.findParametersForAverage(min, avg, max, rem);
			out.println(result);

			double shape = result.shape;
			double scale = result.scale;

			double avg2 = WeibullUtil.computeAverage(shape, scale, min, max);
			double rem2 = WeibullUtil.computePRemainPromille(shape, scale, min, max);
			out.println(" computed: average = " + avg2);
			out.println(" computed: pRemainPromille = " + rem2);

			// check with Apache Commons Math
			final WeibullDistribution mathFunction = new WeibullDistribution(null, shape, scale);

			double cdfAvg = mathFunction.cumulativeProbability(avg - min);
			double cdfMax = mathFunction.cumulativeProbability(max - min);
			double rem3 = (1.0 - cdfMax) * 1000.0;
			out.println("    check: pRemainPromille = " + rem3);
			out.println("    check: ratio (expected: 0.5) = " + (cdfAvg / cdfMax));
		}
	}

	public void printParameterEstimation2(PrintStream out) {
		List<List<Double>> testData;

		testData = testData_min_avg_max_pRemainPromille();

		out.println("\n\n***** Testing parameter estimation (based on the median) *****");

		for (List<Double> list : testData) {
			Double min = list.get(0);
			Double med = list.get(1);
			Double max = list.get(2);
			Double rem = list.get(3);

			out.println("\nInput: [" + min + ", " + med + ", " + max + "], " + rem);

			Parameters result = WeibullUtil.findParametersForMedian(min, med, max, rem);
			out.println(result);

			double shape = result.shape;
			double scale = result.scale;

			double avg2 = WeibullUtil.computeAverage(shape, scale, min, max);
			double rem2 = WeibullUtil.computePRemainPromille(shape, scale, min, max);
			out.println(" computed: average = " + avg2);
			out.println(" computed: pRemainPromille = " + rem2);

			// check with Apache Commons Math
			final WeibullDistribution mathFunction = new WeibullDistribution(null, shape, scale);

			double cdfAvg = mathFunction.cumulativeProbability(med - min);
			double cdfMax = mathFunction.cumulativeProbability(max - min);
			double rem3 = (1.0 - cdfMax) * 1000.0;
			out.println("    check: pRemainPromille = " + rem3);
			out.println("    check: ratio (expected: 0.5) = " + (cdfAvg / cdfMax));
		}
	}

}
