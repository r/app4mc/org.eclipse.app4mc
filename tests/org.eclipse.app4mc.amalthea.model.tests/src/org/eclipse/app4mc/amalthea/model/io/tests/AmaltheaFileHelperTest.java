/**
 ********************************************************************************
 * Copyright (c) 2020, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.io.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.eclipse.app4mc.amalthea.model.io.AmaltheaFileHelper;
import org.junit.Test;

public class AmaltheaFileHelperTest {

	public static final File MODEL_FILE = new File("./test-data/model.amxmi");
	public static final File ZIP_FILE = new File("./test-data/model_zipped.amxmi");


	@Test
	public void shouldGetNamespaceFromModelFile() {
		String namespace = AmaltheaFileHelper.getNamespace(MODEL_FILE);
		assertNotNull(namespace);
		assertEquals("http://app4mc.eclipse.org/amalthea/0.9.9", namespace);
	}

	@Test
	public void shouldGetNamespaceFromZIPFile() {
		String namespace = AmaltheaFileHelper.getNamespace(ZIP_FILE);
		assertNotNull(namespace);
		assertEquals("http://app4mc.eclipse.org/amalthea/0.9.6", namespace);
	}

	@Test
	public void shouldGetInvalidNamespaceFromNull() {
		String namespace = AmaltheaFileHelper.getNamespace(null);
		assertNotNull(namespace);
		assertEquals(AmaltheaFileHelper.INVALID, namespace);
	}

	@Test
	public void shouldGetVersionFromModelFile() {
		String version = AmaltheaFileHelper.getModelVersion(MODEL_FILE);
		assertNotNull(version);
		assertEquals("0.9.9", version);
	}

	@Test
	public void shouldGetVersionFromZIPFile() {
		String version = AmaltheaFileHelper.getModelVersion(ZIP_FILE);
		assertNotNull(version);
		assertEquals("0.9.6", version);
	}

	@Test
	public void shouldGetInvalidVersionFromNull() {
		String version = AmaltheaFileHelper.getModelVersion(null);
		assertNotNull(version);
		assertEquals(AmaltheaFileHelper.INVALID, version);
	}

	@Test
	public void shouldFindInvalidModelVersion() {
		assertFalse(AmaltheaFileHelper.isModelVersionValid(MODEL_FILE));
		assertFalse(AmaltheaFileHelper.isModelVersionValid(ZIP_FILE));
	}

	@Test
	public void shouldFindValidModelVersion() {
		assertTrue(AmaltheaFileHelper.isModelVersionValid(new File("./test-data/HwUtilTestModel-gen.amxmi")));
	}

}
