/**
 ********************************************************************************
 * Copyright (c) 2019-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util.tests;

import static org.junit.Assert.assertNotNull;

import java.util.Set;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.util.DeploymentUtil;
import org.eclipse.app4mc.amalthea.models.DeploymentModels;
import org.junit.Before;
import org.junit.Test;

public class DeploymentUtilsTest {

	Amalthea amaltheaModel;
	Task t1 = null;
	Task t2 = null;
	Task t3 = null;
	Task t4 = null;
	Task t5 = null;
	Task t6 = null;
	Task t7 = null;
	ISR isr1 = null;
	ISR isr2 = null;
	ISR isr3 = null;
	ProcessingUnit core1 = null;
	ProcessingUnit core2 = null;
	ProcessingUnit core3 = null;
	TaskAllocation taT1 = null;
	TaskAllocation taT2 = null;
	TaskAllocation taT3 = null;
	TaskAllocation taT4 = null;
	TaskAllocation taT5 = null;
	TaskAllocation taT6 = null;
	TaskAllocation taT7 = null;

	@Before
	public void initalizeModel() {
		amaltheaModel = DeploymentModels.createModel1();

		Set<ProcessingUnit> coreCollector = AmaltheaIndex.getElements(amaltheaModel.getHwModel(), "core1", ProcessingUnit.class);
		assertNotNull("Core1 not found ", coreCollector.iterator().next());
		core1 = coreCollector.iterator().next();

		coreCollector = AmaltheaIndex.getElements(amaltheaModel.getHwModel(), "core2", ProcessingUnit.class);
		assertNotNull("Core2 not found ", coreCollector.iterator().next());
		core2 = coreCollector.iterator().next();

		coreCollector = AmaltheaIndex.getElements(amaltheaModel.getHwModel(), "core3", ProcessingUnit.class);
		assertNotNull("Core3 not found ", coreCollector.iterator().next());
		core3 = coreCollector.iterator().next();

		Set<Task> taskCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "t1", Task.class);
		t1 = taskCollector.iterator().next();
		assertNotNull("T1 not found", taskCollector.iterator().next());

		taskCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "t2", Task.class);
		t2 = taskCollector.iterator().next();
		assertNotNull("T2 not found", taskCollector.iterator().next());

		taskCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "t3", Task.class);
		t3 = taskCollector.iterator().next();
		assertNotNull("T3 not found", taskCollector.iterator().next());

		taskCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "t4", Task.class);
		t4 = taskCollector.iterator().next();
		assertNotNull("T4 not found", taskCollector.iterator().next());

		taskCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "t5", Task.class);
		t5 = taskCollector.iterator().next();
		assertNotNull("T5 not found", taskCollector.iterator().next());

		taskCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "t6", Task.class);
		t6 = taskCollector.iterator().next();
		assertNotNull("T6 not found", taskCollector.iterator().next());

		taskCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "t7", Task.class);
		t7 = taskCollector.iterator().next();
		assertNotNull("T7 not found", taskCollector.iterator().next());

		Set<ISR> isrCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "isr1", ISR.class);
		isr1 = isrCollector.iterator().next();
		assertNotNull("ISR1 not found", isrCollector.iterator().next());

		isrCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "isr2", ISR.class);
		isr2 = isrCollector.iterator().next();
		assertNotNull("ISR2 not found", isrCollector.iterator().next());

		isrCollector = AmaltheaIndex.getElements(amaltheaModel.getSwModel(), "isr3", ISR.class);
		isr3 = isrCollector.iterator().next();
		assertNotNull("ISR3 not found", isrCollector.iterator().next());

	}

	@Test
	public void testModelExistance() {

		SWModel swModel = amaltheaModel.getSwModel();
		assertNotNull("SW model is not loaded", swModel);

		HWModel hwModel = amaltheaModel.getHwModel();
		assertNotNull("HW model is not loaded", hwModel);

		OSModel osModel = amaltheaModel.getOsModel();
		assertNotNull("OS model is not loaded", osModel);

		MappingModel MappingModel = amaltheaModel.getMappingModel();
		assertNotNull("Mapping model is not loaded", MappingModel);

	}

	@Test
	public void testTaskMapping() {

		Set<Task> tmp = DeploymentUtil.getTasksMappedToCore(core1, amaltheaModel);
		assert(tmp.size() == 2);
		assert(tmp.contains(t1));
		assert(tmp.contains(t3));

		tmp = DeploymentUtil.getTasksMappedToCore(core2, amaltheaModel);
		assert(tmp.size() == 3);
		assert(tmp.contains(t2));
//		assert(tmp.contains(t4));
		assert(tmp.contains(t5));
		assert(tmp.contains(t6));

		tmp = DeploymentUtil.getTasksMappedToCore(core3, amaltheaModel);
		assertNotNull("Empty mapping list is null", tmp);
		assert(tmp.isEmpty());
	}


	@Test
	public void testProcessesMappedToCore() {
		Set<Process> tmp = DeploymentUtil.getProcessesMappedToCore(core1, amaltheaModel);
		assert(tmp.size() == 4);
		assert(tmp.contains(t1));
		assert(tmp.contains(t3));
		assert(tmp.contains(isr1));
		assert(tmp.contains(isr3));

		tmp = DeploymentUtil.getProcessesMappedToCore(core2, amaltheaModel);
		assert(tmp.size() == 4);
		assert(tmp.contains(t2));
//		assert(tmp.contains(t4));
		assert(tmp.contains(t5));
		assert(tmp.contains(t6));
		assert(tmp.contains(isr2));

		tmp = DeploymentUtil.getProcessesMappedToCore(core3, amaltheaModel);
		assertNotNull("Empty mapping list is null", tmp);
		assert(tmp.isEmpty());

	}

	@Test
	public void testISRsMappedToCore() {
		Set<ISR> tmp = DeploymentUtil.getISRsMappedToCore(core1, amaltheaModel);
		assert(tmp.size() == 2);
		assert(tmp.contains(isr1));
		assert(tmp.contains(isr3));

		tmp = DeploymentUtil.getISRsMappedToCore(core2, amaltheaModel);
		assert(tmp.size() == 1);
		assert(tmp.contains(isr2));

		tmp = DeploymentUtil.getISRsMappedToCore(core3, amaltheaModel);
		assertNotNull("Empty mapping list is null", tmp);
		assert(tmp.isEmpty());

	}


	@Test
	public void testAssignedCoreForProcess() {
		Set<ProcessingUnit> tmp = DeploymentUtil.getAssignedCoreForProcess(t1);
		assert(tmp.size() == 1);
		assert(tmp.contains(core1));

		tmp = DeploymentUtil.getAssignedCoreForProcess(t3);
		assert(tmp.size() == 1);
		assert(tmp.contains(core1));


		tmp = DeploymentUtil.getAssignedCoreForProcess(isr1);
		assert(tmp.size() == 1);
		assert(tmp.contains(core1));

		tmp = DeploymentUtil.getAssignedCoreForProcess(isr3);
		assert(tmp.size() == 1);
		assert(tmp.contains(core1));

		tmp = DeploymentUtil.getAssignedCoreForProcess(t2);
		assert(tmp.size() == 1);
		assert(tmp.contains(core2));

		tmp = DeploymentUtil.getAssignedCoreForProcess(t4);
		assertNotNull("Nullpointer instead of empty list", tmp);
		assert(tmp.isEmpty());

		tmp = DeploymentUtil.getAssignedCoreForProcess(t5);
		assert(tmp.size() == 1);
		assert(tmp.contains(core2));

		tmp = DeploymentUtil.getAssignedCoreForProcess(t6);
		assert(tmp.size() == 1);
		assert(tmp.contains(core2));

		tmp = DeploymentUtil.getAssignedCoreForProcess(isr2);
		assert(tmp.size() == 1);
		assert(tmp.contains(core2));

		tmp = DeploymentUtil.getAssignedCoreForProcess(t7);
		assertNotNull("Nullpointer instead of empty list", tmp);
		assert(tmp.isEmpty());
	}

}
