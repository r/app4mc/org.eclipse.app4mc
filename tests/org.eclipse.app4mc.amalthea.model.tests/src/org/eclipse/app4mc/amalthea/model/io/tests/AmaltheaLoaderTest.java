/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.io.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.junit.Test;

public class AmaltheaLoaderTest {

	public static final String MODEL_FILE = "test-data/split-model/democar-hw.amxmi";
	public static final String MODEL_FOLDER = "test-data/split-model/";

	@Test
	public void loadModelFromFile() {
		Amalthea model = AmaltheaLoader.loadFromFileNamed(MODEL_FILE);
		assertNotNull(model);
	}

	@Test
	public void loadModelFromFolder() {
		ResourceSet resourceSet = AmaltheaLoader.loadFromDirectoryNamed(MODEL_FOLDER);
		assertNotNull(resourceSet);
		assertEquals(7, resourceSet.getResources().size());
	}

}
