/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.tests;

import static org.junit.Assert.assertEquals;

import org.eclipse.app4mc.amalthea.model.AmaltheaExtensions;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Component;
import org.eclipse.app4mc.amalthea.model.ComponentStructure;
import org.eclipse.app4mc.amalthea.model.ComponentsModel;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class NameTest {

	private static final String DOT = ".";
	private static final String I_NAMED_GET_QUALIFIED_NAME = "INamed getQualifiedName()";
	private static final String I_REFERABLE_GET_UNIQUE_NAME = "IReferable getUniqueName()";
	private static final String AMALTHEA_EXTENSIONS_TO_PLAIN_STRING = "AmaltheaExtensions.toPlainString()";

	AmaltheaFactory fac = AmaltheaFactory.eINSTANCE;

	@Rule
	public ExpectedException thrown = ExpectedException.none(); // initially, expect no exception

	@Test
	@SuppressWarnings("java:S2637") // passing null to a @NonNull parameter is intended
	public void testArguments1() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Argument is null, expected: EList");

		AmaltheaExtensions.toPlainString(null, DOT);
	}

	@Test
	public void testArguments2() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("List argument contains null");

		BasicEList<String> list = new BasicEList<>();
		list.add("one");
		list.add(null);
		AmaltheaExtensions.toPlainString(list, DOT);
	}

	@Test
	public void testStringOperationsPlain() {
		EList<String> segments = new BasicEList<>();

		assertEquals(AMALTHEA_EXTENSIONS_TO_PLAIN_STRING, "", AmaltheaExtensions.toPlainString(segments, DOT));

		segments.add("one");
		assertEquals(AMALTHEA_EXTENSIONS_TO_PLAIN_STRING, "one", AmaltheaExtensions.toPlainString(segments, DOT));

		segments.add("two");
		assertEquals(AMALTHEA_EXTENSIONS_TO_PLAIN_STRING, "one.two", AmaltheaExtensions.toPlainString(segments, DOT));

		segments.add("three");
		assertEquals(AMALTHEA_EXTENSIONS_TO_PLAIN_STRING, "one.two.three", AmaltheaExtensions.toPlainString(segments, DOT));
	}

	@Test
	public void testStringOperationsEncoded() {
		EList<String> segments = new BasicEList<>();

		assertEquals(AMALTHEA_EXTENSIONS_TO_PLAIN_STRING, "", AmaltheaExtensions.toPlainString(segments, DOT));

		segments.add("one");
		assertEquals(AMALTHEA_EXTENSIONS_TO_PLAIN_STRING, "one", AmaltheaExtensions.toPlainString(segments, DOT));

		segments.add("two");
		assertEquals(AMALTHEA_EXTENSIONS_TO_PLAIN_STRING, "one.two", AmaltheaExtensions.toPlainString(segments, DOT));

		segments.add("three");
		assertEquals(AMALTHEA_EXTENSIONS_TO_PLAIN_STRING, "one.two.three", AmaltheaExtensions.toPlainString(segments, DOT));
	}

	@Test
	public void testUniqueName() {
		SWModel swModel = fac.createSWModel();

		Label lab1 = addLabel(swModel, "Label 1");
		Runnable run1 = addRunnable(swModel, "Run 1");
		Runnable run2 = addRunnable(swModel, "Run 2");

		RunnableCall call = fac.createRunnableCall();
		call.setRunnable(run2);
		run1.getRunnableItems().add(call);

		LabelAccess access = fac.createLabelAccess();
		access.setData(lab1);
		run1.getRunnableItems().add(access);

		assertEquals(I_REFERABLE_GET_UNIQUE_NAME, "Run+1?type=Runnable", run1.getUniqueName());
		assertEquals(I_REFERABLE_GET_UNIQUE_NAME, "Label+1?type=Label", lab1.getUniqueName());

		ComponentsModel compModel = fac.createComponentsModel();
		ComponentStructure struct1 = fac.createComponentStructure();
		struct1.setName("S1");
		compModel.getStructures().add(struct1);

		ComponentStructure struct2 = addSubStructure(struct1, "S2", false);
		ComponentStructure struct3 = addSubStructure(struct2, "S3", true);

		Component comp = fac.createComponent();
		comp.setName("CCC");
		comp.setStructure(struct3);

		assertEquals(I_REFERABLE_GET_UNIQUE_NAME, "CCC?type=Component", comp.getUniqueName());
	}

	private ComponentStructure addSubStructure(ComponentStructure parent, String name, boolean prefix) {
		ComponentStructure struct = fac.createComponentStructure();
		struct.setName(name);
		parent.getSubStructures().add(struct);
		return struct;
	}

	private Label addLabel(SWModel model, String name) {
		Label lab = fac.createLabel();
		model.getLabels().add(lab);
		lab.setName(name);
		return lab;
	}

	private Runnable addRunnable(SWModel model, String name) {
		Runnable run = fac.createRunnable();
		model.getRunnables().add(run);
		run.setName(name);
		return run;
	}

	@Test
	public void testStructure() {
		ComponentsModel model = fac.createComponentsModel();

		ComponentStructure cs1 = addStructure(model,"Top", true);
		ComponentStructure cs2 = addStructure(cs1,"High", false);
		ComponentStructure cs3 = addStructure(cs2,"", true);
		ComponentStructure cs4 = addStructure(cs3,"Medium", true);
		ComponentStructure cs5 = addStructure(cs4,"Bottom", true);

		assertEquals(I_NAMED_GET_QUALIFIED_NAME, "Top", cs1.getQualifiedName());
		assertEquals(I_NAMED_GET_QUALIFIED_NAME, "Top::High", cs2.getQualifiedName());
		assertEquals(I_NAMED_GET_QUALIFIED_NAME, "Top::High::", cs3.getQualifiedName());
		assertEquals(I_NAMED_GET_QUALIFIED_NAME, "Top::High::::Medium", cs4.getQualifiedName());
		assertEquals(I_NAMED_GET_QUALIFIED_NAME, "Top::High::::Medium::Bottom", cs5.getQualifiedName());
	}

	private ComponentStructure addStructure(ComponentsModel model, String name, boolean isPrefix) {
		ComponentStructure cs = fac.createComponentStructure();
		model.getStructures().add(cs);
		cs.setName(name);
		return cs;
	}

	private ComponentStructure addStructure(ComponentStructure struct, String name, boolean isPrefix) {
		ComponentStructure cs = fac.createComponentStructure();
		struct.getSubStructures().add(cs);
		cs.setName(name);
		return cs;
	}

}
