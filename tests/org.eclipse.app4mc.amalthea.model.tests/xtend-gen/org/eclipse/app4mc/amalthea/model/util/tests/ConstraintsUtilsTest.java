/**
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 * Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.model.util.tests;

import java.util.List;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.AbstractEventChain;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.EventChain;
import org.eclipse.app4mc.amalthea.model.SubEventChain;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.ConstraintsBuilder;
import org.eclipse.app4mc.amalthea.model.util.ConstraintsUtil;
import org.eclipse.app4mc.amalthea.models.ConstraintsModels;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("all")
public class ConstraintsUtilsTest {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private ConstraintsBuilder b3 = new ConstraintsBuilder();

  private Amalthea model;

  private org.eclipse.app4mc.amalthea.model.Runnable run1;

  private org.eclipse.app4mc.amalthea.model.Runnable run2;

  private Task task1;

  @Before
  public void initalizeModel() {
    this.model = ConstraintsModels.createModel1();
    this.run1 = IterableExtensions.<org.eclipse.app4mc.amalthea.model.Runnable>head(AmaltheaIndex.<org.eclipse.app4mc.amalthea.model.Runnable>getElements(this.model, "Run1", org.eclipse.app4mc.amalthea.model.Runnable.class));
    this.run2 = IterableExtensions.<org.eclipse.app4mc.amalthea.model.Runnable>head(AmaltheaIndex.<org.eclipse.app4mc.amalthea.model.Runnable>getElements(this.model, "Run2", org.eclipse.app4mc.amalthea.model.Runnable.class));
    this.task1 = IterableExtensions.<Task>head(AmaltheaIndex.<Task>getElements(this.model, "Task1", Task.class));
  }

  @Test
  public void testRunnableDeadline() {
    final Time deadline1 = ConstraintsUtil.getDeadline(this.run1);
    Assert.assertEquals(
      "testRunnableDeadline: null expected", null, deadline1);
    final Time deadline2 = ConstraintsUtil.getDeadline(this.run2, this.model.getConstraintsModel());
    Assert.assertEquals(
      "testRunnableDeadline: 80 ns expected", 80, deadline2.getValue().intValue());
    Assert.assertEquals(
      "testRunnableDeadline: 80 ns expected", TimeUnit.NS, deadline2.getUnit());
  }

  @Test
  public void testProcessDeadline() {
    final Time deadline3 = ConstraintsUtil.getDeadline(this.task1);
    Assert.assertEquals(
      "testProcessDeadline: 20 ms expected", 20, deadline3.getValue().intValue());
    Assert.assertEquals(
      "testProcessDeadline: 20 ms expected", TimeUnit.MS, deadline3.getUnit());
    final Time deadline4 = ConstraintsUtil.getDeadline(this.task1, this.model.getConstraintsModel());
    Assert.assertEquals(
      "testProcessDeadline: same result expected", deadline3, deadline4);
  }

  @Test
  public void testAllAbstractEventChainGetter_Flat() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<ConstraintsModel> _function_1 = (ConstraintsModel it_1) -> {
        final Consumer<EventChain> _function_2 = (EventChain it_2) -> {
          it_2.setName("ec1");
        };
        this.b3.eventChain(it_1, _function_2);
        final Consumer<EventChain> _function_3 = (EventChain it_2) -> {
          it_2.setName("ec2");
        };
        this.b3.eventChain(it_1, _function_3);
        final Consumer<EventChain> _function_4 = (EventChain it_2) -> {
          it_2.setName("ec3");
        };
        this.b3.eventChain(it_1, _function_4);
      };
      this.b1.constraintsModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<AbstractEventChain> allECs = ConstraintsUtil.getAllAbstractEventChains(model.getConstraintsModel());
    Assert.assertTrue("testAllAbstractEventChainGetter_Flat: expected ec1, ec2, and ec3", 
      ((((allECs.size() == 3) && 
        (allECs.get(0) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec1", AbstractEventChain.class)))) && 
        (allECs.get(1) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec2", AbstractEventChain.class)))) && 
        (allECs.get(2) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec3", AbstractEventChain.class)))));
  }

  @Test
  public void testAllAbstractEventChainGetter_NestedRef() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<ConstraintsModel> _function_1 = (ConstraintsModel it_1) -> {
        final Consumer<EventChain> _function_2 = (EventChain it_2) -> {
          it_2.setName("ec1");
        };
        this.b3.eventChain(it_1, _function_2);
        final Consumer<EventChain> _function_3 = (EventChain it_2) -> {
          it_2.setName("ec2");
          this.b3.subchain_ref(it_2, this.b1.<EventChain>_find(it_2, EventChain.class, "ec1"));
        };
        this.b3.eventChain(it_1, _function_3);
        final Consumer<EventChain> _function_4 = (EventChain it_2) -> {
          it_2.setName("ec3");
          this.b3.subchain_ref(it_2, this.b1.<EventChain>_find(it_2, EventChain.class, "ec2"));
        };
        this.b3.eventChain(it_1, _function_4);
      };
      this.b1.constraintsModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<AbstractEventChain> allECs = ConstraintsUtil.getAllAbstractEventChains(model.getConstraintsModel());
    Assert.assertTrue("testAllAbstractEventChainGetter_NestedRef: expected ec1, ec2, and ec3", 
      ((((allECs.size() == 3) && 
        (allECs.get(0) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec1", AbstractEventChain.class)))) && 
        (allECs.get(1) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec2", AbstractEventChain.class)))) && 
        (allECs.get(2) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec3", AbstractEventChain.class)))));
  }

  @Test
  public void testAllAbstractEventChainGetter_Nested() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<ConstraintsModel> _function_1 = (ConstraintsModel it_1) -> {
        final Consumer<EventChain> _function_2 = (EventChain it_2) -> {
          it_2.setName("ec1");
          final Consumer<SubEventChain> _function_3 = (SubEventChain it_3) -> {
            it_3.setName("ec11");
          };
          this.b3.subchain(it_2, _function_3);
        };
        this.b3.eventChain(it_1, _function_2);
        final Consumer<EventChain> _function_3 = (EventChain it_2) -> {
          it_2.setName("ec2");
          final Consumer<SubEventChain> _function_4 = (SubEventChain it_3) -> {
            it_3.setName("ec21");
            final Consumer<SubEventChain> _function_5 = (SubEventChain it_4) -> {
              it_4.setName("ec211");
            };
            this.b3.subchain(it_3, _function_5);
          };
          this.b3.subchain(it_2, _function_4);
        };
        this.b3.eventChain(it_1, _function_3);
        final Consumer<EventChain> _function_4 = (EventChain it_2) -> {
          it_2.setName("ec3");
          final Consumer<SubEventChain> _function_5 = (SubEventChain it_3) -> {
            it_3.setName("ec31");
            final Consumer<SubEventChain> _function_6 = (SubEventChain it_4) -> {
              it_4.setName("ec311");
              final Consumer<SubEventChain> _function_7 = (SubEventChain it_5) -> {
                it_5.setName("ec3111");
              };
              this.b3.subchain(it_4, _function_7);
            };
            this.b3.subchain(it_3, _function_6);
          };
          this.b3.subchain(it_2, _function_5);
        };
        this.b3.eventChain(it_1, _function_4);
      };
      this.b1.constraintsModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<AbstractEventChain> allECs = ConstraintsUtil.getAllAbstractEventChains(model.getConstraintsModel());
    Assert.assertTrue("testAllAbstractEventChainGetter_Nested: expected ec1, ec2, ec3, ec11, ec21, ec31, ec211, ec311, and ec3111", 
      ((((((((((allECs.size() == 9) && 
        (allECs.get(0) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec1", AbstractEventChain.class)))) && 
        (allECs.get(1) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec2", AbstractEventChain.class)))) && 
        (allECs.get(2) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec3", AbstractEventChain.class)))) && 
        (allECs.get(3) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec11", AbstractEventChain.class)))) && 
        (allECs.get(4) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec21", AbstractEventChain.class)))) && 
        (allECs.get(5) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec31", AbstractEventChain.class)))) && 
        (allECs.get(6) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec211", AbstractEventChain.class)))) && 
        (allECs.get(7) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec311", AbstractEventChain.class)))) && 
        (allECs.get(8) == IterableExtensions.<AbstractEventChain>head(AmaltheaIndex.<AbstractEventChain>getElements(model, "ec3111", AbstractEventChain.class)))));
  }
}
