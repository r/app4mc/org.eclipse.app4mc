/**
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.model.tests;

import com.google.common.base.Objects;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.predefined.AmaltheaTemplates;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class StandardSchedulerTests {
  @Test
  public void testStandardSchedulerCreation() {
    final Consumer<StandardSchedulers.Algorithm> _function = (StandardSchedulers.Algorithm it) -> {
      final OSModel osModel = AmaltheaFactory.eINSTANCE.createOSModel();
      final SchedulerDefinition schedDef = AmaltheaTemplates.addStandardSchedulerDefinition(osModel, it);
      Assert.assertTrue((schedDef != null));
      EObject _eContainer = schedDef.eContainer();
      boolean _tripleEquals = (_eContainer == osModel);
      Assert.assertTrue(_tripleEquals);
      String _name = schedDef.getName();
      String _algorithmName = it.getAlgorithmName();
      boolean _equals = Objects.equal(_name, _algorithmName);
      Assert.assertTrue(_equals);
    };
    ((List<StandardSchedulers.Algorithm>)Conversions.doWrapArray(StandardSchedulers.Algorithm.values())).forEach(_function);
  }

  @Test
  public void testStandardSchedulerCreationAlwaysNewCopy() {
    final HashSet<INamed> checkSet = CollectionLiterals.<INamed>newHashSet();
    for (int i = 0; (i < 10); i++) {
      final Consumer<StandardSchedulers.Algorithm> _function = (StandardSchedulers.Algorithm it) -> {
        final OSModel osModel = AmaltheaFactory.eINSTANCE.createOSModel();
        final SchedulerDefinition schedDef = AmaltheaTemplates.addStandardSchedulerDefinition(osModel, it);
        boolean _add = checkSet.add(schedDef);
        Assert.assertTrue(_add);
        final Consumer<SchedulingParameterDefinition> _function_1 = (SchedulingParameterDefinition it_1) -> {
          boolean _add_1 = checkSet.add(it_1);
          Assert.assertTrue(_add_1);
        };
        schedDef.getProcessParameters().forEach(_function_1);
        final Consumer<SchedulingParameterDefinition> _function_2 = (SchedulingParameterDefinition it_1) -> {
          boolean _add_1 = checkSet.add(it_1);
          Assert.assertTrue(_add_1);
        };
        schedDef.getAlgorithmParameters().forEach(_function_2);
      };
      ((List<StandardSchedulers.Algorithm>)Conversions.doWrapArray(StandardSchedulers.Algorithm.values())).forEach(_function);
    }
  }

  @Test
  public void testAddStandardSchedulerToOSModelNamesUnique() {
    final OSModel osm = AmaltheaFactory.eINSTANCE.createOSModel();
    for (int i = 0; (i < 3); i++) {
      final Consumer<StandardSchedulers.Algorithm> _function = (StandardSchedulers.Algorithm it) -> {
        AmaltheaTemplates.addStandardSchedulerDefinition(osm, it);
      };
      ((List<StandardSchedulers.Algorithm>)Conversions.doWrapArray(StandardSchedulers.Algorithm.values())).forEach(_function);
    }
    final HashSet<String> checkSchedSet = CollectionLiterals.<String>newHashSet();
    final Function1<SchedulerDefinition, String> _function = (SchedulerDefinition it) -> {
      return it.getName();
    };
    final Consumer<String> _function_1 = (String it) -> {
      boolean _add = checkSchedSet.add(it);
      Assert.assertTrue(_add);
    };
    ListExtensions.<SchedulerDefinition, String>map(osm.getSchedulerDefinitions(), _function).forEach(_function_1);
    final HashSet<String> checkParamSet = CollectionLiterals.<String>newHashSet();
    final Function1<SchedulingParameterDefinition, String> _function_2 = (SchedulingParameterDefinition it) -> {
      return it.getName();
    };
    final Consumer<String> _function_3 = (String it) -> {
      boolean _add = checkParamSet.add(it);
      Assert.assertTrue(_add);
    };
    ListExtensions.<SchedulingParameterDefinition, String>map(osm.getSchedulingParameterDefinitions(), _function_2).forEach(_function_3);
  }

  @Test
  public void testAddStandardSchedulerToOSModelCorrectReferences() {
    final OSModel osm = AmaltheaFactory.eINSTANCE.createOSModel();
    final Consumer<StandardSchedulers.Algorithm> _function = (StandardSchedulers.Algorithm it) -> {
      AmaltheaTemplates.addStandardSchedulerDefinition(osm, it);
    };
    ((List<StandardSchedulers.Algorithm>)Conversions.doWrapArray(StandardSchedulers.Algorithm.values())).forEach(_function);
    final Consumer<SchedulerDefinition> _function_1 = (SchedulerDefinition it) -> {
      final Consumer<SchedulingParameterDefinition> _function_2 = (SchedulingParameterDefinition it_1) -> {
        EObject _eContainer = it_1.eContainer();
        boolean _tripleEquals = (_eContainer == osm);
        Assert.assertTrue(_tripleEquals);
      };
      it.getAlgorithmParameters().forEach(_function_2);
      final Consumer<SchedulingParameterDefinition> _function_3 = (SchedulingParameterDefinition it_1) -> {
        EObject _eContainer = it_1.eContainer();
        boolean _tripleEquals = (_eContainer == osm);
        Assert.assertTrue(_tripleEquals);
      };
      it.getProcessParameters().forEach(_function_3);
    };
    osm.getSchedulerDefinitions().forEach(_function_1);
    final Consumer<SchedulingParameterDefinition> _function_2 = (SchedulingParameterDefinition it) -> {
      final Set<SchedulerDefinition> algRefs = AmaltheaIndex.<SchedulerDefinition>getReferringObjects(it, SchedulerDefinition.class, AmaltheaPackage.eINSTANCE.getSchedulerDefinition_AlgorithmParameters());
      final Consumer<SchedulerDefinition> _function_3 = (SchedulerDefinition it_1) -> {
        EObject _eContainer = it_1.eContainer();
        boolean _tripleEquals = (_eContainer == osm);
        Assert.assertTrue(_tripleEquals);
      };
      algRefs.forEach(_function_3);
      final Set<SchedulerDefinition> proRefs = AmaltheaIndex.<SchedulerDefinition>getReferringObjects(it, SchedulerDefinition.class, AmaltheaPackage.eINSTANCE.getSchedulerDefinition_ProcessParameters());
      final Consumer<SchedulerDefinition> _function_4 = (SchedulerDefinition it_1) -> {
        EObject _eContainer = it_1.eContainer();
        boolean _tripleEquals = (_eContainer == osm);
        Assert.assertTrue(_tripleEquals);
      };
      proRefs.forEach(_function_4);
    };
    osm.getSchedulingParameterDefinitions().forEach(_function_2);
  }
}
