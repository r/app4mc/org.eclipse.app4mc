/**
 * Copyright (c) 2019 Robert Bosch GmbH and others.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 * Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.model.util.tests;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.LabelAccessStatistic;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.app4mc.amalthea.models.SoftwareModels;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

@SuppressWarnings("all")
public class SoftwareUtilsTest {
  private Amalthea model;

  private org.eclipse.app4mc.amalthea.model.Runnable run1;

  private org.eclipse.app4mc.amalthea.model.Runnable run4;

  @Before
  public void initalizeModel() {
    this.model = SoftwareModels.createModel1();
    this.run1 = IterableExtensions.<org.eclipse.app4mc.amalthea.model.Runnable>head(AmaltheaIndex.<org.eclipse.app4mc.amalthea.model.Runnable>getElements(this.model, "Run1", org.eclipse.app4mc.amalthea.model.Runnable.class));
    this.run4 = IterableExtensions.<org.eclipse.app4mc.amalthea.model.Runnable>head(AmaltheaIndex.<org.eclipse.app4mc.amalthea.model.Runnable>getElements(this.model, "Run4", org.eclipse.app4mc.amalthea.model.Runnable.class));
  }

  @Test
  public void testCollectActivityGraphItems() {
    final EList<ActivityGraphItem> list1 = SoftwareUtil.collectActivityGraphItems(this.run1.getActivityGraph());
    Assert.assertEquals(
      "collectRunnableItems: 6 items expected", 6, list1.size());
    final EList<Ticks> list2 = SoftwareUtil.<Ticks>collectActivityGraphItems(this.run1.getActivityGraph(), null, Ticks.class);
    Assert.assertEquals(
      "collectRunnableItems: 2 items (ticks) expected", 2, list2.size());
    final Function<LabelAccess, Boolean> _function = (LabelAccess i) -> {
      LabelAccessEnum _access = i.getAccess();
      return Boolean.valueOf((_access == LabelAccessEnum.READ));
    };
    final EList<LabelAccess> list3 = SoftwareUtil.<LabelAccess>collectActivityGraphItems(this.run1.getActivityGraph(), null, LabelAccess.class, _function);
    Assert.assertEquals(
      "collectRunnableItems: 2 items (label accesses) expected", 2, list3.size());
    final EList<ActivityGraphItem> list4 = SoftwareUtil.collectActivityGraphItems(this.run4.getActivityGraph());
    Assert.assertEquals(
      "collectRunnableItems: 1 item expected", 1, list4.size());
  }

  @Test
  public void testLabelAccessMethods() {
    final Amalthea model = SoftwareModels.createModel2();
    final org.eclipse.app4mc.amalthea.model.Runnable run1 = IterableExtensions.<org.eclipse.app4mc.amalthea.model.Runnable>head(AmaltheaIndex.<org.eclipse.app4mc.amalthea.model.Runnable>getElements(model, "Run1", org.eclipse.app4mc.amalthea.model.Runnable.class));
    final Set<Label> set1 = SoftwareUtil.getAccessedLabelSet(run1, null);
    Assert.assertEquals(
      "getAccessedLabelSet: 5 items (label accesses) expected", 5, set1.size());
    final Set<Label> set2 = SoftwareUtil.getReadLabelSet(run1, null);
    Assert.assertEquals(
      "getReadLabelSet: 4 items (label accesses) expected", 4, set2.size());
    final Set<Label> set3 = SoftwareUtil.getWriteLabelSet(run1, null);
    Assert.assertEquals(
      "getWriteLabelSet: 3 items (label accesses) expected", 3, set3.size());
    final List<LabelAccess> list1 = SoftwareUtil.getLabelAccessList(run1, null);
    Assert.assertEquals(
      "getLabelAccessList: 10 items (label accesses) expected", 10, list1.size());
    final List<LabelAccess> list2 = SoftwareUtil.getReadLabelAccessList(run1, null);
    Assert.assertEquals(
      "getReadLabelAccessList: 5 items (label accesses) expected", 5, list2.size());
    final List<LabelAccess> list3 = SoftwareUtil.getWriteLabelAccessList(run1, null);
    Assert.assertEquals(
      "getWriteLabelAccessList: 3 items (label accesses) expected", 3, list3.size());
    final Map<Label, List<LabelAccess>> map1 = SoftwareUtil.getLabelToLabelAccessMap(run1, null);
    Assert.assertEquals(
      "getAccessedLabelSet: 5 items (label -> label accesses) expected", 5, map1.size());
    final Map<Label, List<LabelAccessStatistic>> map2 = SoftwareUtil.getLabelAccessStatisticsMap(run1, null);
    Assert.assertEquals(
      "getAccessedLabelSet: 3 items (label -> access statistics) expected", 3, map2.size());
  }

  @Test
  public void testActivityItemsWithModes() {
    final Amalthea model = SoftwareModels.createModel3();
    final ModeLabel ml1 = IterableExtensions.<ModeLabel>head(AmaltheaIndex.<ModeLabel>getElements(model, "car-state-previous", ModeLabel.class));
    final ModeLabel ml2 = IterableExtensions.<ModeLabel>head(AmaltheaIndex.<ModeLabel>getElements(model, "car-state", ModeLabel.class));
    final org.eclipse.app4mc.amalthea.model.Runnable run1 = IterableExtensions.<org.eclipse.app4mc.amalthea.model.Runnable>head(AmaltheaIndex.<org.eclipse.app4mc.amalthea.model.Runnable>getElements(model, "Run1", org.eclipse.app4mc.amalthea.model.Runnable.class));
    final BasicEMap<ModeLabel, String> modes = new BasicEMap<ModeLabel, String>();
    modes.put(ml1, "pre-drive");
    modes.put(ml2, "post-drive");
    final List<org.eclipse.app4mc.amalthea.model.Runnable> list1 = SoftwareUtil.getCalledRunnables(run1, modes);
    Assert.assertEquals(
      "getCalledRunnables: 2 runnables (Run1, Run3) expected", 2, list1.size());
    final List<LabelAccess> list2 = SoftwareUtil.getReadLabelAccessList(run1, modes);
    Assert.assertEquals(
      "getReadLabelAccessList: 2 label accesses (Lab1, Lab2) expected", 2, list2.size());
    final List<LabelAccess> list3 = SoftwareUtil.getWriteLabelAccessList(run1, modes);
    Assert.assertEquals(
      "getWriteLabelAccessList: 2 label accesses (Lab3, Lab4) expected", 2, list3.size());
    modes.put(ml1, "pre-drive");
    modes.put(ml2, "pre-drive");
    final List<org.eclipse.app4mc.amalthea.model.Runnable> list4 = SoftwareUtil.getCalledRunnables(run1, modes);
    Assert.assertEquals(
      "getCalledRunnables: empty list expected", 0, list4.size());
    final List<LabelAccess> list5 = SoftwareUtil.getReadLabelAccessList(run1, modes);
    Assert.assertEquals(
      "getReadLabelAccessList: 1 label access (Lab5) expected", 1, list5.size());
    final List<LabelAccess> list6 = SoftwareUtil.getWriteLabelAccessList(run1, modes);
    Assert.assertEquals(
      "getWriteLabelAccessList: 1 label access (Lab5) expected", 1, list6.size());
    modes.put(ml1, "post-drive");
    modes.put(ml2, "post-drive");
    final List<org.eclipse.app4mc.amalthea.model.Runnable> list7 = SoftwareUtil.getCalledRunnables(run1, modes);
    Assert.assertEquals(
      "getCalledRunnables: 2 runnables (Run2, Run4) expected", 2, list7.size());
  }
}
