/**
 * Copyright (c) 2018-2019 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.models;

import java.util.function.Consumer;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ConditionConjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunction;
import org.eclipse.app4mc.amalthea.model.EnumMode;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLiteral;
import org.eclipse.app4mc.amalthea.model.RelationalOperator;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Switch;
import org.eclipse.app4mc.amalthea.model.SwitchDefault;
import org.eclipse.app4mc.amalthea.model.SwitchEntry;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaWriter;
import org.eclipse.xtext.xbase.lib.Extension;

@SuppressWarnings("all")
public class SoftwareModels {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private SoftwareBuilder b2 = new SoftwareBuilder();

  public static void main(final String[] args) {
    final SoftwareModels swModels = new SoftwareModels();
    final Amalthea model1 = swModels.model1();
    AmaltheaWriter.writeToFileNamed(model1, "test-data/SoftwareUtilTestModel1-gen.amxmi");
    final Amalthea model2 = swModels.model2();
    AmaltheaWriter.writeToFileNamed(model2, "test-data/SoftwareUtilTestModel2-gen.amxmi");
    final Amalthea model3 = swModels.model3();
    AmaltheaWriter.writeToFileNamed(model3, "test-data/SoftwareUtilTestModel3-gen.amxmi");
  }

  public static Amalthea createModel1() {
    return new SoftwareModels().model1();
  }

  public static Amalthea createModel2() {
    return new SoftwareModels().model2();
  }

  public static Amalthea createModel3() {
    return new SoftwareModels().model3();
  }

  public Amalthea model1() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<EnumMode> _function_2 = (EnumMode it_2) -> {
          it_2.setName("state");
          final Consumer<ModeLiteral> _function_3 = (ModeLiteral it_3) -> {
            it_3.setName("pre-drive");
          };
          this.b2.literal(it_2, _function_3);
          final Consumer<ModeLiteral> _function_4 = (ModeLiteral it_3) -> {
            it_3.setName("drive");
          };
          this.b2.literal(it_2, _function_4);
          final Consumer<ModeLiteral> _function_5 = (ModeLiteral it_3) -> {
            it_3.setName("post-drive");
          };
          this.b2.literal(it_2, _function_5);
        };
        this.b2.mode_Enum(it_1, _function_2);
        final Consumer<ModeLabel> _function_3 = (ModeLabel it_2) -> {
          it_2.setName("car-state");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "state"));
          it_2.setInitialValue("pre-drive");
        };
        this.b2.modeLabel(it_1, _function_3);
        final Consumer<Label> _function_4 = (Label it_2) -> {
          it_2.setName("Lab1");
        };
        this.b2.label(it_1, _function_4);
        final Consumer<Label> _function_5 = (Label it_2) -> {
          it_2.setName("Lab2");
        };
        this.b2.label(it_1, _function_5);
        final Consumer<Label> _function_6 = (Label it_2) -> {
          it_2.setName("Lab3");
        };
        this.b2.label(it_1, _function_6);
        final Consumer<Label> _function_7 = (Label it_2) -> {
          it_2.setName("Lab4");
        };
        this.b2.label(it_1, _function_7);
        final Consumer<Label> _function_8 = (Label it_2) -> {
          it_2.setName("Lab5");
        };
        this.b2.label(it_1, _function_8);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_9 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run1");
          final Consumer<ActivityGraph> _function_10 = (ActivityGraph it_3) -> {
            final Consumer<Ticks> _function_11 = (Ticks it_4) -> {
              this.b2.defaultConstant(it_4, 200);
            };
            this.b2.ticks(it_3, _function_11);
            final Consumer<Switch> _function_12 = (Switch it_4) -> {
              final Consumer<SwitchDefault> _function_13 = (SwitchDefault it_5) -> {
                final Consumer<LabelAccess> _function_14 = (LabelAccess it_6) -> {
                  it_6.setAccess(LabelAccessEnum.READ);
                  it_6.setData(this.b1.<Label>_find(it_6, Label.class, "Lab1"));
                };
                this.b2.labelAccess(it_5, _function_14);
                final Consumer<LabelAccess> _function_15 = (LabelAccess it_6) -> {
                  it_6.setAccess(LabelAccessEnum.READ);
                  it_6.setData(this.b1.<Label>_find(it_6, Label.class, "Lab2"));
                };
                this.b2.labelAccess(it_5, _function_15);
                final Consumer<Ticks> _function_16 = (Ticks it_6) -> {
                  this.b2.defaultConstant(it_6, 333);
                };
                this.b2.ticks(it_5, _function_16);
                final Consumer<LabelAccess> _function_17 = (LabelAccess it_6) -> {
                  it_6.setAccess(LabelAccessEnum.WRITE);
                  it_6.setData(this.b1.<Label>_find(it_6, Label.class, "Lab3"));
                };
                this.b2.labelAccess(it_5, _function_17);
                final Consumer<LabelAccess> _function_18 = (LabelAccess it_6) -> {
                  it_6.setAccess(LabelAccessEnum.WRITE);
                  it_6.setData(this.b1.<Label>_find(it_6, Label.class, "Lab4"));
                };
                this.b2.labelAccess(it_5, _function_18);
              };
              this.b2.defaultEntry(it_4, _function_13);
            };
            this.b2.conditionalSwitch(it_3, _function_12);
          };
          this.b2.activityGraph(it_2, _function_10);
        };
        this.b2.runnable(it_1, _function_9);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_10 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run2");
          final Consumer<ActivityGraph> _function_11 = (ActivityGraph it_3) -> {
            final Consumer<Ticks> _function_12 = (Ticks it_4) -> {
              this.b2.defaultConstant(it_4, 400);
            };
            this.b2.ticks(it_3, _function_12);
            final Consumer<Ticks> _function_13 = (Ticks it_4) -> {
              this.b2.defaultConstant(it_4, 40);
            };
            this.b2.ticks(it_3, _function_13);
            final Consumer<Ticks> _function_14 = (Ticks it_4) -> {
              this.b2.defaultConstant(it_4, 4);
            };
            this.b2.ticks(it_3, _function_14);
            final Consumer<RunnableCall> _function_15 = (RunnableCall it_4) -> {
              it_4.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run4"));
            };
            this.b2.runnableCall(it_3, _function_15);
            final Consumer<LabelAccess> _function_16 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.WRITE);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab5"));
            };
            this.b2.labelAccess(it_3, _function_16);
          };
          this.b2.activityGraph(it_2, _function_11);
        };
        this.b2.runnable(it_1, _function_10);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_11 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run3");
          final Consumer<ActivityGraph> _function_12 = (ActivityGraph it_3) -> {
            final Consumer<LabelAccess> _function_13 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.READ);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab5"));
            };
            this.b2.labelAccess(it_3, _function_13);
            final Consumer<Ticks> _function_14 = (Ticks it_4) -> {
              this.b2.defaultConstant(it_4, 600);
            };
            this.b2.ticks(it_3, _function_14);
          };
          this.b2.activityGraph(it_2, _function_12);
        };
        this.b2.runnable(it_1, _function_11);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_12 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run4");
          final Consumer<ActivityGraph> _function_13 = (ActivityGraph it_3) -> {
            final Consumer<Ticks> _function_14 = (Ticks it_4) -> {
              this.b2.defaultConstant(it_4, 700);
            };
            this.b2.ticks(it_3, _function_14);
          };
          this.b2.activityGraph(it_2, _function_13);
        };
        this.b2.runnable(it_1, _function_12);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    return model;
  }

  public Amalthea model2() {
    final AmaltheaFactory fac = AmaltheaFactory.eINSTANCE;
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<Label> _function_2 = (Label it_2) -> {
          it_2.setName("Lab1");
        };
        this.b2.label(it_1, _function_2);
        final Consumer<Label> _function_3 = (Label it_2) -> {
          it_2.setName("Lab2");
        };
        this.b2.label(it_1, _function_3);
        final Consumer<Label> _function_4 = (Label it_2) -> {
          it_2.setName("Lab3");
        };
        this.b2.label(it_1, _function_4);
        final Consumer<Label> _function_5 = (Label it_2) -> {
          it_2.setName("Lab4");
        };
        this.b2.label(it_1, _function_5);
        final Consumer<Label> _function_6 = (Label it_2) -> {
          it_2.setName("Lab5");
        };
        this.b2.label(it_1, _function_6);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_7 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run1");
          final Consumer<ActivityGraph> _function_8 = (ActivityGraph it_3) -> {
            final Consumer<Ticks> _function_9 = (Ticks it_4) -> {
              this.b2.defaultConstant(it_4, 200);
            };
            this.b2.ticks(it_3, _function_9);
            final Consumer<LabelAccess> _function_10 = (LabelAccess it_4) -> {
              it_4.setStatistic(fac.createLabelAccessStatistic());
            };
            this.b2.labelAccess(it_3, _function_10);
            final Consumer<LabelAccess> _function_11 = (LabelAccess it_4) -> {
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab1"));
              it_4.setStatistic(fac.createLabelAccessStatistic());
            };
            this.b2.labelAccess(it_3, _function_11);
            final Consumer<LabelAccess> _function_12 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.READ);
              it_4.setStatistic(fac.createLabelAccessStatistic());
            };
            this.b2.labelAccess(it_3, _function_12);
            final Consumer<LabelAccess> _function_13 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.READ);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab1"));
              it_4.setStatistic(fac.createLabelAccessStatistic());
            };
            this.b2.labelAccess(it_3, _function_13);
            final Consumer<LabelAccess> _function_14 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.READ);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab2"));
            };
            this.b2.labelAccess(it_3, _function_14);
            final Consumer<LabelAccess> _function_15 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.READ);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab3"));
            };
            this.b2.labelAccess(it_3, _function_15);
            final Consumer<LabelAccess> _function_16 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.READ);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab4"));
              it_4.setStatistic(fac.createLabelAccessStatistic());
            };
            this.b2.labelAccess(it_3, _function_16);
            final Consumer<Ticks> _function_17 = (Ticks it_4) -> {
              this.b2.defaultConstant(it_4, 333);
            };
            this.b2.ticks(it_3, _function_17);
            final Consumer<LabelAccess> _function_18 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.WRITE);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab3"));
            };
            this.b2.labelAccess(it_3, _function_18);
            final Consumer<LabelAccess> _function_19 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.WRITE);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab4"));
            };
            this.b2.labelAccess(it_3, _function_19);
            final Consumer<LabelAccess> _function_20 = (LabelAccess it_4) -> {
              it_4.setAccess(LabelAccessEnum.WRITE);
              it_4.setData(this.b1.<Label>_find(it_4, Label.class, "Lab5"));
              it_4.setStatistic(fac.createLabelAccessStatistic());
            };
            this.b2.labelAccess(it_3, _function_20);
          };
          this.b2.activityGraph(it_2, _function_8);
        };
        this.b2.runnable(it_1, _function_7);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    return model;
  }

  public Amalthea model3() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<EnumMode> _function_2 = (EnumMode it_2) -> {
          it_2.setName("state");
          final Consumer<ModeLiteral> _function_3 = (ModeLiteral it_3) -> {
            it_3.setName("pre-drive");
          };
          this.b2.literal(it_2, _function_3);
          final Consumer<ModeLiteral> _function_4 = (ModeLiteral it_3) -> {
            it_3.setName("drive");
          };
          this.b2.literal(it_2, _function_4);
          final Consumer<ModeLiteral> _function_5 = (ModeLiteral it_3) -> {
            it_3.setName("post-drive");
          };
          this.b2.literal(it_2, _function_5);
        };
        this.b2.mode_Enum(it_1, _function_2);
        final Consumer<ModeLabel> _function_3 = (ModeLabel it_2) -> {
          it_2.setName("car-state");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "state"));
          it_2.setInitialValue("pre-drive");
        };
        this.b2.modeLabel(it_1, _function_3);
        final Consumer<ModeLabel> _function_4 = (ModeLabel it_2) -> {
          it_2.setName("car-state-previous");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "state"));
          it_2.setInitialValue("pre-drive");
        };
        this.b2.modeLabel(it_1, _function_4);
        final Consumer<Label> _function_5 = (Label it_2) -> {
          it_2.setName("Lab1");
        };
        this.b2.label(it_1, _function_5);
        final Consumer<Label> _function_6 = (Label it_2) -> {
          it_2.setName("Lab2");
        };
        this.b2.label(it_1, _function_6);
        final Consumer<Label> _function_7 = (Label it_2) -> {
          it_2.setName("Lab3");
        };
        this.b2.label(it_1, _function_7);
        final Consumer<Label> _function_8 = (Label it_2) -> {
          it_2.setName("Lab4");
        };
        this.b2.label(it_1, _function_8);
        final Consumer<Label> _function_9 = (Label it_2) -> {
          it_2.setName("Lab5");
        };
        this.b2.label(it_1, _function_9);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_10 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run1");
        };
        this.b2.runnable(it_1, _function_10);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_11 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run2");
        };
        this.b2.runnable(it_1, _function_11);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_12 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run3");
        };
        this.b2.runnable(it_1, _function_12);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_13 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Run4");
        };
        this.b2.runnable(it_1, _function_13);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final org.eclipse.app4mc.amalthea.model.Runnable r1 = this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(model, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run1");
    final Consumer<ActivityGraph> _function_1 = (ActivityGraph it) -> {
      final Consumer<Ticks> _function_2 = (Ticks it_1) -> {
        this.b2.defaultConstant(it_1, 200);
      };
      this.b2.ticks(it, _function_2);
      final Consumer<Switch> _function_3 = (Switch it_1) -> {
        final Consumer<SwitchEntry> _function_4 = (SwitchEntry it_2) -> {
          final Consumer<ConditionDisjunction> _function_5 = (ConditionDisjunction it_3) -> {
            final Consumer<ConditionConjunction> _function_6 = (ConditionConjunction it_4) -> {
              this.b2.condition(it_4, this.b1.<ModeLabel>_find(it_4, ModeLabel.class, "car-state-previous"), RelationalOperator.EQUAL, "pre-drive");
              this.b2.condition(it_4, this.b1.<ModeLabel>_find(it_4, ModeLabel.class, "car-state"), RelationalOperator.EQUAL, "drive");
            };
            this.b2.condition_AND(it_3, _function_6);
          };
          this.b2.condition_OR(it_2, _function_5);
          final Consumer<LabelAccess> _function_6 = (LabelAccess it_3) -> {
            it_3.setAccess(LabelAccessEnum.READ);
            it_3.setData(this.b1.<Label>_find(it_3, Label.class, "Lab4"));
          };
          this.b2.labelAccess(it_2, _function_6);
          final Consumer<LabelAccess> _function_7 = (LabelAccess it_3) -> {
            it_3.setAccess(LabelAccessEnum.WRITE);
            it_3.setData(this.b1.<Label>_find(it_3, Label.class, "Lab1"));
          };
          this.b2.labelAccess(it_2, _function_7);
        };
        this.b2.entry(it_1, _function_4);
        final Consumer<SwitchEntry> _function_5 = (SwitchEntry it_2) -> {
          final Consumer<ConditionDisjunction> _function_6 = (ConditionDisjunction it_3) -> {
            this.b2.condition(it_3, this.b1.<ModeLabel>_find(it_3, ModeLabel.class, "car-state"), RelationalOperator.EQUAL, "pre-drive");
            this.b2.condition(it_3, this.b1.<ModeLabel>_find(it_3, ModeLabel.class, "car-state"), RelationalOperator.EQUAL, "drive");
          };
          this.b2.condition_OR(it_2, _function_6);
          final Consumer<LabelAccess> _function_7 = (LabelAccess it_3) -> {
            it_3.setAccess(LabelAccessEnum.READ);
            it_3.setData(this.b1.<Label>_find(it_3, Label.class, "Lab5"));
          };
          this.b2.labelAccess(it_2, _function_7);
          final Consumer<LabelAccess> _function_8 = (LabelAccess it_3) -> {
            it_3.setAccess(LabelAccessEnum.WRITE);
            it_3.setData(this.b1.<Label>_find(it_3, Label.class, "Lab5"));
          };
          this.b2.labelAccess(it_2, _function_8);
        };
        this.b2.entry(it_1, _function_5);
        final Consumer<SwitchEntry> _function_6 = (SwitchEntry it_2) -> {
          final Consumer<ConditionDisjunction> _function_7 = (ConditionDisjunction it_3) -> {
            this.b2.condition(it_3, this.b1.<ModeLabel>_find(it_3, ModeLabel.class, "car-state-previous"), RelationalOperator.EQUAL, this.b1.<ModeLabel>_find(it_3, ModeLabel.class, "car-state"));
          };
          this.b2.condition_OR(it_2, _function_7);
          final Consumer<RunnableCall> _function_8 = (RunnableCall it_3) -> {
            it_3.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_3, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run2"));
          };
          this.b2.runnableCall(it_2, _function_8);
          final Consumer<RunnableCall> _function_9 = (RunnableCall it_3) -> {
            it_3.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_3, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run4"));
          };
          this.b2.runnableCall(it_2, _function_9);
        };
        this.b2.entry(it_1, _function_6);
        final Consumer<SwitchDefault> _function_7 = (SwitchDefault it_2) -> {
          final Consumer<LabelAccess> _function_8 = (LabelAccess it_3) -> {
            it_3.setAccess(LabelAccessEnum.READ);
            it_3.setData(this.b1.<Label>_find(it_3, Label.class, "Lab1"));
          };
          this.b2.labelAccess(it_2, _function_8);
          final Consumer<LabelAccess> _function_9 = (LabelAccess it_3) -> {
            it_3.setAccess(LabelAccessEnum.READ);
            it_3.setData(this.b1.<Label>_find(it_3, Label.class, "Lab2"));
          };
          this.b2.labelAccess(it_2, _function_9);
          final Consumer<Ticks> _function_10 = (Ticks it_3) -> {
            this.b2.defaultConstant(it_3, 333);
          };
          this.b2.ticks(it_2, _function_10);
          final Consumer<RunnableCall> _function_11 = (RunnableCall it_3) -> {
            it_3.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_3, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run1"));
          };
          this.b2.runnableCall(it_2, _function_11);
          final Consumer<RunnableCall> _function_12 = (RunnableCall it_3) -> {
            it_3.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_3, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run3"));
          };
          this.b2.runnableCall(it_2, _function_12);
          final Consumer<LabelAccess> _function_13 = (LabelAccess it_3) -> {
            it_3.setAccess(LabelAccessEnum.WRITE);
            it_3.setData(this.b1.<Label>_find(it_3, Label.class, "Lab3"));
          };
          this.b2.labelAccess(it_2, _function_13);
          final Consumer<LabelAccess> _function_14 = (LabelAccess it_3) -> {
            it_3.setAccess(LabelAccessEnum.WRITE);
            it_3.setData(this.b1.<Label>_find(it_3, Label.class, "Lab4"));
          };
          this.b2.labelAccess(it_2, _function_14);
        };
        this.b2.defaultEntry(it_1, _function_7);
      };
      this.b2.conditionalSwitch(it, _function_3);
    };
    this.b2.activityGraph(r1, _function_1);
    final org.eclipse.app4mc.amalthea.model.Runnable r2 = this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(model, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run2");
    final Consumer<ActivityGraph> _function_2 = (ActivityGraph it) -> {
      final Consumer<Ticks> _function_3 = (Ticks it_1) -> {
        this.b2.defaultConstant(it_1, 400);
      };
      this.b2.ticks(it, _function_3);
      final Consumer<Ticks> _function_4 = (Ticks it_1) -> {
        this.b2.defaultConstant(it_1, 40);
      };
      this.b2.ticks(it, _function_4);
      final Consumer<Ticks> _function_5 = (Ticks it_1) -> {
        this.b2.defaultConstant(it_1, 4);
      };
      this.b2.ticks(it, _function_5);
      final Consumer<RunnableCall> _function_6 = (RunnableCall it_1) -> {
        it_1.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_1, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run4"));
      };
      this.b2.runnableCall(it, _function_6);
      final Consumer<LabelAccess> _function_7 = (LabelAccess it_1) -> {
        it_1.setAccess(LabelAccessEnum.WRITE);
        it_1.setData(this.b1.<Label>_find(it_1, Label.class, "Lab5"));
      };
      this.b2.labelAccess(it, _function_7);
    };
    this.b2.activityGraph(r2, _function_2);
    final org.eclipse.app4mc.amalthea.model.Runnable r3 = this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(model, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run3");
    final Consumer<ActivityGraph> _function_3 = (ActivityGraph it) -> {
      final Consumer<LabelAccess> _function_4 = (LabelAccess it_1) -> {
        it_1.setAccess(LabelAccessEnum.READ);
        it_1.setData(this.b1.<Label>_find(it_1, Label.class, "Lab5"));
      };
      this.b2.labelAccess(it, _function_4);
      final Consumer<Ticks> _function_5 = (Ticks it_1) -> {
        this.b2.defaultConstant(it_1, 600);
      };
      this.b2.ticks(it, _function_5);
    };
    this.b2.activityGraph(r3, _function_3);
    final org.eclipse.app4mc.amalthea.model.Runnable r4 = this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(model, org.eclipse.app4mc.amalthea.model.Runnable.class, "Run4");
    final Consumer<ActivityGraph> _function_4 = (ActivityGraph it) -> {
      final Consumer<Ticks> _function_5 = (Ticks it_1) -> {
        this.b2.defaultConstant(it_1, 700);
      };
      this.b2.ticks(it, _function_5);
    };
    this.b2.activityGraph(r4, _function_4);
    return model;
  }
}
