/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.app4mc.amalthea.validations.inchron.tests

import java.util.List
import java.util.stream.Collectors
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile
import org.eclipse.app4mc.validation.core.Severity
import org.eclipse.app4mc.validation.core.ValidationDiagnostic
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue

class InchronOSValidator {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension OperatingSystemBuilder b2 = new OperatingSystemBuilder
	extension HardwareBuilder b3 = new HardwareBuilder
	extension MappingBuilder b4 = new MappingBuilder
	extension SoftwareBuilder b5 = new SoftwareBuilder

	val executor = new ValidationExecutor(#[InchronProfile])

	def List<ValidationDiagnostic> runExecutor(Amalthea model) {
		executor.validate(model)
		executor.results
	}

	@Test
	def void test_OSToPUMapping() {

		val model = amalthea [
			hardwareModel [
				structure [
					name = "hw_struct_1"
					module_ProcessingUnit [name = "PU_1"]
					module_ProcessingUnit [name = "PU_2"]
				]
			]
			osModel [
				operatingSystem [name = "OS_1" taskScheduler [name = "sched_1"]]
				operatingSystem [name = "OS_2" taskScheduler [name = "sched_2"]]
				operatingSystem [name = "OS_3" taskScheduler [name = "sched_3"]]
			]
			mappingModel [
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched_1")
					responsibility += _find(ProcessingUnit, "PU_1")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched_2")
					responsibility += _find(ProcessingUnit, "PU_1")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched_3")
					responsibility += _find(ProcessingUnit, "PU_2")
				]
			]

		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)

		assertTrue(
			result.contains(
				"Operating system \"OS_1\" consists of task schedulers allocated to processing units: PU_1 referenced by other operating systems"))
		assertTrue(
			result.contains(
				"Operating system \"OS_2\" consists of task schedulers allocated to processing units: PU_1 referenced by other operating systems"))
		assertFalse(
			result.contains(
				"Operating system \"OS_3\" consists of task schedulers allocated to processing units: PU_1 referenced by other operating systems"))

	}

	@Test
	def void test_OSSchedulerAllocation() {

		val model = amalthea [
			hardwareModel [
				structure [
					name = "hw_struct_1"
					module_ProcessingUnit [name = "PU_1"]
				]
				structure [
					name = "hw_struct_2"
					module_ProcessingUnit [name = "PU_2"]
					module_ProcessingUnit [name = "PU_3"]
				]
			]
			osModel [
				operatingSystem [name = "OS_1" taskScheduler [name = "sched_1"]]
				operatingSystem [name = "OS_2" taskScheduler [name = "sched_2"]]
			]
			mappingModel [
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched_1")
					responsibility += _find(ProcessingUnit, "PU_1")
					responsibility += _find(ProcessingUnit, "PU_2")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched_2")
					responsibility += _find(ProcessingUnit, "PU_3")
				]
			]
		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)
		assertTrue(
			result.contains(
				"Task Scheduler \"sched_1\" allocated to more than one HwStructure: hw_struct_1,hw_struct_2"))
		assertFalse(
			result.contains(
				"Task Scheduler \"sched_2\" allocated to more than one HwStructure: hw_struct_1,hw_struct_2"))

	}

	@Test
	def void test_UserSpecificScheduler() {

		val model = amalthea [
			softwareModel [
				task [name = "Task_1"]
			]
			osModel [
				operatingSystem [
					name = "OS_1"
					taskScheduler [
						name = "sched_1"
// FIXME
//						schedulingAlgorithm = AmaltheaFactory.eINSTANCE.createUserSpecificSchedulingAlgorithm
					]
				]
				operatingSystem [
					name = "OS_2"
					taskScheduler [
						name = "sched_2"
// FIXME
//						schedulingAlgorithm = AmaltheaFactory.eINSTANCE.createUserSpecificSchedulingAlgorithm
					]
				]
			]
			mappingModel [
				taskAllocation [
					task = _find(Task, "Task_1")
					scheduler = _find(TaskScheduler, "sched_2")
				]
			]

		]
		val validationResult = runExecutor(model)
		val result = validationResult.stream.filter[it.severityLevel == Severity.ERROR].map[it.message].collect(
			Collectors.toList)

// FIXME
//		assertTrue(
//			result.contains(
//				"User Specific Task Scheduler Algorithm \"sched_1\" needs at least one task/runnable allocation"))
//		assertFalse(
//			result.contains(
//				"User Specific Task Scheduler Algorithm \"sched_2\" needs at least one task/runnable allocation"))
	}

}