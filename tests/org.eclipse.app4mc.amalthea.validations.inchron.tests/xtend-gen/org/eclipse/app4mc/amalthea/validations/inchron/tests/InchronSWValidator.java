/**
 * Copyright (c) 2020 INCHRON AG and others.
 * 
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     INCHRON AG - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.inchron.tests;

import com.google.common.base.Objects;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.EnforcedMigration;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.RunnableAllocation;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class InchronSWValidator {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private OperatingSystemBuilder b2 = new OperatingSystemBuilder();

  @Extension
  private MappingBuilder b4 = new MappingBuilder();

  @Extension
  private SoftwareBuilder b5 = new SoftwareBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(Collections.<Class<? extends IProfile>>unmodifiableList(CollectionLiterals.<Class<? extends IProfile>>newArrayList(InchronProfile.class)));

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  @Test
  public void test_SWTaskRunnable() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<OSModel> _function_1 = (OSModel it_1) -> {
        final Consumer<OperatingSystem> _function_2 = (OperatingSystem it_2) -> {
          it_2.setName("OS_1");
          final Consumer<TaskScheduler> _function_3 = (TaskScheduler it_3) -> {
            it_3.setName("sched_1");
          };
          this.b2.taskScheduler(it_2, _function_3);
        };
        this.b2.operatingSystem(it_1, _function_2);
        final Consumer<OperatingSystem> _function_3 = (OperatingSystem it_2) -> {
          it_2.setName("OS_2");
          final Consumer<TaskScheduler> _function_4 = (TaskScheduler it_3) -> {
            it_3.setName("sched_2");
          };
          this.b2.taskScheduler(it_2, _function_4);
        };
        this.b2.operatingSystem(it_1, _function_3);
      };
      this.b1.osModel(it, _function_1);
      final Consumer<SWModel> _function_2 = (SWModel it_1) -> {
        final Consumer<Task> _function_3 = (Task it_2) -> {
          it_2.setName("Task_1");
        };
        this.b5.task(it_1, _function_3);
        final Consumer<Task> _function_4 = (Task it_2) -> {
          it_2.setName("Task_2");
          final Consumer<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Consumer<EnforcedMigration> _function_6 = (EnforcedMigration it_4) -> {
              it_4.setResourceOwner(this.b1.<TaskScheduler>_find(it_4, TaskScheduler.class, "sched_1"));
            };
            this.b5.enforcedMigration(it_3, _function_6);
          };
          this.b5.activityGraph(it_2, _function_5);
        };
        this.b5.task(it_1, _function_4);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_5 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Runnable_1");
        };
        this.b5.runnable(it_1, _function_5);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_6 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("Runnable_2");
          final Consumer<ActivityGraph> _function_7 = (ActivityGraph it_3) -> {
          };
          this.b5.activityGraph(it_2, _function_7);
        };
        this.b5.runnable(it_1, _function_6);
      };
      this.b1.softwareModel(it, _function_2);
      final Consumer<MappingModel> _function_3 = (MappingModel it_1) -> {
        final Consumer<TaskAllocation> _function_4 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "Task_1"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_1"));
        };
        this.b4.taskAllocation(it_1, _function_4);
        final Consumer<TaskAllocation> _function_5 = (TaskAllocation it_2) -> {
          it_2.setTask(this.b1.<Task>_find(it_2, Task.class, "Task_1"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_2"));
        };
        this.b4.taskAllocation(it_1, _function_5);
        final Consumer<RunnableAllocation> _function_6 = (RunnableAllocation it_2) -> {
          it_2.setEntity(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_2, org.eclipse.app4mc.amalthea.model.Runnable.class, "Runnable_1"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_1"));
        };
        this.b4.runnableAllocation(it_1, _function_6);
        final Consumer<RunnableAllocation> _function_7 = (RunnableAllocation it_2) -> {
          it_2.setEntity(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_2, org.eclipse.app4mc.amalthea.model.Runnable.class, "Runnable_1"));
          it_2.setScheduler(this.b1.<TaskScheduler>_find(it_2, TaskScheduler.class, "sched_2"));
        };
        this.b4.runnableAllocation(it_1, _function_7);
      };
      this.b1.mappingModel(it, _function_3);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
    Assert.assertTrue(result.contains("Task \"Task_1\" has no ActivityGraph"));
    Assert.assertFalse(result.contains("Task \"Task_2\" has no ActivityGraph"));
    Assert.assertTrue(result.contains("Runnable \"Runnable_1\" has no ActivityGraph"));
    Assert.assertFalse(result.contains("Runnable \"Runnable_2\" has no ActivityGraph"));
    Assert.assertTrue(result.contains("Task \"Task_1\" is scheduled by more than one operating system: OS_1,OS_2"));
    Assert.assertFalse(result.contains("Task \"Task_2\" is scheduled by more than one operating system: OS_1,OS_2"));
    Assert.assertTrue(result.contains("Runnable \"Runnable_1\" is scheduled by more than one operating system: OS_1,OS_2"));
    Assert.assertFalse(
      result.contains("Runnable \"Runnable_2\" is scheduled by more than one operating system: OS_1,OS_2"));
    Assert.assertTrue(
      result.contains(
        "Enforced Migration of task \"Task_2\" to Task Scheduler: \"sched_1\" that is not part of the list of schedulers allocated to the task"));
    Assert.assertFalse(
      result.contains(
        "Enforced Migration of task \"Task_1\" to Task Scheduler: \"sched_1\" that is not part of the list of schedulers allocated to the task"));
    Assert.assertTrue(result.contains("RunnableAllocation of runnable \"Runnable_1\" is not supported"));
    Assert.assertFalse(result.contains("RunnableAllocation of runnable \"Runnable_2\" is not supported"));
  }
}
