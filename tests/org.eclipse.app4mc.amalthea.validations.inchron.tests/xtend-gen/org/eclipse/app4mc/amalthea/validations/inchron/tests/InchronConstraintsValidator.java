/**
 * Copyright (c) 2020 INCHRON AG and others.
 * 
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     INCHRON AG - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.inchron.tests;

import com.google.common.base.Objects;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.CPUPercentageRequirementLimit;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.ProcessRequirement;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.Requirement;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.ConstraintsBuilder;
import org.eclipse.app4mc.amalthea.validations.inchron.InchronProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class InchronConstraintsValidator {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private ConstraintsBuilder b2 = new ConstraintsBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(Collections.<Class<? extends IProfile>>unmodifiableList(CollectionLiterals.<Class<? extends IProfile>>newArrayList(InchronProfile.class)));

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  public ProcessRequirement createProcessReqWithHwContext(final String name, final String PUname) {
    ProcessRequirement _xblockexpression = null;
    {
      final ProcessRequirement req = AmaltheaFactory.eINSTANCE.createProcessRequirement();
      req.setName(name);
      final CPUPercentageRequirementLimit cpulimit = AmaltheaFactory.eINSTANCE.createCPUPercentageRequirementLimit();
      final ProcessingUnit pu = AmaltheaFactory.eINSTANCE.createProcessingUnit();
      pu.setName(PUname);
      cpulimit.setHardwareContext(pu);
      req.setLimit(cpulimit);
      _xblockexpression = req;
    }
    return _xblockexpression;
  }

  @Test
  public void test_InchronConstraintsCPUPercentageLimit() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<ConstraintsModel> _function_1 = (ConstraintsModel it_1) -> {
        final Consumer<ProcessRequirement> _function_2 = (ProcessRequirement it_2) -> {
          it_2.setName("req_2");
          final Consumer<CPUPercentageRequirementLimit> _function_3 = (CPUPercentageRequirementLimit it_3) -> {
          };
          this.b2.limit_CPUPercentage(it_2, _function_3);
        };
        this.b2.requirement_Process(it_1, _function_2);
        EList<Requirement> _requirements = it_1.getRequirements();
        ProcessRequirement _createProcessReqWithHwContext = this.createProcessReqWithHwContext("req_1", "PU_1");
        _requirements.add(_createProcessReqWithHwContext);
      };
      this.b1.constraintsModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(
      Collectors.<String>toList());
    Assert.assertFalse(
      result.contains("CPU Percentage Requirement Limit of Requirement \"req_1\" is missing Hardware context"));
    Assert.assertTrue(
      result.contains("CPU Percentage Requirement Limit of Requirement \"req_2\" is missing Hardware context"));
  }
}
