/**
 * Copyright (c) 2016-2022 Vector Informatik GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.ta.tests;

import com.google.common.base.Objects;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AsynchronousServerCall;
import org.eclipse.app4mc.amalthea.model.Condition;
import org.eclipse.app4mc.amalthea.model.ConditionConjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunctionEntry;
import org.eclipse.app4mc.amalthea.model.EnumMode;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLiteral;
import org.eclipse.app4mc.amalthea.model.ModeValueCondition;
import org.eclipse.app4mc.amalthea.model.OsEvent;
import org.eclipse.app4mc.amalthea.model.RelationalOperator;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SetEvent;
import org.eclipse.app4mc.amalthea.model.Switch;
import org.eclipse.app4mc.amalthea.model.SwitchEntry;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.WaitEvent;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.validations.ta.TimingArchitectsProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class TASoftwareModelValidatorTests {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private SoftwareBuilder b2 = new SoftwareBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(TimingArchitectsProfile.class);

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  public void conditionSingleConjunction(final SwitchEntry container, final Procedure1<ModeValueCondition> initializer) {
    ConditionDisjunction _condition = container.getCondition();
    boolean _tripleEquals = (_condition == null);
    if (_tripleEquals) {
      container.setCondition(AmaltheaFactory.eINSTANCE.createConditionDisjunction());
      EList<ConditionDisjunctionEntry> _entries = container.getCondition().getEntries();
      ConditionConjunction _createConditionConjunction = AmaltheaFactory.eINSTANCE.createConditionConjunction();
      _entries.add(_createConditionConjunction);
    }
    final ModeValueCondition obj = AmaltheaFactory.eINSTANCE.createModeValueCondition();
    ConditionDisjunctionEntry _get = container.getCondition().getEntries().get(0);
    ConditionConjunction mcc = ((ConditionConjunction) _get);
    EList<Condition> _entries_1 = mcc.getEntries();
    _entries_1.add(obj);
    initializer.apply(obj);
  }

  @Test
  public void test_TASoftwareOsEvent() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<OsEvent> _function_2 = (OsEvent it_2) -> {
          it_2.setName("ev_ok");
        };
        this.b2.osEvent(it_1, _function_2);
        final Consumer<OsEvent> _function_3 = (OsEvent it_2) -> {
          it_2.setName("ev_wait");
        };
        this.b2.osEvent(it_1, _function_3);
        final Consumer<Task> _function_4 = (Task it_2) -> {
          it_2.setName("t1");
          final Consumer<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Consumer<SetEvent> _function_6 = (SetEvent it_4) -> {
              this.b2.eventMask(it_4, this.b1.<OsEvent>_find(it_4, OsEvent.class, "ev_ok"));
            };
            this.b2.setEvent(it_3, _function_6);
            final Consumer<WaitEvent> _function_7 = (WaitEvent it_4) -> {
              this.b2.eventMask(it_4, this.b1.<OsEvent>_find(it_4, OsEvent.class, "ev_ok"));
            };
            this.b2.waitEvent(it_3, _function_7);
          };
          this.b2.activityGraph(it_2, _function_5);
        };
        this.b2.task(it_1, _function_4);
        final Consumer<Task> _function_5 = (Task it_2) -> {
          it_2.setName("t2");
          final Consumer<ActivityGraph> _function_6 = (ActivityGraph it_3) -> {
            final Consumer<WaitEvent> _function_7 = (WaitEvent it_4) -> {
              this.b2.eventMask(it_4, this.b1.<OsEvent>_find(it_4, OsEvent.class, "ev_wait"));
            };
            this.b2.waitEvent(it_3, _function_7);
          };
          this.b2.activityGraph(it_2, _function_6);
        };
        this.b2.task(it_1, _function_5);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("Os Event \"ev_wait\" is waited upon, but it is never set."));
    Assert.assertFalse(result.contains("Os Event \"ev_ok\" is waited upon, but it is never set."));
  }

  @Test
  public void test_TASoftwareModeConditionConjunctionAlwaysFalse() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<EnumMode> _function_2 = (EnumMode it_2) -> {
          it_2.setName("enumerated");
          final Consumer<ModeLiteral> _function_3 = (ModeLiteral it_3) -> {
            it_3.setName("first");
          };
          this.b2.literal(it_2, _function_3);
          final Consumer<ModeLiteral> _function_4 = (ModeLiteral it_3) -> {
            it_3.setName("second");
          };
          this.b2.literal(it_2, _function_4);
        };
        this.b2.mode_Enum(it_1, _function_2);
        final Consumer<ModeLabel> _function_3 = (ModeLabel it_2) -> {
          it_2.setName("ml");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "enumerated"));
          it_2.setInitialValue("first");
        };
        this.b2.modeLabel(it_1, _function_3);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_4 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r");
          final Consumer<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Consumer<Switch> _function_6 = (Switch it_4) -> {
              final Consumer<SwitchEntry> _function_7 = (SwitchEntry it_5) -> {
                it_5.setName("r_mse_ok");
                final Procedure1<ModeValueCondition> _function_8 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("first");
                  it_6.setRelation(RelationalOperator.EQUAL);
                };
                this.conditionSingleConjunction(it_5, _function_8);
                final Procedure1<ModeValueCondition> _function_9 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("second");
                  it_6.setRelation(RelationalOperator.NOT_EQUAL);
                };
                this.conditionSingleConjunction(it_5, _function_9);
              };
              this.b2.entry(it_4, _function_7);
            };
            this.b2.conditionalSwitch(it_3, _function_6);
            final Consumer<Switch> _function_7 = (Switch it_4) -> {
              final Consumer<SwitchEntry> _function_8 = (SwitchEntry it_5) -> {
                it_5.setName("r_mse_twoLitEQ");
                final Procedure1<ModeValueCondition> _function_9 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("first");
                  it_6.setRelation(RelationalOperator.EQUAL);
                };
                this.conditionSingleConjunction(it_5, _function_9);
                final Procedure1<ModeValueCondition> _function_10 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("second");
                  it_6.setRelation(RelationalOperator.EQUAL);
                };
                this.conditionSingleConjunction(it_5, _function_10);
              };
              this.b2.entry(it_4, _function_8);
            };
            this.b2.conditionalSwitch(it_3, _function_7);
            final Consumer<Switch> _function_8 = (Switch it_4) -> {
              final Consumer<SwitchEntry> _function_9 = (SwitchEntry it_5) -> {
                it_5.setName("r_mse_twoLitUnEQ");
                final Procedure1<ModeValueCondition> _function_10 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("first");
                  it_6.setRelation(RelationalOperator.NOT_EQUAL);
                };
                this.conditionSingleConjunction(it_5, _function_10);
                final Procedure1<ModeValueCondition> _function_11 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("second");
                  it_6.setRelation(RelationalOperator.NOT_EQUAL);
                };
                this.conditionSingleConjunction(it_5, _function_11);
              };
              this.b2.entry(it_4, _function_9);
            };
            this.b2.conditionalSwitch(it_3, _function_8);
            final Consumer<Switch> _function_9 = (Switch it_4) -> {
              final Consumer<SwitchEntry> _function_10 = (SwitchEntry it_5) -> {
                it_5.setName("r_mse_sameLiteral");
                final Procedure1<ModeValueCondition> _function_11 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("first");
                  it_6.setRelation(RelationalOperator.EQUAL);
                };
                this.conditionSingleConjunction(it_5, _function_11);
                final Procedure1<ModeValueCondition> _function_12 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("first");
                  it_6.setRelation(RelationalOperator.NOT_EQUAL);
                };
                this.conditionSingleConjunction(it_5, _function_12);
              };
              this.b2.entry(it_4, _function_10);
            };
            this.b2.conditionalSwitch(it_3, _function_9);
          };
          this.b2.activityGraph(it_2, _function_5);
        };
        this.b2.runnable(it_1, _function_4);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.WARNING);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("Conjoining equality of mode literals [first, second] in Switch Entry \"r_mse_twoLitEQ\" always evaluates to FALSE, which might not be intended here."));
    Assert.assertTrue(result.contains("Conjoining unequality of mode literals [first, second] in Switch Entry \"r_mse_twoLitUnEQ\" always evaluates to FALSE, which might not be intended here."));
    Assert.assertTrue(result.contains("Conjoining mode conditions on the same Mode Literal \"first\" with relations [EQUAL, NOT_EQUAL] in Switch Entry \"r_mse_sameLiteral\" always evaluates to FALSE, which might not be intended here."));
    Assert.assertFalse(result.contains("Conjoining equality of mode literals [first, second] in Switch Entry \"r_mse_ok\" always evaluates to FALSE, which might not be intended here."));
    Assert.assertFalse(result.contains("Conjoining unequality of mode literals [first, second] in Switch Entry \"r_mse_ok\" always evaluates to FALSE, which might not be intended here."));
    Assert.assertFalse(result.contains("Conjoining mode conditions on the same Mode Literal \"first\" with relations [EQUAL, NOT_EQUAL] in Switch Entry \"r_mse_ok\" always evaluates to FALSE, which might not be intended here."));
  }

  @Test
  public void test_TASoftwareServerCall() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_2 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("server_runnable");
        };
        this.b2.runnable(it_1, _function_2);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_3 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r_ok");
          final Consumer<ActivityGraph> _function_4 = (ActivityGraph it_3) -> {
            final Consumer<AsynchronousServerCall> _function_5 = (AsynchronousServerCall it_4) -> {
              it_4.setServerRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "server_runnable"));
            };
            this.b2.asynchronousServerCall(it_3, _function_5);
          };
          this.b2.activityGraph(it_2, _function_4);
        };
        this.b2.runnable(it_1, _function_3);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_4 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r_looped");
          final Consumer<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Consumer<AsynchronousServerCall> _function_6 = (AsynchronousServerCall it_4) -> {
              it_4.setServerRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "r_looped"));
            };
            this.b2.asynchronousServerCall(it_3, _function_6);
          };
          this.b2.activityGraph(it_2, _function_5);
        };
        this.b2.runnable(it_1, _function_4);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("The server runnable called by Asynchronous Server Call in Runnable \"r_looped\" must not refer to the containing runnable."));
    Assert.assertFalse(result.contains("The server runnable called by Asynchronous Server Call in Runnable \"r_ok\" must not refer to the containing runnable."));
  }

  @Test
  public void test_TASoftwareRunnableCall() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_2 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("other_runnable");
        };
        this.b2.runnable(it_1, _function_2);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_3 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r_ok");
          final Consumer<ActivityGraph> _function_4 = (ActivityGraph it_3) -> {
            final Consumer<AsynchronousServerCall> _function_5 = (AsynchronousServerCall it_4) -> {
              it_4.setServerRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "other_runnable"));
            };
            this.b2.asynchronousServerCall(it_3, _function_5);
          };
          this.b2.activityGraph(it_2, _function_4);
        };
        this.b2.runnable(it_1, _function_3);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_4 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r_looped");
          final Consumer<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Consumer<RunnableCall> _function_6 = (RunnableCall it_4) -> {
              it_4.setRunnable(this.b1.<org.eclipse.app4mc.amalthea.model.Runnable>_find(it_4, org.eclipse.app4mc.amalthea.model.Runnable.class, "r_looped"));
            };
            this.b2.runnableCall(it_3, _function_6);
          };
          this.b2.activityGraph(it_2, _function_5);
        };
        this.b2.runnable(it_1, _function_4);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("The runnable called by Runnable Call in Runnable \"r_looped\" must not refer to the containing runnable."));
    Assert.assertFalse(result.contains("The runnable called by Runnable Call in Runnable \"r_ok\" must not refer to the containing runnable."));
  }
}
