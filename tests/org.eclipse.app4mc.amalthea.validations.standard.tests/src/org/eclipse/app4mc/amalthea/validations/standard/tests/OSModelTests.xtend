/**
 * *******************************************************************************
 * Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.tests

import java.util.List
import org.eclipse.app4mc.amalthea.model.Amalthea
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage
import org.eclipse.app4mc.amalthea.model.ISR
import org.eclipse.app4mc.amalthea.model.InterruptController
import org.eclipse.app4mc.amalthea.model.ListObject
import org.eclipse.app4mc.amalthea.model.ParameterType
import org.eclipse.app4mc.amalthea.model.ProcessingUnit
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition
import org.eclipse.app4mc.amalthea.model.Semaphore
import org.eclipse.app4mc.amalthea.model.SemaphoreType
import org.eclipse.app4mc.amalthea.model.StructureType
import org.eclipse.app4mc.amalthea.model.Task
import org.eclipse.app4mc.amalthea.model.TaskScheduler
import org.eclipse.app4mc.amalthea.model.TimeUnit
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder
import org.eclipse.app4mc.amalthea.model.builder.HardwareBuilder
import org.eclipse.app4mc.amalthea.model.builder.MappingBuilder
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil
import org.eclipse.app4mc.amalthea.validations.standard.OSProfile
import org.eclipse.app4mc.validation.core.ValidationDiagnostic
import org.eclipse.app4mc.validation.util.ValidationExecutor
import org.junit.Test

import static org.junit.Assert.assertFalse
import static org.junit.Assert.assertTrue
import org.eclipse.app4mc.amalthea.model.predefined.AmaltheaTemplates

class OSModelTests {

	extension AmaltheaBuilder b1 = new AmaltheaBuilder
	extension SoftwareBuilder b2 = new SoftwareBuilder
	extension OperatingSystemBuilder b3 = new OperatingSystemBuilder
	extension HardwareBuilder b4 = new HardwareBuilder
	extension MappingBuilder b5 = new MappingBuilder
	
	val executor = new ValidationExecutor( #[OSProfile] )

	def List<ValidationDiagnostic> runExecutor(Amalthea model) {
		executor.validate(model)
		executor.results
	}

	def List<ValidationDiagnostic> validateParameterTypes(Amalthea model) {
		runExecutor(model).filter[it.validationID == "AM-OS-Scheduling-Parameter-Value-Type-Matches-Defined-Type"].toList
	}

	def List<ValidationDiagnostic> validateParameterMultiplicity(Amalthea model) {
		runExecutor(model).filter[it.validationID == "AM-OS-Scheduling-Parameter-Value-Number-Matches-Defined-Multiplicity"].toList
	}

	def List<ValidationDiagnostic> validateMandatoryParameters(Amalthea model) {
		runExecutor(model).filter[it.validationID == "AM-OS-Mandatory-Scheduling-Parameters-Set"].toList
	}

	def List<ValidationDiagnostic> validateStandardSchedulerDefinitions(Amalthea model) {
		runExecutor(model).filter[it.validationID == "AM-OS-Standard-Scheduler-Definition-Conformance"].toList
	}

	def List<ValidationDiagnostic> validateStandardSchedulingParameterDefinitions(Amalthea model) {
		runExecutor(model).filter[it.validationID == "AM-OS-Standard-Scheduling-Parameter-Definition-Conformance"].toList
	}

	def List<ValidationDiagnostic> validateOverriddenSchedulingParameterEmptyValue(Amalthea model) {
		runExecutor(model).filter[it.validationID == "AM-OS-Scheduling-Parameter-Empty-Overriden-Value"].toList
	}

	def Amalthea createValidTestModel() {
		amalthea [
			hardwareModel [
				structure [
					name = "MC1"
					structureType = StructureType::MICROCONTROLLER
					module_ProcessingUnit [name = "core1"]
					module_ProcessingUnit [name = "core2"]
					module_ProcessingUnit [name = "core3"]
				]
			]
			softwareModel [
				task [name = "t1"]
				task [name = "t2"]
				task [name = "t3"]
				isr [name = "isr1"]
			]
			osModel [
				parameterDefinition [
					name = "hyperPeriod"
					type = ParameterType::TIME
				]
				parameterDefinition [
					name = "timeStamps"
					many = true
					type = ParameterType::TIME
				]
				AmaltheaTemplates.addStandardSchedulingParameterDefinition(it, StandardSchedulers.Parameter.PRIORITY)
				parameterDefinition [
					name = "preemptible"
					mandatory = false
					type = ParameterType::BOOL
					defaultValue = FactoryUtil.createBooleanObject(false)
				]
				AmaltheaTemplates.addStandardSchedulingParameterDefinition(it, StandardSchedulers.Parameter.TASK_GROUP)
				AmaltheaTemplates.addStandardSchedulingParameterDefinition(it, StandardSchedulers.Parameter.QUANT_SIZE)
				parameterDefinition [
					name = "interruptFlag"
					mandatory = false
					type = ParameterType::INTEGER
				]
				schedulerDefinition [
					name = "TDMA"
					algorithmParameters += _find(SchedulingParameterDefinition, "hyperPeriod")
					processParameters += _find(SchedulingParameterDefinition, "timeStamps")
				]
				schedulerDefinition [
					name = "OSEK"
					processParameters += #[
						_find(SchedulingParameterDefinition, "priority"),
						_find(SchedulingParameterDefinition, "preemptible"),
						_find(SchedulingParameterDefinition, "taskGroup")
					]
				]
				schedulerDefinition [
					name = "FixedPriorityPreemptive"
					processParameters += #[
						_find(SchedulingParameterDefinition, "priority"),
						_find(SchedulingParameterDefinition, "preemptible")
					]
				]
				schedulerDefinition [
					name = "PriorityBased"
					processParameters += #[
						_find(SchedulingParameterDefinition, "priority"),
						_find(SchedulingParameterDefinition, "preemptible"),
						_find(SchedulingParameterDefinition, "interruptFlag")
					]
				]
				AmaltheaTemplates.addStandardSchedulerDefinition(it, StandardSchedulers.Algorithm.P_FAIR_PD2)
				operatingSystem [
					name = "Os1"
					taskScheduler [
						name = "sched1"
						definition = _find(SchedulerDefinition, "OSEK")
					]
					taskScheduler [
						name = "sched2"
						definition = _find(SchedulerDefinition, "FixedPriorityPreemptive")
					]
					taskScheduler [
						name = "sched3"
						definition = _find(SchedulerDefinition, "TDMA")
						schedulingParameter("hyperPeriod", FactoryUtil.createTime(10, TimeUnit.MS))
					]
					taskScheduler [
						name = "nestedSched"
						definition = _find(SchedulerDefinition, "FixedPriorityPreemptive")
						parentAssociation [
							parent = _find(TaskScheduler, "sched3")
							schedulingParameter("timeStamps", FactoryUtil.createTime(5, TimeUnit.MS))
						]
					]
					interruptController [
						name = "ic1"
						definition = _find(SchedulerDefinition, "PriorityBased")
					]
				]
				semaphore [
					name = "semCountingSemaphore"
					semaphoreType = SemaphoreType.COUNTING_SEMAPHORE
					initialValue = 0
					maxValue = 3
					ownership = false
				]
				semaphore [
					name = "semResource"
					semaphoreType = SemaphoreType.RESOURCE
					initialValue = 0
					maxValue = 3
					ownership = true
				]
				semaphore [
					name = "semSpinlock"
					semaphoreType = SemaphoreType.SPINLOCK
					initialValue = 0
					maxValue = 1
					ownership = true
				]
				semaphore [
					name = "semMutex"
					semaphoreType = SemaphoreType.MUTEX
					initialValue = 0
					maxValue = 1
					ownership = true
				]
			]
			mappingModel [
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched2")
					responsibility += _find(ProcessingUnit, "core2")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched1")
					responsibility += _find(ProcessingUnit, "core1")
				]
				schedulerAllocation [
					scheduler = _find(TaskScheduler, "sched3")
					responsibility += _find(ProcessingUnit, "core3")
				]
				schedulerAllocation [
					scheduler = _find(InterruptController, "ic1")
					responsibility += _find(ProcessingUnit, "core1")
				]
				taskAllocation [
					task = _find(Task, "t1")
					scheduler = _find(TaskScheduler, "sched1")
					schedulingParameter("priority", FactoryUtil.createIntegerObject(3))
					schedulingParameter("taskGroup", FactoryUtil.createIntegerObject(10))
				]
				taskAllocation [
					task = _find(Task, "t2")
					scheduler = _find(TaskScheduler, "sched2")
					schedulingParameter("priority", FactoryUtil.createIntegerObject(40))
					schedulingParameter("preemptible", FactoryUtil.createBooleanObject(false))
				]
				taskAllocation [
					task = _find(Task, "t3")
					scheduler = _find(TaskScheduler, "sched3")
					schedulingParameter("timeStamps", FactoryUtil.createListObject(#[
						FactoryUtil.createTime(0, TimeUnit.MS),
						FactoryUtil.createTime(2, TimeUnit.MS)
					]))
				]
				isrAllocation [
					isr = _find(ISR, "isr1")
					controller = _find(InterruptController, "ic1")
				]
			]
		]
	}

	@Test
	def void testValidModel() {
		val model = createValidTestModel()

		val validationResult = runExecutor(model)

		val result = validationResult.map[it.message].toList
		assertTrue(result.isEmpty)
	}

	@Test
	def void testAlgorithmParameterTypeValidity() {
		val model = createValidTestModel()
		model.osModel.operatingSystems.head.taskSchedulers.findFirst[name == "sched3"].schedulingParameters.head.value = FactoryUtil.createLongObject(5)

		val typeIssues = validateParameterTypes(model)

		assertTrue(typeIssues.size == 1)
		assertTrue(typeIssues.head.targetObject.eContainingFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(typeIssues.head.message == "The value of Scheduling Parameter \"hyperPeriod\" does not conform to the defined type Time")
	}

	@Test
	def void testAllocationSingleParameterTypeValidity() {
		val model = createValidTestModel()

		model.mappingModel.taskAllocation.last.schedulingParameters.head.value = FactoryUtil.createTime(0, TimeUnit.MS)
		assertTrue(validateParameterTypes(model).empty)

		model.mappingModel.taskAllocation.last.schedulingParameters.head.value = FactoryUtil.createLongObject(0)
		val typeIssues = validateParameterTypes(model)

		assertTrue(typeIssues.size == 1)
		assertTrue(typeIssues.head.targetObject.eContainingFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(typeIssues.head.message == "The value of Scheduling Parameter \"timeStamps\" does not conform to the defined type Time")
	}

	@Test
	def void testAllocationMultiParameterTypeValidity() {
		val model = createValidTestModel()

		val sps = model.mappingModel.taskAllocation.last.schedulingParameters.head.value as ListObject
		sps.values += FactoryUtil.createLongObject(4)
		val typeIssues = validateParameterTypes(model)

		assertTrue(typeIssues.size == 1)
		assertTrue(typeIssues.head.targetObject.eContainingFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(typeIssues.head.message == "The value of Scheduling Parameter \"timeStamps\" does not conform to the defined type Time")
	}

	@Test
	def void testAllocationMultiNestedParameterTypeValidity() {
		val model = createValidTestModel()

		val sps = model.mappingModel.taskAllocation.last.schedulingParameters.head.value as ListObject
		val nested = FactoryUtil.createListObject(#[FactoryUtil.createTime(3, TimeUnit.MS)]);
		sps.values += nested
		// values: [0ms, 2ms, [3ms]] is valid
		assertTrue(validateParameterTypes(model).empty)

		nested.values += FactoryUtil.createLongObject(4)
		// values: [0ms, 2ms, [3ms, 4]] is invalid
		val typeIssues = validateParameterTypes(model)

		assertTrue(typeIssues.size == 1)
		assertTrue(typeIssues.head.targetObject.eContainingFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(typeIssues.head.message == "The value of Scheduling Parameter \"timeStamps\" does not conform to the defined type Time")
	}

	@Test
	def void testSchedulingParameterMultiplicityLower() {
		val model = createValidTestModel()

		model.osModel.operatingSystems.head.taskSchedulers.findFirst[name == "sched3"].schedulingParameters.head.value = null

		var multiIssues = validateParameterMultiplicity(model)
		assertTrue(multiIssues.size == 1)
		assertTrue(multiIssues.head.targetObject.eContainingFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(multiIssues.head.message == "There is no value for Scheduling Parameter \"hyperPeriod\" - "
			+ "the Scheduling Parameter Definition requires exactly one value")
			
		model.osModel.operatingSystems.head.taskSchedulers.findFirst[name == "sched3"].schedulingParameters.head.value = FactoryUtil.createTime(10, TimeUnit.MS)
		val sps = model.mappingModel.taskAllocation.last.schedulingParameters.head.value as ListObject
		sps.values.clear

		multiIssues = validateParameterMultiplicity(model)
		assertTrue(multiIssues.size == 1)
		assertTrue(multiIssues.head.targetObject.eContainingFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(multiIssues.head.message == "There is no value for Scheduling Parameter \"timeStamps\" - "
			+ "the Scheduling Parameter Definition requires at least one value")
	}

	@Test
	def void testSchedulingParameterMultiplicityUpper() {
		val model = createValidTestModel()

		val hpVal = model.osModel.operatingSystems.head.taskSchedulers.findFirst[name == "sched3"].schedulingParameters.head
		hpVal.value = FactoryUtil.createListObject(#[FactoryUtil.createTime(10, TimeUnit.MS)])
		// even though the type is not correct, the multiplicity is valid
		assertTrue(validateParameterMultiplicity(model).empty)

		(hpVal.value as ListObject).values += FactoryUtil.createTime(12, TimeUnit.MS)
		val multiIssues = validateParameterMultiplicity(model)
		assertTrue(multiIssues.size == 1)
		assertTrue(multiIssues.head.targetObject.eContainingFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(multiIssues.head.message == "There are multiple values for Scheduling Parameter \"hyperPeriod\" - "
			+ "the Scheduling Parameter Definition allows not more than one value")
	}

	@Test
	def void testMandatorySchedulerAlgorithmParameters() {
		val model = createValidTestModel()

		model.osModel.operatingSystems.head.taskSchedulers.findFirst[name == "sched3"].schedulingParameters.clear

		val mandatorySPIssues = validateMandatoryParameters(model)
		assertTrue(mandatorySPIssues.size == 1)
		assertTrue(mandatorySPIssues.head.targetFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(mandatorySPIssues.head.message == "Mandatory scheduling parameter \"hyperPeriod\" is not set for Task Scheduler \"sched3\"")
	}

	@Test
	def void testMandatoryTaskAllocationSchedulingParameters() {
		val model = createValidTestModel()

		model.mappingModel.taskAllocation.head.schedulingParameters.clear

		val mandatorySPIssues = validateMandatoryParameters(model)
		assertTrue(mandatorySPIssues.size == 2)
		assertTrue(mandatorySPIssues.stream.allMatch[targetFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters])
		val messages = mandatorySPIssues.map[message]
		assertTrue(messages.contains("Mandatory scheduling parameter \"priority\" is not set for Task Allocation"))
		assertTrue(messages.contains("Mandatory scheduling parameter \"taskGroup\" is not set for Task Allocation"))
	}

	@Test
	def void testMandatorySchedulerAssociationSchedulingParameters() {
		val model = createValidTestModel()

		model.osModel.operatingSystems.head.taskSchedulers.last.parentAssociation.schedulingParameters.clear

		val mandatorySPIssues = validateMandatoryParameters(model)
		assertTrue(mandatorySPIssues.size == 1)
		assertTrue(mandatorySPIssues.head.targetFeature == AmaltheaPackage.eINSTANCE.ISchedulingParameterContainer_SchedulingParameters)
		assertTrue(mandatorySPIssues.head.message == "Mandatory scheduling parameter \"timeStamps\" is not set for Scheduler Association")
	}

	@Test
	def void testMandatoryISRAllocationSchedulingParameters() {
		val model = createValidTestModel()

		model.osModel.schedulingParameterDefinitions.last.mandatory = true

		val mandatorySPIssues = validateMandatoryParameters(model)
		assertTrue(mandatorySPIssues.size == 1)
		assertTrue(mandatorySPIssues.head.targetFeature == AmaltheaPackage.eINSTANCE.ISRAllocation_Priority)
		assertTrue(mandatorySPIssues.head.message == "Mandatory scheduling parameter \"interruptFlag\" is not set for ISR Allocation")
	}

	@Test
	def void testStandardSDWillBeValidated() {
		val model = createValidTestModel()

		val osekSD = model.osModel.schedulerDefinitions.findFirst[name == "OSEK"]
		osekSD.processParameters.head.name = "invalid"

		val standardSDIssues = validateStandardSchedulerDefinitions(model)
		assertTrue(standardSDIssues.size == 3)
		assertTrue(standardSDIssues.stream.allMatch[targetFeature == AmaltheaPackage.eINSTANCE.schedulerDefinition_ProcessParameters])
		val messages = standardSDIssues.map[message]
		#[StandardSchedulers.Algorithm.OSEK, StandardSchedulers.Algorithm.FIXED_PRIORITY_PREEMPTIVE, StandardSchedulers.Algorithm.PRIORITY_BASED].forEach[
			assertTrue(messages.contains("Expected scheduling parameter definition \"priority\" is not provided in processParameters for "
					+ "standard Scheduler Definition \"" + it.algorithmName + "\""
			))
		]

		osekSD.name = "osek" // not a standard scheduler anymore
		val standardSDIssues2 = validateStandardSchedulerDefinitions(model)
		assertTrue(standardSDIssues2.size == 2)
		val messages2 = standardSDIssues2.map[message]
		assertFalse(messages2.contains("Expected scheduling parameter definition \"priority\" is not provided in processParameters for "
				+ "standard Scheduler Definition \"OSEK\""
		))
	}

	@Test
	def void testStandardSDParameterTypeValidated() {
		val model = createValidTestModel()

		val prioritySPD = model.osModel.schedulingParameterDefinitions.findFirst[name == "priority"]
		prioritySPD.type = ParameterType.BOOL

		val standardSPDIssues = validateStandardSchedulingParameterDefinitions(model)
		assertTrue(standardSPDIssues.size == 1)
		assertTrue(standardSPDIssues.head.targetFeature == AmaltheaPackage.eINSTANCE.schedulingParameterDefinition_Type)
		assertTrue(standardSPDIssues.head.message == "Standard scheduling parameter definition \"priority\" expects type set to 'Integer', "
				+ "but found 'Bool' in Scheduling Parameter Definition \"priority\"")
	}

	@Test
	def void testStandardSDParameterManyValidated() {
		val model = createValidTestModel()

		val prioritySPD = model.osModel.schedulingParameterDefinitions.findFirst[name == "priority"]
		prioritySPD.many = true

		val standardSPDIssues = validateStandardSchedulingParameterDefinitions(model)
		assertTrue(standardSPDIssues.size == 1)
		assertTrue(standardSPDIssues.head.targetFeature == AmaltheaPackage.eINSTANCE.schedulingParameterDefinition_Many)
		assertTrue(standardSPDIssues.head.message == "Standard scheduling parameter definition \"priority\" expects many set to 'false', "
				+ "but found 'true' in Scheduling Parameter Definition \"priority\"")
	}

	@Test
	def void testStandardSDParameterMandatoryValidated() {
		val model = createValidTestModel()

		val prioritySPD = model.osModel.schedulingParameterDefinitions.findFirst[name == "priority"]
		prioritySPD.mandatory = false

		val standardSPDIssues = validateStandardSchedulingParameterDefinitions(model)
		assertTrue(standardSPDIssues.size == 1)
		assertTrue(standardSPDIssues.head.targetFeature == AmaltheaPackage.eINSTANCE.schedulingParameterDefinition_Mandatory)
		assertTrue(standardSPDIssues.head.message == "Standard scheduling parameter definition \"priority\" expects mandatory set to 'true', "
				+ "but found 'false' in Scheduling Parameter Definition \"priority\"")
	}

	@Test
	def void testStandardSDParameterDefaultValueValidated() {
		val model = createValidTestModel()

		val quantSizeSPD = model.osModel.schedulingParameterDefinitions.findFirst[name == "quantSize"]
		quantSizeSPD.defaultValue = null

		val standardSPDIssues = validateStandardSchedulingParameterDefinitions(model)
		assertTrue(standardSPDIssues.size == 1)
		assertTrue(standardSPDIssues.head.targetFeature == AmaltheaPackage.eINSTANCE.schedulingParameterDefinition_DefaultValue)
		assertTrue(standardSPDIssues.head.message == "Standard scheduling parameter definition \"quantSize\" defines a default value, "
				+ "which should also be provided here, since Scheduling Parameter Definition \"quantSize\" is optional"
		)
	}

	@Test
	def void testOverriddenSchedulerParameterHasNullValidated() {
		val model = createValidTestModel()
		
		model.mappingModel.taskAllocation.findFirst[task.name == "t2"].schedulingParameters.findFirst[key.name == "preemptible"].value = null
		
		val overriddenSPValueNullIssues = validateOverriddenSchedulingParameterEmptyValue(model)
		assertTrue(overriddenSPValueNullIssues.size == 1)
		assertTrue(overriddenSPValueNullIssues.head.targetFeature == AmaltheaPackage.eINSTANCE.schedulingParameter_Value)
		assertTrue(overriddenSPValueNullIssues.head.message == "There should be a value provided for the overridden Scheduling Parameter \"preemptible\", " 
				+ "otherwise the default value will be used"
		)
		
		model.mappingModel.taskAllocation.findFirst[task.name == "t2"].schedulingParameters.findFirst[key.name == "preemptible"]
				.value = FactoryUtil.createBooleanObject(true)
		model.mappingModel.taskAllocation.findFirst[task.name == "t2"].schedulingParameters.findFirst[key.name == "priority"].value = null
		
		val overriddenSPValueNullIssues2 = validateOverriddenSchedulingParameterEmptyValue(model)
		assertTrue(overriddenSPValueNullIssues2.empty)
	}

	@Test
	def void testSemaphoreInitialNotGreaterThanMaxValueValidated() {
		val model = amalthea [
			osModel [
				semaphore [
					name = "semCorrectEqual"
					initialValue = 2
					maxValue = 2
				]
				semaphore [
					name = "semCorrectLess"
					initialValue = 1
					maxValue = 3
				]
				semaphore [
					name = "semIncorrectGreater"
					initialValue = 2
					maxValue = 1
				]
			]
		]
		val issues = runExecutor(model).filter[it.validationID == "AM-OS-Semaphore-Properties-Conform-Type"].toList

		assertTrue(issues.size == 1)
		assertTrue((issues.head.targetObject as Semaphore).name == "semIncorrectGreater")
	}

	@Test
	def void testSemaphoreInitialValueZeroValidated() {
		val model = createValidTestModel()

		model.osModel.semaphores.forEach[initialValue = 1]
		val issues = runExecutor(model).filter[it.validationID == "AM-OS-Semaphore-Properties-Conform-Type"].toList
		assertTrue(issues.size == 3)
		assertTrue(issues.exists[(targetObject as Semaphore).semaphoreType == SemaphoreType.RESOURCE])
		assertTrue(issues.exists[(targetObject as Semaphore).semaphoreType == SemaphoreType.SPINLOCK])
		assertTrue(issues.exists[(targetObject as Semaphore).semaphoreType == SemaphoreType.MUTEX])
	}

	@Test
	def void testSemaphoreMaxValueOneValidated() {
		val model = createValidTestModel()

		model.osModel.semaphores.forEach[maxValue = 2]
		val issues = runExecutor(model).filter[it.validationID == "AM-OS-Semaphore-Properties-Conform-Type"].toList
		assertTrue(issues.size == 2)
		assertTrue(issues.exists[(targetObject as Semaphore).semaphoreType == SemaphoreType.SPINLOCK])
		assertTrue(issues.exists[(targetObject as Semaphore).semaphoreType == SemaphoreType.MUTEX])
	}

	@Test
	def void testSemaphoreCountingSemaphoreHasNoPCPValidated() {
		val model = createValidTestModel()

		model.osModel.semaphores.forEach[priorityCeilingProtocol = true]
		val issues = runExecutor(model).filter[it.validationID == "AM-OS-Semaphore-Properties-Conform-Type"].toList
		assertTrue(issues.size == 1)
		assertTrue((issues.head.targetObject as Semaphore).semaphoreType == SemaphoreType.COUNTING_SEMAPHORE)
	}

	@Test
	def void testSemaphoreWrongOwnership() {
		val model = createValidTestModel()

		model.osModel.semaphores.forEach[
			ownership = (semaphoreType == SemaphoreType.COUNTING_SEMAPHORE)
		]
		val issues = runExecutor(model).filter[it.validationID == "AM-OS-Semaphore-Properties-Conform-Type"].toList
		assertTrue(issues.size == 3)
		issues.forEach[
			assertTrue(message.startsWith("Ownership must be set to TRUE "))
		]
	}

}
