/**
 * Copyright (c) 2018-2022 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 */
package org.eclipse.app4mc.amalthea.validations.standard.tests;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunctionEntry;
import org.eclipse.app4mc.amalthea.model.EnumMode;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.ModeLiteral;
import org.eclipse.app4mc.amalthea.model.ModeValueCondition;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.RelationalOperator;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Semaphore;
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess;
import org.eclipse.app4mc.amalthea.model.SemaphoreType;
import org.eclipse.app4mc.amalthea.model.Switch;
import org.eclipse.app4mc.amalthea.model.SwitchEntry;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.WaitingBehaviour;
import org.eclipse.app4mc.amalthea.model.builder.AmaltheaBuilder;
import org.eclipse.app4mc.amalthea.model.builder.OperatingSystemBuilder;
import org.eclipse.app4mc.amalthea.model.builder.SoftwareBuilder;
import org.eclipse.app4mc.amalthea.validations.standard.EMFProfile;
import org.eclipse.app4mc.amalthea.validations.standard.SoftwareProfile;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.app4mc.validation.util.ValidationExecutor;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.junit.Assert;
import org.junit.Test;

@SuppressWarnings("all")
public class SoftwareModelTests {
  @Extension
  private AmaltheaBuilder b1 = new AmaltheaBuilder();

  @Extension
  private SoftwareBuilder b2 = new SoftwareBuilder();

  @Extension
  private OperatingSystemBuilder b3 = new OperatingSystemBuilder();

  private final ValidationExecutor executor = new ValidationExecutor(Collections.<Class<? extends IProfile>>unmodifiableList(CollectionLiterals.<Class<? extends IProfile>>newArrayList(EMFProfile.class, SoftwareProfile.class)));

  public List<ValidationDiagnostic> runExecutor(final Amalthea model) {
    List<ValidationDiagnostic> _xblockexpression = null;
    {
      this.executor.validate(model);
      _xblockexpression = this.executor.getResults();
    }
    return _xblockexpression;
  }

  public void condition(final SwitchEntry container, final Procedure1<ModeValueCondition> initializer) {
    ConditionDisjunction _condition = container.getCondition();
    boolean _tripleEquals = (_condition == null);
    if (_tripleEquals) {
      container.setCondition(AmaltheaFactory.eINSTANCE.createConditionDisjunction());
    }
    final ModeValueCondition obj = AmaltheaFactory.eINSTANCE.createModeValueCondition();
    EList<ConditionDisjunctionEntry> _entries = container.getCondition().getEntries();
    _entries.add(obj);
    initializer.apply(obj);
  }

  @Test
  public void testModeLabel() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<EnumMode> _function_2 = (EnumMode it_2) -> {
          it_2.setName("enumerated");
          final Consumer<ModeLiteral> _function_3 = (ModeLiteral it_3) -> {
            it_3.setName("first");
          };
          this.b2.literal(it_2, _function_3);
          final Consumer<ModeLiteral> _function_4 = (ModeLiteral it_3) -> {
            it_3.setName("second");
          };
          this.b2.literal(it_2, _function_4);
        };
        this.b2.mode_Enum(it_1, _function_2);
        final Consumer<ModeLabel> _function_3 = (ModeLabel it_2) -> {
          it_2.setName("ml_ok");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "enumerated"));
          it_2.setInitialValue("first");
        };
        this.b2.modeLabel(it_1, _function_3);
        final Consumer<ModeLabel> _function_4 = (ModeLabel it_2) -> {
          it_2.setName("ml_notOk");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "enumerated"));
          it_2.setInitialValue("third");
        };
        this.b2.modeLabel(it_1, _function_4);
        final Consumer<ModeLabel> _function_5 = (ModeLabel it_2) -> {
          it_2.setName("ml_okok");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "enumerated"));
        };
        this.b2.modeLabel(it_1, _function_5);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("The initialValue \'third\' is not a valid literal of Enum Mode \"enumerated\" ( in Mode Label \"ml_notOk\" )"));
    Assert.assertFalse(result.contains("The initialValue \'first\' is not a valid literal of Enum Mode \"enumerated\" ( in Mode Label \"ml_ok\" )"));
    Assert.assertFalse(result.contains("The initialValue \'\' is not a valid literal of Enum Mode \"enumerated\" ( in Mode Label \"ml_okok\" )"));
    Assert.assertFalse(result.contains("The initialValue \'null\' is not a valid literal of Enum Mode \"enumerated\" ( in Mode Label \"ml_okok\" )"));
  }

  @Test
  public void testModeValue() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<EnumMode> _function_2 = (EnumMode it_2) -> {
          it_2.setName("enumerated");
          final Consumer<ModeLiteral> _function_3 = (ModeLiteral it_3) -> {
            it_3.setName("first");
          };
          this.b2.literal(it_2, _function_3);
          final Consumer<ModeLiteral> _function_4 = (ModeLiteral it_3) -> {
            it_3.setName("second");
          };
          this.b2.literal(it_2, _function_4);
        };
        this.b2.mode_Enum(it_1, _function_2);
        final Consumer<ModeLabel> _function_3 = (ModeLabel it_2) -> {
          it_2.setName("ml");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "enumerated"));
          it_2.setInitialValue("first");
        };
        this.b2.modeLabel(it_1, _function_3);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_4 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r1");
          final Consumer<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Consumer<Switch> _function_6 = (Switch it_4) -> {
              final Consumer<SwitchEntry> _function_7 = (SwitchEntry it_5) -> {
                it_5.setName("r1_mse_ok");
                final Procedure1<ModeValueCondition> _function_8 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("first");
                  it_6.setRelation(RelationalOperator.EQUAL);
                };
                this.condition(it_5, _function_8);
              };
              this.b2.entry(it_4, _function_7);
            };
            this.b2.conditionalSwitch(it_3, _function_6);
          };
          this.b2.activityGraph(it_2, _function_5);
        };
        this.b2.runnable(it_1, _function_4);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_5 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r2");
          final Consumer<ActivityGraph> _function_6 = (ActivityGraph it_3) -> {
            final Consumer<Switch> _function_7 = (Switch it_4) -> {
              final Consumer<SwitchEntry> _function_8 = (SwitchEntry it_5) -> {
                it_5.setName("r2_mse_notOk");
                final Procedure1<ModeValueCondition> _function_9 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("third");
                  it_6.setRelation(RelationalOperator.EQUAL);
                };
                this.condition(it_5, _function_9);
              };
              this.b2.entry(it_4, _function_8);
            };
            this.b2.conditionalSwitch(it_3, _function_7);
          };
          this.b2.activityGraph(it_2, _function_6);
        };
        this.b2.runnable(it_1, _function_5);
        final Consumer<Task> _function_6 = (Task it_2) -> {
          it_2.setName("t1");
          final Consumer<ActivityGraph> _function_7 = (ActivityGraph it_3) -> {
            final Consumer<Switch> _function_8 = (Switch it_4) -> {
              final Consumer<SwitchEntry> _function_9 = (SwitchEntry it_5) -> {
                it_5.setName("t1_mse_ok");
                final Procedure1<ModeValueCondition> _function_10 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("second");
                  it_6.setRelation(RelationalOperator.EQUAL);
                };
                this.condition(it_5, _function_10);
              };
              this.b2.entry(it_4, _function_9);
            };
            this.b2.conditionalSwitch(it_3, _function_8);
          };
          this.b2.activityGraph(it_2, _function_7);
        };
        this.b2.task(it_1, _function_6);
        final Consumer<Task> _function_7 = (Task it_2) -> {
          it_2.setName("t2");
          final Consumer<ActivityGraph> _function_8 = (ActivityGraph it_3) -> {
            final Consumer<Switch> _function_9 = (Switch it_4) -> {
              final Consumer<SwitchEntry> _function_10 = (SwitchEntry it_5) -> {
                it_5.setName("t2_mse_notOk");
                final Procedure1<ModeValueCondition> _function_11 = (ModeValueCondition it_6) -> {
                  it_6.setLabel(this.b1.<ModeLabel>_find(it_6, ModeLabel.class, "ml"));
                  it_6.setValue("fourth");
                  it_6.setRelation(RelationalOperator.EQUAL);
                };
                this.condition(it_5, _function_11);
              };
              this.b2.entry(it_4, _function_10);
            };
            this.b2.conditionalSwitch(it_3, _function_9);
          };
          this.b2.activityGraph(it_2, _function_8);
        };
        this.b2.task(it_1, _function_7);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("The value \'third\' is not a valid literal of Enum Mode \"enumerated\" ( in Runnable \"r2\" )"));
    Assert.assertFalse(result.contains("The value \'first\' is not a valid literal of Enum Mode \"enumerated\" ( in Runnable \"r1\" )"));
    Assert.assertTrue(result.contains("The value \'fourth\' is not a valid literal of Enum Mode \"enumerated\" ( in Task \"t2\" )"));
    Assert.assertFalse(result.contains("The value \'second\' is not a valid literal of Enum Mode \"enumerated\" ( in Task \"t1\" )"));
  }

  @Test
  public void testModeLabelAccess() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<EnumMode> _function_2 = (EnumMode it_2) -> {
          it_2.setName("enumerated");
          final Consumer<ModeLiteral> _function_3 = (ModeLiteral it_3) -> {
            it_3.setName("first");
          };
          this.b2.literal(it_2, _function_3);
          final Consumer<ModeLiteral> _function_4 = (ModeLiteral it_3) -> {
            it_3.setName("second");
          };
          this.b2.literal(it_2, _function_4);
        };
        this.b2.mode_Enum(it_1, _function_2);
        final Consumer<ModeLabel> _function_3 = (ModeLabel it_2) -> {
          it_2.setName("ml");
          it_2.setMode(this.b1.<EnumMode>_find(it_2, EnumMode.class, "enumerated"));
          it_2.setInitialValue("first");
        };
        this.b2.modeLabel(it_1, _function_3);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_4 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r_ok");
          final Consumer<ActivityGraph> _function_5 = (ActivityGraph it_3) -> {
            final Consumer<ModeLabelAccess> _function_6 = (ModeLabelAccess it_4) -> {
              it_4.setAccess(ModeLabelAccessEnum.SET);
              it_4.setData(this.b1.<ModeLabel>_find(it_4, ModeLabel.class, "ml"));
              it_4.setValue("first");
            };
            this.b2.modeLabelAccess(it_3, _function_6);
          };
          this.b2.activityGraph(it_2, _function_5);
        };
        this.b2.runnable(it_1, _function_4);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_5 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r_notOk");
          final Consumer<ActivityGraph> _function_6 = (ActivityGraph it_3) -> {
            final Consumer<ModeLabelAccess> _function_7 = (ModeLabelAccess it_4) -> {
              it_4.setAccess(ModeLabelAccessEnum.SET);
              it_4.setData(this.b1.<ModeLabel>_find(it_4, ModeLabel.class, "ml"));
              it_4.setValue("third");
            };
            this.b2.modeLabelAccess(it_3, _function_7);
          };
          this.b2.activityGraph(it_2, _function_6);
        };
        this.b2.runnable(it_1, _function_5);
        final Consumer<Task> _function_6 = (Task it_2) -> {
          it_2.setName("t_ok");
          final Consumer<ActivityGraph> _function_7 = (ActivityGraph it_3) -> {
            final Consumer<ModeLabelAccess> _function_8 = (ModeLabelAccess it_4) -> {
              it_4.setAccess(ModeLabelAccessEnum.SET);
              it_4.setData(this.b1.<ModeLabel>_find(it_4, ModeLabel.class, "ml"));
              it_4.setValue("second");
            };
            this.b2.modeLabelAccess(it_3, _function_8);
          };
          this.b2.activityGraph(it_2, _function_7);
        };
        this.b2.task(it_1, _function_6);
        final Consumer<Task> _function_7 = (Task it_2) -> {
          it_2.setName("t_notOk");
          final Consumer<ActivityGraph> _function_8 = (ActivityGraph it_3) -> {
            final Consumer<ModeLabelAccess> _function_9 = (ModeLabelAccess it_4) -> {
              it_4.setAccess(ModeLabelAccessEnum.SET);
              it_4.setData(this.b1.<ModeLabel>_find(it_4, ModeLabel.class, "ml"));
              it_4.setValue("fourth");
            };
            this.b2.modeLabelAccess(it_3, _function_9);
          };
          this.b2.activityGraph(it_2, _function_8);
        };
        this.b2.task(it_1, _function_7);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("The value \'third\' is not a valid literal of Enum Mode \"enumerated\" ( in Runnable \"r_notOk\" )"));
    Assert.assertFalse(result.contains("The value \'first\' is not a valid literal of Enum Mode \"enumerated\" ( in Runnable \"r_ok\" )"));
    Assert.assertTrue(result.contains("The value \'fourth\' is not a valid literal of Enum Mode \"enumerated\" ( in Task \"t_notOk\" )"));
    Assert.assertFalse(result.contains("The value \'second\' is not a valid literal of Enum Mode \"enumerated\" ( in Task \"t_ok\" )"));
  }

  @Test
  public void testGroup() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<SWModel> _function_1 = (SWModel it_1) -> {
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_2 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r_ok");
          final Consumer<ActivityGraph> _function_3 = (ActivityGraph it_3) -> {
            final Consumer<Group> _function_4 = (Group it_4) -> {
              it_4.setName("g_ok");
              final Consumer<Group> _function_5 = (Group it_5) -> {
                it_5.setName("g_nested_ok");
              };
              this.b2.group(it_4, _function_5);
            };
            this.b2.group(it_3, _function_4);
          };
          this.b2.activityGraph(it_2, _function_3);
        };
        this.b2.runnable(it_1, _function_2);
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_3 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r_notOk");
          final Consumer<ActivityGraph> _function_4 = (ActivityGraph it_3) -> {
            final Consumer<Group> _function_5 = (Group it_4) -> {
              it_4.setName("g_notOk");
              it_4.setInterruptible(false);
              final Consumer<Group> _function_6 = (Group it_5) -> {
                it_5.setName("g_illegal_nested");
              };
              this.b2.group(it_4, _function_6);
            };
            this.b2.group(it_3, _function_5);
          };
          this.b2.activityGraph(it_2, _function_4);
        };
        this.b2.runnable(it_1, _function_3);
      };
      this.b1.softwareModel(it, _function_1);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final List<ValidationDiagnostic> validationResult = this.runExecutor(model);
    final Predicate<ValidationDiagnostic> _function_1 = (ValidationDiagnostic it) -> {
      Severity _severityLevel = it.getSeverityLevel();
      return Objects.equal(_severityLevel, Severity.ERROR);
    };
    final Function<ValidationDiagnostic, String> _function_2 = (ValidationDiagnostic it) -> {
      return it.getMessage();
    };
    final List<String> result = validationResult.stream().filter(_function_1).<String>map(_function_2).collect(Collectors.<String>toList());
    Assert.assertTrue(result.contains("Group: uninterruptible groups must not contain nested groups (in group \"g_notOk\")"));
    Assert.assertFalse(result.contains("Group: uninterruptible groups must not contain nested groups (in group \"g_ok\")"));
  }

  @Test
  public void testSemaphoreAccess() {
    final Consumer<Amalthea> _function = (Amalthea it) -> {
      final Consumer<OSModel> _function_1 = (OSModel it_1) -> {
        final Consumer<Semaphore> _function_2 = (Semaphore it_2) -> {
          it_2.setName("semCountingSemaphore");
          it_2.setSemaphoreType(SemaphoreType.COUNTING_SEMAPHORE);
          it_2.setInitialValue(0);
          it_2.setMaxValue(3);
        };
        this.b3.semaphore(it_1, _function_2);
        final Consumer<Semaphore> _function_3 = (Semaphore it_2) -> {
          it_2.setName("semResource");
          it_2.setSemaphoreType(SemaphoreType.RESOURCE);
          it_2.setInitialValue(0);
          it_2.setMaxValue(3);
        };
        this.b3.semaphore(it_1, _function_3);
        final Consumer<Semaphore> _function_4 = (Semaphore it_2) -> {
          it_2.setName("semSpinlock");
          it_2.setSemaphoreType(SemaphoreType.SPINLOCK);
          it_2.setInitialValue(0);
          it_2.setMaxValue(1);
        };
        this.b3.semaphore(it_1, _function_4);
        final Consumer<Semaphore> _function_5 = (Semaphore it_2) -> {
          it_2.setName("semMutex");
          it_2.setSemaphoreType(SemaphoreType.MUTEX);
          it_2.setInitialValue(0);
          it_2.setMaxValue(1);
        };
        this.b3.semaphore(it_1, _function_5);
      };
      this.b1.osModel(it, _function_1);
      final Consumer<SWModel> _function_2 = (SWModel it_1) -> {
        final Consumer<org.eclipse.app4mc.amalthea.model.Runnable> _function_3 = (org.eclipse.app4mc.amalthea.model.Runnable it_2) -> {
          it_2.setName("r1");
          final Consumer<ActivityGraph> _function_4 = (ActivityGraph it_3) -> {
            final IActivityGraphItemContainer ag = ((IActivityGraphItemContainer) it_3);
            EObject _eContainer = it_3.eContainer().eContainer().eContainer();
            final Consumer<Semaphore> _function_5 = (Semaphore it_4) -> {
              final Semaphore sem = it_4;
              final Consumer<SemaphoreAccess> _function_6 = (SemaphoreAccess it_5) -> {
                it_5.setSemaphore(sem);
                it_5.setWaitingBehaviour(WaitingBehaviour.ACTIVE);
              };
              this.b2.semaphoreRequest(ag, _function_6);
            };
            ((Amalthea) _eContainer).getOsModel().getSemaphores().forEach(_function_5);
          };
          this.b2.activityGraph(it_2, _function_4);
        };
        this.b2.runnable(it_1, _function_3);
      };
      this.b1.softwareModel(it, _function_2);
    };
    final Amalthea model = this.b1.amalthea(_function);
    final Function1<ValidationDiagnostic, Boolean> _function_1 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-SW-Semaphore-Access"));
    };
    final List<ValidationDiagnostic> noIssues = IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_1));
    Assert.assertTrue(noIssues.isEmpty());
    final Consumer<SemaphoreAccess> _function_2 = (SemaphoreAccess it) -> {
      it.setWaitingBehaviour(WaitingBehaviour.PASSIVE);
    };
    Iterables.<SemaphoreAccess>filter(IterableExtensions.<org.eclipse.app4mc.amalthea.model.Runnable>head(model.getSwModel().getRunnables()).getActivityGraph().getItems(), SemaphoreAccess.class).forEach(_function_2);
    final Function1<ValidationDiagnostic, Boolean> _function_3 = (ValidationDiagnostic it) -> {
      String _validationID = it.getValidationID();
      return Boolean.valueOf(Objects.equal(_validationID, "AM-SW-Semaphore-Access"));
    };
    final List<ValidationDiagnostic> issues = IterableExtensions.<ValidationDiagnostic>toList(IterableExtensions.<ValidationDiagnostic>filter(this.runExecutor(model), _function_3));
    int _size = issues.size();
    boolean _equals = (_size == 1);
    Assert.assertTrue(_equals);
    EObject _targetObject = IterableExtensions.<ValidationDiagnostic>head(issues).getTargetObject();
    SemaphoreType _semaphoreType = ((SemaphoreAccess) _targetObject).getSemaphore().getSemaphoreType();
    boolean _equals_1 = Objects.equal(_semaphoreType, SemaphoreType.SPINLOCK);
    Assert.assertTrue(_equals_1);
  }
}
