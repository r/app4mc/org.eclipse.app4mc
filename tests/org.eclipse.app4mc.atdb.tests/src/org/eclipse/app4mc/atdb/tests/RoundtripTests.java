/**
 ********************************************************************************
 * Copyright (c) 2022 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.eclipse.app4mc.atdb.ATDBBuilder;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.DBConnection.AccessMode;
import org.eclipse.app4mc.atdb.EntityProperty;
import org.eclipse.app4mc.atdb.MetricAggregation;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RoundtripTests {

	private static final String fileName = "test.atdb";
	private static ATDBConnection con;
	private static ATDBBuilder builder;

	private static final List<MusicEntityType> allMETs = List.of(MusicEntityType.values());

	/**
	 * Deletes previous db file (if it exists). Creates new ATDBConnection to new file.
	 * @throws SQLException
	 * @throws IOException
	 */
	@Test
	public void t01_setup() throws SQLException, IOException {
		final File atdbFile = new File(fileName);
		Files.deleteIfExists(Paths.get(fileName));
		assertFalse(atdbFile.exists());
		con = new ATDBConnection(fileName, AccessMode.ReadWrite);
	}

	/**
	 * Creates new ATDBBuilder and checks that it does not create any table.
	 * @throws SQLException
	 */
	@Test
	public void t02_writeEmptyATDB() throws SQLException {
		builder = new ATDBBuilder(con);
		assertFalse(con.tableExists("metaInformation"));
		con.queryAndConsumeResult("SELECT name FROM sqlite_master;", c -> assertFalse(c.next()));
	}

	/**
	 * Creates basic db structure and checks that the tables exist afterwards.
	 * @throws SQLException
	 */
	@Test
	public void t03_writeBasicATDBTables() throws SQLException {
		builder.createBasicDBStructure();
		builder.createBasicViews();
		final List<String> tables = con.queryAndMapToStream(con.getPreparedStatementFor("SELECT name FROM sqlite_master;"),
				r -> r.getString(1)).collect(Collectors.toList());
		assertTrue(tables.containsAll(List.of("metaInformation", "entityType", "entity", "entityInstance",
				"eventType", "property", "propertyValue", "event", "metric", "entityMetricInstanceValue",
				"entityInstanceMetricValue", "entityMetricValue")));
		con.queryAndConsumeResult("SELECT value FROM metaInformation WHERE name = 'dbVersion';", c -> {
		});
		assertFalse(con.tableExists("traceEvent"));
	}

	/**
	 * Writes optional tables and checks if they then exist (as temporary or regular).
	 * @throws SQLException
	 */
	@Test
	public void t04_writeOptionalATDBTables() throws SQLException {
		builder.createOptionalAndTemporaryTables(allMETs, false);
		final List<String> tables = con.queryAndMapToStream(con.getPreparedStatementFor("SELECT name FROM sqlite_temp_master;"),
				r -> r.getString(1)).collect(Collectors.toList());
		final List<String> expectedTemporaryTables = new ArrayList<>();
		expectedTemporaryTables.add("traceEvent");
		allMETs.stream().map(et -> et.getName() + "InstanceTraceInfo").forEach(expectedTemporaryTables::add);
		assertTrue(tables.containsAll(expectedTemporaryTables));
		assertFalse(con.tableExists("traceEvent"));
		con.close();
		con = new ATDBConnection(fileName, AccessMode.ReadWriteInMemory);
		builder = new ATDBBuilder(con);
		assertFalse(con.tableExists("traceEvent"));
		builder.createOptionalAndTemporaryTables(allMETs, true);
		builder.createOptionalViews(allMETs);
		assertTrue(con.tableExists("traceEvent"));
	}

	/**
	 * Fill the ATDB with a music note trace. See <a href="http://lilybin.com/odb2gl/3">http://lilybin.com/odb2gl/3</a> for details.
	 * @throws SQLException
	 */
	@Test
	public void t05_fillTraceEvents() throws SQLException {
		// meta information
		con.insertMetaInformation("testCreator", "webrap");
		con.setTimeBase("ms");

		// should be three, because dbVersion is always in there
		final long metaInfoCount = countRows("SELECT * FROM metaInformation;");
		assertEquals(3, metaInfoCount);

		// entity types
		for(final MusicEntityType entityType:MusicEntityType.values()) {
			for(final String traceAlias:entityType.getTraceAliases()) {
				con.insertEntityType(traceAlias);
			}
		}
		final long entityTypeCount = countRows("SELECT * FROM entityType;");
		assertEquals(Stream.of(MusicEntityType.values()).flatMap(et -> et.getTraceAliases().stream()).count(), entityTypeCount);

		// event types
		for(final MusicEventType eventType:MusicEventType.values()) {
			con.insertEventType(eventType.name());
		}
		final long eventTypeCount = countRows("SELECT * FROM eventType;");
		assertEquals(MusicEventType.values().length, eventTypeCount);

		// properties
		con.insertProperty("pitchInHz", "long");
		con.insertProperty(EntityProperty.RUNNABLES.camelName, EntityProperty.RUNNABLES.type);
		final long propertyCount = countRows("SELECT * FROM property;");
		assertEquals(2, propertyCount);

		// entities
		con.executeBatchUpdate(atdbCon -> {
			atdbCon.insertEntity("Banjo", MusicEntityType.INSTRUMENT.getTraceAliases().get(0));
			atdbCon.insertPropertyValue("Banjo", "pitchInHz", 440);
			atdbCon.insertEntity("Piano", MusicEntityType.INSTRUMENT.getTraceAliases().get(0));
			atdbCon.insertPropertyValue("Piano", "pitchInHz", 440);
			atdbCon.insertEntity("Bass", MusicEntityType.INSTRUMENT.getTraceAliases().get(0));
			atdbCon.insertPropertyValue("Bass", "pitchInHz", 220);
			atdbCon.insertEntity("banjo_c0", MusicEntityType.C0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("banjo_d0", MusicEntityType.D0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("banjo_e0", MusicEntityType.E0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("banjo_g0", MusicEntityType.G0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("banjo_a0", MusicEntityType.A0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("banjo_b0", MusicEntityType.B0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("banjo_c1", MusicEntityType.C1_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntityRefsPropertyValue("Banjo", EntityProperty.RUNNABLES.camelName, List.of("banjo_c0", "banjo_d0", "banjo_e0",
					"banjo_g0", "banjo_a0", "banjo_b0", "banjo_c1"));
			atdbCon.insertEntity("piano_c0", MusicEntityType.C0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("piano_e0", MusicEntityType.E0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("piano_f0", MusicEntityType.F0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("piano_g0", MusicEntityType.G0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntityRefsPropertyValue("Piano", EntityProperty.RUNNABLES.camelName, List.of("piano_c0", "piano_e0", "piano_f0",
					"piano_g0"));
			atdbCon.insertEntity("bass_c0",  MusicEntityType.C0_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntity("bass_c1",  MusicEntityType.C1_NOTE.getTraceAliases().get(0));
			atdbCon.insertEntityRefsPropertyValue("Bass", EntityProperty.RUNNABLES.camelName, List.of("bass_c0", "bass_c1"));

			// entity instances
			atdbCon.insertEntityInstance("Banjo", 0);
			atdbCon.insertEntityInstance("Bass", 0);
			atdbCon.insertEntityInstance("Piano", 0);
			atdbCon.insertEntityInstances("banjo_c0", LongStream.range(0, 2));
			atdbCon.insertEntityInstance("banjo_d0", 0);
			atdbCon.insertEntityInstances("banjo_e0", LongStream.range(0, 2));
			atdbCon.insertEntityInstances("banjo_g0", LongStream.range(0, 2));
			atdbCon.insertEntityInstance("banjo_a0", 0);
			atdbCon.insertEntityInstance("banjo_b0", 0);
			atdbCon.insertEntityInstance("banjo_c1", 0);
			atdbCon.insertEntityInstances("bass_c0", LongStream.range(0, 6));
			atdbCon.insertEntityInstances("bass_c1", LongStream.range(0, 6));
			atdbCon.insertEntityInstances("piano_c0", LongStream.range(0, 6));
			atdbCon.insertEntityInstances("piano_e0", LongStream.range(0, 3));
			atdbCon.insertEntityInstances("piano_f0", LongStream.range(0, 3));
			atdbCon.insertEntityInstances("piano_g0", LongStream.range(0, 6));

			// trace events
			atdbCon.insertTraceEvent(0,   0, "Banjo",    0, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(0,   1, "Bass",     0, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(0,   2, "Piano",    0, "Piano", 0, MusicEventType.start.name(),  null);
			// first measure
			atdbCon.insertTraceEvent(0,   3, "banjo_c0", 0, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(0,   4, "bass_c0",  0, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(0,   5, "bass_c1",  0, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(125, 0, "banjo_c0", 0, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(125, 1, "bass_c0",  0, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(125, 2, "bass_c1",  0, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(125, 3, "banjo_e0", 0, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(250, 0, "banjo_e0", 0, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(250, 1, "banjo_g0", 0, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(250, 2, "piano_c0", 0, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(250, 3, "piano_e0", 0, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(250, 4, "piano_g0", 0, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(375, 0, "piano_c0", 0, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(375, 1, "piano_e0", 0, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(375, 2, "piano_g0", 0, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(450, 0, "banjo_g0", 0, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(500, 0, "banjo_d0", 0, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(500, 1, "bass_c0",  1, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(500, 2, "bass_c1",  1, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(625, 0, "bass_c0",  1, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(625, 1, "bass_c1",  1, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(750, 0, "piano_c0", 1, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(750, 1, "piano_f0", 0, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(750, 2, "piano_g0", 1, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(875, 0, "piano_c0", 1, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(875, 1, "piano_f0", 0, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(875, 2, "piano_g0", 1, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(950, 0, "banjo_d0", 0, "Banjo", 0, MusicEventType.finish.name(), null);
			// second measure
			atdbCon.insertTraceEvent(1000, 0, "banjo_c0", 1, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1000, 1, "bass_c0",  2, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1000, 2, "bass_c1",  2, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1125, 0, "bass_c0",  2, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1125, 1, "bass_c1",  2, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1200, 0, "banjo_c0", 1, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1250, 0, "banjo_e0", 1, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1250, 1, "piano_c0", 2, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1250, 2, "piano_e0", 1, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1250, 3, "piano_g0", 2, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1375, 0, "banjo_e0", 1, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1375, 1, "banjo_g0", 1, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1375, 2, "piano_c0", 2, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1375, 3, "piano_e0", 1, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1375, 4, "piano_g0", 2, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1500, 0, "banjo_g0", 1, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1500, 1, "banjo_a0", 0, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1500, 2, "bass_c0",  3, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1500, 3, "bass_c1",  3, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1625, 0, "bass_c0",  3, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1625, 1, "bass_c1",  3, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1700, 0, "banjo_a0", 0, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1750, 0, "banjo_b0", 0, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1750, 1, "piano_c0", 3, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1750, 2, "piano_f0", 1, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1750, 3, "piano_g0", 3, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(1875, 0, "piano_c0", 3, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1875, 1, "piano_f0", 1, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1875, 2, "piano_g0", 3, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(1950, 0, "banjo_b0", 0, "Banjo", 0, MusicEventType.finish.name(), null);
			// third measure
			atdbCon.insertTraceEvent(2000, 0, "banjo_c1", 0, "Banjo", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2000, 1, "bass_c0",  4, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2000, 2, "bass_c1",  4, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2125, 0, "bass_c0",  4, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2125, 1, "bass_c1",  4, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2250, 0, "piano_c0", 4, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2250, 1, "piano_e0", 2, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2250, 2, "piano_g0", 4, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2375, 0, "piano_c0", 4, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2375, 1, "piano_e0", 2, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2375, 2, "piano_g0", 4, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2450, 0, "banjo_c1", 0, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2500, 0, "bass_c0",  5, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2500, 1, "bass_c1",  5, "Bass",  0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2625, 0, "bass_c0",  5, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2625, 1, "bass_c1",  5, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2750, 0, "piano_c0", 5, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2750, 1, "piano_f0", 2, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2750, 2, "piano_g0", 5, "Piano", 0, MusicEventType.start.name(),  null);
			atdbCon.insertTraceEvent(2875, 0, "piano_c0", 5, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2875, 1, "piano_f0", 2, "Piano", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(2875, 2, "piano_g0", 5, "Piano", 0, MusicEventType.finish.name(), null);

			atdbCon.insertTraceEvent(3000, 0, "Banjo",    0, "Banjo", 0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(3000, 1, "Bass",     0, "Bass",  0, MusicEventType.finish.name(), null);
			atdbCon.insertTraceEvent(3000, 2, "Piano",    0, "Piano", 0, MusicEventType.finish.name(), null);
		});

		final long traceEventCount = countRows("SELECT * FROM traceEvent;");
		assertEquals(86, traceEventCount);

		final PreparedStatement propEntityRefQuery = con.getPrepareQueryFor("SELECT (SELECT name FROM entity WHERE id = value) AS entities "
				+ "FROM entity AS sourceEntity, propertyValue WHERE sourceEntity.name = ? "
				+ "AND entityTypeId = (SELECT id FROM entityType WHERE entityType.name = ?) "
				+ "AND propertyValue.entityId = sourceEntity.id "
				+ "AND propertyValue.propertyId = (SELECT id FROM property WHERE name = ?);");
		propEntityRefQuery.setString(1, "Piano");
		propEntityRefQuery.setString(2, MusicEntityType.INSTRUMENT.getTraceAliases().get(0));
		propEntityRefQuery.setString(3, EntityProperty.RUNNABLES.camelName);
		final List<String> pianoRunnables = con.queryAndMapToStream(propEntityRefQuery, rs -> rs.getString(1))
				.filter(Objects::nonNull).collect(Collectors.toList());
		assertEquals(4, pianoRunnables.size());
		assertTrue(pianoRunnables.containsAll(List.of("piano_c0", "piano_e0", "piano_f0", "piano_g0")));
	}

	/**
	 * Checks if the auto population for entity specific tables works correctly.
	 * @throws SQLException
	 */
	@Test
	public void t06_populateEntitySpecificTables() throws SQLException {
		builder.autoPopulateEntityFilteredTraceEventTables(allMETs);
		assertEquals(14, countRows("SELECT * FROM c0_noteInstanceTraceInfo WHERE isComplete;"));
		assertEquals(8,  countRows("SELECT * FROM g0_noteInstanceTraceInfo WHERE isComplete;"));
		assertEquals(7,  countRows("SELECT * FROM c1_noteInstanceTraceInfo WHERE isComplete;"));
		assertEquals(5,  countRows("SELECT * FROM e0_noteInstanceTraceInfo WHERE isComplete;"));
		assertEquals(3,  countRows("SELECT * FROM f0_noteInstanceTraceInfo WHERE isComplete;"));
		assertEquals(1,  countRows("SELECT * FROM d0_noteInstanceTraceInfo WHERE isComplete;"));
	}

	/**
	 * Adds the entity metric 'swingingTime' (duration) and checks if there is the right amount of entries.
	 * @throws SQLException
	 */
	@Test
	public void t07_addAndCheckDurationMetrics() throws SQLException {
		final String mName = "swingingTime";
		for(final MusicEntityType met:allMETs) {
			con.insertEntityInstanceStateMetricValuesForEntityType(mName, met, List.of(MusicEventType.start.name()),
					List.of(MusicEventType.finish.name()));
		}

		// 40 notes + 3 instruments
		final long swinginEntityInstanceCount = countRows("SELECT * FROM entityInstanceMetricValue WHERE metricId = "
				+ "(SELECT id FROM metric WHERE name = '" + mName + "');");
		assertEquals(43, swinginEntityInstanceCount);
	}

	/**
	 * Adds distance metrics for all start 2 start event distances and checks if there is the right amount of entries.
	 * @throws SQLException
	 */
	@Test
	public void t08_addAndCheckDistanceMetrics() throws SQLException {
		final String mName = "start2Start";
		for(final MusicEntityType met:allMETs) {
			con.insertInterEntityInstanceMetricValuesForEntityType(mName, met, MusicEventType.start.name(), MusicEventType.finish.name());
		}

		// there are 27 note repetitions
		final long start2StartInstanceCount = countRows("SELECT * FROM entityInstanceMetricValue WHERE metricId = "
				+ "(SELECT id FROM metric WHERE name = '" + mName + "');");
		assertEquals(27, start2StartInstanceCount);
	}

	/**
	 * Adds count metrics for all start events and checks if there is the right amount of entries. Also checks some count values.
	 * @throws SQLException
	 */
	@Test
	public void t09_addAndCheckCountMetrics() throws SQLException {
		final String mName = "startCount";
		for(final MusicEntityType met:allMETs) {
			con.insertCountsForEntityTypeAndEvent(mName, met, MusicEventType.start.name());
		}
		final long entityCount = countRows("SELECT * FROM entity;");
		final long startCountMetricCount = countRows("SELECT * FROM entityMetricValue WHERE metricId = "
				+ "(SELECT id FROM metric WHERE name = '" + mName + "');");
		assertEquals(startCountMetricCount, entityCount);

		final long banjo_startCount = Long.parseLong(con.getValueForMetricAndEntity("Banjo", mName));
		assertEquals(1L, banjo_startCount);
		final long banjo_c0_startCount = Long.parseLong(con.getValueForMetricAndEntity("banjo_c0", mName));
		assertEquals(2L, banjo_c0_startCount);
		final long piano_f0_startCount = Long.parseLong(con.getValueForMetricAndEntity("piano_f0", mName));
		assertEquals(3L, piano_f0_startCount);
		final long bass_c0_startCount = Long.parseLong(con.getValueForMetricAndEntity("bass_c0", mName));
		assertEquals(6L, bass_c0_startCount);

		final String piano_d0_startCount = con.getValueForMetricAndEntity("piano_d0", mName);
		assertTrue(piano_d0_startCount.isEmpty());
	}

	/**
	 * Adds aggregated metrics over 'time' dimension. Checks some of the aggregated values.
	 * @throws SQLException
	 */
	@Test
	public void t10_addAndCheckAggregatedTimeMetrics() throws SQLException {
		final String mName = "swingingTime";
		con.insertAggregatedEntityInstanceMetricsForDimension("time");
		// check banjo_c0 values
		final long minBanjoC0Time = Long.parseLong(con.getValueForMetricAndEntity("banjo_c0", mName + "_" + MetricAggregation.Min.name()));
		assertEquals(125, minBanjoC0Time);
		final long maxBanjoC0Time = Long.parseLong(con.getValueForMetricAndEntity("banjo_c0", mName + "_" + MetricAggregation.Max.name()));
		assertEquals(200, maxBanjoC0Time);
		final double avgBanjoC0Time = Double.parseDouble(con.getValueForMetricAndEntity("banjo_c0", mName + "_" + MetricAggregation.Avg.name()));
		assertEquals(Math.round((125d + 200d) * 100d / 2d) / 100d, avgBanjoC0Time, 0);
		final double stDevBanjoC0Time = Double.parseDouble(con.getValueForMetricAndEntity("banjo_c0", mName + "_" + MetricAggregation.StDev.name()));
		assertEquals(Math.round(Math.sqrt(Math.pow(125d - avgBanjoC0Time, 2) + Math.pow(200d - avgBanjoC0Time, 2)) * 100d) / 100d, stDevBanjoC0Time, 0);
		// check piano_c0 values
		final long minPianoC0Time = Long.parseLong(con.getValueForMetricAndEntity("piano_c0", mName + "_" + MetricAggregation.Min.name()));
		assertEquals(125, minPianoC0Time);
		final long maxPianoC0Time = Long.parseLong(con.getValueForMetricAndEntity("piano_c0", mName + "_" + MetricAggregation.Max.name()));
		assertEquals(125, maxPianoC0Time);
		final double avgPianoC0Time = Double.parseDouble(con.getValueForMetricAndEntity("piano_c0", mName + "_" + MetricAggregation.Avg.name()));
		assertEquals(125d, avgPianoC0Time, 0);
		final double stDevPianoC0Time = Double.parseDouble(con.getValueForMetricAndEntity("piano_c0", mName + "_" + MetricAggregation.StDev.name()));
		assertEquals(0d, stDevPianoC0Time, 0);
	}

	/**
	 * Adds events and checks them (resolve event type, entity, and source entity).
	 * @throws SQLException
	 */
	@Test
	public void t11_addAndCheckEvents() throws SQLException {
		con.executeBatchUpdate(atdbCon -> {
			atdbCon.insertEvent("banjo_c0_start", MusicEventType.start.name(), "banjo_c0", "Banjo");
			atdbCon.insertEvent("banjo_c0_finish", MusicEventType.finish.name(), "banjo_c0", "Banjo");
			atdbCon.insertEvent("banjo_e0_start", MusicEventType.start.name(), "banjo_e0", "Banjo");
			atdbCon.insertEvent("banjo_e0_finish", MusicEventType.finish.name(), "banjo_e0", "Banjo");
		});

		final long banjoC0E0EventCount = countRows("SELECT * FROM event WHERE entityId IN (SELECT id FROM entity WHERE name IN ('banjo_c0', 'banjo_e0'));");
		assertEquals(4, banjoC0E0EventCount);
		final String banjoC0StartEntity = con.getEntityForEvent("banjo_c0_start");
		assertEquals("banjo_c0", banjoC0StartEntity);
		final String banjoC0StartEventType = con.getEventTypeForEvent("banjo_c0_start");
		assertEquals(MusicEventType.start.name(), banjoC0StartEventType);
		final String banjoE0FinishEntity = con.getEntityForEvent("banjo_e0_finish");
		assertEquals("banjo_e0", banjoE0FinishEntity);
		final String banjoE0FinishSourceEntity = con.getSourceEntityForEvent("banjo_e0_finish");
		assertEquals("Banjo", banjoE0FinishSourceEntity);
	}

	/**
	 * Adds event chains (including items) and checks them (resolve event by name, type, entity, and source). Checks correct item settings.
	 * Checks stimulus and response events and minItemsCompleted setting.
	 * @throws SQLException
	 */
	@Test
	public void t12_addAndCheckEventChains() throws SQLException {
		con.executeBatchUpdate(atdbCon -> {
			for(final EntityProperty ep:EntityProperty.eventChainProperties) {
				atdbCon.insertProperty(ep.camelName, ep.type);
			}
			atdbCon.insertEventChain("banjoEC");
			atdbCon.insertEventRefPropertyValue("banjoEC", EntityProperty.EC_STIMULUS.camelName, "banjo_c0_start", "", "", "");
			atdbCon.insertEventRefPropertyValue("banjoEC", EntityProperty.EC_RESPONSE.camelName, "banjo_e0_finish", "", "", "");
			atdbCon.insertEventChain("banjoEC_segment1", "banjo_c0_start", "banjo_c0_finish");
			atdbCon.insertEventChain("banjoEC_segment2");
			atdbCon.insertEventRefPropertyValue("banjoEC_segment2", EntityProperty.EC_STIMULUS.camelName, "banjo_c0_finish", "", "", "");
			atdbCon.insertEventRefPropertyValue("banjoEC_segment2", EntityProperty.EC_RESPONSE.camelName, "", MusicEventType.start.name(), "banjo_e0", "Banjo");
			atdbCon.insertEventChain("banjoEC_segment3");
			atdbCon.insertEventRefPropertyValue("banjoEC_segment3", EntityProperty.EC_STIMULUS.camelName, "", MusicEventType.start.name(), "banjo_e0", "");
			atdbCon.insertEventRefPropertyValue("banjoEC_segment3", EntityProperty.EC_RESPONSE.camelName, "", MusicEventType.finish.name(), "banjo_e0", "Banjo");
			atdbCon.insertEntityRefsPropertyValue("banjoEC", EntityProperty.EC_ITEMS.camelName, List.of("banjoEC_segment1", "banjoEC_segment2",
					"banjoEC_segment3"));
		});

		final int ecCount = con.getAllEventChains().size();
		assertEquals(4, ecCount);
		final String segment1_stimulus = con.getEventChainStimulus("banjoEC_segment1");
		assertEquals("banjo_c0_start", segment1_stimulus);
		final String segment2_response = con.getEventChainResponse("banjoEC_segment2");
		assertEquals("banjo_e0_start", segment2_response);
		final String segment3_stimulus = con.getEventChainStimulus("banjoEC_segment3");
		assertEquals("banjo_e0_start", segment3_stimulus);
		final List<String> banjoEC_items = con.getEventChainItems("banjoEC");
		assertEquals(3, banjoEC_items.size());
		assertEquals(con.getEventChainStimulus(banjoEC_items.get(0)), segment1_stimulus);
		assertEquals(con.getEventChainResponse(banjoEC_items.get(1)), segment2_response);
		assertEquals(con.getEventChainStimulus(banjoEC_items.get(2)), segment3_stimulus);
		final int minItemsCompleted = con.getEventChainMinItemsCompleted("banjoEC");
		assertEquals(0, minItemsCompleted);
	}

	/**
	 * Closes db connection and checks that the file exists afterwards.
	 * @throws SQLException
	 */
	@Test
	public void t99_shutdown() throws SQLException {
		con.close();
		final File atdbFile = new File(fileName);
		assertTrue(atdbFile.isFile() && atdbFile.exists());
		atdbFile.deleteOnExit();
	}

	private static long countRows(final String query) throws SQLException {
		final AtomicLong count = new AtomicLong();
		con.queryAndConsumeResult(query, rs -> {
			while (rs.next()) {
				count.incrementAndGet();
			}
		});
		return count.get();
	}

}
