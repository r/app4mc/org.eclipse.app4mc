/**
 ********************************************************************************
 * Copyright (c) 2022 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb.tests;

public enum MusicEventType {
	start,
	finish,
	set_volume,
	set_sustain
}
