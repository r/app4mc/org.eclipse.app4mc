/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.software;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks if an label is mapped to a memory node
 *
 * <ul>
 * <li>Label access must be either read or write</li>
 * </ul>
 */

@Validation(
		id = "Sim-Software-AbstractMemoryElementIsMapped",
		checks = { "Checks if label access type is set" })

public class SimSoftwareLabelAccessType extends AmaltheaValidation {

	public static final String MESSAGE = "Label access must be either read or write";

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getLabelAccess();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof LabelAccess) {
			LabelAccess labelAccess = (LabelAccess) eObject;
			if (!(labelAccess.getAccess() == LabelAccessEnum.READ
					|| labelAccess.getAccess() == LabelAccessEnum.WRITE)) {
				addIssue(results, labelAccess, null, MESSAGE);
			}
		}
	}

}
