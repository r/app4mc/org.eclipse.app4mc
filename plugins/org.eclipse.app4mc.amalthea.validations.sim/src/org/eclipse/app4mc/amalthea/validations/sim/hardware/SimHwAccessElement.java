/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.hardware;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the sanity of HwAccessElement attributes.
 *
 * <ul>
 * <li>Either hwAccessPath must be set or at least one of read latency and datarate must be set</li>
 * <li>Either hwAccessPath must be set or at least one of write latency and datarate must be set</li>
 * </ul>
 */

@Validation(id = "Sim-HW-AccessElement", 
			checks = {"Either hwAccessPath must be set or at least one of read latency and datarate must be set. Either hwAccessPath must be set or at least one of write latency and datarate must be set." })

public class SimHwAccessElement extends AmaltheaValidation {


	public static final String READ_ACCESS_MSG = ": Either hwAccessPath must be set or at least one of read latency and datarate must be set.";
	public static final String WRITE_ACCESS_MSG = ": Either hwAccessPath must be set or at least one of write latency and datarate must be set.";
	
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getHwAccessElement();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof HwAccessElement) {
			HwAccessElement accessElem = (HwAccessElement) eObject;
			
			if(accessElem.getAccessPath() == null) {
				if (accessElem.getReadLatency() == null && accessElem.getDataRate() == null) {
					addIssue(results, accessElem, ePackage.getHwAccessElement_ReadLatency(),
							objectInfo(accessElem) + READ_ACCESS_MSG);
				}
	
				if (accessElem.getWriteLatency() == null && accessElem.getDataRate() == null) {
					addIssue(results, accessElem, ePackage.getHwAccessElement_WriteLatency(),
							objectInfo(accessElem) + WRITE_ACCESS_MSG);
				}
			}
		}
	}

}
