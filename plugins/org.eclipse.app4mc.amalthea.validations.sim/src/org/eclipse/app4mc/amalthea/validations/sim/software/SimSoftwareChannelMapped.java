/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.software;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.util.DeploymentUtil;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks if an channel is mapped to a memory
 *
 * <ul>
 * <li>Channel must be mapped to a memory for simulation</li>
 * </ul>
 */

@Validation(
		id = "Sim-Software-AbstractMemoryElementIsMapped",
		checks = { "Checks if channel is mapped to a Memory" })

public class SimSoftwareChannelMapped extends AmaltheaValidation {

	public static final String MESSAGE = "Channel is not mapped to a Memory";

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getChannel();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof Channel) {
			Channel channel = (Channel) eObject;

			if (!DeploymentUtil.isMapped(channel)) {
				addIssue(results, channel, null, MESSAGE);
			}
		}
	}

}
