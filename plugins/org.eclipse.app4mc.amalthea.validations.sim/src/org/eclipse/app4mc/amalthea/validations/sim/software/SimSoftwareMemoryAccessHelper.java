/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.software;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.AbstractMemoryElement;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.IExecutable;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.util.DeploymentUtil;
import org.eclipse.emf.common.util.EList;

/**
 * Checks feasibility of a memory access (e.g. label, modelabel or channel)
 */

class SimSoftwareMemoryAccessHelper {

	// Suppress default constructor
	private SimSoftwareMemoryAccessHelper() {
		throw new IllegalStateException("Utility class");
	}

	public static boolean validate(final ActivityGraphItem activityGraphItem, final AbstractMemoryElement abstractMemoryElement) {
		//store all memories the label is mapped to (1 entry expected)
		final Set<Memory> memories = DeploymentUtil.getMapping(abstractMemoryElement);
		if (memories.isEmpty() || memories.size() > 1) {
			/*
			 * No memory location was found for this label. As this check is part of another validation we will
			 * return at this point without adding an issue. This validation may be re-run when the mapping is
			 * complete.
			 */
			return true;
		}

		// store all processing units
		// label accesses might be contained in activity graphs within runnables or
		// processes (tasks or ISRs)
		HashSet<ProcessingUnit> processingUnits = new HashSet<>();
		Runnable containingRunnable = getContainingRunnable(activityGraphItem);
		if (containingRunnable != null) {
			EList<RunnableCall> runnableCalls = containingRunnable.getRunnableCalls();
			for (RunnableCall runnableCall : runnableCalls) {
				Process containingProcess = getContainingProcess(runnableCall);
				if (containingProcess != null) {
					processingUnits.addAll(DeploymentUtil.getAssignedCoreForProcess(containingProcess));
				}
			}
		} else {
			Process containingProcess = getContainingProcess(activityGraphItem);
			if (containingProcess != null) {
				processingUnits.addAll(DeploymentUtil.getAssignedCoreForProcess(containingProcess));
			} else if (activityGraphItem.eContainingFeature() instanceof Scheduler) {
				Scheduler scheduler = (Scheduler) activityGraphItem.eContainingFeature();
				for (SchedulerAllocation schedulerAllocation : AmaltheaIndex.getReferringObjects(scheduler,
						SchedulerAllocation.class)) {
					processingUnits.addAll(schedulerAllocation.getResponsibility());
				}
			}
		}

		if (processingUnits.isEmpty()) {
			/*
			 * 	No executing was found for the (task/ISR) contexts in which this activity item's runnable may be launched.
			 *	This is not per se a problem within this validation since the mapping of tasks to schedulers, and schedulers to cores
			 * 	is handled in other validations. However it prevents this validation from checking, if a valid hw access element has been defined.
			 *	Therefore we return at this point without adding issues, only when the mappings are complete, this needs to be evaluated as
			 * 	a task/scheduler combination that is not fully mapped will not be brought to execution. When the mapping is complete this validation
			 * 	may be re-run to completion.
			 *
			*/
			return true;
		}

		for (ProcessingUnit processingUnit : processingUnits) {
			for (Memory memory : memories) {
				Set<HwAccessElement> hwAccessElements =
						AmaltheaIndex.getReferringObjects(memory,HwAccessElement.class);
				boolean foundAccessElement = false;
				for (HwAccessElement hwAccessElement : hwAccessElements) {
					if (hwAccessElement.getSource() == processingUnit) {
						foundAccessElement = true;
						break;
					}
				}
				if (!foundAccessElement) { // unable to locate and access element that links Processing unit and memory
					return false;
				}
			}
		}
		return true;
	}

	private static Process getContainingProcess(ActivityGraphItem item) {
		IExecutable exec = item.getContainingExecutable();
		return (exec instanceof Process) ? (Process) exec : null;
	}

	private static Runnable getContainingRunnable(ActivityGraphItem item) {
		IExecutable exec = item.getContainingExecutable();
		return (exec instanceof Runnable) ? (Runnable) exec : null;
	}

}
