/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.basic;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.Channel;
import org.eclipse.app4mc.amalthea.model.ChannelEvent;
import org.eclipse.app4mc.amalthea.model.ChannelFillCondition;
import org.eclipse.app4mc.amalthea.model.ChannelReceive;
import org.eclipse.app4mc.amalthea.model.ChannelSend;
import org.eclipse.app4mc.amalthea.model.ConditionConjunction;
import org.eclipse.app4mc.amalthea.model.ConditionDisjunction;
import org.eclipse.app4mc.amalthea.model.ConnectionHandler;
import org.eclipse.app4mc.amalthea.model.DataRate;
import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.EventModel;
import org.eclipse.app4mc.amalthea.model.EventStimulus;
import org.eclipse.app4mc.amalthea.model.Frequency;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.HwAccessPath;
import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.model.HwFeature;
import org.eclipse.app4mc.amalthea.model.HwModule;
import org.eclipse.app4mc.amalthea.model.HwPathElement;
import org.eclipse.app4mc.amalthea.model.HwPort;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.IActivityGraphItemContainer;
import org.eclipse.app4mc.amalthea.model.IContinuousValueDeviation;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.ITimeDeviation;
import org.eclipse.app4mc.amalthea.model.IntegerObject;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.MemoryDefinition;
import org.eclipse.app4mc.amalthea.model.MemoryMapping;
import org.eclipse.app4mc.amalthea.model.Mode;
import org.eclipse.app4mc.amalthea.model.ModeCondition;
import org.eclipse.app4mc.amalthea.model.ModeLabel;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeLiteral;
import org.eclipse.app4mc.amalthea.model.NumericStatistic;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.RelativePeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.Semaphore;
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess;
import org.eclipse.app4mc.amalthea.model.SingleStimulus;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Switch;
import org.eclipse.app4mc.amalthea.model.SwitchEntry;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.WhileLoop;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers.Algorithm;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers.Parameter;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;

/**
 * Checks that the model elements are supported in APP4MC.sim.
 *
 * <ul>
 * <li>Checks that the model elements are supported in APP4MC.sim</li>
 * </ul>
 */

@Validation(id = "Sim-supported-model-elements", checks = {
		"Checks that the model elements are supported in APP4MC.sim" })

public class SimSupportedModelElements extends AmaltheaValidation {

	/**
	 * This set contains all model elements that have a transformer/generator class
	 * for APP4MC.sim
	 */
	private static final Set<Class<?>> EXPLICITLY_TRANSFORMED_ELEMENTS = Set.of(
			ActivityGraph.class, ActivityGraphItem.class, Amalthea.class, Channel.class,
			ChannelEvent.class, ChannelReceive.class, ChannelSend.class, ChannelFillCondition.class,
			ConnectionHandler.class, EventModel.class, EventStimulus.class, FrequencyDomain.class, Group.class,
			HwAccessElement.class, HwConnection.class, HwFeature.class, HWModel.class, HwModule.class,
			HwPathElement.class, HwPort.class, HwStructure.class, IActivityGraphItemContainer.class,
			IContinuousValueDeviation.class, IDiscreteValueDeviation.class, InterProcessStimulus.class,
			InterProcessTrigger.class, ITimeDeviation.class, Label.class, LabelAccess.class, MappingModel.class,
			Memory.class, MemoryMapping.class, Mode.class, ModeCondition.class, ModeLabel.class,
			ModeLabelAccess.class, ModeLiteral.class, NumericStatistic.class, OperatingSystem.class,
			OSModel.class, PeriodicStimulus.class, ProcessingUnit.class, ProcessingUnitDefinition.class,
			RelativePeriodicStimulus.class, Runnable.class, SchedulerAllocation.class, Semaphore.class,
			SemaphoreAccess.class, SingleStimulus.class, StimuliModel.class, Stimulus.class, Switch.class,
			SwitchEntry.class, SWModel.class, Task.class, TaskAllocation.class, TaskScheduler.class,
			Ticks.class, WhileLoop.class);

	/**
	 * This set contains all model elements that do not have a transformer/generator
	 * class, but are transformed implicitly in one of the transformer classes
	 */
	private static final Set<Class<?>> IMPLICITLY_TRANSFORMED_ELEMENTS = Set.of(
			ConditionConjunction.class, ConditionDisjunction.class, DataRate.class,
			DataSize.class, Frequency.class, HwAccessPath.class, IntegerObject.class,
			MemoryDefinition.class, Time.class);

	/**
	 * This set is automatically filled to contain the union of the sets of model
	 * elements that are implicitly or explicitly transformed and are, therefore,
	 * white-listed for APP4MC.sim
	 */
	private static final Set<Class<?>> WHITE_LISTED_ELEMENTS = Stream
			.concat(EXPLICITLY_TRANSFORMED_ELEMENTS.stream(), IMPLICITLY_TRANSFORMED_ELEMENTS.stream())
			.collect(Collectors.toSet());

	/**
	 * This set contains all standard schedulers that have transformation support
	 * and are supported in APP4MC.sim
	 */
	private static final Set<Algorithm> WHITE_LISTED_SCHEDULERS = Set.of(
			Algorithm.FIXED_PRIORITY_PREEMPTIVE, Algorithm.PRIORITY_BASED_ROUND_ROBIN);

	/**
	 * This set contains all scheduling parameter that have transformation support
	 * and are supported in APP4MC.sim
	 */
	private static final Set<Parameter> WHITE_LISTED_SCHEDULING_PARAMS = Set.of(
			Parameter.PRIORITY, Parameter.TIME_SLICE_LENGTH);

	@Override
	public EPackage getEPackage() {
		return AmaltheaPackage.eINSTANCE;
	}

	@Override
	public EClassifier getEClassifier() {
		return EcorePackage.eINSTANCE.getEObject();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject.eClass().eContainer() == getEPackage()) {

			// check if it is a white-listed element
			if (WHITE_LISTED_ELEMENTS.stream().anyMatch(clazz -> clazz.isInstance(eObject))) {
				return;
			}

			// check if it is a white-listed schedulerDefinitions
			if (eObject instanceof SchedulerDefinition) {
				String schedName = ((SchedulerDefinition) eObject).getName();
				if (WHITE_LISTED_SCHEDULERS.stream().anyMatch(sched -> sched.getAlgorithmName().equals(schedName))) {
					return;
				}
			}

			// check if it is the "definition" of a white-listed
			// scheduleingParameters
			if (eObject instanceof SchedulingParameterDefinition) {
				String paramName = ((SchedulingParameterDefinition) eObject).getName();
				if (WHITE_LISTED_SCHEDULING_PARAMS.stream()
						.anyMatch(param -> param.getParameterName().equals(paramName))) {
					return;
				}
			}

			// check if it is the "usage" of a white-listed schedulingParameter
			if (eObject instanceof Map.Entry<?, ?>
					&& ((Map.Entry<?, ?>) eObject).getKey() instanceof SchedulingParameterDefinition) {
				String paramName = ((SchedulingParameterDefinition) ((Map.Entry<?, ?>) eObject).getKey()).getName();
				if (WHITE_LISTED_SCHEDULING_PARAMS.stream()
						.anyMatch(param -> param.getParameterName().equals(paramName))) {
					return;
				}
			}

			addIssue(results, eObject, null, objectInfo(eObject) + ": Model element not supported in APP4MC.sim");

		}
	}

}
