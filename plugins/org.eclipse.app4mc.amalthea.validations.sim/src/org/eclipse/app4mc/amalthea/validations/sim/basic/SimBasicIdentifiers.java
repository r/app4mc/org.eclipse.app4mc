/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.basic;

import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the sanity of HwConnection attributes.
 *
 * <ul>
 * <li>All names of IReferable objects must be valid C++ identifier names</li>
 * </ul>
 */

@Validation(id = "Sim-Basic-Identifiers", checks = {
		"All names of IReferable objects must be valid C++ identifier names" })

public class SimBasicIdentifiers extends AmaltheaValidation {

	// C++20 keywords (or reserved words), see https://www.learncpp.com/cpp-tutorial/keywords-and-naming-identifiers/
	private static final Set<String> KEYWORDS = Set.of("alignas", "alignof", "and", "and_eq",
			"asm", "auto", "bitand", "bitor", "bool", "break", "case", "catch", "char", "char8_t", "char16_t",
			"char32_t", "class", "compl", "concept", "const", "consteval", "constexpr", "constinit", "const_cast",
			"continue", "co_await", "co_return", "co_yield", "decltype", "default", "delete", "do", "double",
			"dynamic_cast", "else", "enum", "explicit", "export", "extern", "false", "float", "for", "friend", "goto",
			"if", "inline", "int", "long", "mutable", "namespace", "new", "noexcept", "not", "not_eq", "nullptr",
			"operator", "or", "or_eq", "private", "protected", "public", "register", "reinterpret_cast", "requires",
			"return", "short", "signed", "sizeof", "static", "static_assert", "static_cast", "struct", "switch",
			"template", "this", "thread_local", "throw", "true", "try", "typedef", "typeid", "typename", "union",
			"unsigned", "using", "virtual", "void", "volatile", "wchar_t", "while", "xor", "xor_eq");

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getIReferable();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof IReferable) {
			IReferable ref = (IReferable) eObject;

			if (!validIdentifier(ref.getName())) {
				addIssue(results, ref, ePackage.getINamed_Name(),
						objectInfo(ref) + ": Invalid name (not a valid C++ identifier)");
			}
		}
	}

	private boolean validIdentifier(String name) {
		if (name == null || name.isEmpty() || KEYWORDS.contains(name))
			return false;

		char firstChar = name.charAt(0);
		if (firstChar != '_' && !Character.isLetter(firstChar))
			return false;

		for (int i = 1; i < name.length(); i++) {
			char c = name.charAt(i);
			if (c != '_' && !Character.isLetter(c) && !Character.isDigit(c))
				return false;
		}

		return true;
	}

}
