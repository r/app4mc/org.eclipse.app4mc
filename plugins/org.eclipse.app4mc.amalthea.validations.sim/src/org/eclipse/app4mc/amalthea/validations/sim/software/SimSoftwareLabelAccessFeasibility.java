/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.software;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.AbstractMemoryElement;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks feasibility of a memory access (label or channel)
 *
 * <ul>
 * <li>Checks if a label access is feasible for simulation. This requires that
 * its memory location is accessible from each core, the access can be initiated
 * from.</li>
 * </ul>
 */

@Validation(
		id = "Sim-Software-LabelAccessFeasibility",
		checks = { "Checks if a label access can be performed from certain runnable" })

public class SimSoftwareLabelAccessFeasibility extends AmaltheaValidation {

	public static final String MESSAGE = "Label's memory location is not accessible from each core, onto which this activity item is deployed";

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getLabelAccess();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof LabelAccess) {
			LabelAccess labelAccess = (LabelAccess) eObject;

			if (labelAccess.getData() == null)
				return; // data property not set is handled in another validation
			final AbstractMemoryElement abstractMemoryElement = labelAccess.getData();

			if (!SimSoftwareMemoryAccessHelper.validate(labelAccess, abstractMemoryElement)) {
				addIssue(results, labelAccess, ePackage.getLabelAccess_Data(), MESSAGE);
			}
		}
	}

}
