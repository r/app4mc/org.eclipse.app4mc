/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim;

import org.eclipse.app4mc.amalthea.validations.inchron.hw.InchronHWModuleMissingClock;
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwAccessElement;
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwConnection;
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwMemoryDefinition;
import org.eclipse.app4mc.amalthea.validations.sim.hardware.SimHwProcessingUnit;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "Hardware Validations (APP4MC.sim)")

@ValidationGroup(
	severity = Severity.ERROR,
	validations =  {
		InchronHWModuleMissingClock.class,
		SimHwProcessingUnit.class,
	}
)
@ValidationGroup(
	severity = Severity.WARNING,
	validations = {
		SimHwConnection.class,
		SimHwAccessElement.class,
		SimHwMemoryDefinition.class,
	}
)
public class SimHardwareProfile implements IProfile {
	// Do nothing
}
