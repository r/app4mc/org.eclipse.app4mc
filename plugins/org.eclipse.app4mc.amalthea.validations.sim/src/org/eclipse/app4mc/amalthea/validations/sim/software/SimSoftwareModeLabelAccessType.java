/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.software;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.ModeLabelAccess;
import org.eclipse.app4mc.amalthea.model.ModeLabelAccessEnum;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;


/**
 * Checks if an modeLabel access has valid access type
 * (read/set/increment/decrement)
 *
 * <ul>
 * <li>ModeLabel access must be either read or write</li>
 * </ul>
 */

@Validation(
		id = "Sim-Software-AbstractMemoryElementIsMapped",
		checks = { "Checks if modeLabel access type is valid" })

public class SimSoftwareModeLabelAccessType extends AmaltheaValidation {

	public static final String MESSAGE = "ModeLabel access must be read, set, increment or decrement";

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getModeLabelAccess();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof ModeLabelAccess) {
			ModeLabelAccess modeLabelAccess= (ModeLabelAccess)eObject;

//			if (!(modeLabelAccess.getAccess() == ModeLabelAccessEnum.READ ||
//					modeLabelAccess.getAccess() == ModeLabelAccessEnum.SET||
//					modeLabelAccess.getAccess() == ModeLabelAccessEnum.INCREMENT||
//					modeLabelAccess.getAccess() == ModeLabelAccessEnum.DECREMENT)) {
//				addIssue(results, modeLabelAccess, null, getMessage());
//			}

			if (modeLabelAccess.getAccess() == null || modeLabelAccess.getAccess() == ModeLabelAccessEnum._UNDEFINED_) {
				addIssue(results, modeLabelAccess, null, MESSAGE);
			}
		}
	}

}
