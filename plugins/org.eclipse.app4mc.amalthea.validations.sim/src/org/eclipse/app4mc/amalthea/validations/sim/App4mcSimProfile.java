/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim;

import org.eclipse.app4mc.amalthea.validations.standard.AmaltheaProfile;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ProfileGroup;
import org.eclipse.app4mc.validation.core.IProfileConfiguration;
import org.osgi.service.component.annotations.Component;

/**
 * Validations for AMALTHEA models used in a Timing Architects Simulation
 */
@Profile(
	name = "APP4MC.sim Validations",
	description = "Validations for AMALTHEA models used in a APP4MC.sim simulation."
)

@ProfileGroup(
	profiles = {
		AmaltheaProfile.class,
		SimBasicProfile.class,
		SimHardwareProfile.class,
		SimSoftwareProfile.class,
		SimMappingProfile.class,
		SimOsProfile.class
	}
)

@Component
public class App4mcSimProfile implements IProfileConfiguration {
    // Do nothing
}
