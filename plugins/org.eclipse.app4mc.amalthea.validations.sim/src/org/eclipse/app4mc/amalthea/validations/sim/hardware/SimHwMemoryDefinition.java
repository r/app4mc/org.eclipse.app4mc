/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.hardware;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.MemoryDefinition;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the sanity of MemoryDefinition attributes.
 *
 * <ul>
 * <li>Access latency must be set</li>
 * <li>Data rate must be set</li>
 * </ul>
 */

@Validation(
		id = "Sim-HW-MemoryDefinition",
		checks = {	"Either access latency or datarate (or both) must be set" })

public class SimHwMemoryDefinition extends AmaltheaValidation {

	public static final String ACCESS_MSG = ": Either access latency or datarate (or both) must be set";
	
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getMemoryDefinition();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof MemoryDefinition) {
			MemoryDefinition def = (MemoryDefinition) eObject;

			if (def.getAccessLatency() == null && def.getDataRate() == null) {
				addIssue(results, def, ePackage.getMemoryDefinition_AccessLatency(),
						objectInfo(def) + ACCESS_MSG);
			}
		}
	}

}
