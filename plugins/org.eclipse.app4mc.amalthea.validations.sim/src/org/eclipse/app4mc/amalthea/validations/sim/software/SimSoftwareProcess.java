/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.software;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the sanity of process attributes.
 *
 * <ul>
 * <li>At least one stimulus must be set</li>
 * </ul>
 */

@Validation(
		id = "Sim-Software-Process",
		checks = { "At least one stimulus must be set" })

public class SimSoftwareProcess extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getProcess();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof Process) {
			Process proc = (Process) eObject;

			if (proc.getStimuli().isEmpty()) {
				addIssue(results, proc, ePackage.getProcess_Stimuli(), "Process " + objectInfo(proc) + " has no stimulus");
			}
		}
	}

}
