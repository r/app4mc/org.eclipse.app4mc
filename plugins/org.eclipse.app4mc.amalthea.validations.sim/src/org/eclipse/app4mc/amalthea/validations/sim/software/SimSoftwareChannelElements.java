/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.software;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.ChannelAccess;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks if channel access's property elements is greater 0
 *
 * <ul>
 * <li>Channel must specify the number of elements to be send/received, with elements > 0</li> T
 * </ul>
 */

@Validation(
		id = "Sim-Software-ChannelElements",
		checks = { "Checks if channel access's property elements is greater 0" })

public class SimSoftwareChannelElements extends AmaltheaValidation {

	public static final String MESSAGE = "Channel access must specify elements > 0";

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getChannelAccess();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof ChannelAccess) {
			ChannelAccess channelAccess = (ChannelAccess) eObject;
			if (channelAccess.getElements() < 1) {
				addIssue(results, channelAccess, null, MESSAGE);

			}
		}
	}

}
