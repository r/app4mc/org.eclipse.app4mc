/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.hardware;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the sanity of HwConnection attributes.
 *
 * <ul>
 * <li>Either read latency or data rate or both must be set</li>
 * <li>Either write latency or data rate or both must be set</li>
 * </ul>
 */

@Validation(
		id = "Sim-HW-Connection",
		checks = {	"Either read latency or data rate or both must be set. Either write latency or data rate or both must be set."})

public class SimHwConnection extends AmaltheaValidation {

	public static final String READ_ACCESS_MSG = ": Either read latency or data rate or both must be set";
	public static final String WRITE_ACCESS_MSG = ": Either write latency or data rate or both must be set";
	
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getHwConnection();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof HwConnection) {
			HwConnection con = (HwConnection) eObject;

			if (con.getReadLatency() == null && con.getDataRate() == null) {
				addIssue(results, con, ePackage.getHwConnection_ReadLatency(),
						objectInfo(con) + READ_ACCESS_MSG);
			}

			if (con.getWriteLatency() == null && con.getDataRate() == null) {
				addIssue(results, con, ePackage.getHwConnection_WriteLatency(),
						objectInfo(con) + WRITE_ACCESS_MSG);
			}
		}
	}

}
