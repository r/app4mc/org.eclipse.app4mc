/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.sim.mapping;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the sanity of scheduler allocations.
 *
 * <ul>
 * <li>Executing processing unit must be set</li>
 * </ul>
 */

@Validation(
		id = "Sim-Mapping-SchedulerAllocation",
		checks = { "Executing processing unit must be set" })

public class SimMappingSchedulerAllocation_ExecutingPuSet extends AmaltheaValidation {
	
	public static final String MESSAGE_EXECUTING_PU_NOT_SET = "SchedulerAllocation: Executing processing unit must be set";

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSchedulerAllocation();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof SchedulerAllocation) {
			SchedulerAllocation alloc = (SchedulerAllocation) eObject;

			if (alloc.getExecutingPU() == null) {
				addIssue(results, alloc, ePackage.getSchedulerAllocation_ExecutingPU(), MESSAGE_EXECUTING_PU_NOT_SET);
			}

		}
	}

}
