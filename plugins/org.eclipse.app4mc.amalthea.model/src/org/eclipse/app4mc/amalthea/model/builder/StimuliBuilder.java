/**
 ********************************************************************************
 * Copyright (c) 2016-2019 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.builder;

import java.util.function.Consumer;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ClockStepList;
import org.eclipse.app4mc.amalthea.model.PeriodicStimulus;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.app4mc.amalthea.model.VariableRateStimulus;

public class StimuliBuilder {

	public void periodicStimulus(final StimuliModel container, Consumer<PeriodicStimulus> initializer) {
		final PeriodicStimulus obj = AmaltheaFactory.eINSTANCE.createPeriodicStimulus();
		container.getStimuli().add(obj);
		initializer.accept(obj);
	}

	public void variableRateStimulus(final StimuliModel container, Consumer<VariableRateStimulus> initializer) {
		final VariableRateStimulus obj = AmaltheaFactory.eINSTANCE.createVariableRateStimulus();
		container.getStimuli().add(obj);
		initializer.accept(obj);
	}

	public void clockStepList(final StimuliModel container, Consumer<ClockStepList> initializer) {
		final ClockStepList obj = AmaltheaFactory.eINSTANCE.createClockStepList();
		container.getClocks().add(obj);
		initializer.accept(obj);
	}

}
