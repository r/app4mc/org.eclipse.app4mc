/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AbstractMemoryElement;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.HwFeature;
import org.eclipse.app4mc.amalthea.model.HwFeatureCategory;
import org.eclipse.app4mc.amalthea.model.ISR;
import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.MemoryMapping;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jdt.annotation.NonNull;

public class DeploymentUtil {

	// Suppress default constructor
	private DeploymentUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static Set<Process> getProcessesMappedToCore(final @NonNull ProcessingUnit core, final @NonNull  Amalthea model) {
		Set<Process> result = new HashSet<>();
		result.addAll(getTasksMappedToCore(core, model));
		result.addAll(getISRsMappedToCore(core, model));
		return result;
	}

	/**
	 * Returns a set of tasks mapped to a core. Depends on responsibilities of
	 * schedulers and the task allocated to them Assumption: Scheduler
	 * responsibilities are set empty core affinities are ignored otherwise the
	 * intersection of core affinity and scheduler responsibility is returned
	 *
	 * @return Set of tasks
	 */
	public static Set<Task> getTasksMappedToCore(final @NonNull ProcessingUnit core, final @NonNull Amalthea model) { /// called it mapping
		Set<Task> tasks = new HashSet<>();
		MappingModel mappingModel = model.getMappingModel();
		if (mappingModel == null)
			return tasks;

		// first find all schedulers responsible for the core
		Set<TaskScheduler> schedulers = new HashSet<>();
		for (SchedulerAllocation schedAlloc : mappingModel.getSchedulerAllocation()) {
			if (schedAlloc.getScheduler() instanceof TaskScheduler && schedAlloc.getResponsibility().contains(core)) {
				schedulers.add((TaskScheduler) schedAlloc.getScheduler());
			}
		}

		// check for all task allocations if the assigned scheduler is in the list
		for (TaskAllocation taskAlloc : mappingModel.getTaskAllocation()) {
			if (schedulers.contains(taskAlloc.getScheduler())) {
				// check core affinities - if affinities are empty the responsibility counts
				// otherwise the affinity also needs to contain the core
				if (taskAlloc.getAffinity().isEmpty() || taskAlloc.getAffinity().contains(core)) {
					tasks.add(taskAlloc.getTask());
				}
			}
		}
		return tasks;
	}

	/**
	 * Returns a set of all ISR mapped to that core
	 *
	 * @return Set of interrupt service routines (ISR)
	 */
	public static Set<ISR> getISRsMappedToCore(final @NonNull ProcessingUnit core, final @NonNull Amalthea model) {
		if (core == null || model.getSwModel() == null || model.getSwModel().getIsrs() == null
				|| model.getMappingModel() == null || model.getMappingModel().getIsrAllocation() == null)
			return null;

		Set<ISR> result = new HashSet<>();
		EList<ISRAllocation> isrAlloc = model.getMappingModel().getIsrAllocation();
		EList<SchedulerAllocation> schedulerAllocs = model.getMappingModel().getSchedulerAllocation();
		for (ISR isr : model.getSwModel().getIsrs()) {
			for (ISRAllocation ia : isrAlloc) {
				if (ia.getIsr()!=null && ia.getIsr().equals(isr)) {
					for (SchedulerAllocation coreAlloc : schedulerAllocs) {
						if (coreAlloc.getResponsibility().contains(core) && coreAlloc.getScheduler() == ia.getController()) {
							result.add(isr);

						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * Returns a list of all allocations of a task
	 *
	 * @return List of task allocations
	 */
	public static List<TaskAllocation> getTaskAllocations(final @NonNull Task task, final @NonNull Amalthea model) {
		List<TaskAllocation> allocs = new ArrayList<>();
		for (TaskAllocation ta : model.getMappingModel().getTaskAllocation()) {
			if (ta.getTask()!=null && ta.getTask().equals(task)) {
				allocs.add(ta);
			}
		}
		return allocs;
	}

	/**
	 * Returns a list of all allocations of a task
	 *
	 * @return List of task allocations
	 */
	public static Set<TaskAllocation> getTaskAllocations(final @NonNull Task task) {
		return AmaltheaIndex.getReferringObjects(task, TaskAllocation.class);

	}

	/**
	 * Returns a list of all allocations of a isr
	 *
	 * @return List of isr allocations
	 */
	public static Set<ISRAllocation> getIsrAllocations(final @NonNull Process isr) {
		return AmaltheaIndex.getReferringObjects(isr, ISRAllocation.class);
	}

	/**
	 * Returns true if at least a memory element mapping exists
	 */
	public static boolean isMapped(final @NonNull AbstractMemoryElement element) {
		return !getMapping(element).isEmpty();
	}

	/**
	 * Set of memories the abstract element is mapped to (should be only one!)
	 * if memory mapping is incomplete (memory is null) -> neglect
	 *
	 * @return Set of Memories
	 */
	public static Set<Memory> getMapping(final @NonNull AbstractMemoryElement element) {
		return element.getMappings().stream()
				.map(MemoryMapping::getMemory)
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
	}

	/**
	 * Set of memories the label is mapped to (should be only one!)
	 *
	 * @return Set of Memories
	 */
	public static Set<Memory> getLabelMapping(final @NonNull Label label) {
		return getMapping(label);
	}

	/**
	 * Returns a created Memory element which was already added to the model
	 *
	 * @return MemoryMapping
	 */
	public static MemoryMapping setMapping(final @NonNull AbstractMemoryElement element, final @NonNull Memory mem, final @NonNull Amalthea model) {
		if (mem == null || element == null)
			return null;

		MemoryMapping mapping = AmaltheaFactory.eINSTANCE.createMemoryMapping();
		mapping.setAbstractElement(element);
		mapping.setMemory(mem);
		ModelUtil.getOrCreateMappingModel(model).getMemoryMapping().add(mapping);
		return mapping;
	}

	/**
	 * Returns a created LabelMapping element which was already added to the model
	 *
	 * @return MemoryMapping
	 */
	public static MemoryMapping setLabelMapping(final @NonNull Label label, final @NonNull Memory mem, final @NonNull Amalthea model) {
		return setMapping(label, mem, model);
	}

	/**
	 * Returns the cores the process is assigned to. Empty core affinities are
	 * ignored, otherwise the intersection of core affinity and scheduler
	 * responsibility is returned.
	 *
	 * @param process Task or ISR
	 * @param model the containing model
	 * @return Set of cores
	 * @deprecated
	 * This method was replaced by the function below, which does not need the containing model as a
	 * parameter, instead the Amalthea index is used to access the loaded model.
	 * use: public static Set<ProcessingUnit> getAssignedCoreForProcess(final @NonNull Process process)
	 *
	 */
	@Deprecated
	public static Set<ProcessingUnit> getAssignedCoreForProcess(Process process, Amalthea model) {
		if (model == null || process == null) return null;

		final MappingModel mappingModel = model.getMappingModel();
		if (mappingModel == null) return null;

		Set<ProcessingUnit> processingUnits = new HashSet<>();

		if (process instanceof Task) {
			getAssignedCoresForTask((Task) process, mappingModel, processingUnits);
		}

		if (process instanceof ISR) {
			getAssignedCoresForISR((ISR) process, mappingModel, processingUnits);
		}

		return processingUnits;
	}

	@Deprecated
	private static void getAssignedCoresForISR(ISR isr, final MappingModel mappingModel, Set<ProcessingUnit> processingUnits) {

		for (ISRAllocation isrAlloc : mappingModel.getIsrAllocation()) {
			if (isrAlloc.getIsr()!=null && isrAlloc.getIsr().equals(isr)) {
				for (SchedulerAllocation coreAlloc : mappingModel.getSchedulerAllocation()) {
					if (coreAlloc.getScheduler() != null && coreAlloc.getScheduler().equals(isrAlloc.getController())) {
						processingUnits.addAll(coreAlloc.getResponsibility());
					}
				}
			}
		}
	}

	@Deprecated
	private static void getAssignedCoresForTask(Task task, final MappingModel mappingModel, Set<ProcessingUnit> processingUnits) {

		Set<TaskAllocation> taskAllocations = new HashSet<>();

		for (TaskAllocation taskAlloc : mappingModel.getTaskAllocation()) {
			if (taskAlloc.getTask()!=null && taskAlloc.getTask().equals(task)) {
				taskAllocations.add(taskAlloc);
			}
		}

		for (SchedulerAllocation schedAlloc : mappingModel.getSchedulerAllocation()) {
			if (!(schedAlloc.getScheduler() instanceof TaskScheduler)) continue;

			TaskScheduler scheduler1 = (TaskScheduler) schedAlloc.getScheduler();

			for (TaskAllocation taskAlloc : taskAllocations) {
				TaskScheduler scheduler2 = taskAlloc.getScheduler();

				// check if the same scheduler manages the task and the core -- this includes hierarchical schedulers.
				if (scheduler1!=null && (scheduler1.equals(scheduler2) || scheduler1.getChildSchedulers().contains(scheduler2))) {
					// check core affinity -- retain the core affinity with the scheduler responsibility
					if (!taskAlloc.getAffinity().isEmpty()) {
						for (ProcessingUnit core : taskAlloc.getAffinity()) {
							if (schedAlloc.getResponsibility().contains(core))
								processingUnits.add(core);
						}
					} else {
						// responsibility defines core mapping
						processingUnits.addAll(schedAlloc.getResponsibility());
					}
				}
			}
		}
	}

	/**
	 * Returns the cores the process is assigned to. Empty core affinities are
	 * ignored, otherwise the intersection of core affinity and scheduler
	 * responsibility is returned.
	 *
	 * @param process Task or ISR
	 * @return Set of cores
	 */
	public static Set<ProcessingUnit> getAssignedCoreForProcess(final @NonNull Process process){

		final Set<ProcessingUnit> processingUnits = new HashSet<>();

		if (process instanceof Task) {
			getAssignedCoresForTask((Task) process,  processingUnits);
		}

		if (process instanceof ISR) {
			getAssignedCoresForISR((ISR) process,  processingUnits);
		}

		return processingUnits;
	}


	private static void getAssignedCoresForTask(final @NonNull Task task, final Set<ProcessingUnit> processingUnits) {
		final Set<Scheduler> schedulers = new HashSet<>();
		final Set<ProcessingUnit> processingUnitsFromAffinity = new HashSet<>();
		AmaltheaIndex.getReferringObjects(task, TaskAllocation.class).stream().forEach(ta -> {
			schedulers.add(ta.getScheduler());
			processingUnitsFromAffinity.addAll(ta.getAffinity());
		});

		for (Scheduler scheduler : schedulers) {
			if (scheduler != null) {
				AmaltheaIndex.getReferringObjects(scheduler, SchedulerAllocation.class).stream()
					.forEach(schedulerAllocation -> processingUnits.addAll(schedulerAllocation.getResponsibility()));
			}
		}
		if (!processingUnitsFromAffinity.isEmpty()) {
			processingUnits.removeIf(pu -> !processingUnitsFromAffinity.contains(pu));
		}
	}

	private static void getAssignedCoresForISR(final @NonNull ISR isr, final Set<ProcessingUnit> processingUnits) {
		final Set<InterruptController> controllers = new HashSet<>();
		AmaltheaIndex.getReferringObjects(isr, ISRAllocation.class).stream()
			.forEach(isrAllocation -> controllers.add(isrAllocation.getController()));

		for (Scheduler controller : controllers) {
			if (controller != null) {
				AmaltheaIndex.getReferringObjects(controller, SchedulerAllocation.class).stream()
					.forEach(sa -> processingUnits.addAll(sa.getResponsibility()));
			}
		}
	}


	/**
	 * @param procUnitDef	 processing unit definition
	 */
	public static List<HwFeatureCategory> getFeatureCategories(ProcessingUnitDefinition procUnitDef) {
		List<HwFeatureCategory> result = new ArrayList<>();
		for (HwFeature feature : procUnitDef.getFeatures()) {
			if (!result.contains(feature.getContainingCategory())) {
				result.add(feature.getContainingCategory());
			}
		}
		return result;
	}

	/**
	 * @param hwFeatureCat			selection criteria
	 * @param procUnitDefinitons	list of potential definitions
	 */
	public static List<ProcessingUnitDefinition> getProcessingUnitDefinitionsForHwCategories(HwFeatureCategory hwFeatureCat,
			List<ProcessingUnitDefinition> procUnitDefinitons) {
		List<ProcessingUnitDefinition> result = new ArrayList<>();
		for (ProcessingUnitDefinition procUnitDef : procUnitDefinitons) {
			for (HwFeature feature : hwFeatureCat.getFeatures()) {
				if (procUnitDef.getFeatures().contains(feature))
					result.add(procUnitDef);
			}
		}
		return result;
	}

	/**
	 * @param feature				selection criteria
	 * @param procUnitDefinitons	list of potential definitions
	 */
	public static List<ProcessingUnitDefinition> getProcessingUnitDefinitionsForHwFeature(HwFeature feature,
			List<ProcessingUnitDefinition> procUnitDefinitons) {
		List<ProcessingUnitDefinition> result = new ArrayList<>();
		for (ProcessingUnitDefinition procUnitDef : procUnitDefinitons) {
			if (procUnitDef.getFeatures().contains(feature))
				result.add(procUnitDef);
		}
		return result;
	}

}
