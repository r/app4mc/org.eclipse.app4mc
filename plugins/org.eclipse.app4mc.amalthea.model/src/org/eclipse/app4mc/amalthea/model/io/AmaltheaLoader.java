/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.io;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.emf.AmaltheaResource;
import org.eclipse.app4mc.amalthea.model.emf.AmaltheaResourceFactory;
import org.eclipse.app4mc.amalthea.model.emf.AmaltheaResourceSetImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class AmaltheaLoader {

	// Suppress default constructor
	private AmaltheaLoader() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Static method to load an AMALTHEA model from a file with a given pathname string.
	 * <p>
	 * Possible path names:
	 * <ul>
	 * <li>absolute (example: "d:/temp/model.amxmi")</li>
	 * <li>relative to project folder (example: "input/model.amxmi")</li>
	 * </ul>
	 *
	 * @param pathname	String
	 * @return
	 * 		AMALTHEA model - null if load failed
	 */
	public static Amalthea loadFromFileNamed(String pathname) {
		if (pathname == null) return null;

		final File file = new File(pathname);
		return loadFromFile(file);
	}

	/**
	 * Static method to load an AMALTHEA model from a file.
	 *
	 * @param file	standard Java file
	 * @return
	 * 		AMALTHEA model - null if load failed
	 */
	public static Amalthea loadFromFile(File file) {
		if (file == null || !file.isFile() || !file.exists()) return null;

		final URI uri = URI.createFileURI(file.getAbsolutePath());
		return loadFromURI(uri);
	}

	/**
	 * Static method to load an AMALTHEA model from a file URI.
	 *
	 * @param uri	org.eclipse.emf.common.util.URI
	 * @return
	 * 		AMALTHEA model - null if load failed
	 */
	public static Amalthea loadFromURI(URI uri) {
		if (uri == null) return null;

		final ResourceSet resSet = initializeResourceSet();
		final Resource res = resSet.createResource(uri);
		if (res == null) return null;

		try {
			res.load(null);
		} catch (IOException e) {
			// ignore
		}

		// reset cache
		if (res instanceof AmaltheaResource) {
			((AmaltheaResource) res).setIntrinsicIDToEObjectMap(null);
		}

		for (final EObject content : res.getContents()) {
			if (content instanceof Amalthea) {
				return (Amalthea) content;
			}
		}

		return null;
	}

	/**
	 * Static method to load AMALTHEA models from a directory with a given pathname string.
	 * <p>
	 * Possible path names:
	 * <ul>
	 * <li>absolute (example: "d:/temp/")</li>
	 * <li>relative to project folder (example: "input/")</li>
	 * </ul>
	 *
	 * @param pathname	String
	 * @return
	 * 		ResourceSet - null if load failed
	 */
	public static ResourceSet loadFromDirectoryNamed(String pathname) {
		if (pathname == null) return null;

		final File folder = new File(pathname);
		return loadFromDirectory(folder);
	}

	/**
	 * Static method to load AMALTHEA models from a directory.
	 *
	 * @param folder	standard Java file
	 * @return
	 * 		ResourceSet - null if load failed
	 */
	public static ResourceSet loadFromDirectory(File folder) {
		if (folder == null || !folder.isDirectory() || !folder.exists()) return null;

		File[] fileList = folder.listFiles((file, name) -> name.endsWith(".amxmi"));

		List<URI> uriList = new ArrayList<>();
		for (File amxmiFile : fileList) {
			final URI uri = URI.createFileURI(amxmiFile.getAbsolutePath());
			if (uri != null) {
				uriList.add(uri);
			}
		}

		return loadFromURIs(uriList);
	}

	/**
	 * Static method to load AMALTHEA models from a list of URIs.
	 *
	 * @param uriList	List of URIs
	 * @return
	 * 		ResourceSet - null if load failed
	 */
	public static ResourceSet loadFromURIs(List<URI> uriList) {
		if (uriList == null || uriList.isEmpty()) return null;

		final ResourceSet resSet = initializeResourceSet();

		for (URI uri : uriList) {
			final Resource res = resSet.createResource(uri);
			try {
				res.load(null);
			} catch (IOException e) {
				// ignore
			}
		}

		// resolve proxies
		EcoreUtil.resolveAll(resSet);

		// disable caches for intrinsic IDs
		for (Resource resource : resSet.getResources()) {
			if (resource instanceof AmaltheaResource) {
				((AmaltheaResource) resource).setIntrinsicIDToEObjectMap(null);
			}
		}

		return resSet;
	}

	public static Collection<Amalthea> collectAmaltheaModels(ResourceSet resourceSet) {
		Collection<Amalthea> models = new ArrayList<>();

		for (final Resource resource : resourceSet.getResources()) {
			for (final EObject model : resource.getContents()) {
				if ((model instanceof Amalthea)) {
					models.add((Amalthea) model);
				}
			}
		}

		return models;
	}

	private static ResourceSet initializeResourceSet() {
		AmaltheaPackage.eINSTANCE.eClass(); // register the package
		final ResourceSet resSet = new AmaltheaResourceSetImpl();
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("amxmi", new AmaltheaResourceFactory());

		return resSet;
	}

}
