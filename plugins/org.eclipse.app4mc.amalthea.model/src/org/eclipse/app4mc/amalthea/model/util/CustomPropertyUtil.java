/**
 ********************************************************************************
 * Copyright (c) 2015-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Strings.isNullOrEmpty;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.BooleanObject;
import org.eclipse.app4mc.amalthea.model.IAnnotatable;
import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.app4mc.amalthea.model.IntegerObject;
import org.eclipse.app4mc.amalthea.model.ListObject;
import org.eclipse.app4mc.amalthea.model.MapObject;
import org.eclipse.app4mc.amalthea.model.ReferenceObject;
import org.eclipse.app4mc.amalthea.model.StringObject;
import org.eclipse.app4mc.amalthea.model.Time;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.emf.common.util.EList;
import org.eclipse.jdt.annotation.NonNull;

import com.google.common.base.Splitter;


public final class CustomPropertyUtil {

	// Suppress default constructor
	private CustomPropertyUtil() {
		throw new IllegalStateException("Utility class");
	}

	private static final String ARG1_MESSAGE = "First argument is null, expected instance of IAnnotatable";
	private static final String ARG2_MESSAGE = "Key is null or empty, expected non empty String";

	public static Value customPut(@NonNull IAnnotatable object, @NonNull String key, boolean bool) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(key), ARG2_MESSAGE);
		
		BooleanObject valueObject;
		valueObject = FactoryUtil.createBooleanObject(bool);
		
		// return old value
		return object.getCustomProperties().put(key, valueObject);
	}

	public static Value customPut(@NonNull IAnnotatable object, @NonNull String key, int num) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(key), ARG2_MESSAGE);

		IntegerObject valueObject;
		valueObject = FactoryUtil.createIntegerObject(num);

		// return old value
		return object.getCustomProperties().put(key, valueObject);
	}

	public static Value customPut(@NonNull IAnnotatable object, @NonNull String key, String str) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(key), ARG2_MESSAGE);

		StringObject valueObject;
		valueObject = FactoryUtil.createStringObject(str);

		// return old value
		return object.getCustomProperties().put(key, valueObject);
	}

	public static Value customPut(@NonNull IAnnotatable object, @NonNull String key, Time time) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(key), ARG2_MESSAGE);

		Time valueObject;
		valueObject = FactoryUtil.createTime(time);

		// return old value
		return object.getCustomProperties().put(key, valueObject);
	}

	public static Value customPut(@NonNull IAnnotatable object, @NonNull String key, IReferable reference) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(key), ARG2_MESSAGE);

		ReferenceObject valueObject;
		valueObject = FactoryUtil.createReferenceObject(reference);

		// return old value
		return object.getCustomProperties().put(key, valueObject);
	}

	public static Boolean customGetBoolean(@NonNull IAnnotatable object, @NonNull String firstKey, String... nextKeys) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(firstKey), ARG2_MESSAGE);
		
		Value valueObject = customGetValue(object, firstKey, nextKeys);
		if (valueObject instanceof BooleanObject) {
			return ((BooleanObject) valueObject).isValue();
		} else {
			return null;
		}
	}

	public static Integer customGetInteger(@NonNull IAnnotatable object, @NonNull String firstKey, String... nextKeys) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(firstKey), ARG2_MESSAGE);

		Value valueObject = customGetValue(object, firstKey, nextKeys);
		if (valueObject instanceof IntegerObject) {
			return ((IntegerObject) valueObject).getValue();
		} else {
			return null;
		}
	}

	public static String customGetString(@NonNull IAnnotatable object, @NonNull String firstKey, String... nextKeys) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(firstKey), ARG2_MESSAGE);

		Value valueObject = customGetValue(object, firstKey, nextKeys);
		if (valueObject instanceof StringObject) {
			return ((StringObject) valueObject).getValue();
		} else {
			return null;
		}
	}

	public static Time customGetTime(@NonNull IAnnotatable object, @NonNull String firstKey, String... nextKeys) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(firstKey), ARG2_MESSAGE);

		Value valueObject = customGetValue(object, firstKey, nextKeys);
		if (valueObject instanceof Time) {
			return FactoryUtil.createTime((Time) valueObject);
		} else {
			return null;
		}
	}

	public static IReferable customGetReference(@NonNull IAnnotatable object, @NonNull String firstKey, String... nextKeys) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(firstKey), ARG2_MESSAGE);

		Value valueObject = customGetValue(object, firstKey, nextKeys);
		if (valueObject instanceof ReferenceObject) {
			return ((ReferenceObject) valueObject).getValue();
		} else {
			return null;
		}
	}

	public static Value customGetValue(@NonNull IAnnotatable object, @NonNull String key, char separator) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(key), ARG2_MESSAGE);

		List<String> keys = Splitter.on(separator).splitToList(key);

		String firstKey = keys.get(0);
		if (firstKey == null || firstKey.isEmpty()) return null;

		String[] nextKeys = (keys.size() < 2) ? new String[0] : keys.subList(1, keys.size()).toArray(new String[0]);

		return customGetValue(object, firstKey, nextKeys);
	}

	public static Value customGetValue(@NonNull IAnnotatable object, @NonNull String firstKey, String... nextKeys) {
		checkArgument(object != null, ARG1_MESSAGE);
		checkArgument(! isNullOrEmpty(firstKey), ARG2_MESSAGE);

		Value valueObject = object.getCustomProperties().get(firstKey);

		for (String key : nextKeys) {
			if (valueObject == null) break;

			if (valueObject instanceof MapObject && !isNullOrEmpty(key)) {
				valueObject = ((MapObject) valueObject).getEntries().get(key);
			} else if (valueObject instanceof ListObject && isIndex(key)) {
				int index = Integer.parseInt(key);
				EList<Value> list = ((ListObject) valueObject).getValues();
				valueObject = (index < list.size()) ? list.get(index) : null;
			} else {
				valueObject = null;
			}

		}
		return valueObject;

	}

    private static boolean isIndex(String str) {
        if (str == null) {
            return false;
        }

        int len = str.length();
        for (int i = 0; i < len; i++) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

}
