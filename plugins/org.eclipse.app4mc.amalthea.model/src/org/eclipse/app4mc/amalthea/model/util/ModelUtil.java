/**
 ********************************************************************************
 * Copyright (c) 2015, 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.util;

import static com.google.common.base.Preconditions.checkArgument;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.CommonElements;
import org.eclipse.app4mc.amalthea.model.ComponentsModel;
import org.eclipse.app4mc.amalthea.model.ConfigModel;
import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.EventModel;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.PropertyConstraintsModel;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.StimuliModel;
import org.eclipse.jdt.annotation.NonNull;

public class ModelUtil {

	// Suppress default constructor
	private ModelUtil() {
		throw new IllegalStateException("Utility class");
	}

	private static final String ARG_NULL_MESSAGE = "Argument is null, expected: Amalthea";

	// Get namespace from AmaltheaPackage (for symmetric access and convenience)
	public static final String MODEL_NAMESPACE = AmaltheaPackage.eNS_URI;
	// Extract AMALTHEA version
	public static final String MODEL_VERSION;

	static {
		int lastIndex = MODEL_NAMESPACE.lastIndexOf('/');
		MODEL_VERSION = (lastIndex == -1)
				? MODEL_NAMESPACE
				: MODEL_NAMESPACE.substring(lastIndex + 1);
	}

	public static CommonElements getOrCreateCommonElements(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getCommonElements() == null) {
			model.setCommonElements(AmaltheaFactory.eINSTANCE.createCommonElements());
		}
		return model.getCommonElements();
	}

	public static SWModel getOrCreateSwModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getSwModel() == null) {
			model.setSwModel(AmaltheaFactory.eINSTANCE.createSWModel());
		}
		return model.getSwModel();
	}

	public static HWModel getOrCreateHwModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getHwModel() == null) {
			model.setHwModel(AmaltheaFactory.eINSTANCE.createHWModel());
		}
		return model.getHwModel();
	}

	public static OSModel getOrCreateOsModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getOsModel() == null) {
			model.setOsModel(AmaltheaFactory.eINSTANCE.createOSModel());
		}
		return model.getOsModel();
	}

	public static StimuliModel getOrCreateStimuliModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getStimuliModel() == null) {
			model.setStimuliModel(AmaltheaFactory.eINSTANCE.createStimuliModel());
		}
		return model.getStimuliModel();
	}

	public static EventModel getOrCreateEventModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getEventModel() == null) {
			model.setEventModel(AmaltheaFactory.eINSTANCE.createEventModel());
		}
		return model.getEventModel();
	}

	public static ConstraintsModel getOrCreateConstraintsModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getConstraintsModel() == null) {
			model.setConstraintsModel(AmaltheaFactory.eINSTANCE.createConstraintsModel());
		}
		return model.getConstraintsModel();
	}

	public static PropertyConstraintsModel getOrCreatePropertyConstraintsModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getPropertyConstraintsModel() == null) {
			model.setPropertyConstraintsModel(AmaltheaFactory.eINSTANCE.createPropertyConstraintsModel());
		}
		return model.getPropertyConstraintsModel();
	}

	public static MappingModel getOrCreateMappingModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getMappingModel() == null) {
			model.setMappingModel(AmaltheaFactory.eINSTANCE.createMappingModel());
		}
		return model.getMappingModel();
	}

	public static ComponentsModel getOrCreateComponentsModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getComponentsModel() == null) {
			model.setComponentsModel(AmaltheaFactory.eINSTANCE.createComponentsModel());
		}
		return model.getComponentsModel();
	}

	public static ConfigModel getOrCreateConfigModel(final @NonNull Amalthea model) {
		checkArgument(model != null, ARG_NULL_MESSAGE);

		if (model.getConfigModel() == null) {
			model.setConfigModel(AmaltheaFactory.eINSTANCE.createConfigModel());
		}
		return model.getConfigModel();
	}

	public static ActivityGraph getOrCreateActivityGraph(final @NonNull Process process) {
		checkArgument(process != null, ARG_NULL_MESSAGE);
		if (process.getActivityGraph() == null) {
			process.setActivityGraph(AmaltheaFactory.eINSTANCE.createActivityGraph());
		}
		return process.getActivityGraph();
	}

	public static ActivityGraph getOrCreateActivityGraph(final @NonNull Runnable runnable) {
		checkArgument(runnable != null, ARG_NULL_MESSAGE);
		if (runnable.getActivityGraph() == null) {
			runnable.setActivityGraph(AmaltheaFactory.eINSTANCE.createActivityGraph());
		}
		return runnable.getActivityGraph();
	}

}
