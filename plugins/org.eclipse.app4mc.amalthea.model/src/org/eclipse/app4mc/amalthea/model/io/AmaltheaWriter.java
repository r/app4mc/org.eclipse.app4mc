/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.io;

import java.io.File;
import java.io.IOException;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.emf.AmaltheaResource;
import org.eclipse.app4mc.amalthea.model.emf.AmaltheaResourceFactory;
import org.eclipse.app4mc.amalthea.model.emf.AmaltheaResourceSetImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class AmaltheaWriter {

	// Suppress default constructor
	private AmaltheaWriter() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Static method to save an AMALTHEA model to a file with a given pathname string.
	 * <p>
	 * Possible path names:
	 * <ul>
	 * <li>absolute (example: "d:/temp/model_new.amxmi")</li>
	 * <li>relative to project folder (example: "output/model_new.amxmi")</li>
	 * </ul>
	 *
	 * @param model		source model
	 * @param pathname	String
	 * @return
	 * 		boolean - true if file is written successfully
	 */
	public static boolean writeToFileNamed(Amalthea model, String pathname) {
		return writeToFileNamed(model, pathname, false);
	}

	/**
	 * Static method to save an AMALTHEA model to a file with a given pathname string.
	 * <p>
	 * Possible path names:
	 * <ul>
	 * <li>absolute (example: "d:/temp/model_new.amxmi")</li>
	 * <li>relative to project folder (example: "output/model_new.amxmi")</li>
	 * </ul>
	 *
	 * @param model		source model
	 * @param pathname	String
	 * @param useZip	boolean
	 * @return
	 * 		boolean - true if file is written successfully
	 */
	public static boolean writeToFileNamed(Amalthea model, String pathname, boolean useZip) {
		if (model == null || pathname == null || pathname.isEmpty()) return false;

		final File file = new File(pathname);
		return writeToFile(model, file, useZip);
	}

	/**
	 * Static method to save an AMALTHEA model to a file.
	 *
	 * @param model		source model
	 * @param file		standard Java file
	 * @return
	 * 		boolean - true if file is written successfully
	 */
	public static boolean writeToFile(Amalthea model, File file) {
		return writeToFile(model, file, false);
	}

	/**
	 * Static method to save an AMALTHEA model to a file.
	 *
	 * @param model		source model
	 * @param file		standard Java file
	 * @param useZip	boolean
	 * @return
	 * 		boolean - true if file is written successfully
	 */
	public static boolean writeToFile(Amalthea model, File file, boolean useZip) {
		if (model == null || file == null) return false;

		final URI uri = URI.createFileURI(file.getAbsolutePath());
		return writeToURI(model, uri, useZip);
	}

	/**
	 * Static method to save an AMALTHEA model to a file URI.
	 *
	 * @param model		source model
	 * @param uri		org.eclipse.emf.common.util.URI
	 * @return
	 * 		boolean - true if file is written successfully
	 */
	public static boolean writeToURI(Amalthea model, URI uri) {
		return writeToURI(model, uri, false);
	}

	/**
	 * Static method to save an AMALTHEA model to a file URI.
	 *
	 * @param model		source model
	 * @param uri		org.eclipse.emf.common.util.URI
	 * @param useZip	boolean
	 * @return
	 * 		boolean - true if file is written successfully
	 */
	public static boolean writeToURI(Amalthea model, URI uri, boolean useZip) {
		if (model == null || uri == null) return false;

		final ResourceSet resSet = initializeResourceSet();
		final Resource res = resSet.createResource(uri);

		if (res instanceof AmaltheaResource) {
			((AmaltheaResource) res).setUseZip(useZip);
		}

		res.getContents().add(EcoreUtil.copy(model));

		try {
			res.save(null);
		} catch (IOException e) {
			return false;
		}

		return true;
	}

	private static ResourceSet initializeResourceSet() {
		AmaltheaPackage.eINSTANCE.eClass(); // register the package
		final ResourceSet resSet = new AmaltheaResourceSetImpl();
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("amxmi", new AmaltheaResourceFactory());

		return resSet;
	}

}
