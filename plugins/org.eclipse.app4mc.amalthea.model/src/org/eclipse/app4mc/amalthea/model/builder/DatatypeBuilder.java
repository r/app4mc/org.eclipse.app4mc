/**
 ********************************************************************************
 * Copyright (c) 2018-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.builder;

import java.util.function.Consumer;

import org.eclipse.app4mc.amalthea.model.Alias;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Array;
import org.eclipse.app4mc.amalthea.model.BaseTypeDefinition;
import org.eclipse.app4mc.amalthea.model.DataTypeDefinition;
import org.eclipse.app4mc.amalthea.model.Pointer;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Struct;
import org.eclipse.app4mc.amalthea.model.StructEntry;

public class DatatypeBuilder {

	@SuppressWarnings("java:S100") // domain specific builder language
	public void typeDefinition_BaseType(final SWModel container, final Consumer<BaseTypeDefinition> initializer) {
		final BaseTypeDefinition obj = AmaltheaFactory.eINSTANCE.createBaseTypeDefinition();
		container.getTypeDefinitions().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void typeDefinition_DataType(final SWModel container, final Consumer<DataTypeDefinition> initializer) {
		final DataTypeDefinition obj = AmaltheaFactory.eINSTANCE.createDataTypeDefinition();
		container.getTypeDefinitions().add(obj);
		initializer.accept(obj);
	}

	// ********** Base Types - Aliases **********

	public void alias(final BaseTypeDefinition container, final Consumer<Alias> initializer) {
		final Alias obj = AmaltheaFactory.eINSTANCE.createAlias();
		container.getAliases().add(obj);
		initializer.accept(obj);
	}

	// ********** Data Types - Struct **********

	public void struct(final DataTypeDefinition container, final Consumer<Struct> initializer) {
		final Struct obj = AmaltheaFactory.eINSTANCE.createStruct();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void struct(final Array container, final Consumer<Struct> initializer) {
		final Struct obj = AmaltheaFactory.eINSTANCE.createStruct();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void struct(final StructEntry container, final Consumer<Struct> initializer) {
		final Struct obj = AmaltheaFactory.eINSTANCE.createStruct();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void struct(final Pointer container, final Consumer<Struct> initializer) {
		final Struct obj = AmaltheaFactory.eINSTANCE.createStruct();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void entry(final Struct container, final Consumer<StructEntry> initializer) {
		final StructEntry obj = AmaltheaFactory.eINSTANCE.createStructEntry();
		container.getEntries().add(obj);
		initializer.accept(obj);
	}

	// ********** Data Types - Array **********

	public void array(final DataTypeDefinition container, final Consumer<Array> initializer) {
		final Array obj = AmaltheaFactory.eINSTANCE.createArray();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void array(final Array container, final Consumer<Array> initializer) {
		final Array obj = AmaltheaFactory.eINSTANCE.createArray();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void array(final StructEntry container, final Consumer<Array> initializer) {
		final Array obj = AmaltheaFactory.eINSTANCE.createArray();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void array(final Pointer container, final Consumer<Array> initializer) {
		final Array obj = AmaltheaFactory.eINSTANCE.createArray();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	// ********** Data Types - Pointer **********

	public void pointer(final DataTypeDefinition container, final Consumer<Pointer> initializer) {
		final Pointer obj = AmaltheaFactory.eINSTANCE.createPointer();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void pointer(final Array container, final Consumer<Pointer> initializer) {
		final Pointer obj = AmaltheaFactory.eINSTANCE.createPointer();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void pointer(final StructEntry container, final Consumer<Pointer> initializer) {
		final Pointer obj = AmaltheaFactory.eINSTANCE.createPointer();
		container.setDataType(obj);
		initializer.accept(obj);
	}

	public void pointer(final Pointer container, final Consumer<Pointer> initializer) {
		final Pointer obj = AmaltheaFactory.eINSTANCE.createPointer();
		container.setDataType(obj);
		initializer.accept(obj);
	}

}
