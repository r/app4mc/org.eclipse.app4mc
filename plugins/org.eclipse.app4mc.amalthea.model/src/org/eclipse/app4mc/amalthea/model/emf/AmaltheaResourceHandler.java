/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.emf;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.BasicResourceHandler;

public class AmaltheaResourceHandler extends BasicResourceHandler {

	@Override
	public void preLoad(XMLResource resource, InputStream inputStream, Map<?, ?> options) {
		// enable cache for intrinsic IDs
		if (resource instanceof AmaltheaResource) {
			((AmaltheaResource) resource).setIntrinsicIDToEObjectMap(new HashMap<>());
		}
	}

	@Override
	public void postLoad(XMLResource resource, InputStream inputStream, Map<?, ?> options) {
		// Keep cache for intrinsic IDs
		// It will be used to resolve cross-resource references

		resetExtrinsicIDs(resource);
	}

	@Override
	public void preSave(XMLResource resource, OutputStream outputStream, Map<?, ?> options) {

		// disable cache for intrinsic IDs
		if (resource instanceof AmaltheaResource) {
			((AmaltheaResource) resource).setIntrinsicIDToEObjectMap(null);
		}

		setExtrinsicIDs(resource);
	}

	@Override
	public void postSave(XMLResource resource, OutputStream outputStream, Map<?, ?> options) {
		resetExtrinsicIDs(resource);
	}

	private void setExtrinsicIDs(XMLResource resource) {
		if (resource == null) return;

		// set xmi:id for all objects with a valid ID attribute
		TreeIterator<EObject> allProperContents = EcoreUtil.getAllProperContents(resource, false);
		while (allProperContents.hasNext()) {
			EObject eObject = allProperContents.next();
			String id = EcoreUtil.getID(eObject);
			if (id != null)
				resource.setID(eObject, id);
		}
	}

	private void resetExtrinsicIDs(XMLResource resource) {
		if (resource == null) return;

		if (resource instanceof AmaltheaResource) {
			// reset the ID <-> EObject maps
			((AmaltheaResource) resource).resetIDMaps();
		} else {
			// reset xmi:id for all objects with ID attribute
			TreeIterator<EObject> allProperContents = EcoreUtil.getAllProperContents(resource, false);
			while (allProperContents.hasNext()) {
				EObject eObject = allProperContents.next();
				if (eObject.eClass().getEIDAttribute() != null)
					resource.setID(eObject, null);
			}
		}
	}

}
