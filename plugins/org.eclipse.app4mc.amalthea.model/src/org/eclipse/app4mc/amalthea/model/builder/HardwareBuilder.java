/**
 ********************************************************************************
 * Copyright (c) 2018-2019 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.builder;

import java.util.function.Consumer;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Cache;
import org.eclipse.app4mc.amalthea.model.CacheDefinition;
import org.eclipse.app4mc.amalthea.model.ConnectionHandler;
import org.eclipse.app4mc.amalthea.model.ConnectionHandlerDefinition;
import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.HwAccessPath;
import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.model.HwFeature;
import org.eclipse.app4mc.amalthea.model.HwFeatureCategory;
import org.eclipse.app4mc.amalthea.model.HwPort;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.MemoryDefinition;
import org.eclipse.app4mc.amalthea.model.PowerDomain;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition;

public class HardwareBuilder {

	public HWModel hardwareModelRoot(final Consumer<HWModel> initializer) {
		final HWModel obj = AmaltheaFactory.eINSTANCE.createHWModel();
		initializer.accept(obj);
		return obj;
	}

	// ********** Top level elements **********

	@SuppressWarnings("java:S100") // domain specific builder language
	public void definition_Cache(final HWModel container, final Consumer<CacheDefinition> initializer) {
		final CacheDefinition obj = AmaltheaFactory.eINSTANCE.createCacheDefinition();
		container.getDefinitions().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void definition_Memory(final HWModel container, final Consumer<MemoryDefinition> initializer) {
		final MemoryDefinition obj = AmaltheaFactory.eINSTANCE.createMemoryDefinition();
		container.getDefinitions().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void definition_ProcessingUnit(final HWModel container, final Consumer<ProcessingUnitDefinition> initializer) {
		final ProcessingUnitDefinition obj = AmaltheaFactory.eINSTANCE.createProcessingUnitDefinition();
		container.getDefinitions().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void definition_ConnectionHandler(final HWModel container, final Consumer<ConnectionHandlerDefinition> initializer) {
		final ConnectionHandlerDefinition obj = AmaltheaFactory.eINSTANCE.createConnectionHandlerDefinition();
		container.getDefinitions().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void domain_Frequency(final HWModel container, final Consumer<FrequencyDomain> initializer) {
		final FrequencyDomain obj = AmaltheaFactory.eINSTANCE.createFrequencyDomain();
		container.getDomains().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void domain_Power(final HWModel container, final Consumer<PowerDomain> initializer) {
		final PowerDomain obj = AmaltheaFactory.eINSTANCE.createPowerDomain();
		container.getDomains().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void featureCategory(final HWModel container, final Consumer<HwFeatureCategory> initializer) {
		final HwFeatureCategory obj = AmaltheaFactory.eINSTANCE.createHwFeatureCategory();
		container.getFeatureCategories().add(obj);
		initializer.accept(obj);
	}

	public void structure(final HWModel container, final Consumer<HwStructure> initializer) {
		final HwStructure obj = AmaltheaFactory.eINSTANCE.createHwStructure();
		container.getStructures().add(obj);
		initializer.accept(obj);
	}

	// ********** Features **********

	public void feature(final HwFeatureCategory container, final Consumer<HwFeature> initializer) {
		final HwFeature obj = AmaltheaFactory.eINSTANCE.createHwFeature();
		container.getFeatures().add(obj);
		initializer.accept(obj);
	}

	// ********** Structures **********

	public void structure(final HwStructure container, final Consumer<HwStructure> initializer) {
		final HwStructure obj = AmaltheaFactory.eINSTANCE.createHwStructure();
		container.getStructures().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void module_Memory(final HwStructure container, final Consumer<Memory> initializer) {
		final Memory obj = AmaltheaFactory.eINSTANCE.createMemory();
		container.getModules().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void module_ProcessingUnit(final HwStructure container, final Consumer<ProcessingUnit> initializer) {
		final ProcessingUnit obj = AmaltheaFactory.eINSTANCE.createProcessingUnit();
		container.getModules().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void module_Cache(final HwStructure container, final Consumer<Cache> initializer) {
		final Cache obj = AmaltheaFactory.eINSTANCE.createCache();
		container.getModules().add(obj);
		initializer.accept(obj);
	}

	@SuppressWarnings("java:S100") // domain specific builder language
	public void module_ConnectionHandler(final HwStructure container, final Consumer<ConnectionHandler> initializer) {
		final ConnectionHandler obj = AmaltheaFactory.eINSTANCE.createConnectionHandler();
		container.getModules().add(obj);
		initializer.accept(obj);
	}

	public void connection(final HwStructure container, final Consumer<HwConnection> initializer) {
		final HwConnection obj = AmaltheaFactory.eINSTANCE.createHwConnection();
		container.getConnections().add(obj);
		initializer.accept(obj);
	}

	public void port(final HwStructure container, final Consumer<HwPort> initializer) {
		final HwPort obj = AmaltheaFactory.eINSTANCE.createHwPort();
		container.getPorts().add(obj);
		initializer.accept(obj);
	}

	public void port(final Memory container, final Consumer<HwPort> initializer) {
		final HwPort obj = AmaltheaFactory.eINSTANCE.createHwPort();
		container.getPorts().add(obj);
		initializer.accept(obj);
	}

	public void port(final ProcessingUnit container, final Consumer<HwPort> initializer) {
		final HwPort obj = AmaltheaFactory.eINSTANCE.createHwPort();
		container.getPorts().add(obj);
		initializer.accept(obj);
	}

	public void port(final Cache container, final Consumer<HwPort> initializer) {
		final HwPort obj = AmaltheaFactory.eINSTANCE.createHwPort();
		container.getPorts().add(obj);
		initializer.accept(obj);
	}

	public void port(final ConnectionHandler container, final Consumer<HwPort> initializer) {
		final HwPort obj = AmaltheaFactory.eINSTANCE.createHwPort();
		container.getPorts().add(obj);
		initializer.accept(obj);
	}

	public void cache(final ProcessingUnit container, final Consumer<Cache> initializer) {
		final Cache obj = AmaltheaFactory.eINSTANCE.createCache();
		container.getCaches().add(obj);
		initializer.accept(obj);
	}

	public void access(final ProcessingUnit container, final Consumer<HwAccessElement> initializer) {
		final HwAccessElement obj = AmaltheaFactory.eINSTANCE.createHwAccessElement();
		container.getAccessElements().add(obj);
		initializer.accept(obj);
	}

	public void path(final HwAccessElement container, final Consumer<HwAccessPath> initializer) {
		final HwAccessPath obj = AmaltheaFactory.eINSTANCE.createHwAccessPath();
		container.setAccessPath(obj);
		initializer.accept(obj);
	}

}
