/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.AmaltheaServices;
import org.eclipse.app4mc.amalthea.model.BaseObject;
import org.eclipse.app4mc.amalthea.model.IAnnotatable;
import org.eclipse.app4mc.amalthea.model.IExecutable;
import org.eclipse.app4mc.amalthea.model.LocalModeLabelAssignment;
import org.eclipse.app4mc.amalthea.model.Value;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Local Mode Label Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.LocalModeLabelAssignmentImpl#getCustomProperties <em>Custom Properties</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.LocalModeLabelAssignmentImpl#getContainingExecutable <em>Containing Executable</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.LocalModeLabelAssignmentImpl#getContainingProcess <em>Containing Process</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.LocalModeLabelAssignmentImpl#getContainingRunnable <em>Containing Runnable</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.LocalModeLabelAssignmentImpl#getContainingActivityGraph <em>Containing Activity Graph</em>}</li>
 * </ul>
 *
 * @generated
 */
@SuppressWarnings("deprecation")
public class LocalModeLabelAssignmentImpl extends LocalModeValueImpl implements LocalModeLabelAssignment {
	/**
	 * The cached value of the '{@link #getCustomProperties() <em>Custom Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCustomProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, Value> customProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocalModeLabelAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getLocalModeLabelAssignment();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<String, Value> getCustomProperties() {
		if (customProperties == null) {
			customProperties = new EcoreEMap<String,Value>(AmaltheaPackage.eINSTANCE.getCustomProperty(), CustomPropertyImpl.class, this, AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CUSTOM_PROPERTIES);
		}
		return customProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @since 1.2
	 * @generated
	 */
	@Override
	public IExecutable getContainingExecutable() {
		return AmaltheaServices.<IExecutable>getContainerOfType(this, IExecutable.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link org.eclipse.app4mc.amalthea.model.ActivityGraphItem#getContainingProcess() model documentation} for details.
	 * @generated
	 */
	@Deprecated
	@Override
	public org.eclipse.app4mc.amalthea.model.Process getContainingProcess() {
		return AmaltheaServices.<org.eclipse.app4mc.amalthea.model.Process>getContainerOfType(this, org.eclipse.app4mc.amalthea.model.Process.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link org.eclipse.app4mc.amalthea.model.ActivityGraphItem#getContainingRunnable() model documentation} for details.
	 * @generated
	 */
	@Deprecated
	@Override
	public org.eclipse.app4mc.amalthea.model.Runnable getContainingRunnable() {
		return AmaltheaServices.<org.eclipse.app4mc.amalthea.model.Runnable>getContainerOfType(this, org.eclipse.app4mc.amalthea.model.Runnable.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActivityGraph getContainingActivityGraph() {
		return AmaltheaServices.<ActivityGraph>getContainerOfType(this, ActivityGraph.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CUSTOM_PROPERTIES:
				return ((InternalEList<?>)getCustomProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CUSTOM_PROPERTIES:
				if (coreType) return getCustomProperties();
				else return getCustomProperties().map();
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_EXECUTABLE:
				return getContainingExecutable();
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_PROCESS:
				return getContainingProcess();
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_RUNNABLE:
				return getContainingRunnable();
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_ACTIVITY_GRAPH:
				return getContainingActivityGraph();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CUSTOM_PROPERTIES:
				((EStructuralFeature.Setting)getCustomProperties()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CUSTOM_PROPERTIES:
				getCustomProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CUSTOM_PROPERTIES:
				return customProperties != null && !customProperties.isEmpty();
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_EXECUTABLE:
				return getContainingExecutable() != null;
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_PROCESS:
				return getContainingProcess() != null;
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_RUNNABLE:
				return getContainingRunnable() != null;
			case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_ACTIVITY_GRAPH:
				return getContainingActivityGraph() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IAnnotatable.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CUSTOM_PROPERTIES: return AmaltheaPackage.IANNOTATABLE__CUSTOM_PROPERTIES;
				default: return -1;
			}
		}
		if (baseClass == BaseObject.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ActivityGraphItem.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_EXECUTABLE: return AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_EXECUTABLE;
				case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_PROCESS: return AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_PROCESS;
				case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_RUNNABLE: return AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_RUNNABLE;
				case AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_ACTIVITY_GRAPH: return AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_ACTIVITY_GRAPH;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IAnnotatable.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.IANNOTATABLE__CUSTOM_PROPERTIES: return AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CUSTOM_PROPERTIES;
				default: return -1;
			}
		}
		if (baseClass == BaseObject.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ActivityGraphItem.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_EXECUTABLE: return AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_EXECUTABLE;
				case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_PROCESS: return AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_PROCESS;
				case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_RUNNABLE: return AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_RUNNABLE;
				case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_ACTIVITY_GRAPH: return AmaltheaPackage.LOCAL_MODE_LABEL_ASSIGNMENT__CONTAINING_ACTIVITY_GRAPH;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //LocalModeLabelAssignmentImpl
