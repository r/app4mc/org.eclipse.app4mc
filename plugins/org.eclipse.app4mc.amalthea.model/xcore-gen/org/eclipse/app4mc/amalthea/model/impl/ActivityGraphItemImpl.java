/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.AmaltheaServices;
import org.eclipse.app4mc.amalthea.model.IExecutable;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity Graph Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ActivityGraphItemImpl#getContainingExecutable <em>Containing Executable</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ActivityGraphItemImpl#getContainingProcess <em>Containing Process</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ActivityGraphItemImpl#getContainingRunnable <em>Containing Runnable</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ActivityGraphItemImpl#getContainingActivityGraph <em>Containing Activity Graph</em>}</li>
 * </ul>
 *
 * @generated
 */
@SuppressWarnings("deprecation")
public abstract class ActivityGraphItemImpl extends BaseObjectImpl implements ActivityGraphItem {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityGraphItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getActivityGraphItem();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @since 1.2
	 * @generated
	 */
	@Override
	public IExecutable getContainingExecutable() {
		return AmaltheaServices.<IExecutable>getContainerOfType(this, IExecutable.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link org.eclipse.app4mc.amalthea.model.ActivityGraphItem#getContainingProcess() model documentation} for details.
	 * @generated
	 */
	@Deprecated
	@Override
	public org.eclipse.app4mc.amalthea.model.Process getContainingProcess() {
		return AmaltheaServices.<org.eclipse.app4mc.amalthea.model.Process>getContainerOfType(this, org.eclipse.app4mc.amalthea.model.Process.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated See {@link org.eclipse.app4mc.amalthea.model.ActivityGraphItem#getContainingRunnable() model documentation} for details.
	 * @generated
	 */
	@Deprecated
	@Override
	public org.eclipse.app4mc.amalthea.model.Runnable getContainingRunnable() {
		return AmaltheaServices.<org.eclipse.app4mc.amalthea.model.Runnable>getContainerOfType(this, org.eclipse.app4mc.amalthea.model.Runnable.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ActivityGraph getContainingActivityGraph() {
		return AmaltheaServices.<ActivityGraph>getContainerOfType(this, ActivityGraph.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_EXECUTABLE:
				return getContainingExecutable();
			case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_PROCESS:
				return getContainingProcess();
			case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_RUNNABLE:
				return getContainingRunnable();
			case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_ACTIVITY_GRAPH:
				return getContainingActivityGraph();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_EXECUTABLE:
				return getContainingExecutable() != null;
			case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_PROCESS:
				return getContainingProcess() != null;
			case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_RUNNABLE:
				return getContainingRunnable() != null;
			case AmaltheaPackage.ACTIVITY_GRAPH_ITEM__CONTAINING_ACTIVITY_GRAPH:
				return getContainingActivityGraph() != null;
		}
		return super.eIsSet(featureID);
	}

} //ActivityGraphItemImpl
