/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Interface</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.SubInterface#getContainingInterface <em>Containing Interface</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSubInterface()
 * @model
 * @generated
 */
public interface SubInterface extends ComponentInterface {
	/**
	 * Returns the value of the '<em><b>Containing Interface</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.app4mc.amalthea.model.ComponentInterface#getSubInterfaces <em>Sub Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Interface</em>' container reference.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getSubInterface_ContainingInterface()
	 * @see org.eclipse.app4mc.amalthea.model.ComponentInterface#getSubInterfaces
	 * @model opposite="subInterfaces" transient="false" changeable="false"
	 * @generated
	 */
	ComponentInterface getContainingInterface();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	EList<String> getNamePrefixSegments();

} // SubInterface
