/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Mode Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.LocalModeValue#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.LocalModeValue#getValueSource <em>Value Source</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getLocalModeValue()
 * @model abstract="true"
 * @generated
 */
public interface LocalModeValue extends EObject {
	/**
	 * Returns the value of the '<em><b>Label</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' reference.
	 * @see #setLabel(LocalModeLabel)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getLocalModeValue_Label()
	 * @model required="true"
	 * @generated
	 */
	LocalModeLabel getLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.LocalModeValue#getLabel <em>Label</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' reference.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(LocalModeLabel value);

	/**
	 * Returns the value of the '<em><b>Value Source</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Source</em>' containment reference.
	 * @see #setValueSource(ILocalModeValueSource)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getLocalModeValue_ValueSource()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ILocalModeValueSource getValueSource();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.LocalModeValue#getValueSource <em>Value Source</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Source</em>' containment reference.
	 * @see #getValueSource()
	 * @generated
	 */
	void setValueSource(ILocalModeValueSource value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" diagnosticsUnique="false" contextUnique="false"
	 * @generated
	 */
	boolean validateInvariants(DiagnosticChain diagnostics, Map<Object, Object> context);

} // LocalModeValue
