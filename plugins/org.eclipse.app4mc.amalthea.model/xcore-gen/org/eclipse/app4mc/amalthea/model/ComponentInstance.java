/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentInstance#getContainingSystem <em>Containing System</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ComponentInstance#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentInstance()
 * @model
 * @generated
 */
public interface ComponentInstance extends ReferableBaseObject, ITaggable {
	/**
	 * Returns the value of the '<em><b>Containing System</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.eclipse.app4mc.amalthea.model.ISystem#getComponentInstances <em>Component Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing System</em>' container reference.
	 * @see #setContainingSystem(ISystem)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentInstance_ContainingSystem()
	 * @see org.eclipse.app4mc.amalthea.model.ISystem#getComponentInstances
	 * @model opposite="componentInstances" transient="false"
	 * @generated
	 */
	ISystem getContainingSystem();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ComponentInstance#getContainingSystem <em>Containing System</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing System</em>' container reference.
	 * @see #getContainingSystem()
	 * @generated
	 */
	void setContainingSystem(ISystem value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Component)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getComponentInstance_Type()
	 * @model
	 * @generated
	 */
	Component getType();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.ComponentInstance#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Component value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" unique="false"
	 * @generated
	 */
	EList<String> getNamePrefixSegments();

} // ComponentInstance
