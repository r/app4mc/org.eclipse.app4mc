/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IExecutable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * @since 1.2
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.IExecutable#getLocalLabels <em>Local Labels</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.IExecutable#getActivityGraph <em>Activity Graph</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getIExecutable()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IExecutable extends EObject {
	/**
	 * Returns the value of the '<em><b>Local Labels</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.app4mc.amalthea.model.LocalModeLabel}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.app4mc.amalthea.model.LocalModeLabel#getContainingExecutable <em>Containing Executable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Labels</em>' containment reference list.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getIExecutable_LocalLabels()
	 * @see org.eclipse.app4mc.amalthea.model.LocalModeLabel#getContainingExecutable
	 * @model opposite="containingExecutable" containment="true"
	 * @generated
	 */
	EList<LocalModeLabel> getLocalLabels();

	/**
	 * Returns the value of the '<em><b>Activity Graph</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Graph</em>' containment reference.
	 * @see #setActivityGraph(ActivityGraph)
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getIExecutable_ActivityGraph()
	 * @model containment="true"
	 * @generated
	 */
	ActivityGraph getActivityGraph();

	/**
	 * Sets the value of the '{@link org.eclipse.app4mc.amalthea.model.IExecutable#getActivityGraph <em>Activity Graph</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity Graph</em>' containment reference.
	 * @see #getActivityGraph()
	 * @generated
	 */
	void setActivityGraph(ActivityGraph value);

} // IExecutable
