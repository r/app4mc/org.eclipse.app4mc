/**
 * *******************************************************************************
 *  Copyright (c) 2015-2022 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model;

import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IScheduling Parameter Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.ISchedulingParameterContainer#getSchedulingParameters <em>Scheduling Parameters</em>}</li>
 * </ul>
 *
 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getISchedulingParameterContainer()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ISchedulingParameterContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Scheduling Parameters</b></em>' map.
	 * The key is of type {@link org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition},
	 * and the value is of type {@link org.eclipse.app4mc.amalthea.model.Value},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduling Parameters</em>' map.
	 * @see org.eclipse.app4mc.amalthea.model.AmaltheaPackage#getISchedulingParameterContainer_SchedulingParameters()
	 * @model mapType="org.eclipse.app4mc.amalthea.model.SchedulingParameter&lt;org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition, org.eclipse.app4mc.amalthea.model.Value&gt;"
	 * @generated
	 */
	EMap<SchedulingParameterDefinition, Value> getSchedulingParameters();

} // ISchedulingParameterContainer
