/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.util.Collection;
import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.ComputationItem;
import org.eclipse.app4mc.amalthea.model.ISchedulingParameterContainer;
import org.eclipse.app4mc.amalthea.model.RunnableAllocation;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.Value;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scheduler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerImpl#getSchedulingParameters <em>Scheduling Parameters</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerImpl#getDefinition <em>Definition</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerImpl#getComputationItems <em>Computation Items</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerImpl#getSchedulerAllocations <em>Scheduler Allocations</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.SchedulerImpl#getRunnableAllocations <em>Runnable Allocations</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class SchedulerImpl extends ReferableBaseObjectImpl implements Scheduler {
	/**
	 * The cached value of the '{@link #getSchedulingParameters() <em>Scheduling Parameters</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSchedulingParameters()
	 * @generated
	 * @ordered
	 */
	protected EMap<SchedulingParameterDefinition, Value> schedulingParameters;

	/**
	 * The cached value of the '{@link #getDefinition() <em>Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefinition()
	 * @generated
	 * @ordered
	 */
	protected SchedulerDefinition definition;

	/**
	 * The cached value of the '{@link #getComputationItems() <em>Computation Items</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComputationItems()
	 * @generated
	 * @ordered
	 */
	protected EList<ComputationItem> computationItems;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SchedulerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getScheduler();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public SchedulerDefinition getDefinition() {
		if (definition != null && definition.eIsProxy()) {
			InternalEObject oldDefinition = (InternalEObject)definition;
			definition = (SchedulerDefinition)eResolveProxy(oldDefinition);
			if (definition != oldDefinition) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AmaltheaPackage.SCHEDULER__DEFINITION, oldDefinition, definition));
			}
		}
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SchedulerDefinition basicGetDefinition() {
		return definition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDefinition(SchedulerDefinition newDefinition) {
		SchedulerDefinition oldDefinition = definition;
		definition = newDefinition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AmaltheaPackage.SCHEDULER__DEFINITION, oldDefinition, definition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EMap<SchedulingParameterDefinition, Value> getSchedulingParameters() {
		if (schedulingParameters == null) {
			schedulingParameters = new EcoreEMap<SchedulingParameterDefinition,Value>(AmaltheaPackage.eINSTANCE.getSchedulingParameter(), SchedulingParameterImpl.class, this, AmaltheaPackage.SCHEDULER__SCHEDULING_PARAMETERS);
		}
		return schedulingParameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ComputationItem> getComputationItems() {
		if (computationItems == null) {
			computationItems = new EObjectContainmentEList<ComputationItem>(ComputationItem.class, this, AmaltheaPackage.SCHEDULER__COMPUTATION_ITEMS);
		}
		return computationItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<SchedulerAllocation> getSchedulerAllocations() {
		EReference _schedulerAllocation_Scheduler = AmaltheaPackage.eINSTANCE.getSchedulerAllocation_Scheduler();
		return AmaltheaIndex.<SchedulerAllocation>getInverseReferences(this, AmaltheaPackage.eINSTANCE.getScheduler_SchedulerAllocations(), 
			java.util.Collections.<EReference>unmodifiableSet(org.eclipse.xtext.xbase.lib.CollectionLiterals.<EReference>newHashSet(_schedulerAllocation_Scheduler)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<RunnableAllocation> getRunnableAllocations() {
		EReference _runnableAllocation_Scheduler = AmaltheaPackage.eINSTANCE.getRunnableAllocation_Scheduler();
		return AmaltheaIndex.<RunnableAllocation>getInverseReferences(this, AmaltheaPackage.eINSTANCE.getScheduler_RunnableAllocations(), 
			java.util.Collections.<EReference>unmodifiableSet(org.eclipse.xtext.xbase.lib.CollectionLiterals.<EReference>newHashSet(_runnableAllocation_Scheduler)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER__SCHEDULING_PARAMETERS:
				return ((InternalEList<?>)getSchedulingParameters()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.SCHEDULER__COMPUTATION_ITEMS:
				return ((InternalEList<?>)getComputationItems()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER__SCHEDULING_PARAMETERS:
				if (coreType) return getSchedulingParameters();
				else return getSchedulingParameters().map();
			case AmaltheaPackage.SCHEDULER__DEFINITION:
				if (resolve) return getDefinition();
				return basicGetDefinition();
			case AmaltheaPackage.SCHEDULER__COMPUTATION_ITEMS:
				return getComputationItems();
			case AmaltheaPackage.SCHEDULER__SCHEDULER_ALLOCATIONS:
				return getSchedulerAllocations();
			case AmaltheaPackage.SCHEDULER__RUNNABLE_ALLOCATIONS:
				return getRunnableAllocations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER__SCHEDULING_PARAMETERS:
				((EStructuralFeature.Setting)getSchedulingParameters()).set(newValue);
				return;
			case AmaltheaPackage.SCHEDULER__DEFINITION:
				setDefinition((SchedulerDefinition)newValue);
				return;
			case AmaltheaPackage.SCHEDULER__COMPUTATION_ITEMS:
				getComputationItems().clear();
				getComputationItems().addAll((Collection<? extends ComputationItem>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER__SCHEDULING_PARAMETERS:
				getSchedulingParameters().clear();
				return;
			case AmaltheaPackage.SCHEDULER__DEFINITION:
				setDefinition((SchedulerDefinition)null);
				return;
			case AmaltheaPackage.SCHEDULER__COMPUTATION_ITEMS:
				getComputationItems().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.SCHEDULER__SCHEDULING_PARAMETERS:
				return schedulingParameters != null && !schedulingParameters.isEmpty();
			case AmaltheaPackage.SCHEDULER__DEFINITION:
				return definition != null;
			case AmaltheaPackage.SCHEDULER__COMPUTATION_ITEMS:
				return computationItems != null && !computationItems.isEmpty();
			case AmaltheaPackage.SCHEDULER__SCHEDULER_ALLOCATIONS:
				return !getSchedulerAllocations().isEmpty();
			case AmaltheaPackage.SCHEDULER__RUNNABLE_ALLOCATIONS:
				return !getRunnableAllocations().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ISchedulingParameterContainer.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.SCHEDULER__SCHEDULING_PARAMETERS: return AmaltheaPackage.ISCHEDULING_PARAMETER_CONTAINER__SCHEDULING_PARAMETERS;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ISchedulingParameterContainer.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.ISCHEDULING_PARAMETER_CONTAINER__SCHEDULING_PARAMETERS: return AmaltheaPackage.SCHEDULER__SCHEDULING_PARAMETERS;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //SchedulerImpl
