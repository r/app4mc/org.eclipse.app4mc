/**
 * *******************************************************************************
 *  Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 * 
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 *  SPDX-License-Identifier: EPL-2.0
 * 
 *     Generated using Eclipse EMF
 * 
 * *******************************************************************************
 */
package org.eclipse.app4mc.amalthea.model.impl;

import java.util.Collection;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.Component;
import org.eclipse.app4mc.amalthea.model.ComponentStructure;
import org.eclipse.app4mc.amalthea.model.ComponentsModel;
import org.eclipse.app4mc.amalthea.model.IComponentContainer;
import org.eclipse.app4mc.amalthea.model.IInterfaceContainer;
import org.eclipse.app4mc.amalthea.model.MainInterface;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Components Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ComponentsModelImpl#getComponents <em>Components</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ComponentsModelImpl#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ComponentsModelImpl#getStructures <em>Structures</em>}</li>
 *   <li>{@link org.eclipse.app4mc.amalthea.model.impl.ComponentsModelImpl#getSystems <em>Systems</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ComponentsModelImpl extends BaseObjectImpl implements ComponentsModel {
	/**
	 * The cached value of the '{@link #getComponents() <em>Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<Component> components;

	/**
	 * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<MainInterface> interfaces;

	/**
	 * The cached value of the '{@link #getStructures() <em>Structures</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStructures()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentStructure> structures;

	/**
	 * The cached value of the '{@link #getSystems() <em>Systems</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystems()
	 * @generated
	 * @ordered
	 */
	protected EList<org.eclipse.app4mc.amalthea.model.System> systems;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ComponentsModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AmaltheaPackage.eINSTANCE.getComponentsModel();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Component> getComponents() {
		if (components == null) {
			components = new EObjectContainmentEList<Component>(Component.class, this, AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS);
		}
		return components;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<MainInterface> getInterfaces() {
		if (interfaces == null) {
			interfaces = new EObjectContainmentEList<MainInterface>(MainInterface.class, this, AmaltheaPackage.COMPONENTS_MODEL__INTERFACES);
		}
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ComponentStructure> getStructures() {
		if (structures == null) {
			structures = new EObjectContainmentEList<ComponentStructure>(ComponentStructure.class, this, AmaltheaPackage.COMPONENTS_MODEL__STRUCTURES);
		}
		return structures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<org.eclipse.app4mc.amalthea.model.System> getSystems() {
		if (systems == null) {
			systems = new EObjectContainmentEList<org.eclipse.app4mc.amalthea.model.System>(org.eclipse.app4mc.amalthea.model.System.class, this, AmaltheaPackage.COMPONENTS_MODEL__SYSTEMS);
		}
		return systems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS:
				return ((InternalEList<?>)getComponents()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.COMPONENTS_MODEL__INTERFACES:
				return ((InternalEList<?>)getInterfaces()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.COMPONENTS_MODEL__STRUCTURES:
				return ((InternalEList<?>)getStructures()).basicRemove(otherEnd, msgs);
			case AmaltheaPackage.COMPONENTS_MODEL__SYSTEMS:
				return ((InternalEList<?>)getSystems()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS:
				return getComponents();
			case AmaltheaPackage.COMPONENTS_MODEL__INTERFACES:
				return getInterfaces();
			case AmaltheaPackage.COMPONENTS_MODEL__STRUCTURES:
				return getStructures();
			case AmaltheaPackage.COMPONENTS_MODEL__SYSTEMS:
				return getSystems();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS:
				getComponents().clear();
				getComponents().addAll((Collection<? extends Component>)newValue);
				return;
			case AmaltheaPackage.COMPONENTS_MODEL__INTERFACES:
				getInterfaces().clear();
				getInterfaces().addAll((Collection<? extends MainInterface>)newValue);
				return;
			case AmaltheaPackage.COMPONENTS_MODEL__STRUCTURES:
				getStructures().clear();
				getStructures().addAll((Collection<? extends ComponentStructure>)newValue);
				return;
			case AmaltheaPackage.COMPONENTS_MODEL__SYSTEMS:
				getSystems().clear();
				getSystems().addAll((Collection<? extends org.eclipse.app4mc.amalthea.model.System>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS:
				getComponents().clear();
				return;
			case AmaltheaPackage.COMPONENTS_MODEL__INTERFACES:
				getInterfaces().clear();
				return;
			case AmaltheaPackage.COMPONENTS_MODEL__STRUCTURES:
				getStructures().clear();
				return;
			case AmaltheaPackage.COMPONENTS_MODEL__SYSTEMS:
				getSystems().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS:
				return components != null && !components.isEmpty();
			case AmaltheaPackage.COMPONENTS_MODEL__INTERFACES:
				return interfaces != null && !interfaces.isEmpty();
			case AmaltheaPackage.COMPONENTS_MODEL__STRUCTURES:
				return structures != null && !structures.isEmpty();
			case AmaltheaPackage.COMPONENTS_MODEL__SYSTEMS:
				return systems != null && !systems.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == IComponentContainer.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS: return AmaltheaPackage.ICOMPONENT_CONTAINER__COMPONENTS;
				default: return -1;
			}
		}
		if (baseClass == IInterfaceContainer.class) {
			switch (derivedFeatureID) {
				case AmaltheaPackage.COMPONENTS_MODEL__INTERFACES: return AmaltheaPackage.IINTERFACE_CONTAINER__INTERFACES;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == IComponentContainer.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.ICOMPONENT_CONTAINER__COMPONENTS: return AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS;
				default: return -1;
			}
		}
		if (baseClass == IInterfaceContainer.class) {
			switch (baseFeatureID) {
				case AmaltheaPackage.IINTERFACE_CONTAINER__INTERFACES: return AmaltheaPackage.COMPONENTS_MODEL__INTERFACES;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //ComponentsModelImpl
