/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.CommonElements;
import org.eclipse.app4mc.amalthea.model.edit.common.container.CoreClassifiersContainerIP;
import org.eclipse.app4mc.amalthea.model.edit.common.container.MemoryClassifiersContainerIP;
import org.eclipse.app4mc.amalthea.model.edit.common.container.NamespacesContainerIP;
import org.eclipse.app4mc.amalthea.model.edit.common.container.TagsContainerIP;
import org.eclipse.app4mc.amalthea.model.provider.CommonElementsItemProvider;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandWrapper;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ViewerNotification;

public class ExtendedCommonElementsIP extends CommonElementsItemProvider {

	private static final Set<EStructuralFeature> SPECIAL_FEATURES = Set.of(
				AmaltheaPackage.eINSTANCE.getCommonElements_CoreClassifiers(), // COMMON_ELEMENTS__CORE_CLASSIFIERS
				AmaltheaPackage.eINSTANCE.getCommonElements_MemoryClassifiers(), // COMMON_ELEMENTS__MEMORY_CLASSIFIERS
				AmaltheaPackage.eINSTANCE.getCommonElements_Tags(), // COMMON_ELEMENTS__TAGS
				AmaltheaPackage.eINSTANCE.getCommonElements_Namespaces() // COMMON_ELEMENTS__NAMESPACES
			);

	private static final Set<Integer> SPECIAL_FEATURE_IDS =
			SPECIAL_FEATURES.stream()
				.mapToInt(EStructuralFeature::getFeatureID).boxed()
				.collect(Collectors.toCollection(HashSet::new));

	// Container item providers
	protected CoreClassifiersContainerIP coreClassifiersCIP;
	protected MemoryClassifiersContainerIP memoryClassifiersCIP;
	protected TagsContainerIP tagsCIP;
	protected NamespacesContainerIP namespacesCIP;

	public ExtendedCommonElementsIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	public CoreClassifiersContainerIP getCoreClassifiersContainerIP(final CommonElements owner) {
		if (this.coreClassifiersCIP == null) {
			this.coreClassifiersCIP = new CoreClassifiersContainerIP(this.adapterFactory, owner);
		}
		return this.coreClassifiersCIP;
	}

	public MemoryClassifiersContainerIP getMemoryClassifiersContainerIP(final CommonElements owner) {
		if (this.memoryClassifiersCIP == null) {
			this.memoryClassifiersCIP = new MemoryClassifiersContainerIP(this.adapterFactory, owner);
		}
		return this.memoryClassifiersCIP;
	}

	public TagsContainerIP getTagsContainerIP(final CommonElements owner) {
		if (this.tagsCIP == null) {
			this.tagsCIP = new TagsContainerIP(this.adapterFactory, owner);
		}
		return this.tagsCIP;
	}

	public NamespacesContainerIP getNamespacesContainerIP(final CommonElements owner) {
		if (this.namespacesCIP == null) {
			this.namespacesCIP = new NamespacesContainerIP(this.adapterFactory, owner);
		}
		return this.namespacesCIP;
	}

	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {

		for (EStructuralFeature feature : super.getChildrenFeatures(object)) {
			if (isValidValue(object, child, feature)) {
				return feature;
			}
		}
		return null;
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(final Object object) {
		// Do NOT modify cached collection!
		Set<? extends EStructuralFeature> result = new HashSet<>(super.getChildrenFeatures(object));

		// reduce result
		result.removeAll(SPECIAL_FEATURES);

		return result;
	}

	@Override
	public Collection<?> getChildren(final Object object) {
		final List<Object> children = new ArrayList<>(super.getChildren(object));
		final CommonElements commonElements = (CommonElements) object;

		// only display virtual folders if not empty
		if (!commonElements.getCoreClassifiers().isEmpty())
			children.add(getCoreClassifiersContainerIP(commonElements));
		if (!commonElements.getMemoryClassifiers().isEmpty())
			children.add(getMemoryClassifiersContainerIP(commonElements));
		if (!commonElements.getTags().isEmpty())
			children.add(getTagsContainerIP(commonElements));
		if (!commonElements.getNamespaces().isEmpty())
			children.add(getNamespacesContainerIP(commonElements));
		return children;
	}

	@Override
	protected Command createAddCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection, final int index) {
		return createWrappedCommand(super.createAddCommand(domain, owner, feature, collection, index), owner, feature);
	}

	@Override
	protected Command createRemoveCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection) {
		return createWrappedCommand(super.createRemoveCommand(domain, owner, feature, collection), owner, feature);
	}

	protected Command createWrappedCommand(final Command command, final EObject owner, final EStructuralFeature feature) {
		int featureID = feature.getFeatureID();

		if (!SPECIAL_FEATURE_IDS.contains(featureID)) {
			return command;
		}

		return new CommandWrapper(command) {
			@Override
			public Collection<?> getAffectedObjects() {
				Collection<?> affected = super.getAffectedObjects();
				if (affected.contains(owner)) {
					if (featureID == AmaltheaPackage.COMMON_ELEMENTS__CORE_CLASSIFIERS) {
						affected = Collections.singleton(getCoreClassifiersContainerIP((CommonElements) owner));
					} else if (featureID == AmaltheaPackage.COMMON_ELEMENTS__MEMORY_CLASSIFIERS) {
						affected = Collections.singleton(getMemoryClassifiersContainerIP((CommonElements) owner));
					} else if (featureID == AmaltheaPackage.COMMON_ELEMENTS__TAGS) {
						affected = Collections.singleton(getTagsContainerIP((CommonElements) owner));
					} else if (featureID == AmaltheaPackage.COMMON_ELEMENTS__NAMESPACES) {
						affected = Collections.singleton(getNamespacesContainerIP((CommonElements) owner));
					}
				}
				return affected;
			}
		};
	}

	@Override
	public void dispose() {
		if (this.coreClassifiersCIP != null) {
			this.coreClassifiersCIP.dispose();
		}
		if (this.memoryClassifiersCIP != null) {
			this.memoryClassifiersCIP.dispose();
		}
		if (this.tagsCIP != null) {
			this.tagsCIP.dispose();
		}
		if (this.namespacesCIP != null) {
			this.namespacesCIP.dispose();
		}

		super.dispose();
	}

	@Override
	public void notifyChanged(final Notification notification) {
		updateChildren(notification);

		int featureID = notification.getFeatureID(CommonElements.class);

		if (SPECIAL_FEATURE_IDS.contains(featureID)) {
			// update virtual folder labels
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, true));
		} else {
			// default
			super.notifyChanged(notification);
		}
	}

}
