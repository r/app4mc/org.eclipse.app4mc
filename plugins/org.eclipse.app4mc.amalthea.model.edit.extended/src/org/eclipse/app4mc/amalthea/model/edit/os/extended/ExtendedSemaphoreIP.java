/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.os.extended;

import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.edit.ExtendedOSModelIP;
import org.eclipse.app4mc.amalthea.model.provider.SemaphoreItemProvider;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

public class ExtendedSemaphoreIP extends SemaphoreItemProvider {

	public ExtendedSemaphoreIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(final Object object) {
		final Object parent = super.getParent(object);

		// if parent is OSModel then adapt to definitions container
		if (parent instanceof OSModel) {
			final Object parentIP = this.adapterFactory.adapt(parent, ITreeItemContentProvider.class);
			if (parentIP instanceof ExtendedOSModelIP) {
				return ((ExtendedOSModelIP) parentIP).getOsSemaphoresContainerIP((OSModel) parent);
			}
		}

		return parent;
	}

}
