/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.ComponentsModel;
import org.eclipse.app4mc.amalthea.model.edit.comp.container.CompDefinitionContainerIP;
import org.eclipse.app4mc.amalthea.model.edit.comp.container.CompInterfaceContainerIP;
import org.eclipse.app4mc.amalthea.model.edit.comp.container.CompStructureContainerIP;
import org.eclipse.app4mc.amalthea.model.provider.ComponentsModelItemProvider;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandWrapper;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ViewerNotification;

public class ExtendedComponentsModelIP extends ComponentsModelItemProvider {

	private static final Set<EStructuralFeature> SPECIAL_FEATURES = Set.of(
				AmaltheaPackage.eINSTANCE.getIComponentContainer_Components(), // COMPONENTS_MODEL__COMPONENTS
				AmaltheaPackage.eINSTANCE.getIInterfaceContainer_Interfaces(), // COMPONENTS_MODEL__INTERFACES
				AmaltheaPackage.eINSTANCE.getComponentsModel_Structures() // COMPONENTS_MODEL__STRUCTURES
			);

	private static final Set<Integer> SPECIAL_FEATURE_IDS =
			SPECIAL_FEATURES.stream()
				.mapToInt(EStructuralFeature::getFeatureID).boxed()
				.collect(Collectors.toCollection(HashSet::new));

	// Container item providers
	protected CompDefinitionContainerIP compDefinitionCIP;
	protected CompInterfaceContainerIP compInterfaceCIP;
	protected CompStructureContainerIP compStructureCIP;

	public ExtendedComponentsModelIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	public CompDefinitionContainerIP getCompDefinitionContainerIP(final ComponentsModel compModel) {
		if (this.compDefinitionCIP == null) {
			this.compDefinitionCIP = new CompDefinitionContainerIP(this.adapterFactory, compModel);
		}
		return this.compDefinitionCIP;
	}

	public CompInterfaceContainerIP getCompInterfaceContainerIP(final ComponentsModel compModel) {
		if (this.compInterfaceCIP == null) {
			this.compInterfaceCIP = new CompInterfaceContainerIP(this.adapterFactory, compModel);
		}
		return this.compInterfaceCIP;
	}

	public CompStructureContainerIP getCompStructureContainerIP(final ComponentsModel compModel) {
		if (this.compStructureCIP == null) {
			this.compStructureCIP = new CompStructureContainerIP(this.adapterFactory, compModel);
		}
		return this.compStructureCIP;
	}

	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {

		for (EStructuralFeature feature : super.getChildrenFeatures(object)) {
			if (isValidValue(object, child, feature)) {
				return feature;
			}
		}
		return null;
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(final Object object) {
		// Do NOT modify cached collection!
		Set<? extends EStructuralFeature> result = new HashSet<>(super.getChildrenFeatures(object));

		// reduce result
		result.removeAll(SPECIAL_FEATURES);

		return result;
	}

	@Override
	public Collection<?> getChildren(final Object object) {
		final List<Object> children = new ArrayList<>(super.getChildren(object));
		final ComponentsModel compModel = (ComponentsModel) object;

		// only display virtual folders if not empty (on top of the list)
		if (!compModel.getStructures().isEmpty())
			children.add(0, getCompStructureContainerIP(compModel));
		if (!compModel.getInterfaces().isEmpty())
			children.add(0, getCompInterfaceContainerIP(compModel));
		if (!compModel.getComponents().isEmpty())
			children.add(0, getCompDefinitionContainerIP(compModel));
		return children;
	}

	@Override
	protected Command createAddCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection, final int index) {
		return createWrappedCommand(super.createAddCommand(domain, owner, feature, collection, index), owner, feature);
	}

	@Override
	protected Command createRemoveCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection) {
		return createWrappedCommand(super.createRemoveCommand(domain, owner, feature, collection), owner, feature);
	}

	protected Command createWrappedCommand(final Command command, final EObject owner, final EStructuralFeature feature) {
		int featureID = feature.getFeatureID();

		if (!SPECIAL_FEATURE_IDS.contains(featureID)) {
			return command;
		}

		return new CommandWrapper(command) {
			@Override
			public Collection<?> getAffectedObjects() {
				Collection<?> affected = super.getAffectedObjects();
				if (affected.contains(owner)) {
					if (featureID == AmaltheaPackage.COMPONENTS_MODEL__COMPONENTS) {
						affected = Collections.singleton(getCompDefinitionContainerIP((ComponentsModel) owner));
					} else if (featureID == AmaltheaPackage.COMPONENTS_MODEL__INTERFACES) {
						affected = Collections.singleton(getCompInterfaceContainerIP((ComponentsModel) owner));
					} else if (featureID == AmaltheaPackage.COMPONENTS_MODEL__STRUCTURES) {
						affected = Collections.singleton(getCompStructureContainerIP((ComponentsModel) owner));
					}
				}
				return affected;
			}
		};
	}

	@Override
	public void dispose() {
		if (this.compDefinitionCIP != null) {
			this.compDefinitionCIP.dispose();
		}
		if (this.compInterfaceCIP != null) {
			this.compInterfaceCIP.dispose();
		}
		if (this.compStructureCIP != null) {
			this.compStructureCIP.dispose();
		}

		super.dispose();
	}

	@Override
	public void notifyChanged(final Notification notification) {
		updateChildren(notification);

		int featureID = notification.getFeatureID(ComponentsModel.class);

		if (SPECIAL_FEATURE_IDS.contains(featureID)) {
			// update virtual folder labels
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, true));
		} else {
			// default
			super.notifyChanged(notification);
		}
	}

}
