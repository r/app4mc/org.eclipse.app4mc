/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.hw.extended;

import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.provider.ConnectionHandlerItemProvider;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

public class ExtendedConnectionHandlerIP extends ConnectionHandlerItemProvider {

	public ExtendedConnectionHandlerIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(final Object object) {
		final Object parent = super.getParent(object);

		// if parent is HwStructure then adapt to modules container
		if (parent instanceof HwStructure) {
			final Object parentIP = this.adapterFactory.adapt(parent, ITreeItemContentProvider.class);
			if (parentIP instanceof ExtendedHwStructureIP) {
				return ((ExtendedHwStructureIP) parentIP).getModulesContainerIP((HwStructure) parent);
			}
		}

		return parent;
	}

}
