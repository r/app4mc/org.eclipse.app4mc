/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.comp.extended;

import org.eclipse.app4mc.amalthea.model.Composite;
import org.eclipse.app4mc.amalthea.model.System;
import org.eclipse.app4mc.amalthea.model.provider.ConnectorItemProvider;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

public class ExtendedConnectorIP extends ConnectorItemProvider {

	public ExtendedConnectorIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(final Object object) {
		final Object parent = super.getParent(object);

		if (parent instanceof System) {
			final ExtendedSystemIP systemItemProvider = (ExtendedSystemIP) this.adapterFactory
					.adapt(parent, ITreeItemContentProvider.class);
			return systemItemProvider != null ? systemItemProvider.getCompConnectorContainerIP((System) parent) : null;
		}

		if (parent instanceof Composite) {
			final ExtendedCompositeIP compositeItemProvider = (ExtendedCompositeIP) this.adapterFactory
					.adapt(parent, ITreeItemContentProvider.class);
			return compositeItemProvider != null ? compositeItemProvider.getCompConnectorContainerIP((Composite) parent) : null;
		}

		return parent;
	}

}
