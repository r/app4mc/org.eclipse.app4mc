/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.common.container;

import java.util.Collection;

import org.eclipse.app4mc.amalthea.model.CommonElements;
import org.eclipse.app4mc.amalthea.model.provider.TransientItemProvider;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EStructuralFeature;

public class MemoryClassifiersContainerIP extends TransientItemProvider {

	public MemoryClassifiersContainerIP(final AdapterFactory adapterFactory, final CommonElements parent) {
		super(adapterFactory);
		parent.eAdapters().add(this);
	}

	@Override
	public EStructuralFeature myFeature() {
		return myPackage().getCommonElements_MemoryClassifiers();
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getText(java.lang.Object)
	 */
	@Override
	public String getText(final Object object) {
		final StringBuilder buffer = new StringBuilder();
		buffer.append("Memory Classifiers ("); //$NON-NLS-1$
		buffer.append(((CommonElements) getTarget()).getMemoryClassifiers().size());
		buffer.append(")"); //$NON-NLS-1$

		return buffer.toString();
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#collectNewChildDescriptors(java.util.Collection,
	 *      java.lang.Object)
	 */
	@Override
	protected void collectNewChildDescriptors(final Collection<Object> newChildDescriptors, final Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
		newChildDescriptors.add(createChildParameter(myFeature(), myFactory().createMemoryClassifier()));
	}

}
