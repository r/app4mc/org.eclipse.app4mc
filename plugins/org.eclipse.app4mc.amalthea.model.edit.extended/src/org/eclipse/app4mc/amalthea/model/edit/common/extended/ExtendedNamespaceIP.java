/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.common.extended;

import org.eclipse.app4mc.amalthea.model.CommonElements;
import org.eclipse.app4mc.amalthea.model.edit.ExtendedCommonElementsIP;
import org.eclipse.app4mc.amalthea.model.provider.NamespaceItemProvider;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

public class ExtendedNamespaceIP extends NamespaceItemProvider {

	public ExtendedNamespaceIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(final Object object) {
		final Object parent = super.getParent(object);

		if (parent instanceof CommonElements) {
			final ExtendedCommonElementsIP parentItemProvider = (ExtendedCommonElementsIP) this.adapterFactory
					.adapt(parent, ITreeItemContentProvider.class);
			return parentItemProvider != null ? parentItemProvider.getNamespacesContainerIP((CommonElements) parent) : null;
		}

		return parent;
	}

}
