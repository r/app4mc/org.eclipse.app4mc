/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.amalthea.model.edit.hw.container.HwDefinitionContainerIP;
import org.eclipse.app4mc.amalthea.model.edit.hw.container.HwDomainContainerIP;
import org.eclipse.app4mc.amalthea.model.edit.hw.container.HwFeatureContainerIP;
import org.eclipse.app4mc.amalthea.model.provider.HWModelItemProvider;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandWrapper;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ViewerNotification;

public class ExtendedHWModelIP extends HWModelItemProvider {

	private static final Set<EStructuralFeature> SPECIAL_FEATURES = Set.of(
				AmaltheaPackage.eINSTANCE.getHWModel_FeatureCategories(), // HW_MODEL__FEATURE_CATEGORIES
				AmaltheaPackage.eINSTANCE.getHWModel_Definitions(), // HW_MODEL__DEFINITIONS
				AmaltheaPackage.eINSTANCE.getHWModel_Domains() // HW_MODEL__DOMAINS
			);

	private static final Set<Integer> SPECIAL_FEATURE_IDS =
			SPECIAL_FEATURES.stream()
				.mapToInt(EStructuralFeature::getFeatureID).boxed()
				.collect(Collectors.toCollection(HashSet::new));

	// Container item providers
	protected HwFeatureContainerIP hwFeatureCIP;
	protected HwDefinitionContainerIP hwDefinitionCIP;
	protected HwDomainContainerIP hwDomainCIP;

	public ExtendedHWModelIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	public HwFeatureContainerIP getHwFeaturesContainerIP(final HWModel hwModel) {
		if (this.hwFeatureCIP == null) {
			this.hwFeatureCIP = new HwFeatureContainerIP(this.adapterFactory, hwModel);
		}
		return this.hwFeatureCIP;
	}

	public HwDefinitionContainerIP getHwDefinitionsContainerIP(final HWModel hwModel) {
		if (this.hwDefinitionCIP == null) {
			this.hwDefinitionCIP = new HwDefinitionContainerIP(this.adapterFactory, hwModel);
		}
		return this.hwDefinitionCIP;
	}

	public HwDomainContainerIP getHwDomainsContainerIP(final HWModel hwModel) {
		if (this.hwDomainCIP == null) {
			this.hwDomainCIP = new HwDomainContainerIP(this.adapterFactory, hwModel);
		}
		return this.hwDomainCIP;
	}

	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {

		for (EStructuralFeature feature : super.getChildrenFeatures(object)) {
			if (isValidValue(object, child, feature)) {
				return feature;
			}
		}
		return null;
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(final Object object) {
		// Do NOT modify cached collection!
		Set<? extends EStructuralFeature> result = new HashSet<>(super.getChildrenFeatures(object));

		// reduce result
		result.removeAll(SPECIAL_FEATURES);

		return result;
	}

	@Override
	public Collection<?> getChildren(final Object object) {
		final List<Object> children = new ArrayList<>(super.getChildren(object));
		final HWModel hwModel = (HWModel) object;

		// only display virtual folders if not empty (on top of the list)
		if (!hwModel.getFeatureCategories().isEmpty())
			children.add(0, getHwFeaturesContainerIP(hwModel));
		if (!hwModel.getDomains().isEmpty())
			children.add(0, getHwDomainsContainerIP(hwModel));
		if (!hwModel.getDefinitions().isEmpty())
			children.add(0, getHwDefinitionsContainerIP(hwModel));
		return children;
	}

	@Override
	protected Command createAddCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection, final int index) {
		return createWrappedCommand(super.createAddCommand(domain, owner, feature, collection, index), owner, feature);
	}

	@Override
	protected Command createRemoveCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection) {
		return createWrappedCommand(super.createRemoveCommand(domain, owner, feature, collection), owner, feature);
	}

	protected Command createWrappedCommand(final Command command, final EObject owner, final EStructuralFeature feature) {
		int featureID = feature.getFeatureID();

		if (!SPECIAL_FEATURE_IDS.contains(featureID)) {
			return command;
		}

		return new CommandWrapper(command) {
			@Override
			public Collection<?> getAffectedObjects() {
				Collection<?> affected = super.getAffectedObjects();
				if (affected.contains(owner)) {
					if (featureID == AmaltheaPackage.HW_MODEL__FEATURE_CATEGORIES) {
						affected = Collections.singleton(getHwFeaturesContainerIP((HWModel) owner));
					} else if (featureID == AmaltheaPackage.HW_MODEL__DEFINITIONS) {
						affected = Collections.singleton(getHwDefinitionsContainerIP((HWModel) owner));
					} else if (featureID == AmaltheaPackage.HW_MODEL__DOMAINS) {
						affected = Collections.singleton(getHwDomainsContainerIP((HWModel) owner));
					}
				}
				return affected;
			}
		};
	}

	@Override
	public void dispose() {
		if (this.hwFeatureCIP != null) {
			this.hwFeatureCIP.dispose();
		}
		if (this.hwDefinitionCIP != null) {
			this.hwDefinitionCIP.dispose();
		}
		if (this.hwDomainCIP != null) {
			this.hwDomainCIP.dispose();
		}

		super.dispose();
	}

	@Override
	public void notifyChanged(final Notification notification) {
		updateChildren(notification);

		int featureID = notification.getFeatureID(HWModel.class);

		if (SPECIAL_FEATURE_IDS.contains(featureID)) {
			// update virtual folder labels
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, true));
		} else {
			// default
			super.notifyChanged(notification);
		}
	}

}
