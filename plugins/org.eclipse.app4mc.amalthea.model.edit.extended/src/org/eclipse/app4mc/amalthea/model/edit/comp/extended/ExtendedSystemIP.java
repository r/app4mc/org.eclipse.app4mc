/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.comp.extended;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.ISystem;
import org.eclipse.app4mc.amalthea.model.edit.comp.container.CompConnectorContainerIP;
import org.eclipse.app4mc.amalthea.model.edit.comp.container.CompInstanceContainerIP;
import org.eclipse.app4mc.amalthea.model.provider.SystemItemProvider;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandWrapper;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ViewerNotification;

public class ExtendedSystemIP extends SystemItemProvider {

	protected CompConnectorContainerIP compConnectorCIP;
	protected CompInstanceContainerIP compInstanceCIP;

	private final EStructuralFeature featureCONNECTORS = AmaltheaPackage.eINSTANCE.getISystem_Connectors();
	private final EStructuralFeature featureINSTANCES = AmaltheaPackage.eINSTANCE.getISystem_ComponentInstances();

	public ExtendedSystemIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	public CompConnectorContainerIP getCompConnectorContainerIP(final ISystem compSystem) {
		if (this.compConnectorCIP == null) {
			this.compConnectorCIP = new CompConnectorContainerIP(this.adapterFactory, compSystem);
		}
		return this.compConnectorCIP;
	}

	public CompInstanceContainerIP getCompInstanceContainerIP(final ISystem compSystem) {
		if (this.compInstanceCIP == null) {
			this.compInstanceCIP = new CompInstanceContainerIP(this.adapterFactory, compSystem);
		}
		return this.compInstanceCIP;
	}

	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {

		for (EStructuralFeature feature : super.getChildrenFeatures(object)) {
			if (isValidValue(object, child, feature)) {
				return feature;
			}
		}
		return null;
	}

	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(final Object object) {
		// Do NOT modify cached collection!
		Set<? extends EStructuralFeature> result = new HashSet<>(super.getChildrenFeatures(object));

		// reduce result
		result.remove(this.featureCONNECTORS);
		result.remove(this.featureINSTANCES);

		return result;
	}

	@Override
	public Collection<?> getChildren(final Object object) {
		final List<Object> children = new ArrayList<>(super.getChildren(object));
		ISystem composite = (ISystem) object;

		// only display virtual folders if not empty (on top of the list)
		if (!composite.getComponentInstances().isEmpty())
			children.add(0, getCompInstanceContainerIP(composite));
		if (!composite.getConnectors().isEmpty())
			children.add(0, getCompConnectorContainerIP(composite));

		return children;
	}

	@Override
	protected Command createAddCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection, final int index) {
		return createWrappedCommand(super.createAddCommand(domain, owner, feature, collection, index), owner, feature);
	}

	@Override
	protected Command createRemoveCommand(final EditingDomain domain, final EObject owner,
			final EStructuralFeature feature, final Collection<?> collection) {
		return createWrappedCommand(super.createRemoveCommand(domain, owner, feature, collection), owner, feature);
	}

	protected Command createWrappedCommand(final Command command, final EObject owner,
			final EStructuralFeature feature) {
		if (feature == featureCONNECTORS || feature == featureINSTANCES) {
			return new CommandWrapper(command) {
				@Override
				public Collection<?> getAffectedObjects() {
					Collection<?> affected = super.getAffectedObjects();
					if (affected.contains(owner)) {
						if (feature == featureCONNECTORS) {
							affected = Collections.singleton(getCompConnectorContainerIP(((ISystem) owner)));
						} else if (feature == featureINSTANCES) {
							affected = Collections.singleton(getCompInstanceContainerIP(((ISystem) owner)));
						}
					}
					return affected;
				}
			};
		}
		return command;
	}

	@Override
	public void dispose() {
		if (this.compConnectorCIP != null) {
			this.compConnectorCIP.dispose();
		}
		if (this.compInstanceCIP != null) {
			this.compInstanceCIP.dispose();
		}
		super.dispose();
	}

	@Override
	public void notifyChanged(final Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ISystem.class)) {
		case AmaltheaPackage.ISYSTEM__CONNECTORS:
		case AmaltheaPackage.ISYSTEM__COMPONENT_INSTANCES:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, true));
			return;
		default:
			super.notifyChanged(notification);
		}
	}

}