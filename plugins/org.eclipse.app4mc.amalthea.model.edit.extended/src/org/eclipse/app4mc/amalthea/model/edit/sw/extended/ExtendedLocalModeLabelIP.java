/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.edit.sw.extended;

import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.provider.LocalModeLabelItemProvider;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

public class ExtendedLocalModeLabelIP extends LocalModeLabelItemProvider {

	public ExtendedLocalModeLabelIP(final AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * @see org.eclipse.emf.edit.provider.ItemProviderAdapter#getParent(java.lang.Object)
	 */
	@Override
	public Object getParent(final Object object) {
		final Object executable = super.getParent(object);
		if (executable instanceof Runnable) {
			final ExtendedRunnableIP runnableIP = (ExtendedRunnableIP) this.adapterFactory
					.adapt(executable, ITreeItemContentProvider.class);
			return runnableIP != null ? runnableIP.getLocalLabelsContainerIP((Runnable) executable) : null;
		}
		return executable;
	}

}
