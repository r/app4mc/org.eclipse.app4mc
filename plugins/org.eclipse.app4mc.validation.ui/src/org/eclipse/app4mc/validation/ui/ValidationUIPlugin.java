/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;

public class ValidationUIPlugin extends AbstractUIPlugin {

	/**
	 * The shared instance
	 */
	private static ValidationUIPlugin plugin = new ValidationUIPlugin();

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ValidationUIPlugin getDefault() {
		return plugin;
	}

	/**
	 * @return the plugin's id.
	 */
	public static String getPluginId() {
		return plugin.getBundle().getSymbolicName();
	}

}
