/**
 ********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.ui.provider;

import java.util.Comparator;

import org.eclipse.app4mc.validation.ui.ProfileDialogSettings;
import org.eclipse.app4mc.validation.util.CachedProfile;
import org.eclipse.jface.viewers.ITreeContentProvider;

public class ProfileContentProvider implements ITreeContentProvider {

	@Override
	public Object[] getElements(Object inputElement) {
		return ((ProfileDialogSettings) inputElement).getProfiles();
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		CachedProfile profile = (CachedProfile) parentElement;
		return profile.getCachedProfiles().values().stream()
				.sorted(Comparator.comparing(CachedProfile::getName))
				.toArray();
	}

	@Override
	public Object getParent(Object element) {
		CachedProfile profile = (CachedProfile) element;
		return profile.getParentProfile();
	}

	@Override
	public boolean hasChildren(Object element) {
		CachedProfile profile = (CachedProfile) element;
		return !profile.getCachedProfiles().isEmpty();
	}

}
