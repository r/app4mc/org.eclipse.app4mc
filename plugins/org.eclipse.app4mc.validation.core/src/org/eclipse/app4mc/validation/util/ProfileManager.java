/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.util;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.IProfileConfiguration;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

@Component(service=ProfileManager.class)
public class ProfileManager {

	private final HashMap<Class<? extends IProfile>, CachedProfile> profileMap = new HashMap<>();

	@Reference(
			cardinality = ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC,
			policyOption = ReferencePolicyOption.GREEDY)
	public void bindProfileConfiguration(IProfileConfiguration config) {
		synchronized (profileMap) {
			Class<? extends IProfile> profileClass = config.getClass();
			profileMap.put(profileClass, getCachedProfile(profileClass));
		}
	}

	public void unbindProfileConfiguration(IProfileConfiguration config) {
		synchronized (profileMap) {
			profileMap.remove(config.getClass());
		}
	}

	public Map<Class<? extends IProfile>, CachedProfile> getRegisteredValidationProfiles() {
		return Collections.unmodifiableMap(profileMap);
	}

	protected void loadProfile(Class<? extends IProfile> profileClass) {
		if (profileClass == null) {
			return;
		}

		profileMap.put(profileClass, new CachedProfile(profileClass));
	}

	public CachedProfile getCachedProfile(Class<? extends IProfile> profileClass) {
		if (profileClass == null) {
			return null;
		}

		if (!profileMap.containsKey(profileClass)) {
			loadProfile(profileClass);
		}

		return profileMap.get(profileClass);
	}

	// *** Some helper methods

	public void dumpProfile(Class<? extends IProfile> profileClass, PrintStream out) {
		if (out == null) return;

		CachedProfile profile = getCachedProfile(profileClass);
		dumpProfile(profile, 0, out);
	}

	private void dumpProfile(CachedProfile profile, int indentLevel, PrintStream stream) {
		if (profile == null) return;

		char[] charArray = new char[indentLevel * 4];
		Arrays.fill(charArray, ' ');
		String indent = new String(charArray);

		// Profile data
		stream.println(indent + "PROFILE " + profile.getProfileClass().getSimpleName());
		stream.println(indent + " - name: " + profile.getName());
		if (!profile.getDescription().isEmpty()) {
			stream.println(indent + " - description: " + profile.getDescription());
		}

		// Validations
		if (!profile.getCachedValidations().isEmpty()) {
			stream.println(indent + " - validations:");
		}
		for (CachedValidator conf : profile.getCachedValidations().values()) {
			stream.println(indent + "    " + conf.getValidationID() + "(" + conf.getSeverity() + " - "
					+ conf.getTargetClass().getSimpleName() + ")");

			for (String check : conf.getValidationChecks()) {
				stream.println(indent + "        * " + check);
			}
		}

		// Sub-Profiles
		if (!profile.getCachedProfiles().isEmpty()) {
			stream.println(indent + " - profiles:");
		}
		for (CachedProfile conf : profile.getCachedProfiles().values()) {
			dumpProfile(conf, 1, stream);
		}
	}

}
