/**
 ********************************************************************************
 * Copyright (c) 2019-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.validation.util;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.stream.Collectors;

import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.IValidation;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * A validator for EMF models. Loads the validations from a list of
 * {@link IProfile} classes
 */
public class ValidationExecutor {

	private static final String SEPARATOR = "--------------------------------";

	private final ConcurrentMap<EClassifier, CopyOnWriteArraySet<CachedValidator>> validationMap;
	protected final ConcurrentLinkedQueue<ValidationDiagnostic> results = new ConcurrentLinkedQueue<>();

	public ValidationExecutor(final Class<? extends IProfile> profileClass) {
		if (profileClass == null) {
			throw new IllegalArgumentException("Loading aborted - Undefined profile class (null)");
		}

		ValidationAggregator validationAggregator = new ValidationAggregator();
		validationAggregator.addProfile(profileClass);
		this.validationMap = validationAggregator.getConcurrentValidationMap();
	}

	public ValidationExecutor(final Collection<Class<? extends IProfile>> profileClasses) {
		if (profileClasses == null || profileClasses.isEmpty()) {
			throw new IllegalArgumentException("Loading aborted - Undefined profile classes");
		}

		ValidationAggregator validationAggregator = new ValidationAggregator();
		validationAggregator.addProfiles(profileClasses);
		this.validationMap = validationAggregator.getConcurrentValidationMap();
	}

	public ValidationExecutor(final ConcurrentMap<EClassifier, CopyOnWriteArraySet<CachedValidator>> validationMap) {
		this.validationMap = validationMap;
	}

	/**
	 * Validates the model object (and all contained objects)
	 *
	 * @param model		the model object to validate
	 */
	public void validate(final EObject model) {
		validate(Collections.singletonList(model));
	}

	/**
	 * Validates the model objects (and all contained objects)
	 *
	 * @param models		the list of model objects to validate
	 */
	public void validate(final List<EObject> models) {
		this.results.clear();

		for (EObject eObject : models) {
			validateObject(eObject); // need to validate the root, because the iterator misses this one

			TreeIterator<EObject> iterator = EcoreUtil.getAllContents(eObject, false);
			while (iterator.hasNext()) {
				validateObject(iterator.next());
			}
		}
	}

	public List<ValidationDiagnostic> getResults() {
		return new ArrayList<>(this.results);
	}

	protected void validateObject(final EObject object) {
		if (object == null) return;

		Set<CachedValidator> validations = validationMap.get(object.eClass());
		if (validations == null) return;

		for (CachedValidator validator : validations) {
			List<ValidationDiagnostic> resultList = new ArrayList<>();
			try {
				validator.getValidatorInstance().validate(object, resultList);
			} catch (Exception ex) {
				addExceptionResult(object, validator.getValidatorClass(), ex);
				continue;
			}

			// set message id and severity
			for (ValidationDiagnostic result : resultList) {
				result.setValidationID(validator.getValidationID());
				if (validator.getSeverity() != Severity.UNDEFINED) {
					result.setSeverityLevel(validator.getSeverity());
				}
				this.results.add(result);
			}
		}

	}

	private void addExceptionResult(final EObject object, Class<? extends IValidation> validatorClass, Exception ex) {
		ValidationDiagnostic exception = new ValidationDiagnostic(
				"Validation exception in " + validatorClass + ": " + ex.getMessage(), object);
		exception.setValidationID("AM-Runtime-Exception");
		exception.setSeverityLevel(Severity.ERROR);
		this.results.add(exception);
	}

	public void dumpValidationMap(PrintStream out) {
		if (out == null) return;

		List<EClassifier> classifiers = validationMap.keySet().stream()
				.sorted(Comparator.comparing(EClassifier::getName)).collect(Collectors.toList());

		for (EClassifier cl : classifiers) {
			out.println("Validations for " + cl.getName() + ":");
			for (CachedValidator vConf : validationMap.get(cl)) {
				out.println("    " + vConf.getSeverity() + " - " + vConf.getValidationID());
			}
		}
	}

	public void dumpResultMap(PrintStream out) {
		if (out == null) return;

		Map<EObject, List<ValidationDiagnostic>> map = results.stream()
				.collect(Collectors.groupingBy(ValidationDiagnostic::getTargetObject));

		out.println("Results (per object):");
		out.println(SEPARATOR);

		for (Entry<EObject, List<ValidationDiagnostic>> entry : map.entrySet()) {
			out.println("EObject: " + entry.getKey());
			out.println(SEPARATOR);

			for (ValidationDiagnostic result : entry.getValue()) {
				out.println("  " + result.getValidationID() + " -- " + result.getSeverityLevel());
				out.println("  " + result.getMessage());
				out.println(SEPARATOR);
			}
		}
	}

}
