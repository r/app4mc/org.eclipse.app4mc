/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

public abstract class AConverter implements IRunnableWithProgress {

	protected final Amalthea model;
	protected final ATDBConnection con;
	private final String converionSubject;

	protected AConverter(final Amalthea model, final ATDBConnection con, final String conversionSubject) {
		this.model = model;
		this.con = con;
		converionSubject = conversionSubject;
	}

	@Override
	public void run(IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException {
		progressMonitor.beginTask("Converting " + converionSubject + "...", 1);
		try {
			execute();
			progressMonitor.worked(1);
		} catch (SQLException e) {
			throw new InvocationTargetException(e);
		} finally {
			progressMonitor.done();
		}
	}

	protected abstract void execute() throws InvocationTargetException, InterruptedException, SQLException;

}
