/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb.handler;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;

public class ATDBAndOptionalAMXMIExpression {

	@Evaluate
	public boolean evaluate(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection) {
		if (selection != null && selection.size() >= 1 && selection.size() <= 2) {
			final List<String> extensions = Stream.of(selection.toArray()).filter(IFile.class::isInstance)
				.map(IFile.class::cast).map(IFile::getFileExtension).collect(Collectors.toList());
			if (extensions.size() == selection.size() && extensions.contains("atdb") &&
				(selection.size() == 1 || extensions.contains("amxmi"))) {
				return true;
			}
		}
		return false;
	}

}
