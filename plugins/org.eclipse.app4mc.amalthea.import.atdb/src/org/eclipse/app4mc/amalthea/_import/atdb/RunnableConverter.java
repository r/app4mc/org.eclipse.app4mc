/**
 ********************************************************************************
 * Copyright (c) 2020-2022 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea._import.atdb.AmaltheaModelUtil.TypedRoleInEObject;
import org.eclipse.app4mc.amalthea.model.ActivityGraph;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.AmaltheaServices;
import org.eclipse.app4mc.amalthea.model.Frequency;
import org.eclipse.app4mc.amalthea.model.FrequencyUnit;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.LabelAccess;
import org.eclipse.app4mc.amalthea.model.LabelAccessEnum;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.TimeUnit;
import org.eclipse.app4mc.amalthea.model.util.FactoryUtil;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.app4mc.amalthea.model.util.RuntimeUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.MetricAggregation;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class RunnableConverter extends AConverter {

	private static final String RUNNINGTIME_PREFIX = "runningTime_";

	private final boolean extractRuntimes;
	private final boolean extractLabelAccesses;
	private final Frequency frequency;

	public RunnableConverter(final Amalthea model, final ATDBConnection con,
			final boolean extractRuntimes, final boolean extractLabelAccesses, final double frequencyInHz) {
		super(model, con, "runnables");
		this.extractRuntimes = extractRuntimes;
		this.extractLabelAccesses = extractLabelAccesses;
		frequency = FactoryUtil.createFrequency(frequencyInHz, FrequencyUnit.HZ);
	}

	@Override
	protected void execute() throws SQLException {
		final SWModel swModel = ModelUtil.getOrCreateSwModel(model);
		final TimeUnit timeBase = TimeUnit.getByName(con.getTimeBase().toLowerCase());

		final List<Runnable> runnables = new ArrayList<>();
		con.getAllRunnables().forEach(runnableName -> {
			final Runnable runnable = AmaltheaModelUtil.getOrAddNew(swModel,
					AmaltheaPackage.eINSTANCE.getSWModel_Runnables(), runnableName, Runnable.class);
			runnables.add(runnable);
		});


		for(final Runnable runnable:runnables) {
			importRunnable(runnable, timeBase);
		}
	}
	
	private void importRunnable(final Runnable runnable, final TimeUnit timeBase) throws SQLException {
		final ActivityGraph ag = ModelUtil.getOrCreateActivityGraph(runnable);
		// label reads
		importLabelAccesses(runnable, LabelAccessEnum.READ);
		
		// runtimes
		if (extractRuntimes && timeBase != null && frequency.getValue() >= 1) {
			double runningTimeAvg = -1;
			double runningTimeStDev = -1;
			try {
				runningTimeAvg = Double.parseDouble(con.getValueForMetricAndEntity(runnable.getName(), RUNNINGTIME_PREFIX + MetricAggregation.Avg));
				runningTimeStDev = Double.parseDouble(con.getValueForMetricAndEntity(runnable.getName(), RUNNINGTIME_PREFIX + MetricAggregation.StDev));
			} catch (NumberFormatException e) {
				// fail silently
			}
			try {
				final long runningTimeMin = Long.parseLong(con.getValueForMetricAndEntity(runnable.getName(), RUNNINGTIME_PREFIX + MetricAggregation.Min));
				final long runningTimeMax = Long.parseLong(con.getValueForMetricAndEntity(runnable.getName(), RUNNINGTIME_PREFIX + MetricAggregation.Max));
				final Ticks ticks = AmaltheaFactory.eINSTANCE.createTicks();
				IDiscreteValueDeviation dvd;
				if (runningTimeMin == runningTimeMax) {
					final long constTicks = (long)RuntimeUtil.getTicksForExecutionTimeInSeconds(
							AmaltheaServices.convertToSeconds(runningTimeMin, timeBase), frequency);
					dvd = FactoryUtil.createDiscreteValueConstant(constTicks);
				} else {
					final long ticksMin = (long)RuntimeUtil.getTicksForExecutionTimeInSeconds(
							AmaltheaServices.convertToSeconds(runningTimeMin, timeBase), frequency);
					final long ticksMax = (long)RuntimeUtil.getTicksForExecutionTimeInSeconds(
							AmaltheaServices.convertToSeconds(runningTimeMax, timeBase), frequency);
					if (runningTimeAvg >= 0 && runningTimeStDev >= 0) {
						final double ticksAvg = RuntimeUtil.getTicksForExecutionTimeInSeconds(
								AmaltheaServices.convertToSeconds(runningTimeAvg, timeBase), frequency);
						final double ticksStDev = RuntimeUtil.getTicksForExecutionTimeInSeconds(
								AmaltheaServices.convertToSeconds(runningTimeStDev, timeBase), frequency);
						dvd = FactoryUtil.createDiscreteValueGaussDistribution(ticksAvg, ticksStDev, ticksMin, ticksMax);
					} else {
						dvd = FactoryUtil.createDiscreteValueBoundaries(ticksMin, ticksMax);
					}
				}
				ticks.setDefault(dvd);
				RuntimeUtil.clearRuntimeOfRunnable(runnable, null);
				ag.getItems().add(ticks);
			} catch (NumberFormatException e) {
				// fail silently
			}
		}
		// label writes
		importLabelAccesses(runnable, LabelAccessEnum.WRITE);
		
		if (ag.getItems().isEmpty()) {
			EcoreUtil.delete(ag);
		}
	}
	
	private void importLabelAccesses(final Runnable runnable, LabelAccessEnum accessKind) throws SQLException {
		if (extractLabelAccesses && (accessKind == LabelAccessEnum.READ || accessKind == LabelAccessEnum.WRITE)) {
			final ActivityGraph ag = ModelUtil.getOrCreateActivityGraph(runnable);
			final List<String> labelNames = accessKind == LabelAccessEnum.READ
					? con.getLabelsReadByRunnable(runnable.getName())
					: con.getLabelsWrittenByRunnable(runnable.getName());
			labelNames.forEach(labelName -> {
				final LabelAccess la = AmaltheaModelUtil.getOrAddNewWithContainer(ag,
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getIActivityGraphItemContainer_Items(),
								LabelAccess.class,
								AmaltheaPackage.eINSTANCE.getLabelAccess()),
						new TypedRoleInEObject<>(AmaltheaPackage.eINSTANCE.getLabelAccess_Data(),
								Label.class,
								AmaltheaPackage.eINSTANCE.getLabel()),
						labelName).getKey();
				la.setAccess(accessKind);
			});
		}
	}

}
