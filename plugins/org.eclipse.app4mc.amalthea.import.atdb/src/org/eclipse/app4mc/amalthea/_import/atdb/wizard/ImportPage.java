/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Timing-Architects Embedded Systems GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb.wizard;

import java.io.File;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.WizardResourceImportPage;
import org.eclipse.ui.internal.ide.IDEWorkbenchMessages;

@SuppressWarnings("restriction")
public class ImportPage extends WizardResourceImportPage {

	private Text sourceNameField;
	private Button extractLabelsAndAccesses;
	private Button extractRunnableRuntimes;
	private Composite handleExistingGroup;
	private boolean overwriteExisting = false;

	protected ImportPage(final IStructuredSelection selection) {
		this("atdbImportPage", selection); //$NON-NLS-1$
		setTitle(Messages.ImportPage_title);
		this.setMessage(Messages.ImportPage_message);
	}

	protected ImportPage(final String name, final IStructuredSelection selection) {
		super(name, selection);
	}

	@Override
	protected void createSourceGroup(final Composite parent) {
		final Composite sourceContainerGroup = new Composite(parent, SWT.NONE);
		final GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		sourceContainerGroup.setLayout(layout);
		sourceContainerGroup.setFont(parent.getFont());
		sourceContainerGroup.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL));

		// source label
		final Label groupLabel = new Label(sourceContainerGroup, SWT.NONE);
		groupLabel.setText(Messages.ImportPage_fromATDB);
		groupLabel.setFont(parent.getFont());

		// source name entry field
		sourceNameField = new Text(sourceContainerGroup, SWT.READ_ONLY | SWT.BORDER);
		final GridData data = new GridData(GridData.HORIZONTAL_ALIGN_FILL | GridData.GRAB_HORIZONTAL);
		data.widthHint = SIZING_TEXT_FIELD_WIDTH;
		sourceNameField.setLayoutData(data);
		sourceNameField.setFont(parent.getFont());

		// source browse button
		final Button sourceBrowseButton = new Button(sourceContainerGroup, SWT.PUSH);
		sourceBrowseButton.setText(IDEWorkbenchMessages.WizardImportPage_browse2);
		sourceBrowseButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				selectFile();
			}
		});
		sourceBrowseButton.setLayoutData(new GridData(GridData.HORIZONTAL_ALIGN_FILL));
		sourceBrowseButton.setFont(parent.getFont());
		setButtonLayoutData(sourceBrowseButton);
	}

	@Override
	protected void createOptionsGroupButtons(Group optionsGroup) {
		extractLabelsAndAccesses = new Button(optionsGroup, SWT.CHECK);
		extractLabelsAndAccesses.setFont(optionsGroup.getFont());
		extractLabelsAndAccesses.setText(Messages.ImportPage_optionExtractLabelsAndAccesses);
		extractLabelsAndAccesses.setSelection(true);

		extractRunnableRuntimes = new Button(optionsGroup, SWT.CHECK);
		extractRunnableRuntimes.setFont(optionsGroup.getFont());
		extractRunnableRuntimes.setText(Messages.ImportPage_optionExtractRunableRuntimes);
		extractRunnableRuntimes.setSelection(true);

		handleExistingGroup = new Composite(optionsGroup, SWT.NONE);
		handleExistingGroup.setLayout(new RowLayout(SWT.HORIZONTAL));
		final Button updateExisting = new Button(handleExistingGroup, SWT.RADIO);
		updateExisting.setFont(optionsGroup.getFont());
		updateExisting.setText("Update existing");
		updateExisting.setSelection(true);
		updateExisting.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final Button source = (Button) e.getSource();
				if (source.getSelection()) {
					overwriteExisting = false;
				}
			}
		});
		final Button localOverwriteExisting = new Button(handleExistingGroup, SWT.RADIO);
		localOverwriteExisting.setFont(optionsGroup.getFont());
		localOverwriteExisting.setText("Overwrite existing");
		localOverwriteExisting.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final Button source = (Button) e.getSource();
				if (source.getSelection()) {
					ImportPage.this.overwriteExisting = true;
				}
			}
		});
		handleExistingGroup.setVisible(false);
	}

	@Override
	protected ITreeContentProvider getFileProvider() {
		return null;
	}

	@Override
	protected ITreeContentProvider getFolderProvider() {
		return null;
	}

	@Override
	protected boolean determinePageCompletion() {
		boolean result = super.determinePageCompletion();

		final String path = sourceNameField.getText();
		final File file = new File(path);
		result &= file.exists();

		return result;
	}

	private void selectFile() {
		final FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
		fileDialog.setText(Messages.ImportPage_selectFile);
		fileDialog.setFilterExtensions(new String[] { "*.atdb" }); //$NON-NLS-1$
		final String open = fileDialog.open();
		if (open != null) {
			sourceNameField.setText(open);
			updateWidgetEnablements();
		}
	}

	@Override
	protected void updateWidgetEnablements() {
		if (handleExistingGroup != null) {
			final IFile target = getAMXMITarget();
			handleExistingGroup.setVisible(target != null && target.exists());
		}
		super.updateWidgetEnablements();
	}

	String getATDBSource() {
		return sourceNameField.getText();
	}

	IFile getAMXMITarget() {
		final String source = getATDBSource();
		final IContainer target = getTargetContainer();
		if (target == null || source.length() == 0) {
			return null;
		}
		// get the file name of the atdb
		final int from = Math.max(source.lastIndexOf('/'), source.lastIndexOf('\\'));
		final int to = source.lastIndexOf(".atdb"); //$NON-NLS-1$
		final String name = source.substring(from + 1, to);
		return target.getFile(target.getProjectRelativePath().append(name).addFileExtension("amxmi"));
	}

	IContainer getTargetContainer() {
		return getSpecifiedContainer();
	}

	boolean isExtractLabelsAndAccesses() {
		return extractLabelsAndAccesses == null || extractLabelsAndAccesses.getSelection();
	}

	boolean isExtractRunnableRuntimes() {
		return extractRunnableRuntimes == null || extractRunnableRuntimes.getSelection();
	}

	boolean isOverwriteExisting() {
		return overwriteExisting;
	}

}
