/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Timing-Architects Embedded Systems GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea._import.atdb;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaWriter;
import org.eclipse.app4mc.amalthea.model.util.HardwareUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

public class ImportTransformation implements IRunnableWithProgress {

	private final String atdbSource;
	private final String amxmiFile;
	private final boolean extractLabelsAndAccesses;
	private final boolean extractRunnableRuntimes;
	private final boolean overwriteExisting;

	public ImportTransformation(final String source, final String target, final boolean extractLabelsAndAccesses,
			final boolean extractRunnableRuntimes, final boolean overwriteExisting) {
		atdbSource = source;
		amxmiFile = target;
		this.extractLabelsAndAccesses = extractLabelsAndAccesses;
		this.extractRunnableRuntimes = extractRunnableRuntimes;
		this.overwriteExisting = overwriteExisting;
	}

	@Override
	public void run(final IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException {
		if (atdbSource.isEmpty() || amxmiFile.isEmpty()) {
			return;
		}
		final SubMonitor subMon = SubMonitor.convert(progressMonitor, "Creating AMALTHEA model from ATDB...", 8);

		final SubMonitor openATDBMonitor = subMon.split(1);
		final Path amxmiFilePath = Paths.get(amxmiFile);
		if (overwriteExisting) {
			try {
				Files.deleteIfExists(amxmiFilePath);
			} catch (IOException e) {
				throw new InvocationTargetException(e);
			}
		}
		openATDBMonitor.beginTask("Opening ATDB file...", 1);
		try (final ATDBConnection con = new ATDBConnection(atdbSource)) {
			openATDBMonitor.worked(1);
			final Amalthea model = Files.exists(amxmiFilePath)
					? AmaltheaLoader.loadFromFileNamed(amxmiFile)
					: AmaltheaFactory.eINSTANCE.createAmalthea();
			if (model == null) {
				throw new InvocationTargetException(new Exception("Could not load amalthea model from file: " + amxmiFile + "!"));
			}

			// hardware
			final SubMonitor hwConvMonitor = subMon.split(1);
			final IRunnableWithProgress hwConverter = new HWConverter(model, con);
			hwConverter.run(hwConvMonitor);

			// labels
			if (extractLabelsAndAccesses) {
				final SubMonitor labelConvMonitor = subMon.split(1);
				final IRunnableWithProgress labelConverter = new LabelConverter(model, con);
				labelConverter.run(labelConvMonitor);
			}

			// runnables
			final SubMonitor runConvMonitor = subMon.split(1);
			final double freqInHz = HardwareUtil.getModulesFromHwModel(ProcessingUnit.class, model).stream()
					.mapToLong(HardwareUtil::getFrequencyOfModuleInHz).average().orElse(0);
			final IRunnableWithProgress runnableConverter = new RunnableConverter(model, con,
					extractRunnableRuntimes, extractLabelsAndAccesses, freqInHz);
			runnableConverter.run(runConvMonitor);

			// stimuli
			final SubMonitor stimConvMonitor = subMon.split(1);
			final IRunnableWithProgress stimulusConverter = new StimulusConverter(model, con);
			stimulusConverter.run(stimConvMonitor);

			// processes
			final SubMonitor procConvMonitor = subMon.split(1);
			final IRunnableWithProgress processConverter = new ProcessConverter(model, con);
			processConverter.run(procConvMonitor);

			// events
			final SubMonitor evConvMonitor = subMon.split(1);
			final IRunnableWithProgress eventConverter = new EventConverter(model, con);
			eventConverter.run(evConvMonitor);

			// event chains
			final SubMonitor ecConvMonitor = subMon.split(1);
			final IRunnableWithProgress eventChainConverter = new EventChainConverter(model, con);
			eventChainConverter.run(ecConvMonitor);

			AmaltheaWriter.writeToFileNamed(model, amxmiFile);
		} catch (SQLException e) {
			throw new InvocationTargetException(e);
		} finally {
			progressMonitor.done();
		}
	}
}
