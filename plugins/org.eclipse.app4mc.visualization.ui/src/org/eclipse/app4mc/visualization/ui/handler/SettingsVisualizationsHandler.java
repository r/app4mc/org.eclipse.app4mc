/*********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.ui.handler;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.eclipse.app4mc.visualization.ui.VisualizationPart;
import org.eclipse.app4mc.visualization.ui.registry.ModelVisualization;
import org.eclipse.app4mc.visualization.ui.registry.ModelVisualizationRegistry;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.extensions.Service;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ListSelectionDialog;

/**
 * Handler that is used to select the visualizations that should be shown by this
 * {@link VisualizationPart}.
 */
public class SettingsVisualizationsHandler {

	@Inject
	@Service
	ModelVisualizationRegistry registry;

	@Execute
	public void execute(Shell shell, @Active MPart activePart) {
		if (activePart.getObject() instanceof VisualizationPart) {
			VisualizationPart part = (VisualizationPart) activePart.getObject();

			List<ModelVisualization> allVisualizations = registry.getAllVisualizations().stream()
					.sorted(Comparator.comparing(this::label))
					.collect(Collectors.toList());

			// use part to get initial selection
			List<ModelVisualization> initial = new ArrayList<>();
			allVisualizations.stream()
				.filter(mv -> part.getVisualizations().contains(mv.getId()))
				.forEach(initial::add);

			ListSelectionDialog dialog = new ListSelectionDialog(shell,
					allVisualizations,
					ArrayContentProvider.getInstance(),
					LabelProvider.createTextProvider(mv -> label((ModelVisualization) mv)),
					"Only show selected visualizations:");
			dialog.setTitle("APP4MC Visualization Settings");
			dialog.setInitialElementSelections(initial);
			dialog.open();

			// update part visualizations
			if (dialog.getReturnCode() == Window.OK) {
				part.getVisualizations().clear();
				for (Object selected : dialog.getResult()) {
					if (selected instanceof ModelVisualization) {
						ModelVisualization mv = (ModelVisualization) selected;
						part.getVisualizations().add(mv.getId());
					}
				}
			}
		}
	}

	private String label(ModelVisualization mv) {
		String type = mv.getType().substring(mv.getType().lastIndexOf(".") + 1);
		String name = mv.getName();

		return "[" + type + "]  " + name;
	}

}
