/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.visualization.ui.handler;

import org.eclipse.app4mc.visualization.ui.VisualizationPart;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.MDirectToolItem;
import org.eclipse.e4.ui.model.application.ui.menu.MToolBarElement;

/**
 * Handler that is used to toggle the pin behavior or the
 * {@link VisualizationPart}. Pinning means that the part is not reacting on
 * selection changes.
 */
public class PinVisualizationHandler {

	@Execute
	public void execute(@Active MPart activePart) {
		if (activePart.getObject() instanceof VisualizationPart) {
			VisualizationPart part = (VisualizationPart) activePart.getObject();

			for (MToolBarElement element : activePart.getToolbar().getChildren()) {
				if (element.getElementId().equals("org.eclipse.app4mc.visualization.ui.directtoolitem.pin")) {
					MDirectToolItem toolItem = (MDirectToolItem) element;

					boolean pinned = part.isPinned();
					part.setPinned(!pinned);
					toolItem.setSelected(!pinned);
				}
			}
		}
	}

	@CanExecute
    public boolean canExecute(@Active MPart activePart) {
		if (activePart.getObject() instanceof VisualizationPart) {
			VisualizationPart part = (VisualizationPart) activePart.getObject();
			return part.hasActiveModelElement();
		}
		return false;
	}
}