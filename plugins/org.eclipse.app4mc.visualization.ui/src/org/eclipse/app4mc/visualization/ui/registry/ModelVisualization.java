/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.visualization.ui.registry;

import javax.annotation.PostConstruct;

/**
 * POJO that holds the information of registered {@link Visualization}.
 */
public class ModelVisualization {

	/**
	 * The unique id of the {@link Visualization}.
	 */
	private final String id;

	/**
	 * The {@link Visualization} instance.
	 */
	private final Visualization visualizationImpl;

	/**
	 * The name of the {@link Visualization}.
	 */
	private final String name;

	/**
	 * The description of the {@link Visualization}.
	 */
	private final String description;

	/**
	 * The model type under which the visualization is registered and the should be
	 * injected to the {@link PostConstruct} method.
	 */
	private final String type;

	/**
	 *
	 * @param id                The unique id of the {@link Visualization}.
	 * @param visualizationImpl The {@link Visualization} instance.
	 * @param name              The name of the {@link Visualization}.
	 * @param description       The description of the {@link Visualization}.
	 * @param type              The model type under which the visualization is
	 *                          registered and the should be injected to the
	 *                          {@link PostConstruct} method.
	 */
	public ModelVisualization(String id, Visualization visualizationImpl, String name, String description, String type) {
		this.id = id;
		this.visualizationImpl = visualizationImpl;
		this.name = name;
		this.description = description;
		this.type = type;
	}

	/**
	 *
	 * @return The unique id of the {@link Visualization}.
	 */
	public String getId() {
		return id;
	}

	/**
	 *
	 * @return The {@link Visualization} instance.
	 */
	public Visualization getVisualization() {
		return visualizationImpl;
	}

	/**
	 *
	 * @return The name of the {@link Visualization}.
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @return The description of the {@link Visualization}.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 *
	 * @return The model type under which the visualization is registered and the
	 *         should be injected to the {@link PostConstruct} method.
	 */
	public String getType() {
		return type;
	}
}
