/*********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.ui.handler;

import java.util.List;

import org.eclipse.app4mc.visualization.ui.VisualizationPart;
import org.eclipse.app4mc.visualization.ui.registry.ModelVisualization;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.commands.MCommand;
import org.eclipse.e4.ui.model.application.commands.MCommandsFactory;
import org.eclipse.e4.ui.model.application.commands.MParameter;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.menu.ItemType;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;
import org.eclipse.e4.ui.workbench.modeling.EModelService;

/**
 * Dynamic menu contribution that adds menu items for visualizations to the
 * current active model type in the VisualizationPart.
 */
public class VisualizationMenuContribution {

	@AboutToShow
	public void aboutToShow(
			List<MMenuElement> items,
			@Active MPart activePart,
			EModelService modelService,
			MApplication app) {

		if (activePart.getObject() instanceof VisualizationPart) {
			VisualizationPart part = (VisualizationPart) activePart.getObject();
			List<ModelVisualization> visualizations = part.getAvailableModelVisualizations();
			for (ModelVisualization modelVisualization : visualizations) {
				MHandledMenuItem menuItem = MMenuFactory.INSTANCE.createHandledMenuItem();
				menuItem.setLabel(modelVisualization.getName());
				menuItem.setTooltip(modelVisualization.getDescription());
				menuItem.setElementId(modelVisualization.getId());
				menuItem.setContributorURI("platform:/plugin/org.eclipse.app4mc.visualization.ui");

				menuItem.setType(ItemType.CHECK);
				menuItem.setSelected(modelVisualization.getId().equals(part.getActiveVisualization().getId()));

				List<MCommand> command = modelService.findElements(
						app,
						"org.eclipse.app4mc.visualization.ui.command.showapp4mcvisualization",
						MCommand.class);
				menuItem.setCommand(command.get(0));

				MParameter parameter = MCommandsFactory.INSTANCE.createParameter();
				parameter.setName("app4mc.visualization.id");
				parameter.setValue(modelVisualization.getId());
				menuItem.getParameters().add(parameter);

				items.add(menuItem);
			}
		}
	}

}
