/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.visualization.ui.handler;

import java.util.List;

import org.eclipse.app4mc.visualization.ui.VisualizationPart;
import org.eclipse.app4mc.visualization.ui.registry.ModelVisualization;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

/**
 * Handler for the visualization selection toolbar item. Selection of the item
 * itself will update the visualization. The item will be disabled if no
 * visualization is available for the current selected model type.
 */
public class ReloadVisualizationHandler {

	@Execute
    public void execute(@Active MPart part) {
		if (part.getObject() instanceof VisualizationPart) {
			VisualizationPart visualizationPart = (VisualizationPart) part.getObject();
			if (visualizationPart.getActiveVisualization() != null) {
				visualizationPart.showVisualization(visualizationPart.getActiveVisualization().getId(), true);
			}
		}
    }

	@CanExecute
    public boolean canExecute(@Active MPart activePart) {

		if (activePart.getObject() instanceof VisualizationPart) {
			VisualizationPart part = (VisualizationPart) activePart.getObject();
			List<ModelVisualization> visualizations = part.getAvailableModelVisualizations();
			return visualizations.size() > 0;
		}

		return false;
	}
}
