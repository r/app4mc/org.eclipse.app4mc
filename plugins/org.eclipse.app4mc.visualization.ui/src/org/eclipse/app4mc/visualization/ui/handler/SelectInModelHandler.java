/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.visualization.ui.handler;

import java.util.HashMap;

import org.eclipse.app4mc.visualization.ui.VisualizationPart;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.CanExecute;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

/**
 * Handler that triggers selection on the model editor.
 */
public class SelectInModelHandler {

	@Execute
	public void execute(@Active MPart activePart, IEventBroker broker) {
		if (activePart.getObject() instanceof VisualizationPart) {
			VisualizationPart part = (VisualizationPart) activePart.getObject();
			if (part.hasActiveModelElement()) {
				HashMap<String, Object> data = new HashMap<>();
				data.put("modelElements", part.getActiveModelElements());
				broker.send("org/eclipse/app4mc/amalthea/editor/SELECT", data);
			}
		}
	}

	@CanExecute
    public boolean canExecute(@Active MPart activePart) {
		if (activePart.getObject() instanceof VisualizationPart) {
			VisualizationPart part = (VisualizationPart) activePart.getObject();
			return part.hasActiveModelElement();
		}
		return false;
	}
}