/*********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.ui.handler;

import java.util.Map.Entry;

import org.eclipse.app4mc.visualization.ui.VisualizationPart;
import org.eclipse.e4.core.contexts.Active;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

/**
 * Handler that is used to select the visualizations that should be shown by this
 * {@link VisualizationPart}.
 */
public class SettingsLastChoicesHandler {

	@Execute
	public void execute(Shell shell, @Active MPart activePart) {
		if (activePart.getObject() instanceof VisualizationPart) {
			VisualizationPart part = (VisualizationPart) activePart.getObject();

			StringBuilder info = new StringBuilder();
			info.append("\nLast choices:");
			for (Entry<String, String> entry : part.getLastChoices().entrySet()) {
				String object = entry.getKey().substring(entry.getKey().lastIndexOf(".") + 1);
				String visualization = entry.getValue().substring(entry.getValue().lastIndexOf(".") + 1);
				info.append("\n * ");
				info.append(object);
				info.append(" --> ");
				info.append(visualization);
			}

			MessageDialog.openInformation(shell, "APP4MC Visualization Choices", info.toString());
		}
	}

}
