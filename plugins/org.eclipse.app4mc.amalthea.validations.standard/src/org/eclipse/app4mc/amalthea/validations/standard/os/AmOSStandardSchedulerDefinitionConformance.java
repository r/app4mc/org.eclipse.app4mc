/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.os;

import java.util.List;
import java.util.Optional;

import org.eclipse.app4mc.amalthea.model.SchedulerDefinition;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers.Algorithm;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers.Parameter;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

/**
 * Checks for each scheduler definition, if it represents a standard scheduler (by name) and if all standard parameters are there.
 *
 * <ul>
 * <li>Referred algorithm and process parameters from the standard scheduler definition should also be in this scheduler definition.
 * This definition may have additional parameters, though.</li>
 * </ul>
 */

@Validation(
		id = "AM-OS-Standard-Scheduler-Definition-Conformance",
		checks = { "Standard schedulers with their parameters should be modeled as defined by the APP4MC standard scheduler library" })

public class AmOSStandardSchedulerDefinitionConformance extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSchedulerDefinition();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof SchedulerDefinition) {
			final SchedulerDefinition definition = (SchedulerDefinition) object;
			final Algorithm standard = StandardSchedulers.getAlgorithm(definition.getName());
			if (standard == null)
				return;

			validateSPDs(standard.getAlgorithmParameters(), definition.getAlgorithmParameters(),
					ePackage.getSchedulerDefinition_AlgorithmParameters(), definition, results);
			validateSPDs(standard.getProcessParameters(), definition.getProcessParameters(),
					ePackage.getSchedulerDefinition_ProcessParameters(), definition, results);
		}
	}

	private void validateSPDs(final List<Parameter> standardParameters,
			final List<SchedulingParameterDefinition> candidateParameters, final EReference containerReference,
			final SchedulerDefinition container, final List<ValidationDiagnostic> results) {
		for (Parameter standardParam : standardParameters) {
			final Optional<SchedulingParameterDefinition> oCandidateSPD = candidateParameters.stream()
					.filter(c -> c.getName().equals(standardParam.getParameterName())).findFirst();
			if (!oCandidateSPD.isPresent()) {
				addIssue(results, container, containerReference,
						"Expected scheduling parameter definition \"" + standardParam.getParameterName() + "\" is not provided in "
								+ containerReference.getName() + " for standard " + objectInfo(container));
			}
		}
	}

}
