/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.os;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.impl.SchedulingParameterImpl;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks whether an overridden scheduling parameter actually provides a value.
 *
 * <ul>
 * <li>An overridden scheduling parameter should provide a value.</li>
 * </ul>
 */

@Validation(
		id = "AM-OS-Scheduling-Parameter-Empty-Overriden-Value",
		checks = { "There should be a value if a default value of a scheduling parameter is overridden" })

public class AmOSSchedulingParameterEmptyOverriddenValue extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSchedulingParameter();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof SchedulingParameterImpl) {
			final SchedulingParameterImpl sp = (SchedulingParameterImpl) object;

			if (sp.getKey() == null) return; // checked by standard EMF validations

			final List<Value> values = AmOSSchedulingParameterMultiplicityMatchesDefinition.getAllValues(sp.getValue());
			final SchedulingParameterDefinition spd = sp.getKey();

			if (spd.eIsSet(ePackage.getSchedulingParameterDefinition_DefaultValue()) && values.isEmpty()) {
				final String message = "There should be " + (spd.isMany() ? "at least one" : "a")
						+ " value provided for the overridden " + typeInfo(sp) + " \"" + spd.getName()
						+ "\", otherwise the default value will be used";
				addIssue(results, sp, ePackage.getSchedulingParameter_Value(), message);
			}
		}
	}

}
