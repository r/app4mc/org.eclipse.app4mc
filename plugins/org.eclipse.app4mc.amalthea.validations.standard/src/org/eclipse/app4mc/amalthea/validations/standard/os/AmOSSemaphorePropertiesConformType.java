/**
 ********************************************************************************
 * Copyright (c) 2022 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.os;

import static org.eclipse.app4mc.amalthea.model.SemaphoreType.COUNTING_SEMAPHORE;
import static org.eclipse.app4mc.amalthea.model.SemaphoreType.MUTEX;
import static org.eclipse.app4mc.amalthea.model.SemaphoreType.RESOURCE;
import static org.eclipse.app4mc.amalthea.model.SemaphoreType.SPINLOCK;

import java.util.EnumSet;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.Semaphore;
import org.eclipse.app4mc.amalthea.model.SemaphoreType;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks for each semaphore, if properties are set conforming the semaphore type.
 *
 * <ul>
 * <li>The initialValue must not be greater than the maxValue.</li>
 * <li>The initialValue must be 0 for Resource, Spinlock, or Mutex.</li>
 * <li>The maxValue must be 1 for Spinlock or Mutex.</li>
 * <li>The ownership must be <code>true</code> for Resource, Spinlock, or Mutex.</li>
 * <li>The priorityCeilingProtocol must be <code>false</code> for CountingSemaphore.</li>
 * </ul>
 */

@Validation(
		id = "AM-OS-Semaphore-Properties-Conform-Type",
		checks = { "Semaphore properties conform to semaphore type" })

public class AmOSSemaphorePropertiesConformType extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSemaphore();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof Semaphore) {
			final Semaphore sem = (Semaphore) eObject;
			final SemaphoreType semType = sem.getSemaphoreType();

			if (sem.getInitialValue() > sem.getMaxValue()) {
				addIssue(results, sem, ePackage.getSemaphore_InitialValue(),
						"The initial value must be less than or equal to the max value.");
			}

			if (EnumSet.of(RESOURCE, SPINLOCK, MUTEX).contains(semType) && sem.getInitialValue() != 0) {
				addIssue(results, sem, ePackage.getSemaphore_InitialValue(),
						"The initial value must be set to 0 for semaphore type '" + semType + "'.");
			}

			if (EnumSet.of(SPINLOCK, MUTEX).contains(semType) && sem.getMaxValue() != 1) {
				addIssue(results, sem, ePackage.getSemaphore_MaxValue(),
						"The maximum value must be set to 1 for semaphore type '" + semType + "'.");
			}

			if (EnumSet.of(RESOURCE, SPINLOCK, MUTEX).contains(semType) && !sem.isOwnership()) {
				addIssue(results, sem, ePackage.getSemaphore_Ownership(),
						"Ownership must be set to TRUE for semaphore type '" + semType + "'.");
			}
			
			if (semType == COUNTING_SEMAPHORE && sem.isPriorityCeilingProtocol()) {
				addIssue(results, sem, ePackage.getSemaphore_PriorityCeilingProtocol(),
						"Priority Ceiling Protocol must be set to FALSE for semaphore type '" + semType + "'.");
			}
		}
	}

}
