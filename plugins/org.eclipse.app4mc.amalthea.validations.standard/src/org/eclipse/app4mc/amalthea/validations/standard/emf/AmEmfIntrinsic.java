/**
 * *******************************************************************************
 * Copyright (c) 2019-2022 Robert Bosch GmbH and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.emf;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.AmaltheaServices;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.IReferable;
import org.eclipse.app4mc.amalthea.model.util.AmaltheaValidator;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.IValidation;
import org.eclipse.app4mc.validation.core.Severity;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.EValidator.SubstitutionLabelProvider;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * Checks EMF constraints and generated AMALTHEA invariants
 */

@Validation(
	id = "AM-EMF-INTRINSIC",
	checks = {	"EMF extended metadata constraints (generated)",
				"AMALTHEA invariants (generated)" })

public class AmEmfIntrinsic implements IValidation {

	private static final SubstitutionLabelProvider LABEL_PROVIDER = createCustomLabelProvider();

	@Override
	public EPackage getEPackage() {
		return AmaltheaPackage.eINSTANCE;
	}

	@Override
	public EClassifier getEClassifier() {
		return EcorePackage.eINSTANCE.getEObject();
	}

	@Override
	public void validate(final EObject eObject, final List<ValidationDiagnostic> resultList) {
		if (eObject.eClass().eContainer() == getEPackage()) {

			final BasicDiagnostic diagnostics = new BasicDiagnostic();

			final Map<Object, Object> context = new HashMap<>();
			context.put(EValidator.SubstitutionLabelProvider.class, LABEL_PROVIDER);
			
			// call standard EMF validator
			boolean valid = AmaltheaValidator.INSTANCE.validate(eObject.eClass(), eObject, diagnostics, context);

			if (!valid) {
				for (final Diagnostic emfDiagnostic : diagnostics.getChildren()) {

					final EObject problematicObject = emfDiagnostic.getData().stream()
							.filter(EObject.class::isInstance)
							.map(EObject.class::cast)
							.findFirst().orElse(eObject);

					final EStructuralFeature problematicFeature = emfDiagnostic.getData().stream()
							.filter(EStructuralFeature.class::isInstance)
							.map(EStructuralFeature.class::cast)
							.findFirst().orElse(null);

					final String subMessages = emfDiagnostic.getChildren().isEmpty() ? "" :
							emfDiagnostic.getChildren().stream()
							.map(Diagnostic::getMessage)
							.map(String::trim)
							.collect(Collectors.joining(", ", " => ", ""));

					final String message = emfDiagnostic.getMessage()
								+ objectOrContainerInfo(problematicObject)
								+ subMessages;

					ValidationDiagnostic result = new ValidationDiagnostic(message, problematicObject, problematicFeature);
					result.setSeverityLevel(getSeverity(emfDiagnostic));

					resultList.add(result);
				}
			}
		}
	}

	private Severity getSeverity(final Diagnostic emfDiagnostic) {
		switch (emfDiagnostic.getSeverity()) {
			case Diagnostic.INFO:		return Severity.INFO;
			case Diagnostic.WARNING:	return Severity.WARNING;
			case Diagnostic.ERROR:		return Severity.ERROR;
			default:					return Severity.UNDEFINED;
		}

	}

	private String objectOrContainerInfo(final EObject object) {
		if (object != null) {

			if (object instanceof IReferable && isValid(((IReferable) object).getName())) {
				return " ( in " + objectInfo(object) + " )";
			}

			final IReferable container = AmaltheaServices.getContainerOfType(object, IReferable.class);
			if (container != null && isValid(container.getName())) {
				return (" ( in " + objectInfo(container)) + " )";
			}
		}

		return "";
	}

	private boolean isValid(String s) {
		return !(s == null || s.isEmpty());
	}

	private static SubstitutionLabelProvider createCustomLabelProvider() {
		return new EValidator.SubstitutionLabelProvider() {
			@Override
			public String getFeatureLabel(final EStructuralFeature eStructuralFeature) {
				return eStructuralFeature.getName();
			}
	
			@Override
			public String getObjectLabel(final EObject eObject) {
				final String s1 = eObject.eClass().getName();
				final String s2 = (eObject instanceof INamed) ? " " + ((INamed) eObject).getName() : "";
				return s1 + s2;
			}
	
			@Override
			public String getValueLabel(final EDataType eDataType, final Object value) {
				return EcoreUtil.convertToString(eDataType, value); // default
			}
		};
	}

}
