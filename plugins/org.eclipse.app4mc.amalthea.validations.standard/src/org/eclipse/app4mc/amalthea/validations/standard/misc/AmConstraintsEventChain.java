/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.misc;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AbstractEventChain;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventChainItem;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the correctness of event chains
 *
 * <ul>
 * <li>Stimulus and response shall not reference the same event</li>
 * <li>The stimulus of the first segment has to be the same as the stimulus of the event chain</li>
 * <li>The stimulus of other segments have to be equal to the response of the previous segment</li>
 * <li>The response of the last segment has to be the same as the response of the event chain</li>
 * </ul>
 */

@Validation(
		id = "AM-Constraints-EventChain",
		checks = {	"Stimulus and response shall not reference the same event",
					"The stimulus of the first segment has to be the same as the stimulus of the event chain",
					"The stimulus of other segments have to be equal to the response of the previous segment",
					"The response of the last segment has to be the same as the response of the event chain" })

public class AmConstraintsEventChain extends AmaltheaValidation {

	private static final String EVENT_CHAIN = "Event Chain ";

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getAbstractEventChain();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof AbstractEventChain) {
			AbstractEventChain chain = (AbstractEventChain) object;

			checkChainConsistency(chain, results);
		}
	}

	private void checkChainConsistency(final AbstractEventChain eventChain, List<ValidationDiagnostic> results) {
		if (eventChain == null) return;

		Event stimulus = eventChain.getStimulus();
		Event response = eventChain.getResponse();
		if (stimulus != null && response != null && stimulus == response) {
			addIssue(results, eventChain,
					AmaltheaPackage.eINSTANCE.getAbstractEventChain_Stimulus(),
					EVENT_CHAIN + name(eventChain) + ": stimulus and response refer to the same event");
		}

		switch (eventChain.getItemType()) {
		case SEQUENCE:
			checkSequence(eventChain, eventChain.getItems(), results);
			break;
		case PARALLEL:
			checkParallel(eventChain, eventChain.getItems(), results);
			break;

		default:
			break;
		}
	}

	private void checkSequence(final AbstractEventChain eventChain, final EList<EventChainItem> eventChainItems, List<ValidationDiagnostic> results) {

		if (eventChainItems == null || eventChainItems.isEmpty()) return;

		if (eventChainItems.contains(null)) {
			addIssue(results, eventChain,
					AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
					EVENT_CHAIN + name(eventChain) + ": sequence item list contains null reference");
		}

		List<AbstractEventChain> subEventChains = eventChainItems.stream()
				.map(EventChainItem::getEventChain)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		AbstractEventChain first = subEventChains.get(0);
		AbstractEventChain last = subEventChains.get(subEventChains.size() - 1);

		if (eventChain.getStimulus() != first.getStimulus()) {
			addIssue(results, eventChain,
					AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
					EVENT_CHAIN + name(eventChain) + ": stimulus of first sequence item <> stimulus of event chain");
		}

		if (eventChain.getResponse() != last.getResponse()) {
			addIssue(results, eventChain,
					AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
					EVENT_CHAIN + name(eventChain) + ": response of last sequence item <> response of event chain");
		}

		for (int i = 0; i < subEventChains.size() - 1; i++) {
			AbstractEventChain current = subEventChains.get(i);
			AbstractEventChain next = subEventChains.get(i + 1);

			if (current.getResponse() != next.getStimulus()) {
				addIssue(results, eventChain,
						AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
						EVENT_CHAIN + name(eventChain) + ": response of sequence item " + i + " <> stimulus of sequence item " + (i+1));
			}
		}
	}

	private void checkParallel(final AbstractEventChain eventChain, final EList<EventChainItem> eventChainItems, List<ValidationDiagnostic> results) {

		if (eventChainItems == null || eventChainItems.isEmpty()) return;

		if (eventChainItems.contains(null)) {
			addIssue(results, eventChain,
					AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
					EVENT_CHAIN + name(eventChain) + ": parallel item list contains null reference");
		}

		List<AbstractEventChain> subEventChains = eventChainItems.stream()
				.map(EventChainItem::getEventChain)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		for (AbstractEventChain subEC : subEventChains) {
			// check stimuli (should be identical)
			if (eventChain.getStimulus() != subEC.getStimulus()) {
				addIssue(results, eventChain,
						AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
						EVENT_CHAIN + name(eventChain) + ": stimulus of parallel sub chain " + name(subEC) + " <> stimulus of event chain");
			}
			// check responses (should be identical)
			if (eventChain.getResponse() != subEC.getResponse()) {
				addIssue(results, eventChain,
						AmaltheaPackage.eINSTANCE.getAbstractEventChain_Items(),
						EVENT_CHAIN + name(eventChain) + ": response of parallel sub chain " + name(subEC) + " <> response of event chain");
			}
		}
	}

}
