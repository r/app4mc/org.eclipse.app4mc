/**
 ********************************************************************************
 * Copyright (c) 2022 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.software;

import static org.eclipse.app4mc.amalthea.model.SemaphoreType.SPINLOCK;
import static org.eclipse.app4mc.amalthea.model.WaitingBehaviour.ACTIVE;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.Semaphore;
import org.eclipse.app4mc.amalthea.model.SemaphoreAccess;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the correctness of semaphore accesses
 *
 * <ul>
 * <li>Semaphore accesses to a spinlock can only have active as waiting behaviour</li>
 * </ul>
 */

@Validation(
		id = "AM-SW-Semaphore-Access",
		checks = { "Waiting behaviour must be 'active' for spinlock semaphore access" })

public class AmSwSemaphoreAccess extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSemaphoreAccess();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof SemaphoreAccess) {
			final SemaphoreAccess semAcc = (SemaphoreAccess) eObject;

			if (semAcc.getSemaphore() == null)
				return;

			final Semaphore sem = semAcc.getSemaphore();

			if (sem.getSemaphoreType() == SPINLOCK && semAcc.getWaitingBehaviour() != ACTIVE) {
				addIssue(results, semAcc, ePackage.getSemaphoreAccess_WaitingBehaviour(),
						"The waiting behaviour must be set to '" + ACTIVE + "' for spinlock semaphore access.");
			}
		}
	}

}
