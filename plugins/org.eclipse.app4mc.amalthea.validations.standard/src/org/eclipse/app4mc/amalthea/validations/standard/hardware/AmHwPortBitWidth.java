/**
 ********************************************************************************
 * Copyright (c) 2019-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.hardware;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.HwPort;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the correctness of HwPort enumerations
 *
 * <ul>
 * <li>PortType must be set</li>
 * <li>PortInterface must be set</li>
 * </ul>
 */

@Validation(
		id = "AM-HW-Port-BitWidth",
		checks = { "Bitwidth should be greater than zero" })

public class AmHwPortBitWidth extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getHwPort();
	}

	@Override
	public void validate(final EObject object, final List<ValidationDiagnostic> results) {
		if (object instanceof HwPort) {
			HwPort port = (HwPort) object;

			// ***** Bitwidth should be greater than zero

			if (port.getBitWidth() <= 0) {
				addIssue(results, port, ePackage.getHwPort_BitWidth(),
						"HW Port " + qualifiedName(port) + ": undefined bitwidth");
			}

		}
	}

}
