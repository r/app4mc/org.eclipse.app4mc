/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.os;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.ListObject;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.impl.SchedulingParameterImpl;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks whether the value type of a scheduling parameter matches the defined type of its parameter definition.
 *
 * <ul>
 * <li>Scheduling parameter type must match its defined type.</li>
 * </ul>
 */

@Validation(
		id = "AM-OS-Scheduling-Parameter-Value-Type-Matches-Defined-Type",
		checks = { "The type of the specified scheduling parameter must match the type defined in the scheduling parameter definition" })

public class AmOSSchedulingParameterValueTypeMatchesDefinedType extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSchedulingParameter();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof SchedulingParameterImpl) {
			final SchedulingParameterImpl sp = (SchedulingParameterImpl) object;

			if (sp.getKey() == null || sp.getValue() == null || sp.getValue().eClass() == null) return; // checked by standard EMF validations

			final Set<EClass> valueEClasses = getAllUsedEClassesForParameterValue(sp.getValue());
			final Set<EClass> allowedEClasses = getAllowedEClassesForParameterDefinition(sp.getKey());
			if (!allowedEClasses.containsAll(valueEClasses)) {
				addIssue(results, sp, ePackage.getSchedulingParameter_Value(), "The value of " + typeInfo(sp) + " \"" + sp.getKey().getName()
						+ "\" does not conform to the defined type " + sp.getKey().getType());
			}
		}
	}

	private static Set<EClass> getAllUsedEClassesForParameterValue(final Value value) {
		final Set<EClass> ret = new HashSet<>();
		if (value == null || value.eClass() == null) return ret;
		if (value instanceof ListObject) {
			((ListObject)value).getValues().forEach(v -> ret.addAll(getAllUsedEClassesForParameterValue(v)));
		} else {
			ret.add(value.eClass());
		}
		return ret;
	}

	private static Set<EClass> getAllowedEClassesForParameterDefinition(final SchedulingParameterDefinition spd) {
		final Set<EClass> ret = new HashSet<>();
		if (spd.getType() == null) return ret;

		switch (spd.getType()) {
		case _UNDEFINED_:
			ret.addAll(List.of(
					AmaltheaPackage.eINSTANCE.getBooleanObject(),
					AmaltheaPackage.eINSTANCE.getDoubleObject(),
					AmaltheaPackage.eINSTANCE.getFloatObject(),
					AmaltheaPackage.eINSTANCE.getBigIntegerObject(),
					AmaltheaPackage.eINSTANCE.getLongObject(),
					AmaltheaPackage.eINSTANCE.getIntegerObject(),
					AmaltheaPackage.eINSTANCE.getStringObject(),
					AmaltheaPackage.eINSTANCE.getTime()));
			break;
		case BOOL:
			ret.add(AmaltheaPackage.eINSTANCE.getBooleanObject());
			break;
		case FLOAT:
			ret.addAll(List.of(
					AmaltheaPackage.eINSTANCE.getDoubleObject(),
					AmaltheaPackage.eINSTANCE.getFloatObject()));
			break;
		case INTEGER:
			ret.addAll(List.of(
					AmaltheaPackage.eINSTANCE.getBigIntegerObject(),
					AmaltheaPackage.eINSTANCE.getLongObject(),
					AmaltheaPackage.eINSTANCE.getIntegerObject()));
			break;
		case STRING:
			ret.add(AmaltheaPackage.eINSTANCE.getStringObject());
			break;
		case TIME:
			ret.add(AmaltheaPackage.eINSTANCE.getTime());
			break;
		}

		if (spd.isMany()) {
			ret.add(AmaltheaPackage.eINSTANCE.getListObject());
		}

		return ret;
	}

}
