/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.mapping;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the correctness of SchedulerAllocations per processing unit.
 *
 * <ul>
 * <li>A processing unit should have at most one interrupt controller responsible for it</li>
 * <li>A processing unit should have at most one top level scheduler responsible for it</li>
 * </ul>
 */

@Validation(
		id = "AM-Mapping-Scheduler-Allocation-Top-Level-Responsibility",
		checks = { "A processing unit should have at most one top level task scheduler and one interrupt controller responsible for it" })

public class AmMappingSchedulerAllocationTopLevelResponsibility extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getMappingModel();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof MappingModel) {
			final MappingModel mm = (MappingModel) object;

			// sets to check for multiple core responsibilities
			final Set<ProcessingUnit> pUsWithTLScheduler = new HashSet<>();
			final Set<ProcessingUnit> pUsWithIC = new HashSet<>();

			for(final SchedulerAllocation sall:mm.getSchedulerAllocation()) {
				if (null == sall.getScheduler()) continue; // skip allocations without scheduler value

				final Scheduler sched = sall.getScheduler();
				for(final ProcessingUnit pu:sall.getResponsibility()) {
					if (sched instanceof InterruptController && !pUsWithIC.add(pu)) {
						addIssue(results, sall, ePackage.getSchedulerAllocation_Responsibility(), objectInfo(pu)
								+ " should have at most one interrupt controller that is responsible for it");
					}

					if (sched instanceof TaskScheduler && ((TaskScheduler)sched).getParentScheduler() == null && !pUsWithTLScheduler.add(pu)) {
						addIssue(results, sall, ePackage.getSchedulerAllocation_Responsibility(), objectInfo(pu)
								+ " should have at most one top level scheduler that is responsible for it");
					}
				}
			}
		}
	}

}
