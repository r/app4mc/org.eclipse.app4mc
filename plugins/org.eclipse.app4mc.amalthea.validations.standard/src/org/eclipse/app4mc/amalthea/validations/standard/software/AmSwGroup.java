/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.software;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the correctness of groups
 *
 * <ul>
 * <li>Groups where interruptible is set to <b>FALSE</b> must not have any nested groups</li>
 * </ul>
 */

@Validation(
		id = "AM-SW-Group",
		checks = { "The uninterruptible group must not contain nested groups" })

public class AmSwGroup extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getGroup();
	}

	@Override
	public void validate(final EObject object, final List<ValidationDiagnostic> results) {
		if (object instanceof Group) {
			final Group argument = (Group) object;

			if (!argument.isInterruptible() &&
					argument.getItems().stream().anyMatch(Group.class::isInstance)) {
				addIssue(results, argument, ePackage.getIActivityGraphItemContainer_Items(),
						"Group: uninterruptible groups must not contain nested groups (in group \"" + argument.getName() + "\")");
			}

		}
	}

}
