/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.mapping;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks the correctness of SchedulerAllocations for hierarchical task schedulers.
 *
 * <ul>
 * <li>A child scheduler should only be responsible for a subset of processing units of its ancestors</li>
 * </ul>
 */

@Validation(
		id = "AM-Mapping-Scheduler-Allocation-Hierarchy",
		checks = { "A child scheduler should only be responsible for a subset of processing units of its ancestors" })

public class AmMappingSchedulerAllocationHierarchy extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSchedulerAllocation();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof SchedulerAllocation) {
			final SchedulerAllocation sall = (SchedulerAllocation) object;

			final Scheduler sched = sall.getScheduler();
			if (sched == null || !(sched instanceof TaskScheduler)) return; // only task schedulers can be hierarchical

			final TaskScheduler ts = (TaskScheduler)sched;
			// skip empty responsibilities or top level schedulers
			if (sall.getResponsibility().isEmpty() || null == ts.getParentScheduler()) return;

			final Set<ProcessingUnit> ancestorResponsibilities = new HashSet<>();
			TaskScheduler parent = ts.getParentScheduler();
			do {
				parent.getSchedulerAllocations().stream().map(SchedulerAllocation::getResponsibility)
					.forEach(ancestorResponsibilities::addAll);
				parent = parent.getParentScheduler();
			} while (ancestorResponsibilities.isEmpty() && null != parent);

			if (!ancestorResponsibilities.containsAll(sall.getResponsibility())) {
				addIssue(results, sall, ePackage.getSchedulerAllocation_Responsibility(), objectInfo(sall)
						+ " for child " + objectInfo(ts) + " should only be responsible for a "
						+ "subset of processing units of its parent schedulers");
			}
		}
	}

}
