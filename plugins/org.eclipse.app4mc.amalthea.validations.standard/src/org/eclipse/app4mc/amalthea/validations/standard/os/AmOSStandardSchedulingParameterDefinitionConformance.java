/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.os;

import java.util.List;
import java.util.Objects;

import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.predefined.AmaltheaTemplates;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks for each scheduling parameter definition (if used in a standard scheduler) if the parameters conform to the standard
 * scheduler's definition.
 *
 * <ul>
 * <li>For each standard scheduling parameter, the type should be the same as modeled in this scheduler definition.</li>
 * <li>For each standard scheduling parameter, the attributes should be the same as modeled in this scheduler definition.</li>
 * <li>If the scheduling parameter is not mandatory and the standard definition provides a default value, the scheduling parameter
 * definition shall also have that default value.</li>
 * </ul>
 */

@Validation(
		id = "AM-OS-Standard-Scheduling-Parameter-Definition-Conformance",
		checks = { "Scheduling parameter definition that are used in a standard scheduler should conform to the parameters defined by "
				+ "the APP4MC standard scheduler library" })

public class AmOSStandardSchedulingParameterDefinitionConformance extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSchedulingParameterDefinition();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof SchedulingParameterDefinition) {
			final SchedulingParameterDefinition candidateSPD = (SchedulingParameterDefinition) object;
			final OSModel dummyOSModel = AmaltheaFactory.eINSTANCE.createOSModel();

			// find all scheduler definition referring to this scheduling parameter definitions
			candidateSPD.getSchedulerDefinitions().stream()
					// check if any of these scheduler definitions is a standard scheduler and look for a parameter definition
					.flatMap(sd -> StandardSchedulers.getAllParametersOfAlgorithm(sd.getName()).stream()).filter(Objects::nonNull).distinct()
					// that has the same name as this parameter, go and check it against the just found standard parameter
					.filter(spd -> spd.getParameterName().equals(candidateSPD.getName())).findFirst()
					.map(spd -> AmaltheaTemplates.addStandardSchedulingParameterDefinition(dummyOSModel, spd)).ifPresent(standardSPD -> {

				// check all attributes (this includes the type)
				ePackage.getSchedulingParameterDefinition().getEAttributes().forEach(spdAttribute -> {
					final Object standardValue = standardSPD.eGet(spdAttribute);
					final Object candidateValue = candidateSPD.eGet(spdAttribute);
					if (!standardValue.equals(candidateValue)) {
						addIssue(results, candidateSPD, spdAttribute, "Standard scheduling parameter definition \"" + standardSPD.getName()
								+ "\" expects " + spdAttribute.getName() + " set to '" + standardValue + "', but found '" + candidateValue
								+ "' in " + objectInfo(candidateSPD));
					}
				});

				// check if the default value should be provided
				if (!candidateSPD.isMandatory() && candidateSPD.getDefaultValue() == null && standardSPD.getDefaultValue() != null) {
					addIssue(results, candidateSPD, ePackage.getSchedulingParameterDefinition_DefaultValue(), "Standard scheduling parameter "
							+ "definition \"" + standardSPD.getName() + "\" defines a default value, which should also be provided here, "
							+ "since " + objectInfo(candidateSPD) + " is optional");
				}
			});
		}
	}

}
