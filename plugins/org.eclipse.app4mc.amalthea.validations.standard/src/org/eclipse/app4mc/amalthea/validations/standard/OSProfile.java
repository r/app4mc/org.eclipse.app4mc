/**
 ********************************************************************************
 * Copyright (c) 2021-2022 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard;

import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSMandatorySchedulingParametersSet;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSSchedulingParameterEmptyOverriddenValue;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSSchedulingParameterMultiplicityMatchesDefinition;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSSchedulingParameterValueTypeMatchesDefinedType;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSSemaphorePropertiesConformType;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSStandardSchedulerDefinitionConformance;
import org.eclipse.app4mc.amalthea.validations.standard.os.AmOSStandardSchedulingParameterDefinitionConformance;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "OS Validations")

@ValidationGroup(
	severity = Severity.ERROR,
	validations =  {
		AmOSSchedulingParameterValueTypeMatchesDefinedType.class,
		AmOSSchedulingParameterMultiplicityMatchesDefinition.class,
		AmOSMandatorySchedulingParametersSet.class,
		AmOSSemaphorePropertiesConformType.class
	}
)

@ValidationGroup(
	severity = Severity.WARNING,
	validations =  {
		AmOSSchedulingParameterEmptyOverriddenValue.class,
		AmOSStandardSchedulerDefinitionConformance.class,
		AmOSStandardSchedulingParameterDefinitionConformance.class
	}
)

public class OSProfile implements IProfile {
    // Do nothing
}
