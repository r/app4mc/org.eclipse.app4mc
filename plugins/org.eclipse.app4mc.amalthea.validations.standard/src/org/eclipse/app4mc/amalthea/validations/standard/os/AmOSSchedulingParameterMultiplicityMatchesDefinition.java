/**
 ********************************************************************************
 * Copyright (c) 2021 Vector Informatik GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Vector Informatik GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.validations.standard.os;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.ListObject;
import org.eclipse.app4mc.amalthea.model.SchedulingParameterDefinition;
import org.eclipse.app4mc.amalthea.model.Value;
import org.eclipse.app4mc.amalthea.model.impl.SchedulingParameterImpl;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

/**
 * Checks whether the multiplicity of scheduling parameters matches the parameter definition.
 *
 * <ul>
 * <li>Number of scheduling parameter values must match their parameter definition.</li>
 * </ul>
 */

@Validation(
		id = "AM-OS-Scheduling-Parameter-Value-Number-Matches-Defined-Multiplicity",
		checks = { "The number of values of the specified scheduling parameter must match the defined multiplicity in the scheduling parameter definition" })

public class AmOSSchedulingParameterMultiplicityMatchesDefinition extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getSchedulingParameter();
	}

	@Override
	public void validate(EObject object, List<ValidationDiagnostic> results) {
		if (object instanceof SchedulingParameterImpl) {
			final SchedulingParameterImpl sp = (SchedulingParameterImpl) object;

			if (sp.getKey() == null) return; // checked by standard EMF validations

			final List<Value> values = getAllValues(sp.getValue());
			final SchedulingParameterDefinition spd = sp.getKey();

			if (spd.isMandatory() && values.isEmpty()) {
				final String message = "There is no value for " + typeInfo(sp) + " \"" + sp.getKey().getName()
						+ "\" - the " + typeInfo(sp.getKey()) + " requires " + (spd.isMany() ? "at least" : "exactly") + " one value";
				addIssue(results, sp, ePackage.getSchedulingParameter_Value(), message);
			}
			if (!spd.isMany() && values.size() > 1) {
				final String message = "There are multiple values for " + typeInfo(sp) + " \"" + sp.getKey().getName()
						+ "\" - the " + typeInfo(sp.getKey()) + " allows not more than one value";
				addIssue(results, sp, ePackage.getSchedulingParameter_Value(), message);
			}
		}
	}

	static List<Value> getAllValues(final Value value) {
		final List<Value> ret = new ArrayList<>();
		if (value == null) return ret;
		if (value instanceof ListObject) {
			((ListObject)value).getValues().forEach(v -> ret.addAll(getAllValues(v)));
		} else {
			ret.add(value);
		}
		return ret;
	}

}
