/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.util.svg;

import java.io.IOException;
import java.util.Set;

import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import net.sourceforge.plantuml.eclipse.utils.PlantumlConstants;

public abstract class AbstractDiagram {

	static final Set<Character> FORBIDDEN = Set.of('\\', '\'', '"', '[', ']', ',', ';', ':', '#', '~', '§', '$', '%', '&');

	private StringBuilder diagramBuilder = new StringBuilder();
	private BiMap<Object, String> idMap = HashBiMap.create();

	public void resetDiagramData() {
		diagramBuilder = new StringBuilder();
		idMap = HashBiMap.create();
	}

	public void append(CharSequence chars) {
		diagramBuilder.append(chars);
	}

	public void append(String str) {
		diagramBuilder.append(str);
	}

	public void appendName(String name) {
		diagramBuilder.append("\"");
		diagramBuilder.append(clean(name));
		diagramBuilder.append("\"");
	}

	private CharSequence clean(String str) {
		if (str == null || str.isEmpty()) {
			// str is undefined -> use "???"
			return "???";
		}

		// replace forbidden characters
		StringBuilder sb = new StringBuilder();
		for (char c : str.toCharArray()) {
			sb.append(FORBIDDEN.contains(c) ? '_' : c);
		}
		return sb;
	}

	public void appendId(Object obj) {
		diagramBuilder.append(getOrCreateId(obj));
	}

	public void appendUrl(Object obj) {
		diagramBuilder.append(", URL=\"#" + getOrCreateId(obj) + "\"");
	}

	public String getDiagramText() {
		return diagramBuilder.toString();
	}

	/**
	 * Get unique ID to an object
	 * 
	 * @param obj the object
	 * @return the ID
	 */
	public String getOrCreateId(Object obj) {
		return idMap.computeIfAbsent(obj, k -> "obj" + idMap.size());
	}

	/**
	 * Get an object by ID
	 * 
	 * @param id
	 * @return the object with provided id
	 */
	public Object getObjectById(String id) {
		return idMap.inverse().get(id);
	}

	public String getGraphvizPath() {
		// read GRAPHVIZ_PATH value from preferences
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode("net.sourceforge.plantuml.eclipse");
		String dotPath1 = prefs.get(PlantumlConstants.GRAPHVIZ_PATH, null);
	
		if (dotPath1 != null && !dotPath1.isEmpty()) {
			return dotPath1;
		}

		// Check if environment variable GRAPHVIZ_DOT is set
		String dotPath2 = System.getenv("GRAPHVIZ_DOT");
		if (dotPath2 != null && !dotPath2.isEmpty()) {
			return dotPath2;
		}

		// Try with command only (works if included in PATH)
		if (Platform.getOS().equals(Platform.OS_WIN32)) {
			return "dot.exe";
		} else {
			return "dot";
		}
	}

	public abstract String renderToSvg() throws IOException;

}
