/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.util.svg;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ProcessBuilder.Redirect;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GraphvizDiagram extends AbstractDiagram {

	/**
	 * Renders DOT source to SVG
	 * 
	 * @return the rendered SVG diagram
	 */
	public String renderToSvg() throws IOException {
		String source = getDiagramText();
		if (source == null || source.isEmpty()) {
			return null;
		}

		List<String> command = new ArrayList<>(2);
		command.add(getGraphvizPath());
		command.add("-Tsvg");
		Process process = new ProcessBuilder(command).redirectError(Redirect.PIPE).start();
		OutputStreamWriter writer = new OutputStreamWriter(process.getOutputStream());
		InputStreamReader reader = new InputStreamReader(process.getInputStream(), StandardCharsets.UTF_8);
		writer.append(source);
		writer.close();
		StringBuilder sb = new StringBuilder();
		char[] buf = new char[1024];
		int len;
		while ((len = reader.read(buf)) >= 0) {
			sb.append(buf, 0, len);
		}
		return sb.toString();
	}

	/**
	 * Executes the DOT command, passes the model to stdin
	 * 
	 * @param args arguments to the dot command
	 */
	public void runGraphviz(String... args) throws IOException {
		String source = getDiagramText();
		if (source == null || source.isEmpty()) {
			return;
		}

		List<String> command = new ArrayList<>(args.length + 1);
		command.add(getGraphvizPath());
		command.addAll(Arrays.asList(args));
		Process process = new ProcessBuilder(command).redirectError(Redirect.PIPE).start();
		OutputStreamWriter writer = new OutputStreamWriter(process.getOutputStream());
		writer.append(source);
		writer.close();
	}

}
