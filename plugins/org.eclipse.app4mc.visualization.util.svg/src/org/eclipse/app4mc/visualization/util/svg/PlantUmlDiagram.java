/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.visualization.util.svg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceStringReader;
import net.sourceforge.plantuml.core.DiagramDescription;

public class PlantUmlDiagram extends AbstractDiagram {

	private String svg = null;

	public String getDiagramSVG() {
		return svg;
	}

	@Override
	public void resetDiagramData() {
		super.resetDiagramData();
		svg = null;
	}
	
	/**
	 * Renders PlantUML source to SVG
	 * 
	 * @return the rendered SVG diagram
	 */
	public String renderToSvg() throws IOException {
		String source = getDiagramText();
		if (source == null || source.isEmpty()) {
			return null;			
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		// call PlantUML and render to SVG
		final SourceStringReader reader = new SourceStringReader(source);
		final DiagramDescription desc = reader.outputImage(baos, new FileFormatOption(FileFormat.SVG));
		if (desc != null && !desc.getDescription().isEmpty()) {
			svg = baos.toString(StandardCharsets.UTF_8.name());
			return svg;
		}

		return null;
	}

}
