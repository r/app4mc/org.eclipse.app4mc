/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.svg;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.app4mc.amalthea.model.AbstractEventChain;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventChainItem;
import org.eclipse.app4mc.amalthea.model.EventChainItemType;
import org.eclipse.app4mc.amalthea.model.EventChainReference;
import org.eclipse.app4mc.amalthea.model.SubEventChain;
import org.eclipse.app4mc.visualization.util.svg.PlantUmlDiagram;
import org.eclipse.emf.ecore.EObject;

public class EventChainMapGenerator {

	private static final String REF_ICON = "<&link-intact> ";
	private static final String STIMULUS_ICON = "<&account-login> ";
	private static final String RESPONSE_ICON = "<&account-logout> ";

	// Suppress default constructor
	private EventChainMapGenerator() {
		throw new IllegalStateException("Utility class");
	}

	public static void updateDiagram(Context context) {

		// create local context (casted and extended global context)
		LocalContext lc = createLocalContext(context);
		if (lc == null) {
			return; // wrong input -> do nothing
		}

		// reset old diagram data
		lc.diagram.resetDiagramData();

		// generate new diagram
		lc.diagram.append("' Created by EventChainMapGenerator (" + timestamp() + ")\n\n");
		buildContent(lc);
	}

	private static void buildContent(LocalContext lc) {

		lc.diagram.append("' ===== Event chain (as PlantUML mind map) =====\n\n");

		lc.diagram.append("@startmindmap\n\n");

		createChain(lc, null, lc.chain, "+");

		lc.diagram.append("\n@endmindmap");
	}

	private static void createChain(LocalContext lc, EventChainItem chainItem, AbstractEventChain chain, String prefix) {
		if (chain == null)
			return;

		// handle predecessor stack (parent is first element)
		AbstractEventChain parent = lc.predecessors.isEmpty() ? null : lc.predecessors.get(0);

		// chain
		final String chainName = getChainName(chain);
		final String chainType = getChainType(chain);
		final String chainLink = getLinkToObject(chain, lc);
		final String chainIcon = (chainItem instanceof EventChainReference) ? REF_ICON : "";

		lc.diagram.append(prefix + " " + chainIcon + chainName + chainType + chainLink + "\n");

		String nextPrefix = prefix + "+";
		boolean isRecursive = lc.predecessors.contains(chain);
		boolean showItems = !(chainItem instanceof EventChainReference) || (lc.config.isExpandSubchainReferences() && !isRecursive);
		boolean showEvents = lc.config.isShowAllEvents() || !showItems || chain.getItems().isEmpty();

		// stimulus event
		if (showEvents) {
			Event stimulus = chain.getStimulus();
			String stimulusName = getEventName(stimulus, parent, chainItem, EventType.STIMULUS);
			String stimulusLink = getLinkToObject(stimulus, lc);

			if (lc.config.isShowRepeatingEventsGrayed()) {
				stimulusName = updateEventName(stimulusName, stimulus, lc.previousEvents);				
			}

			lc.diagram.append(nextPrefix + "_ " + STIMULUS_ICON + stimulusName + stimulusLink + "\n");
		}

		// sub chains
		if (showItems) {
			lc.predecessors.add(0, chain);
			for (EventChainItem item : chain.getItems()) {
				createChain(lc, item, item.getEventChain(), nextPrefix);
			}
			lc.predecessors.remove(0);
		}
		if (isRecursive) {
			lc.diagram.append(nextPrefix + "_ ...\n");
		}

		// response event
		if (showEvents) {
			Event response = chain.getResponse();
			String responseName = getEventName(response, parent, chainItem, EventType.RESPONSE);
			String responseLink = getLinkToObject(response, lc);

			if (lc.config.isShowRepeatingEventsGrayed()) {
				responseName = updateEventName(responseName, response, lc.previousEvents);				
			}

			lc.diagram.append(nextPrefix + "_ " + RESPONSE_ICON + responseName + responseLink + "\n");
		}
	}

	private static String getLinkToObject(EObject eObj, LocalContext lc) {
		if (eObj == null || lc.diagram == null || !lc.config.isShowLinks())
			return "";

		final String id = lc.diagram.getOrCreateId(eObj);
		return " [[#" + id + " link]]";
	}

	private static String getChainName(AbstractEventChain chain) {
		if (chain == null)
			return null;

		String name = chain.getName();
		boolean isUndefined = (name == null) || name.isEmpty();

		if (chain instanceof SubEventChain) {
				return isUndefined ? "_" : name;
		}

		// chain is instance of EventChain --> name is required
		return "<u>" + (isUndefined ? "<color:red><b>???</b></color>" : name) + "</u>";
	}

	private static String getChainType(AbstractEventChain chain) {
		if (chain == null)
			return null;

		if (chain.getItemType() == EventChainItemType.PARALLEL) {
			int max = chain.getItems().size();
			int min = chain.getMinItemsCompleted();

			if (max < 2) {
				return ""; // no choice
			}
			if (min < 1 || min > max) {
				return " <i>(<color:red>???</color>)</i>"; // wrong number
			}
			if (min == 1) {
				return " <i>(alternative)</i>";
			}
			if (min < max) {
				return " <i>(" + min + " of " + max + ")</i>";
			}
			// min == max
			return " <i>(fork-join)</i>";
		}

		// sequence
		return "";
	}

	private static String getEventName(Event event, AbstractEventChain parent, EventChainItem chainItem, EventType type) {
		// check event name
		if (event == null || event.getName() == null || event.getName().isEmpty())
			return "<color:red><b>???</b></color>";

		// check consistency
		if (isConsistent(parent, chainItem, type)) {
			return event.getName();			
		}

		return "<color:red><b>" + event.getName() + "</b></color>";
	}

	private static String updateEventName(String eventName, Event event, List<Event> previousEvents) {
		// handle list of previous events (in order of appearance)
		Object previous = previousEvents.isEmpty() ? null : previousEvents.get(previousEvents.size() - 1);

		if (event == previous) {
			// gray out name
			return eventName.contains("<color:red>")
					? eventName.replace("<color:red>", "<color:lightGray>")
					: "<color:lightGray>" + eventName + "</color>";
		} else {
			// return unchanged name and handle list of previous events
			previousEvents.add(event);
			return eventName;
		}
	}

	private static boolean isConsistent(AbstractEventChain parent, EventChainItem chainItem, EventType type) {
		if (parent == null)
			return true; // no restriction

		AbstractEventChain chain = chainItem.getEventChain();
		if (chain == null)
			return false; // no sub chain defined

		boolean isParallel = (parent.getItemType() == EventChainItemType.PARALLEL);
		boolean isStimulus = (type == EventType.STIMULUS);
		
		if (isParallel) {
			return isStimulus ? chain.getStimulus() == parent.getStimulus() : chain.getResponse() == parent.getResponse();
		}

		// sequence
		int index = parent.getItems().indexOf(chainItem);
		if (index < 0)
			return false; // inconsistent list of sub chains

		if (isStimulus) {
			if (index == 0) {
				// first element
				return chain.getStimulus() == parent.getStimulus();
			} else {
				AbstractEventChain previousChain = parent.getItems().get(index - 1).getEventChain();
				return (previousChain != null) && (chain.getStimulus() == previousChain.getResponse());
			}
		} else {
			// response
			if (index == parent.getItems().size() - 1) {
				// last element
				return chain.getResponse() == parent.getResponse();
			} else {
				AbstractEventChain nextChain = parent.getItems().get(index + 1).getEventChain();
				return (nextChain != null) && (chain.getResponse() == nextChain.getStimulus());
			}
		}
	}

	private enum EventType {
		STIMULUS, RESPONSE
	}

	static String timestamp() {
		final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		return dateFormat.format(new Date());
	}

	// **** handling of local context

	private static LocalContext createLocalContext(Context context) {
		if (context.object instanceof AbstractEventChain
				&& context.config instanceof EventChainMapConfig
				&& context.diagram instanceof PlantUmlDiagram) {

			// Create local context
			return new LocalContext(
					(AbstractEventChain) context.object,
					(EventChainMapConfig) context.config,
					(PlantUmlDiagram) context.diagram);
		}

		// should never happen
		return null;
	}

	private static class LocalContext {
		public final AbstractEventChain chain;
		public final EventChainMapConfig config;
		public final PlantUmlDiagram diagram;

		// some bookkeeping to avoid endless recursion or to know the previously written event
		public final List<AbstractEventChain> predecessors = new ArrayList<>();
		public final List<Event> previousEvents = new ArrayList<>();

		public LocalContext(AbstractEventChain chain, EventChainMapConfig config, PlantUmlDiagram diagram) {
			this.chain = chain;
			this.config = config;
			this.diagram = diagram;
		}
	}

}
