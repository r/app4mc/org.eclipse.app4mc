/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.svg;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Consumer;

import org.eclipse.app4mc.visualization.util.svg.SvgUtil;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.layout.RowLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationListener;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;

public abstract class AbstractVisualization {

	/**
	 * Helper for adding a toggle button
	 * 
	 * @param parent          container element
	 * @param text            label text
	 * @param toolTip         label tool tip
	 * @param f               button select action; takes the button's selection status as an argument
	 * @param initialSelected initial selection state of the button
	 */
	protected void addToggleButton(Composite parent, String text, String toolTip, final Consumer<Boolean> f, boolean initialSelected) {
		final Button btn = new Button(parent, SWT.TOGGLE | SWT.FLAT);
		btn.setText(text);
		btn.setToolTipText(toolTip);
		btn.setSelection(initialSelected);
		btn.addListener(SWT.Selection, e -> f.accept(btn.getSelection()));
	}

	protected void addZoomBox(Composite buttonArea, AbstractConfig config) {
		// Align the box with the other buttons
		final Composite zoomArea = new Composite(buttonArea, SWT.NONE);
		RowLayoutFactory.fillDefaults().margins(1, 1).applyTo(zoomArea);

		final Composite box = new Composite(zoomArea, SWT.BORDER);

		final Button btnLeft = new Button(box, SWT.ARROW | SWT.LEFT | SWT.FLAT);
		btnLeft.addListener(SWT.Selection, e -> config.decrementScale());

		final CLabel scaleLabel = new CLabel(box, SWT.FLAT | SWT.CENTER);
		scaleLabel.setText(String.format("%d %%", config.getScale())); // set initial label text

		config.addChangeListener(e -> {
			if (e.getPropertyName().equals("scale"))
				scaleLabel.setText(String.format("%d %%", (int) e.getNewValue())); // update label text
		});

		final Button btnRight = new Button(box, SWT.ARROW | SWT.RIGHT | SWT.FLAT);
		btnRight.addListener(SWT.Selection, e -> config.incrementScale());

		RowLayoutFactory.fillDefaults().fill(true).applyTo(box);
	}

	protected Browser addBrowser(Composite pane, IEventBroker broker, final Context context) {
		Browser browser = new Browser(pane, SWT.NONE);

		// Setup navigation to a selected element in the model viewer
		if (broker != null) {
			browser.addLocationListener(LocationListener.changingAdapter(c -> {
				c.doit = true;
			
				Object target = null;
				int idx = c.location.lastIndexOf('#');
				if (idx >= 0) {
					target = context.diagram.getObjectById(c.location.substring(idx + 1));
				}
				if (target != null) {
					HashMap<String, Object> data = new HashMap<>();
					data.put("modelElements", Collections.singletonList(target));
					broker.send("org/eclipse/app4mc/amalthea/editor/SELECT", data);
			
					c.doit = false;
				}
			}));
		}

		// React to configuration parameter changes
		context.config.addChangeListener(e -> {
			if (e.getPropertyName().equals("scale"))
				updateSvgScale(browser, (int) e.getNewValue());
			if (e.getPropertyName().startsWith("parameter"))
				updateBrowserContent(browser, context);
		});

		return browser;
	}

	protected void updateSvgScale(Browser browser, int newScale) {
		// Update SVG size in browser via JavaScript/DOM
		if (browser != null) {
			browser.execute(SvgUtil.buildUpdateScaleCommand(newScale));
		}
	}

	/**
	 * Visualizes a given model element in a browser.
	 * <p>
	 * The plantUML graph is constructed and compiled in a separate thread.
	 * 
	 * @param browser 
	 * @param context 
	 */
	protected void updateBrowserContent(Browser browser, Context context) {
		new Thread(() -> {
			// Build PlantUML diagram text
			updateDiagram(context);

			// Render to SVG
			String result;
			try {
				result = context.diagram.renderToSvg();
			} catch (IOException e) {
				result = "Error invoking PlantUML: \"" + e.getMessage()
				+ "\". Make sure you have configured the path to the dot executable properly in the PlantUML preferences.";
				Platform.getLog(AbstractVisualization.class).error(result, e);
				return;
			}

			// Apply initial scale and display
			if (result != null && !browser.isDisposed()) {
				final String browserContent = SvgUtil.initiallyApplyScale(result, context.config.getScale());
				browser.getDisplay().asyncExec(() -> {
					if (!browser.isDisposed()) {
						browser.setText(browserContent);
					}
				});
			}

		}).start();
	}

	protected abstract void updateDiagram(Context context);

}
