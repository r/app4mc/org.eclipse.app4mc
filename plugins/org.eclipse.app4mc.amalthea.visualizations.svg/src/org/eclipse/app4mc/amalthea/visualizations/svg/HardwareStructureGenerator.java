/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.svg;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.HwAccessElement;
import org.eclipse.app4mc.amalthea.model.HwConnection;
import org.eclipse.app4mc.amalthea.model.HwModule;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.visualization.util.svg.PlantUmlDiagram;

public class HardwareStructureGenerator {

	// Suppress default constructor
	private HardwareStructureGenerator() {
		throw new IllegalStateException("Utility class");
	}

	public static void updateDiagram(Context context) {

		// create local context (casted and extended global context)
		LocalContext lc = createLocalContext(context);
		if (lc == null) {
			return; // wrong input -> do nothing
		}

		// reset old diagram data
		lc.diagram.resetDiagramData();

		// generate new diagram
		lc.diagram.append("' Created by HardwareStructureGenerator (" + timestamp() + ")");
		buildContent(lc);
	}

	private static void buildContent(LocalContext lc) {

		lc.diagram.append("\n\n@startuml");

		lc.diagram.append("\n\n' ===== Hardware Structure (as PlantUML frames/components) =====");

		generateStructures(lc);

		lc.diagram.append("\n\n@enduml");
	}

	private static void generateStructures(LocalContext lc) {

		if (!lc.structures.isEmpty()) {
			lc.diagram.append("\n\n\' ===== FRAMES (structures and modules) =====");

			for (final HwStructure hwStruct : lc.structures) {
				generateLevels(lc, "", hwStruct);
			}

			lc.diagram.append("\n\n\' ===== ROUTES (physical and logical connections) =====");

			for (final HwStructure hwStruct : lc.structures) {
				generateRoutes(lc, "", hwStruct);
			}
		}

		lc.diagram.append("\n\nskinparam component {");
		lc.diagram.append("\n\tbackgroundColor<<ProcessingUnit>> #8CACFF");
		lc.diagram.append("\n\tborderColor<<ProcessingUnit>> #000000");
		lc.diagram.append("\n\tbackgroundColor<<ConnectionHandler>> #FFFFA0");
		lc.diagram.append("\n\tborderColor<<ConnectionHandler>> #000000");
		lc.diagram.append("\n\tbackgroundColor<<Memory>> #60FF82");
		lc.diagram.append("\n\tborderColor<<Memory>> #000000");
		lc.diagram.append("\n\tbackgroundColor<<Cache>> #C8FFA6");
		lc.diagram.append("\n\tborderColor<<Cache>> #000000");
		lc.diagram.append("\n\tArrowFontColor #C0C0C0");
		lc.diagram.append("\n}");
	}

	private static void generateLevels(LocalContext lc, String prefix, HwStructure s) {

		if (s == null) return;

		final String id = lc.diagram.getOrCreateId(s);
		lc.components.add(id);
		
		lc.diagram.append("\n\n");
		lc.diagram.append(prefix);
		lc.diagram.append("frame ");
		lc.diagram.appendName(s.getName());
		lc.diagram.append(" as ");
		lc.diagram.append(id);
		lc.diagram.append(" {");

		if (lc.config.isShowModules()) {
			for (final HwModule m : s.getModules()) {
				addLevel(lc, prefix + "\t", m);
			}
		}

		for (final HwStructure su : s.getStructures()) {
			generateLevels(lc, prefix + "\t", su);
		}

		lc.diagram.append("\n");
		lc.diagram.append(prefix);
		lc.diagram.append("}\n");
		lc.diagram.append(prefix);
		lc.diagram.append("url of " + id + " is [[#" + id + "]]");
	}

	private static void addLevel(LocalContext lc, String prefix, HwModule m) {

		if (m == null) return;

		final String id = lc.diagram.getOrCreateId(m);
		final String command = "component \"" + m.getName() + "\" as " + id + " <<" + m.eClass().getName() + ">>";

		if (!lc.commands.contains(command)) {
			lc.commands.add(command);
			lc.components.add(id);

			lc.diagram.append("\n");
			lc.diagram.append(prefix);
			lc.diagram.append(command);
			lc.diagram.append("\n");
			lc.diagram.append(prefix);
			lc.diagram.append("url of " + id + " is [[#" + id + "]]");
		}
	}

	private static void generateRoutes(LocalContext lc, String prefix, HwStructure s) {

		if (s == null) return;

		if (lc.config.isShowPhysicalConnections()) {
			addPhysicalConnections(lc, s);
		}

		if (lc.config.isShowModules() && lc.config.isShowLogicalConnections()) {
			addLogicalConnections(lc, s);
		}

		for (final HwStructure su : s.getStructures()) {
			generateRoutes(lc, prefix + "\t", su);
		}
	}

	private static void addPhysicalConnections(LocalContext lc, HwStructure s) {

		if (!s.getConnections().isEmpty()) {

			lc.diagram.append("\n\n\' add physical connections of structure ");
			lc.diagram.append(s.getName());
			lc.diagram.append(":");

			for (final HwConnection c : s.getConnections()) {
				INamed container1 = (c.getPort1() == null) ? null : (INamed) c.getPort1().eContainer();
				INamed container2 = (c.getPort2() == null) ? null : (INamed) c.getPort2().eContainer();

				 addRoute(lc, container1, container2, c, " -- ");
			}
		}
	}

	private static void addLogicalConnections(LocalContext lc, HwStructure s) {

		for (final HwModule m : s.getModules()) {

			if (m instanceof ProcessingUnit) {
				final ProcessingUnit pu = (ProcessingUnit) m;

				if (!pu.getAccessElements().isEmpty()) {
					lc.diagram.append("\n\n\' add logical connections of processing unit ");
					lc.diagram.append(pu.getName());
					lc.diagram.append(":");

					for (final HwAccessElement ae : pu.getAccessElements()) {

						if (ae.getDestination() != null) {
							addRoute(lc, pu, ae.getDestination(), ae, " ..> ");
						}
					}
				}
			}
		}
	}

	private static void addRoute(LocalContext lc, INamed source, INamed destination, INamed con, String conType) {

		if (source == null || destination == null) return;
		
		final String id1 = lc.diagram.getOrCreateId(source);
		final String id2 = lc.diagram.getOrCreateId(destination);
		final String conLabel = (con.getName() == null || con.getName().isEmpty()) ? con.toString() : con.getName();

		final String command = id1 + conType + id2 + " : " + conLabel;

		if (!lc.commands.contains(command)) {
			lc.commands.add(command);

			if (lc.components.add(id1)) {
				// add new definition: source
				lc.diagram.append("\n\tcomponent ");
				lc.diagram.appendName(source.getName());
				lc.diagram.append(" as " + id1);
			}
			if (lc.components.add(id2)) {
				// add new definition: destination
				lc.diagram.append("\n\tcomponent ");
				lc.diagram.appendName(destination.getName());
				lc.diagram.append(" as " + id2);
			}

			lc.diagram.append("\n\t");
			lc.diagram.append(command);
		}
	}

	static String timestamp() {
		final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		return dateFormat.format(new Date());
	}

	// **** handling of local context

	private static LocalContext createLocalContext(Context context) {
		if (context.object instanceof List<?>
				&& context.config instanceof HardwareStructureConfig
				&& context.diagram instanceof PlantUmlDiagram) {

			// Cast object to List<HwStructure>
			List<HwStructure> hwStructures = new ArrayList<>();
			for (Object object : (List<?>) context.object) {
				if (object instanceof HwStructure) {
					hwStructures.add((HwStructure) object);
				}
			}

			// Create local context
			return new LocalContext(
					hwStructures,
					(HardwareStructureConfig) context.config,
					(PlantUmlDiagram) context.diagram);
		}

		// should never happen
		return null;
	}

	private static class LocalContext {
		public final List<HwStructure> structures;
		public final HardwareStructureConfig config;
		public final PlantUmlDiagram diagram;

		// some bookkeeping to avoid duplicate relations or missing components
		public final Set<String> commands = new HashSet<>(); // previous commands
		public final Set<String> components = new HashSet<>(); // previous component IDs

		public LocalContext(List<HwStructure> structures, HardwareStructureConfig config, PlantUmlDiagram diagram) {
			this.structures = structures;
			this.config = config;
			this.diagram = diagram;
		}
	}

}
