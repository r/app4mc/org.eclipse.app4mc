/**
 ********************************************************************************
 * Copyright (c) 2023 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.svg;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.HWModel;
import org.eclipse.app4mc.visualization.ui.VisualizationParameters;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.app4mc.visualization.util.svg.PlantUmlDiagram;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.RowLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

@Component(property = {
		"name=Hardware Structure",
		"description=Block Diagram Visualization for the hardware model"
})
public class HardwareStructureVisualization2 extends AbstractVisualization implements Visualization {

	/**
	 * Entry point for the visualization framework
	 * 
	 * @param hwModel		HWModel that shall be visualized
	 * @param parameters	visualization parameters
	 * @param parent		parent component
	 * @param broker		event broker for element selection
	 */
	@PostConstruct
	public void createVisualization(
			HWModel hwModel,
			VisualizationParameters parameters,
			Composite parent,
			IEventBroker broker) {

		// Create central context object with all relevant inputs
		final HardwareStructureConfig config = new HardwareStructureConfig(parameters);
		final Context context = createContext(hwModel, config);

		Composite pane = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().applyTo(pane);
		Composite buttonArea = new Composite(pane, SWT.NONE);

		addToggleButton(buttonArea, "Show Modules", "Show modules (processing units, caches, memories, connection handlers)",
				config::setShowModules, config.isShowModules());
		addToggleButton(buttonArea, "Show Physical Connections", "Show physical connections of software structures",
				config::setShowPhysicalConnections, config.isShowPhysicalConnections());
		addToggleButton(buttonArea, "Show Logical Connections", "Show locical connections of processing units",
				config::setShowLogicalConnections, config.isShowLogicalConnections());

		addZoomBox(buttonArea, context.config);

		RowLayoutFactory.swtDefaults().fill(true).applyTo(buttonArea);

		Browser browser = addBrowser(pane, broker, context);
		
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(browser);

		// Create and display content
		updateBrowserContent(browser, context);
	}

	// **** handling of generic context

	private Context createContext(HWModel hwModel, HardwareStructureConfig config) {
		return new Context(
				hwModel.getStructures(),
				config,
				new PlantUmlDiagram());
	}

	// **** call specific content generator

	protected void updateDiagram(Context context) {
		HardwareStructureGenerator.updateDiagram(context);
	}

}
