/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb;

import java.util.List;
import java.util.Set;

public interface EntityType<T> {

	String getName();

	default String getUCName() {
		final String lcName = this.getName();
		final String ucName = lcName.substring(0, 1).toUpperCase() + lcName.substring(1);
		return ucName;
	}

	List<String> getTraceAliases();

	Set<T> getPossibleEvents();

	List<String> getValidityConstraints();
}
