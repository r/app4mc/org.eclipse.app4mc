/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.service.processing;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.Ticks;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.ProcessingService;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.osgi.service.component.annotations.Component;

@Component(
		property = {
			"name = Cleanup | Remove empty Ticks objects",
			"description = Remove Ticks objects that do not specify a value (constant or deviation)" })

public class CleanupTicks implements ProcessingService {

	@PostConstruct
	public String cleanup(Amalthea model) {
		Set<Ticks> toDelete = new HashSet<>();

		TreeIterator<EObject> iterator = EcoreUtil.getAllContents(model, false);
		while (iterator.hasNext()) {
			EObject next = iterator.next();

			if (next instanceof Ticks) {
				Ticks ticks = (Ticks) next;
				if (ticks.getDefault() == null && ticks.getExtended().isEmpty()) {
					toDelete.add(ticks);
				}
			}
		}

		EcoreUtil.removeAll(toDelete);

		return "Deleted " + toDelete.size() + " empty Ticks objects.";
	}

}
