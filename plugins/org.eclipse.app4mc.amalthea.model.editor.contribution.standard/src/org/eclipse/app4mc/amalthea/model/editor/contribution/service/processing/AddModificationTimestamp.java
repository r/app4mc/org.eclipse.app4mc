/*********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.service.processing;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.IAnnotatable;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.ProcessingService;
import org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil;
import org.osgi.service.component.annotations.Component;

@Component(
		property = {
			"name = Custom properties | Add modification timestamp",
			"description = Creates a custom property with the current date and time" })

public class AddModificationTimestamp implements ProcessingService {

	private final SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public int cardinality() {
		return -1;	// unlimited
	}

	@PostConstruct
	public void addTimestamp(List<IAnnotatable> objects) {
		for (IAnnotatable obj : objects) {
			CustomPropertyUtil.customPut(obj, "modified", timestampFormat.format(new Date()));			
		}
	}

}
