/*********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.service.processing;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.DataSize;
import org.eclipse.app4mc.amalthea.model.DataSizeUnit;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.ProcessingService;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.osgi.service.component.annotations.Component;

@Component(
		property = {
			"name = Cleanup | Remove invalid DataSize objects",
			"description = Remove DataSize objects with value <= 0 or missing unit" })

public class CleanupDataSize implements ProcessingService {

	@PostConstruct
	public String cleanup(Amalthea model) {
		Set<DataSize> toDelete = new HashSet<>();

		TreeIterator<EObject> iterator = EcoreUtil.getAllContents(model, false);
		while (iterator.hasNext()) {
			EObject next = iterator.next();

			if (next instanceof DataSize) {
				DataSize size = (DataSize) next;
				if (size.getValue().signum() < 1 || size.getUnit() == DataSizeUnit._UNDEFINED_) {
					toDelete.add(size);
				}
			}
		}

		EcoreUtil.removeAll(toDelete);

		return "Deleted " + toDelete.size() + " invalid DataSize objects.";
	}

}
