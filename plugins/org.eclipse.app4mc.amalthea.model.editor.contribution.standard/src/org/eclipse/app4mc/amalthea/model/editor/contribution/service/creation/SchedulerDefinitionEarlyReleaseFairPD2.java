/*********************************************************************************
 * Copyright (c) 2021-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.service.creation;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.CreationService;
import org.eclipse.app4mc.amalthea.model.predefined.AmaltheaTemplates;
import org.eclipse.app4mc.amalthea.model.predefined.StandardSchedulers.Algorithm;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.osgi.service.component.annotations.Component;

@Component(
		property = {
			"name = Scheduler Definition | Proportionate Fair (Pfair) | Early Release Fair PD2",
			"description = Creates a EarlyReleaseFairPD2 scheduler definition" },
		service = CreationService.class)

public class SchedulerDefinitionEarlyReleaseFairPD2 implements CreationService {

	@Override
	public EStructuralFeature modelFeature() {
		return AmaltheaPackage.eINSTANCE.getOSModel_SchedulerDefinitions();
	}

	@PostConstruct
	public void create(OSModel osModel) {
		AmaltheaTemplates.addStandardSchedulerDefinition(osModel, Algorithm.EARLY_RELEASE_FAIR_PD2);
	}

}
