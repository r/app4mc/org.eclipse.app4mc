/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb._import.btf.model.BTFCombiState;
import org.eclipse.app4mc.atdb._import.btf.model.BTFCountMetric;
import org.eclipse.app4mc.atdb._import.btf.model.BTFEntityState;
import org.eclipse.app4mc.atdb._import.btf.model.BTFEntityType;
import org.eclipse.app4mc.atdb._import.btf.model.BTFInterInstanceMetric;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.jface.operation.IRunnableWithProgress;

public class ATDBMetricCalculator implements IRunnableWithProgress {

	private final ATDBConnection con;

	public ATDBMetricCalculator(final ATDBConnection con) {
		this.con = con;
	}

	@Override
	public void run(IProgressMonitor progressMonitor) throws InvocationTargetException {
		progressMonitor.beginTask("Calculating metrics...", 5 + BTFEntityState.actStates.size());
		try {
			// for each state: add a '[state]Time' metric signifying how long the entity instance was in that state
			for (final BTFEntityState state : BTFEntityState.actStates) {
				con.executeBatchUpdate(atdbCon -> {
					for (final Entry<BTFEntityType, Entry<List<Enum<?>>, List<Enum<?>>>> et2io:state.entityType2InOutEvents.entrySet()) {
						final BTFEntityType entityType = et2io.getKey();
						// map and collect events as SQL list
						final List<String> incomingEvents = et2io.getValue().getKey().stream().map(e -> e.toString().toLowerCase())
								.collect(Collectors.toList());
						// map and collect events as string list
						final List<String> outgoingEvents = et2io.getValue().getValue().stream().map(e -> e.toString().toLowerCase())
								.collect(Collectors.toList());
						atdbCon.insertEntityInstanceStateMetricValuesForEntityType(state + "Time", entityType, incomingEvents, outgoingEvents);
					}
				});
				progressMonitor.worked(1);
			}

			con.executeBatchUpdate(atdbCon -> {
				// consider combi states for processes
				for (final BTFCombiState cState : BTFCombiState.values()) {
					final List<String> relevantStates = cState.getStates().stream().map(s -> s + "Time").collect(Collectors.toList());
					atdbCon.insertCombinedEntityInstanceStateMetricValuesForEntityType(cState + "Time", BTFEntityType.PROCESS, relevantStates);
				}
			});
			progressMonitor.worked(1);

			con.executeBatchUpdate(atdbCon -> {
				// calculate inter-instance metrics
				for (final BTFInterInstanceMetric metric : BTFInterInstanceMetric.values()) {
					for (final Entry<BTFEntityType, Entry<Enumerator, Enumerator>> et2fs:metric.entityType2FirstAndSecond.entrySet()) {
						final BTFEntityType entityType = et2fs.getKey();
						final String firstInstanceEvent = et2fs.getValue().getKey().toString();
						final String secondInstanceEvent = et2fs.getValue().getValue().toString();
						atdbCon.insertInterEntityInstanceMetricValuesForEntityType(metric.toString(), entityType,
								firstInstanceEvent, secondInstanceEvent);
					}
				}
			});
			progressMonitor.worked(1);

			con.executeBatchUpdate(atdbCon -> {
				// calculate stimuli activate2activate distances
				final String activateEvent = BTFInterInstanceMetric.activateToActivate.entityType2FirstAndSecond
						.entrySet().iterator().next().getValue().getKey().toString();
				atdbCon.insertInterSourceEntityInstanceMetricValuesForEntityType(BTFInterInstanceMetric.activateToActivate.toString(),
						BTFEntityType.PROCESS, activateEvent);
			});
			progressMonitor.worked(1);

			con.executeBatchUpdate(atdbCon -> {
				// calculate entity metrics min max avg for all time metric values
				atdbCon.insertAggregatedEntityInstanceMetricsForDimension("time");
			});
			progressMonitor.worked(1);

			con.executeBatchUpdate(atdbCon -> {
				// insert entity specific count metrics
				for (final BTFCountMetric cm : BTFCountMetric.values()) {
					atdbCon.insertMetric(cm.toString(), "count");
					atdbCon.insertCountsForEntityTypeAndEvent(cm.toString(), cm.entityType, cm.eventToCount.toString().toLowerCase());
				}
			});
			progressMonitor.worked(1);
		} catch (SQLException e) {
			throw new InvocationTargetException(e);
		} finally {
			progressMonitor.done();
		}
	}

}
