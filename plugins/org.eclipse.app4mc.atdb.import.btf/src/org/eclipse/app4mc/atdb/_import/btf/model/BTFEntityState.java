/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf.model;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.app4mc.amalthea.model.ProcessEventType;
import org.eclipse.app4mc.amalthea.model.RunnableEventType;

public enum BTFEntityState {

	// initial state
	notInitialized(new SimpleEntry<>(Collections.emptyList(), List.of(RunnableEventType.START)), //
			new SimpleEntry<>(Collections.emptyList(), List.of(ProcessEventType.ACTIVATE))),
	// in BTF this is actually the "active" state
	startDelay(new SimpleEntry<>(List.of(ProcessEventType.ACTIVATE), List.of(ProcessEventType.START))),
	running(new SimpleEntry<>(
					List.of(RunnableEventType.START, RunnableEventType.RESUME),
					List.of(RunnableEventType.TERMINATE, RunnableEventType.SUSPEND)), //
			new SimpleEntry<>(
					List.of(ProcessEventType.START, ProcessEventType.RESUME, ProcessEventType.RUN),
					List.of(ProcessEventType.TERMINATE, ProcessEventType.PREEMPT, ProcessEventType.POLL, ProcessEventType.WAIT))),
	polling(new SimpleEntry<>(List.of(ProcessEventType.POLL, ProcessEventType.POLL_PARKING), List.of(ProcessEventType.RUN, ProcessEventType.PARK))),
	parking(new SimpleEntry<>(List.of(ProcessEventType.PARK), List.of(ProcessEventType.POLL_PARKING, ProcessEventType.RELEASE_PARKING))),
	waiting(new SimpleEntry<>(List.of(ProcessEventType.WAIT), List.of(ProcessEventType.RELEASE))),
	ready(new SimpleEntry<>(List.of(RunnableEventType.SUSPEND), List.of(RunnableEventType.RESUME)), //
			new SimpleEntry<>(List.of(ProcessEventType.PREEMPT, ProcessEventType.RELEASE, ProcessEventType.RELEASE_PARKING),
					List.of(ProcessEventType.RESUME))),
	// meta state (not represented as combi state for simplicity)
	response(new SimpleEntry<>(List.of(ProcessEventType.ACTIVATE), List.of(ProcessEventType.TERMINATE))),
	// final state
	terminated(new SimpleEntry<>(List.of(RunnableEventType.TERMINATE), Collections.emptyList()), //
			new SimpleEntry<>(List.of(ProcessEventType.TERMINATE), Collections.emptyList()));

	public final Map<BTFEntityType, Entry<List<Enum<?>>, List<Enum<?>>>> entityType2InOutEvents;
	// list of all states excluding initial and final states
	public static final List<BTFEntityState> actStates = Stream.of(values())
			.filter(p -> p.entityType2InOutEvents.values().stream().noneMatch(e -> e.getKey().isEmpty()))
			.filter(p -> p.entityType2InOutEvents.values().stream().noneMatch(e -> e.getValue().isEmpty()))
			.collect(Collectors.toList());

	@SafeVarargs BTFEntityState(final Entry<List<Enum<?>>, List<Enum<?>>>... inOutEvents) {
		entityType2InOutEvents = new LinkedHashMap<>();
		Stream.of(inOutEvents).forEach(e -> {
			String entityTypeName = e.getKey().stream().findFirst().orElseGet(e.getValue().stream().findFirst()::get).getClass().getSimpleName();
			entityTypeName = entityTypeName.substring(0, entityTypeName.indexOf("EventType")).toLowerCase();
			entityType2InOutEvents.put(BTFEntityType.getForName(entityTypeName), e);
		});
	}

	public static List<String> getValidityConstraints(final BTFEntityType entityType) {
		final List<String> validityConstraints = new ArrayList<>();
		// events for initial and final states must occur exactly once
		Stream.of(values()).filter(s -> !actStates.contains(s)).map(s -> s.entityType2InOutEvents.get(entityType))
				.filter(Objects::nonNull).forEach(entry -> {
					entry.getKey().forEach(e -> validityConstraints.add(e.toString().toLowerCase() + "EventCount = 1"));
					entry.getValue().forEach(e -> validityConstraints.add(e.toString().toLowerCase() + "EventCount = 1"));
				});
		// sum of incoming events must equal sum of outgoing events (excluding loop events)
		actStates.stream().map(s -> s.entityType2InOutEvents.get(entityType)).filter(Objects::nonNull).forEach(entry -> {
			final Set<Enum<?>> loopEvents = new LinkedHashSet<>(entry.getKey());
			loopEvents.retainAll(entry.getValue());
			final String left = entry.getKey().stream().filter(e -> !loopEvents.contains(e)).map(e -> e.toString().toLowerCase() + "EventCount")
					.collect(Collectors.joining(" + "));
			final String right = entry.getValue().stream().filter(e -> !loopEvents.contains(e)).map(e -> e.toString().toLowerCase() + "EventCount")
					.collect(Collectors.joining(" + "));
			validityConstraints.add(left + " = " + right);
		});
		return validityConstraints;
	}

	public static Set<Enum<?>> getPossibleEventsFor(final BTFEntityType entityType) {
		final Set<Enum<?>> result = new LinkedHashSet<>(BTFCountMetric.getInvolvedBTFEventsForEntityType(entityType));
		Stream.of(values()).map(s -> s.entityType2InOutEvents.get(entityType))
				.filter(Objects::nonNull).flatMap(e -> Stream.concat(e.getKey().stream(), e.getValue().stream())).distinct()
				.forEach(result::add);
		return result;
	}

}
