/**
 ********************************************************************************
 * Copyright (c) 2015-2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Timing-Architects Embedded Systems GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import org.eclipse.app4mc.atdb.ATDBBuilder;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.DBConnection.AccessMode;
import org.eclipse.app4mc.atdb._import.btf.model.BTFEntityType;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;


public class ImportTransformation implements IRunnableWithProgress {

	private final String btfFile;
	private final String atdbFile;
	private final boolean calculateMetrics;
	private final boolean persistTraceEvents;
	private final boolean doInMemoryDBImport;

	public ImportTransformation(final String source, final String target, final boolean calculateMetrics,
			final boolean persistTraceEvents, final boolean doInMemoryDBImport) {
		if (source.endsWith(".btf") && target.endsWith(".atdb")) {
			btfFile = source;
			atdbFile = target;
		} else {
			btfFile = "";
			atdbFile = "";
		}
		this.calculateMetrics = calculateMetrics;
		this.persistTraceEvents = persistTraceEvents;
		this.doInMemoryDBImport = doInMemoryDBImport;
	}

	@Override
	public void run(final IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException {
		if (!btfFile.isEmpty() && !atdbFile.isEmpty()) {
			final SubMonitor subMon = SubMonitor.convert(progressMonitor, "Converting BTF trace to ATDB...", 100);
			AccessMode accessMode = doInMemoryDBImport ? AccessMode.ReadWriteInMemory : AccessMode.ReadWrite;
			try (final ATDBConnection con = new ATDBConnection(atdbFile, accessMode)) {
				final SubMonitor createATDBMonitor = subMon.split(1, SubMonitor.SUPPRESS_NONE);
				createATDBMonitor.beginTask("Creating empty ATDB...", 1);
				final ATDBBuilder atdbBuilder = new ATDBBuilder(con).createBasicDBStructure().createBasicViews()
						.createOptionalAndTemporaryTables(BTFEntityType.literals, persistTraceEvents);
				if (persistTraceEvents)
					atdbBuilder.createOptionalViews(BTFEntityType.literals);
				con.flushAllStatements();
				createATDBMonitor.done();

				final SubMonitor btfImportMonitor = subMon.split(calculateMetrics ? 59 : 89, SubMonitor.SUPPRESS_NONE);
				final IRunnableWithProgress btfImporter = new BTFImporter(con, btfFile);
				btfImporter.run(btfImportMonitor);
				con.flushAllStatements();

				final SubMonitor writeIndicesMonitor = subMon.split(10, SubMonitor.SUPPRESS_NONE);
				writeIndicesMonitor.beginTask("Adding indices and writing events to data base...", 1);
				atdbBuilder.createBasicDBStrctureIndices();
				con.flushAllStatements();
				writeIndicesMonitor.done();
				btfImportMonitor.done();

				if (calculateMetrics) {
					final SubMonitor metricCalcMonitor = subMon.split(30, SubMonitor.SUPPRESS_NONE);
					calculateMetrics(metricCalcMonitor, con);
				}
			} catch (SQLException e) {
				throw new InvocationTargetException(e);
			} finally {
				progressMonitor.done();
			}
		}
	}

	public static void calculateMetrics(final IProgressMonitor progressMonitor, final ATDBConnection con)
			throws InvocationTargetException, InterruptedException, SQLException {
		final ATDBBuilder atdbBuilder = new ATDBBuilder(con);
		atdbBuilder.createTemporaryEntityFilteredTraceEventTables(BTFEntityType.literals)
				.autoPopulateEntityFilteredTraceEventTables(BTFEntityType.literals);
		final IRunnableWithProgress metricCalc = new ATDBMetricCalculator(con);
		metricCalc.run(progressMonitor);
		progressMonitor.done();
	}
}
