/**
 ********************************************************************************
 * Copyright (c) 2015-2021 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Timing-Architects Embedded Systems GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf.wizard;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.eclipse.app4mc.atdb._import.btf.ImportTransformation;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IImportWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.ide.IDE;
import org.osgi.framework.FrameworkUtil;

public class ImportWizard extends Wizard implements IImportWizard {

	private ImportPage mainPage;
	private IStructuredSelection selection;

	@Override
	public void addPages() {
		super.addPages();
		mainPage = new ImportPage(selection);
		addPage(mainPage);
	}

	@Override
	public void init(final IWorkbench workbench, final IStructuredSelection selection) {
		this.selection = selection;

		final List<?> selectedResources = IDE.computeSelectedResources(selection);
		if (!selectedResources.isEmpty()) {
			this.selection = new StructuredSelection(selectedResources);
		}

		setWindowTitle(Messages.ImportWizard_title);
		setNeedsProgressMonitor(true);
	}

	@Override
	public boolean performFinish() {
		final String source = mainPage.getSource();
		final IContainer target = mainPage.getTargetContainer();
		// get the file name of the btf
		final int from = Math.max(source.lastIndexOf('/'), source.lastIndexOf('\\'));
		final int to = source.lastIndexOf(".btf"); //$NON-NLS-1$
		final String name = source.substring(from + 1, to);
		final String atdbFile = target.getLocation() + "/" + name + ".atdb"; //$NON-NLS-1$

		final boolean calculateMetrics = mainPage.isCalculateMetrics();
		final boolean persistTraceEvents = mainPage.isPersistTraceEvents();
		final boolean doInMemoryDBImport = mainPage.isDoInMemoryDBImport();

		final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {

			@Override
			protected void execute(final IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException, CoreException {
				final IRunnableWithProgress transformer = new ImportTransformation(source, atdbFile, calculateMetrics,
						persistTraceEvents, doInMemoryDBImport);
				transformer.run(progressMonitor);
				// refresh workspace
				target.refreshLocal(1, progressMonitor);
				progressMonitor.done();
			}
		};

		try {
			getContainer().run(true, true, operation);
		} catch (InvocationTargetException e1) {
			Platform.getLog(getClass()).log(new Status(IStatus.ERROR, FrameworkUtil.getBundle(getClass()).getSymbolicName(), e1.getLocalizedMessage(), e1));
			return false;
		} catch (InterruptedException e2) {
			Thread.currentThread().interrupt();
			return false;
		}

		// open new atdb file in viewer
		final IProject project = target.getProject();
		final IFile file = project.getFile(atdbFile.substring(atdbFile.lastIndexOf('/') + 1));
		final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		try {
			IDE.openEditor(page, file);
		}
		catch (final PartInitException e) {
			Platform.getLog(getClass()).log(new Status(IStatus.ERROR, FrameworkUtil.getBundle(getClass()).getSymbolicName(), e.getLocalizedMessage(), e));
			return false;
		}

		return true;
	}

}
