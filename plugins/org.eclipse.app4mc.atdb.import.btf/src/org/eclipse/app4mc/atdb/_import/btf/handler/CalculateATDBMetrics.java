/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf.handler;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

import javax.inject.Named;

import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.DBConnection.AccessMode;
import org.eclipse.app4mc.atdb._import.btf.ImportTransformation;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.osgi.framework.FrameworkUtil;

public class CalculateATDBMetrics {

	@Execute
	public void execute(final @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection, final IWorkbenchWindow runnableContext) {
		final List<?> selectionList = selection.toList();
		final IFile atdbSource = selectionList.stream().filter(IFile.class::isInstance).map(IFile.class::cast)
				.filter(f -> f.getFileExtension().equalsIgnoreCase("atdb"))
				.findFirst().orElseThrow(() -> new IllegalArgumentException("Selection does not contain an *.atdb file!"));
		if (atdbSource.getLocation() == null || atdbSource.getLocation().toString().length() == 0) {
			return;
		}
		final String atdbFile = atdbSource.getLocation().toString();
		if (runnableContext == null) {
			return;
		}
		final WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {

			@Override
			protected void execute(final IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException, CoreException {
				try (final ATDBConnection con = new ATDBConnection(atdbFile, AccessMode.ReadWriteInMemory)) {
					if (con.tableExists("traceEvent")) {
						ImportTransformation.calculateMetrics(progressMonitor, con);
					}
				} catch (SQLException e) {
					throw new InvocationTargetException(e);
				} finally {
					progressMonitor.done();
				}
				// refresh workspace
				atdbSource.refreshLocal(1, progressMonitor);
			}
		};

		try {
			runnableContext.run(true, true, operation);
		} catch (InvocationTargetException e1) {
			Platform.getLog(CalculateATDBMetrics.class).log(new Status(IStatus.ERROR,
					FrameworkUtil.getBundle(CalculateATDBMetrics.class).getSymbolicName(), e1.getLocalizedMessage(), e1));
		} catch (InterruptedException e2) {
			Thread.currentThread().interrupt();
		}
	}

}
