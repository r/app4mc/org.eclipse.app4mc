/**
 ********************************************************************************
 * Copyright (c) 2020-2021 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.btf.model;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.app4mc.amalthea.model.ProcessEventType;
import org.eclipse.app4mc.amalthea.model.RunnableEventType;
import org.eclipse.app4mc.atdb.EntityType;

public enum BTFEntityType implements EntityType<Enum<?>> {

	PROCESS(ProcessEventType.class, "T", "I"),
	RUNNABLE(RunnableEventType.class, "R");

	private static final Map<String, BTFEntityType> name2Literal = Stream.of(values()).collect(Collectors.toMap(et -> et.entityTypeName, et -> et));
	public static final List<BTFEntityType> literals = List.of(values());

	private final List<String> traceAliases;
	private final String entityTypeName;

	BTFEntityType(final Class<? extends Enum<?>> eventTypeClass, final String... traceAlias) {
		traceAliases = List.of(traceAlias);
		String className = eventTypeClass.getSimpleName();
		entityTypeName = className.substring(0, className.indexOf("EventType")).toLowerCase();
	}

	@Override
	public String getName() {
		return entityTypeName;
	}

	public boolean isTraceAlias(final String traceAlias) {
		return traceAliases.stream().anyMatch(traceAlias::equals);
	}

	@Override
	public List<String> getTraceAliases() {
		return traceAliases;
	}

	public static BTFEntityType getForName(final String name) {
		return name2Literal.get(name);
	}

	@Override
	public Set<Enum<?>> getPossibleEvents() {
		return BTFEntityState.getPossibleEventsFor(this);
	}

	@Override
	public List<String> getValidityConstraints() {
		return BTFEntityState.getValidityConstraints(this);
	}

}
