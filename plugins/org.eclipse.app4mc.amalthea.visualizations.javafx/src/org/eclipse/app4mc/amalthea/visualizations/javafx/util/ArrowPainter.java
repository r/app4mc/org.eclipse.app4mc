/*********************************************************************************
 * Copyright (c) 2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.visualizations.javafx.util;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 * Painter implementation that renders an arrow on a {@link Canvas}.
 */
public class ArrowPainter {

	private Paint arrowColor;
	private double[] lineDashes;

	/**
	 * Create an {@link ArrowPainter} with orange color and a dashed line style.
	 */
	public ArrowPainter() {
		this(Color.ORANGE, new double[] {6, 3});
	}

	/**
	 * Create an {@link ArrowPainter} with the given color and dash style.
	 *
	 * @param arrowColor The color to use.
	 * @param lineDashes The line dash to use.
	 */
	public ArrowPainter(Paint arrowColor, double[] lineDashes) {
		this.arrowColor = arrowColor;
		this.lineDashes = lineDashes;
	}

	/**
	 * Paint an arrow on the given {@link GraphicsContext}.
	 *
	 * @param gc     The {@link GraphicsContext} that should be used for painting.
	 * @param startX The start X of the arrow.
	 * @param startY The start Y of the arrow.
	 * @param endX   The end X of the arrow.
	 * @param endY   The end Y of the arrow.
	 */
	public void paint(GraphicsContext gc, double startX, double startY, double endX, double endY) {

		Paint previousStroke = gc.getStroke();
		double[] previousDashes = gc.getLineDashes();

		gc.setStroke(arrowColor);
		gc.setLineDashes(lineDashes);
		gc.strokeLine(
				startX,
				startY,
				endX,
				endY);

		double factor = 10 / Math.hypot(startX-endX, startY-endY);
        double factorO = 7 / Math.hypot(startX-endX, startY-endY);

        // part in direction of main line
        double dx = (startX - endX) * factor;
        double dy = (startY - endY) * factor;

        // part ortogonal to main line
        double ox = (startX - endX) * factorO;
        double oy = (startY - endY) * factorO;

        gc.setLineDashes(0);
        // upper arrow line
		gc.strokeLine(
				endX + dx - oy,
				endY + dy + ox,
				endX,
				endY);
        // lower arrow line
		gc.strokeLine(
				endX + dx + oy,
				endY + dy - ox,
				endX,
				endY);

		gc.setStroke(previousStroke);
		gc.setLineDashes(previousDashes);
	}
}
