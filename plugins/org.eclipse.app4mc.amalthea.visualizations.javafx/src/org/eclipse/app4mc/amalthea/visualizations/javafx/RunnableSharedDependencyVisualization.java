/*********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.javafx;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.visualizations.javafx.util.ArrowPainter;
import org.eclipse.app4mc.amalthea.visualizations.javafx.util.RunnableFilterDialog;
import org.eclipse.app4mc.amalthea.visualizations.javafx.util.RunnableHelper;
import org.eclipse.app4mc.amalthea.visualizations.javafx.util.RunnablePainter;
import org.eclipse.app4mc.visualization.ui.VisualizationParameters;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import javafx.embed.swt.FXCanvas;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

@Component(property= {
		"name=Shared Runnable Label Dependencies",
		"description=Visualize the selected Runnables and the shared Labels"
},
service = {Visualization.class, RunnableSharedDependencyVisualization.class} )
public class RunnableSharedDependencyVisualization implements Visualization {

	private ArrowPainter arrowPainter = new ArrowPainter();

	@PostConstruct
	public void createVisualization(List<Runnable> runnables, VisualizationParameters parameters, Composite parent, IEventBroker broker) {
		parent.setLayout(new GridLayout());

		FXCanvas fxCanvas = new FXCanvas(parent, SWT.NONE);
		GridDataFactory
		    .fillDefaults()
		    .grab(true, true)
		    .applyTo(fxCanvas);

		// create the root layout pane
		BorderPane layout = new BorderPane();
		layout.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));

		// create a Scene instance
		// set the layout container as root
		// set the background fill to the background color of the shell
		Scene scene = new Scene(layout);

		// set the Scene to the FXCanvas
		fxCanvas.setScene(scene);

		// if to many Runnables, show dialog to provide filter
		List<Runnable> toRender = runnables;
		RunnableFilterDialog dialog = new RunnableFilterDialog(parent.getShell(), runnables);
		if (runnables.size() > 100) {
			if (dialog.open() == Window.OK) {
				toRender = dialog.getSelectedRunnables();
			} else {
				toRender = Collections.emptyList();
			}
		}

		List<RunnablePainter> painters = toRender.stream()
				.map(r -> new RunnablePainter(r, false))
				.collect(Collectors.toList());

		// add zoom buttons
		Button zoomIn = new Button("+");
		zoomIn.setPrefSize(40, 40);

		Button zoomOut = new Button("-");
		zoomOut.setPrefSize(40, 40);

		Button filter = new Button("F");
		filter.setPrefSize(40, 40);
		filter.setOnAction(event -> {
			if (dialog.open() == Window.OK) {
				painters.clear();
				painters.addAll(dialog.getSelectedRunnables().stream()
						.map(r -> new RunnablePainter(r, false))
						.collect(Collectors.toList()));

				RunnableHelper.setMaxScaleFactor(painters);
				// set scale AFTER the calculation of maxScale
				RunnableHelper.setInitialScaleFactor(painters, parameters);

				parent.redraw();
			}
		});

		zoomIn.setOnAction(event -> {
			if (!zoomIn.isDisabled()) {
				painters.forEach(RunnablePainter::zoomIn);

				double scaleFactor = painters.get(0).getScaleFactor();
				RunnableHelper.persistScaleFactor(scaleFactor, parameters);

				if (scaleFactor >= painters.get(0).getMaxScaleFactor()) {
					zoomIn.setDisable(true);
				}
				if (zoomOut.isDisabled()) {
					zoomOut.setDisable(false);
				}
			}

			parent.redraw();
		});

		zoomOut.setOnAction(event -> {
			if (!zoomOut.isDisabled()) {
				painters.forEach(RunnablePainter::zoomOut);

				double scaleFactor = painters.get(0).getScaleFactor();
				RunnableHelper.persistScaleFactor(scaleFactor, parameters);

				if (scaleFactor <= 0.1d) {
					zoomOut.setDisable(true);
				}
				if (zoomIn.isDisabled()) {
					zoomIn.setDisable(false);
				}
			}

			parent.redraw();
		});

		VBox vBox = new VBox(5);
		vBox.setPadding(new Insets(5));
		vBox.getChildren().addAll(zoomIn, zoomOut, filter);
		layout.setLeft(vBox);

		RunnableHelper.setMaxScaleFactor(painters);
		// set scale AFTER the calculation of maxScale
		RunnableHelper.setInitialScaleFactor(painters, parameters);

		// disable zoom buttons if max scaling is reached on start
		if (!painters.isEmpty()) {
			if (painters.get(0).getScaleFactor() >= painters.get(0).getMaxScaleFactor()) {
				zoomIn.setDisable(true);
			}
			if (painters.get(0).getScaleFactor() <= 0.1d) {
				zoomOut.setDisable(true);
			}
		}

		Canvas canvas = new Canvas();

		ScrollPane scrollable = new ScrollPane();
		scrollable.setStyle("-fx-background: rgb(255,255,255);\n -fx-background-color: rgb(255,255,255)");
		scrollable.setContent(canvas);

		layout.setCenter(scrollable);

		canvas.setOnMouseClicked(event -> {
			for (RunnablePainter p : painters) {
				if (p.getRunnableRectangle().contains(event.getX(), event.getY())) {
					HashMap<String, Object> data = new HashMap<>();
					data.put("modelElements", Arrays.asList(p.getRunnable()));
					broker.send("org/eclipse/app4mc/amalthea/editor/SELECT", data);
				}
			}
		});

		canvas.setOnScroll(event -> {
			if (event.isControlDown()) {
				double deltaY = event.getDeltaY();

				if (deltaY < 0){
					zoomOut.fire();
				} else {
					zoomIn.fire();
				}

				event.consume();
			}
		});

		canvas.setOnMouseMoved(event -> {
			Point2D screenToLocal = canvas.screenToLocal(event.getScreenX(), event.getScreenY());
			double x = screenToLocal.getX();
			double y = screenToLocal.getY();

			boolean activate = false;
			for (RunnablePainter p : painters) {
				if (p.getRunnableRectangle().contains(x, y)) {
					activate = true;
					break;
				}
			}

			if (activate) {
				scene.setCursor(Cursor.HAND);
			} else {
				scene.setCursor(Cursor.DEFAULT);
			}
		});

		GraphicsContext gc = canvas.getGraphicsContext2D();

		parent.addPaintListener(e -> {

			org.eclipse.swt.graphics.Rectangle parentBounds = parent.getBounds();

			// need to calculate the minimum dimensions based on the scaling level
			double[] grid = RunnableHelper.calculateGrid(painters.size());
			double[] minDim = RunnableHelper.calculateMinimumCellDimensions(grid, painters);

			double minWidth = grid[0] * minDim[0];
			double minHeight = grid[1] * minDim[1];

			double canvasWidth = minWidth;
			double canvasHeight = minHeight;

			double width = minWidth / grid[0];
			double height = minHeight / grid[1];

			double parentBoundsWidth = parentBounds.width - 35d - vBox.getWidth();
			if (parentBoundsWidth > minWidth) {
				canvasWidth = parentBoundsWidth;
			}

			double parentBoundsHeight = parentBounds.height - 30d;
			if (parentBoundsHeight > minHeight) {
				canvasHeight = parentBoundsHeight;
			}

			// textures with bigger dimensions than 8192 x 8192 can't processed by even modern graphic cards
			// therefore we max it out by applying the max zoom factor we set to the painter before
			canvasWidth = Math.min(RunnableHelper.CANVAS_MAX, canvasWidth);
			canvasHeight = Math.min(RunnableHelper.CANVAS_MAX, canvasHeight);

			canvas.setWidth(canvasWidth);
			canvas.setHeight(canvasHeight);
			gc.clearRect(0, 0, canvasWidth, canvasHeight);

			double xStart = parentBounds.x;
			double yStart = parentBounds.y;

			int count = 0;
			RunnablePainter p = null;
			for (int row = 0; row < grid[1]; row++) {
				for (int column = 0; column < grid[0]; column++) {
					if (count >= painters.size()) {
						break;
					}

					p = painters.get(count);

					Rectangle bounds = new Rectangle(
							Double.valueOf(xStart),
							Double.valueOf(yStart),
							Double.valueOf(width),
							Double.valueOf(height));

					p.paint(gc, bounds);

					count++;
					xStart += width;
				}

				xStart = parentBounds.x;
				yStart += height;
			}

			// paint arrows
			for (RunnablePainter rp : painters) {
				Map<Label, Rectangle> writeRectangles = rp.getWriteRectangles();
				writeRectangles.entrySet().forEach(entry -> {
					for (RunnablePainter rp2 : painters) {
						Rectangle readRect = rp2.getReadRectangle(entry.getKey());
						if (readRect != null) {
							Rectangle writeRect = entry.getValue();

							double startX = writeRect.getX() + writeRect.getWidth();
							double startY = writeRect.getY() + ((writeRect.getHeight() / 10) * 9);
							double endX = readRect.getX();
							double endY = readRect.getY() + ((readRect.getHeight() / 10) * 9);

							arrowPainter.paint(gc, startX, startY, endX, endY);
						}
					}

				});
			}
		});
	}

}
