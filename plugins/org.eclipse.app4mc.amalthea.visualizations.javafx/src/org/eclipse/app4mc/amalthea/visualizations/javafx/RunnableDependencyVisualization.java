/*********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.javafx;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.visualizations.javafx.util.ArrowPainter;
import org.eclipse.app4mc.amalthea.visualizations.javafx.util.RunnableHelper;
import org.eclipse.app4mc.amalthea.visualizations.javafx.util.RunnablePainter;
import org.eclipse.app4mc.visualization.ui.VisualizationParameters;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import javafx.embed.swt.FXCanvas;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

@Component(property= {
		"name=Runnable Label Dependencies",
		"description=Visualize Runnable Label Dependencies"
})
public class RunnableDependencyVisualization implements Visualization {

	private ArrowPainter arrowPainter = new ArrowPainter();

	@PostConstruct
	public void createVisualization(Runnable runnable, VisualizationParameters parameters, Composite parent, IEventBroker broker) {

		if (runnable.eContainer() instanceof SWModel) {
			parent.setLayout(new GridLayout());

			FXCanvas fxCanvas = new FXCanvas(parent, SWT.NONE);

			GridDataFactory
				.fillDefaults()
				.grab(true, true)
				.applyTo(fxCanvas);

			// create the root layout pane
			BorderPane layout = new BorderPane();
			layout.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));

			// create a Scene instance
			// set the layout container as root
			// set the background fill to the background color of the shell
			Scene scene = new Scene(layout);

			// set the Scene to the FXCanvas
			fxCanvas.setScene(scene);

			List<Runnable> runnables = RunnableHelper.getRunnableDependencies((SWModel)runnable.eContainer(), runnable, false);
			int indexOfSelected = runnables.indexOf(runnable);

			List<RunnablePainter> painters = runnables.stream()
					.map(r -> new RunnablePainter(r, false))
					.collect(Collectors.toList());

			List<RunnablePainter> readPainter = painters.subList(0, indexOfSelected);
			RunnablePainter selectedPainter = painters.get(indexOfSelected);
			List<RunnablePainter> writePainter = indexOfSelected < painters.size() - 1
					? painters.subList(indexOfSelected + 1, painters.size())
					: Collections.emptyList();

			double[] columnWidths = calculateColumnWidths(readPainter, selectedPainter, writePainter);
			double[] rowHeights = calculateRowHeights(readPainter, selectedPainter, writePainter);

			// calculate the max scale factor
			double minWidth = Arrays.stream(columnWidths).sum();
			double minHeight = Arrays.stream(rowHeights).sum();

			double max = Math.max(minWidth, minHeight);
			double maxFactor = RunnableHelper.CANVAS_MAX / max;
			painters.forEach(p -> p.setMaxScaleFactor(maxFactor));
			// set scale AFTER the calculation of maxScale
			RunnableHelper.setInitialScaleFactor(painters, parameters);

			// add zoom buttons
			Button zoomIn = new Button("+");
			zoomIn.setPrefSize(40, 40);

			Button zoomOut = new Button("-");
			zoomOut.setPrefSize(40, 40);

			zoomIn.setOnAction(event -> {
				if (!zoomIn.isDisabled()) {
					painters.forEach(RunnablePainter::zoomIn);

					double scaleFactor = painters.get(0).getScaleFactor();
					RunnableHelper.persistScaleFactor(scaleFactor, parameters);

					if (scaleFactor >= painters.get(0).getMaxScaleFactor()) {
						zoomIn.setDisable(true);
					}
					if (zoomOut.isDisabled()) {
						zoomOut.setDisable(false);
					}
				}

				parent.redraw();
			});

			zoomOut.setOnAction(event -> {
				if (!zoomOut.isDisabled()) {
					painters.forEach(RunnablePainter::zoomOut);

					double scaleFactor = painters.get(0).getScaleFactor();
					RunnableHelper.persistScaleFactor(scaleFactor, parameters);

					if (scaleFactor <= 0.1d) {
						zoomOut.setDisable(true);
					}
					if (zoomIn.isDisabled()) {
						zoomIn.setDisable(false);
					}
				}

				parent.redraw();
			});

			VBox vBox = new VBox(5);
			vBox.setPadding(new Insets(5));
			vBox.getChildren().addAll(zoomIn, zoomOut);
			layout.setLeft(vBox);

			// disable zoom buttons if max scaling is reached on start
			if (!painters.isEmpty()) {
				if (painters.get(0).getScaleFactor() >= painters.get(0).getMaxScaleFactor()) {
					zoomIn.setDisable(true);
				}
				if (painters.get(0).getScaleFactor() <= 0.1d) {
					zoomOut.setDisable(true);
				}
			}

			Canvas canvas = new Canvas();

			ScrollPane scrollable = new ScrollPane();
			scrollable.setPadding(new Insets(0));
			scrollable.setStyle("-fx-background: rgb(255,255,255);\n -fx-background-color: rgb(255,255,255)");
			scrollable.setContent(canvas);

			layout.setCenter(scrollable);

			canvas.setOnMouseClicked(event -> {
				for (RunnablePainter p : painters) {
					if (p.equals(selectedPainter)) {
						continue;
					}
					if (p.getRunnableRectangle().contains(event.getX(), event.getY())) {
						HashMap<String, Object> data = new HashMap<>();
						data.put("modelElements", Arrays.asList(p.getRunnable()));
						broker.send("org/eclipse/app4mc/amalthea/editor/SELECT", data);
					}
				}
			});

			canvas.setOnScroll(event -> {
				if (event.isControlDown()) {
					double deltaY = event.getDeltaY();

					if (deltaY < 0){
						zoomOut.fire();
					} else {
						zoomIn.fire();
					}

					event.consume();
				}
			});

			canvas.setOnMouseMoved(event -> {
				Point2D screenToLocal = canvas.screenToLocal(event.getScreenX(), event.getScreenY());
				double x = screenToLocal.getX();
				double y = screenToLocal.getY();

				boolean activate = false;
				for (RunnablePainter p : painters) {
					if (p.equals(selectedPainter)) {
						continue;
					}
					if (p.getRunnableRectangle().contains(x, y)) {
						activate = true;
						break;
					}
				}

				if (activate) {
					scene.setCursor(Cursor.HAND);
				} else {
					scene.setCursor(Cursor.DEFAULT);
				}
			});

			GraphicsContext gc = canvas.getGraphicsContext2D();

			parent.addPaintListener(e -> {

				org.eclipse.swt.graphics.Rectangle parentBounds = parent.getBounds();

				double[] scaledColumnWidths = Arrays.stream(columnWidths).map(width -> width * selectedPainter.getScaleFactor()).toArray();
				double[] scaledRowHeights = Arrays.stream(rowHeights).map(height -> height * selectedPainter.getScaleFactor()).toArray();

				double minWidth1 = Arrays.stream(scaledColumnWidths).sum();
				double minHeight1 = Arrays.stream(scaledRowHeights).sum();

				double canvasWidth = minWidth1;
				double canvasHeight = minHeight1;

				double parentBoundsWidth = parentBounds.width - 30d - vBox.getWidth();
				if (parentBoundsWidth > minWidth1) {
					canvasWidth = parentBoundsWidth;
				}

				double parentBoundsHeight = parentBounds.height - 30d;
				if (parentBoundsHeight > minHeight1) {
					canvasHeight = parentBoundsHeight;
				}

				// textures with bigger dimensions than 8192 x 8192 can't processed by even modern graphic cards
				// therefore we max it out by applying the max zoom factor we set to the painter before
				canvasWidth = Math.min(RunnableHelper.CANVAS_MAX, canvasWidth);
				canvasHeight = Math.min(RunnableHelper.CANVAS_MAX, canvasHeight);

				canvas.setWidth(canvasWidth);
				canvas.setHeight(canvasHeight);
				gc.clearRect(0, 0, canvasWidth, canvasHeight);

				double xStart = parentBounds.x;
				double yStart = parentBounds.y;

				Rectangle bounds = null;

				int row = 0;
				int column = 0;

				// paint the read dependencies
				for (RunnablePainter p1 : readPainter) {
					bounds = new Rectangle(
							Double.valueOf(xStart),
							Double.valueOf(yStart),
							scaledColumnWidths[column],
							scaledRowHeights[row]);

					p1.paint(gc, bounds);

					yStart += scaledRowHeights[row];

					row++;
				}

				// paint the selected Runnable
				xStart = parentBounds.x + scaledColumnWidths[0];
				yStart = parentBounds.y;

				selectedPainter.setRunnableHeaderFontFill(Color.WHITE);
				selectedPainter.setRunnableColor(Color.GREEN);

				row = 0;
				column = 1;

				bounds = new Rectangle(
						Double.valueOf(xStart),
						Double.valueOf(yStart),
						scaledColumnWidths[column],
						scaledRowHeights[row]);

				selectedPainter.paint(gc, bounds);

				// paint the write dependencies
				xStart = parentBounds.x + scaledColumnWidths[0] + scaledColumnWidths[1];
				yStart = parentBounds.y;

				row = 0;
				column = 2;

				for (RunnablePainter p2 : writePainter) {
					bounds = new Rectangle(
							Double.valueOf(xStart),
							Double.valueOf(yStart),
							scaledColumnWidths[column],
							scaledRowHeights[row]);

					p2.paint(gc, bounds);

					yStart += scaledRowHeights[row];

					row++;
				}

				// paint arrows
				for (RunnablePainter rp : painters) {
					Map<Label, Rectangle> writeRectangles = rp.getWriteRectangles();
					writeRectangles.entrySet().forEach(entry -> {
						for (RunnablePainter rp2 : painters) {
							Rectangle readRect = rp2.getReadRectangle(entry.getKey());
							if (readRect != null) {
								Rectangle writeRect = entry.getValue();

								double startX = writeRect.getX() + writeRect.getWidth();
								double startY = writeRect.getY() + ((writeRect.getHeight() / 10) * 9);
								double endX = readRect.getX();
								double endY = readRect.getY() + ((readRect.getHeight() / 10) * 9);

								arrowPainter.paint(gc, startX, startY, endX, endY);
							}
						}

					});
				}
			});
		}
	}

	private static double[] calculateColumnWidths(List<RunnablePainter> readPainter, RunnablePainter selectedPainter, List<RunnablePainter> writePainter) {
		double[] widths = { 0d, 0d, 0d };

		for (RunnablePainter runnablePainter : readPainter) {
			widths[0] = Math.max(widths[0], runnablePainter.getPreferredMinimumWidth());
		}

		widths[1] = selectedPainter.getPreferredMinimumWidth();

		for (RunnablePainter runnablePainter : writePainter) {
			widths[2] = Math.max(widths[2], runnablePainter.getPreferredMinimumWidth());
		}

		return widths;
	}

	private static double[] calculateRowHeights(List<RunnablePainter> readPainter, RunnablePainter selectedPainter, List<RunnablePainter> writePainter) {
		int rowCount = Math.max(readPainter.size(), writePainter.size());
		if (rowCount == 0) {
			// selected a Runnable with no dependencies
			// at least one row for the selected Runnable
			rowCount = 1;
		}
		double[] heights = new double[rowCount];

		for (int row = 0; row < rowCount; row++) {
			double height = 0;

			if (readPainter.size() > row) {
				height = Math.max(height, readPainter.get(row).getPreferredMinimumHeight());
			}

			if (row == 0) {
				height = Math.max(height, selectedPainter.getPreferredMinimumHeight());
			}

			if (writePainter.size() > row) {
				height = Math.max(height, writePainter.get(row).getPreferredMinimumHeight());
			}

			heights[row] = height;
		}

		return heights;
	}

}
