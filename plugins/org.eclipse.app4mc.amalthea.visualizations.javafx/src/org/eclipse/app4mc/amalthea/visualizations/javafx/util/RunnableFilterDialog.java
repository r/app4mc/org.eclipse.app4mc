/*********************************************************************************
 * Copyright (c) 2020-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */
package org.eclipse.app4mc.amalthea.visualizations.javafx.util;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.Activation;
import org.eclipse.app4mc.amalthea.model.AmaltheaFactory;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * Dialog to filter a list of Runnables.
 */
public class RunnableFilterDialog extends Dialog {

	private static final String STATISTICS_MSG = "Currently selected {0} of {1}";

	private List<Runnable> originalList;
	private List<Runnable> result;

	private String nameFilter = "";
	private Activation activationFilter = null;

	/**
	 * Fake empty Activation needed to support clearing of activation filter.
	 */
	private Activation emptyActivation;

	public RunnableFilterDialog(Shell parentShell, List<Runnable> toFilter) {
		super(parentShell);
		originalList = toFilter;
		result = toFilter;

		emptyActivation = AmaltheaFactory.eINSTANCE.createCustomActivation();
		emptyActivation.setName("---");

		activationFilter = emptyActivation;
	}

	private void filterRunnable() {
		result = originalList.stream()
				.filter(r -> r.getName().contains(nameFilter)
						&& (activationFilter == null
							|| activationFilter.equals(emptyActivation)
							|| r.getActivations().contains(activationFilter)))
				.collect(Collectors.toList());
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);

		container.setLayout(new GridLayout(2, false));

		Label hint = new Label(container, SWT.NONE);
		hint.setText("The number of Runnables to visualize is greater than 100. The painting may take very long or even fail at all.\n\nPlease try to reduce the number of Runnables by applying a filter.");

		GridDataFactory.swtDefaults()
			.align(SWT.FILL, SWT.BEGINNING)
			.span(2, 1)
			.grab(true, false)
			.applyTo(hint);

		Label statistics = new Label(container, SWT.NONE);
		statistics.setText(MessageFormat.format(STATISTICS_MSG, result.size(), originalList.size()));

		GridDataFactory.swtDefaults()
			.align(SWT.FILL, SWT.BEGINNING)
			.span(2, 1)
			.grab(true, false)
			.applyTo(statistics);

		Label name = new Label(container, SWT.NONE);
		name.setText("Name filter:");

		GridDataFactory.swtDefaults()
			.align(SWT.FILL, SWT.BEGINNING)
			.grab(false, false)
			.applyTo(name);

		Text nameFilterText = new Text(container, SWT.BORDER);
		nameFilterText.setText(nameFilter);
		nameFilterText.addModifyListener(e -> {
			nameFilter = nameFilterText.getText();

			filterRunnable();

			statistics.setText(MessageFormat.format(STATISTICS_MSG, result.size(), originalList.size()));
		});

		GridDataFactory.swtDefaults()
			.align(SWT.FILL, SWT.FILL)
			.grab(true, false)
			.applyTo(nameFilterText);

		Label activation = new Label(container, SWT.NONE);
		activation.setText("Activation filter:");

		GridDataFactory.swtDefaults()
			.align(SWT.FILL, SWT.BEGINNING)
			.grab(false, false)
			.applyTo(activation);


		ComboViewer activationFilterCombo = new ComboViewer(container, SWT.READ_ONLY);

		activationFilterCombo.setContentProvider(ArrayContentProvider.getInstance());
		activationFilterCombo.setLabelProvider(new LabelProvider() {
		    @Override
		    public String getText(Object element) {
		        if (element instanceof Activation) {
		        	Activation activation = (Activation) element;
		            return activation.getName();
		        }
		        return super.getText(element);
		    }
		});

		List<Activation> activations = originalList.stream().flatMap(r -> r.getActivations().stream()).distinct().collect(Collectors.toList());
		activations.add(0, emptyActivation);
		activationFilterCombo.setInput(activations);

		if (activationFilter != null) {
			activationFilterCombo.setSelection(new StructuredSelection(List.of(activationFilter)));
		}

		GridDataFactory.swtDefaults()
			.align(SWT.FILL, SWT.FILL)
			.grab(true, false)
			.applyTo(activationFilterCombo.getControl());

		activationFilterCombo.addSelectionChangedListener(event -> {
		    IStructuredSelection selection = (IStructuredSelection) event.getSelection();
		    if (selection.size() > 0) {
		    	activationFilter = (Activation) selection.getFirstElement();

		    	filterRunnable();

				statistics.setText(MessageFormat.format(STATISTICS_MSG, result.size(), originalList.size()));
		    }
		});

		return container;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Filter Runnables");
	}

	@Override
	protected boolean isResizable() {
		return true;
	}

	public List<Runnable> getSelectedRunnables() {
		return result;
	}
}
