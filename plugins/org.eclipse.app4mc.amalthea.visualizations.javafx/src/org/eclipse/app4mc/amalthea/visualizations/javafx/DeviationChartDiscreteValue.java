/*********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.javafx;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.apache.commons.math3.distribution.BetaDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.WeibullDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueBetaDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueBoundaries;
import org.eclipse.app4mc.amalthea.model.DiscreteValueGaussDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueHistogram;
import org.eclipse.app4mc.amalthea.model.DiscreteValueHistogramEntry;
import org.eclipse.app4mc.amalthea.model.DiscreteValueStatistics;
import org.eclipse.app4mc.amalthea.model.DiscreteValueUniformDistribution;
import org.eclipse.app4mc.amalthea.model.DiscreteValueWeibullEstimatorsDistribution;
import org.eclipse.app4mc.amalthea.model.IDiscreteValueDeviation;
import org.eclipse.app4mc.amalthea.model.util.WeibullUtil;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.osgi.service.component.annotations.Component;

import javafx.embed.swt.FXCanvas;
import javafx.scene.Scene;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ValueAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

@Component(property= {
		"name=Probability Density Diagram (discrete values)",
		"description=Visualize the Probability Density Function (PDF) of the deviation"
})
public class DeviationChartDiscreteValue extends AbstractDeviationChart implements Visualization {

	@PostConstruct
	public void createVisualization(IDiscreteValueDeviation dev, Composite parent) {
		parent.setLayout(new GridLayout());

		FXCanvas canvas = new FXCanvas(parent, SWT.NONE);

		GridDataFactory
		    .fillDefaults()
		    .grab(true, true)
		    .applyTo(canvas);

		// create the root layout pane
		BorderPane layout = new BorderPane();
		layout.setBackground(new Background(new BackgroundFill(Color.WHITE, null, null)));

		// create a Scene instance
		// set the layout container as root
		// set the background fill to the background color of the shell
		Scene scene = new Scene(layout);

		// set the Scene to the FXCanvas
		canvas.setScene(scene);

		// ***** display message 'invalid input' *****

		if (!isValid(dev)) {
			Label output = new Label();
			output.setText("Invalid input");
			layout.setCenter(output);
			return;
		}

		// ***** display chart with single peek (includes DiscreteValueConstant) *****

		final Double lowerBound = (dev.getLowerBound() != null) ? dev.getLowerBound().doubleValue() : null;
		final Double upperBound = (dev.getUpperBound() != null) ? dev.getUpperBound().doubleValue() : null;

		if (isSinglePeek(lowerBound, upperBound)) {
			AreaChart<Number, Number> chart = addNewChart(layout, dev, null);
			setChartXBounds(chart, lowerBound - 5, upperBound + 5);
			setChartYBounds(chart, 100);
			addSinglePeek(chart, 80, lowerBound);
			return;
		}

		// ***** display standard chart *****

		AreaChart<Number, Number> chart = addNewChart(layout, dev, null);

		if (lowerBound != null && upperBound != null && lowerBound < upperBound) {
			final double margin = 0.25 * (upperBound - lowerBound);
			final double xMin = lowerBound - margin;
			final double xMax = upperBound + margin;
			setChartXBounds(chart, xMin, xMax);
		}

		String status = null;
		if (dev instanceof DiscreteValueHistogram) {
			fillChart(chart, (DiscreteValueHistogram) dev);
		} else if (dev instanceof DiscreteValueGaussDistribution) {
			status = fillChart(chart, (DiscreteValueGaussDistribution) dev);
		} else if (dev instanceof DiscreteValueBoundaries) {
			fillChart(chart, (DiscreteValueBoundaries) dev);
		} else if (dev instanceof DiscreteValueStatistics) {
			fillChart(chart, (DiscreteValueStatistics) dev);
		} else if (dev instanceof DiscreteValueUniformDistribution) {
			fillChart(chart, (DiscreteValueUniformDistribution) dev);
		} else if (dev instanceof DiscreteValueBetaDistribution) {
			fillChart(chart, (DiscreteValueBetaDistribution) dev);
		} else if (dev instanceof DiscreteValueWeibullEstimatorsDistribution) {
			status = fillChart(chart, (DiscreteValueWeibullEstimatorsDistribution) dev);
		}

		// ***** display status line *****

		addNewStatus(layout, status);
	}

	private void fillChart(AreaChart<Number, Number> chart, DiscreteValueHistogram histogram) {
		long maxY = 0;

		// add chart content
		for (DiscreteValueHistogramEntry entry : histogram.getEntries()) {
			double lower = entry.getLowerBound();
			double upper = entry.getUpperBound();
			long occur = entry.getOccurrences();

			maxY = Math.max(occur, maxY);

			final Series<Number, Number> series = new XYChart.Series<>();
			series.getData().add(new Data<>(lower, 0L));
			series.getData().add(new Data<>(lower, occur));
			series.getData().add(new Data<>(upper, occur));
			series.getData().add(new Data<>(upper, 0L));

			// add and style series
			addSeriesStandard(chart, series);
		}

		// adopt range and add markers (min, avg, max)
		setChartYBounds(chart, maxY * 1.2);
		addMarkers(chart, maxY * 1.1, histogram.getLowerBound().doubleValue(), histogram.getAverage(), histogram.getUpperBound().doubleValue());
	}

	private String fillChart(AreaChart<Number, Number> chart, DiscreteValueGaussDistribution dev) {
		double maxY = 0.000001; // initialize with a lower bound > 0

		// add chart content
		final Double lowerBound = (dev.getLowerBound() != null) ? dev.getLowerBound().doubleValue() : null;
		final Double upperBound = (dev.getUpperBound() != null) ? dev.getUpperBound().doubleValue() : null;
		final Double average = dev.getAverage();

		// standard display range for non truncated Gauss
		double xMin = dev.getMean() - 4 * dev.getSd();
		double xMax = dev.getMean() + 4 * dev.getSd();

		if (lowerBound != null && upperBound != null && lowerBound < upperBound) {
			// standard display range for truncated Gauss with upper and lower bounds
			double margin = 0.25 * (upperBound - lowerBound);
			xMin = lowerBound - margin;
			xMax = upperBound + margin;
		} else {
			// only lower bound set
			if (lowerBound != null && upperBound == null) {
				xMin = Math.min(xMin, lowerBound - dev.getSd());
				xMax = Math.max(xMax, lowerBound + dev.getSd());
			}
			// only upper bound set
			if (lowerBound == null && upperBound != null) {
				xMin = Math.min(xMin, upperBound - dev.getSd());
				xMax = Math.max(xMax, upperBound + dev.getSd());
			}
		}

		final double step = (xMax - xMin) / 200;

		final NormalDistribution mathFunction = new NormalDistribution(null, dev.getMean(), dev.getSd());
		final Series<Number, Number> seriesMain = new XYChart.Series<>();
		final Series<Number, Number> seriesLeft = new XYChart.Series<>();
		final Series<Number, Number> seriesRight = new XYChart.Series<>();

		for (double x = xMin; x <= xMax; x=x+step) {
			double y = mathFunction.density(x);
			maxY = Math.max(y, maxY);

			if (lowerBound != null && x < lowerBound) {
				seriesLeft.getData().add(new Data<>(x, y));
			} else if (upperBound != null && x > upperBound) {
				seriesRight.getData().add(new Data<>(x, y));
			} else {
				seriesMain.getData().add(new Data<>(x, y));
			}
		}

		// add and style series
		addSeriesOffLimit(chart, seriesLeft);
		addSeriesOffLimit(chart, seriesRight);
		addSeriesStandard(chart, seriesMain);

		// adopt range
		setChartXBounds(chart, xMin, xMax);
		setChartYBounds(chart, maxY * 1.2);

		// add markers (min, avg, max)
		addMarkers(chart, maxY * 1.1, lowerBound, average, upperBound);

		// add warning if probability is too low (< 10%)
		final double cdfUpper = (upperBound == null) ? 1 : mathFunction.cumulativeProbability(upperBound);
		final double cdfLower = (lowerBound == null) ? 0 : mathFunction.cumulativeProbability(lowerBound);
		final double p = cdfUpper - cdfLower;

		if (p < 0.1) {
			DecimalFormat df = new DecimalFormat("0.0000", new DecimalFormatSymbols(Locale.ENGLISH));
			return "Cumulative probability within specified bounds is " + df.format(p);
		}

		return null;
	}

	private void fillChart(AreaChart<Number, Number> chart, DiscreteValueBoundaries dev) {
		final double lower = dev.getLowerBound().doubleValue();
		final double upper = dev.getUpperBound().doubleValue();

		final Series<Number, Number> series = new XYChart.Series<>();
		series.getData().add(new Data<>(lower, 60));
		series.getData().add(new Data<>(upper, 60));
		addSeriesGradient(chart, series);

		setChartYBounds(chart, 100);
		addMarkers(chart, 80, lower, null, upper);
	}

	private void fillChart(AreaChart<Number, Number> chart, DiscreteValueStatistics stat) {
		double maxY = 0;

		final double lower = stat.getLowerBound().doubleValue();
		final double upper = stat.getUpperBound().doubleValue();
		final double average = stat.getAverage().doubleValue();

		if (lower < upper) {
			double y1 = 100.0 / (average - lower);
			double y2 = 100.0 / (upper - average);
			maxY = Math.max(y1, y2);

			final Series<Number, Number> series1 = new XYChart.Series<>();
			series1.getData().add(new Data<>(lower, y1));
			series1.getData().add(new Data<>(average, y1));

			final Series<Number, Number> series2 = new XYChart.Series<>();
			series2.getData().add(new Data<>(average, y2));
			series2.getData().add(new Data<>(upper, y2));

			// add and style series
			addSeriesGradient(chart, series1);
			addSeriesGradient(chart, series2);
		} else {
			maxY = 100; // single value
		}

		// adopt range and add markers (min, avg, max)
		setChartYBounds(chart, maxY * 1.2);
		addMarkers(chart, maxY * 1.1, lower, average, upper);
	}

	private void fillChart(AreaChart<Number, Number> chart, DiscreteValueUniformDistribution dev) {
		final double lower = dev.getLowerBound().doubleValue();
		final double upper = dev.getUpperBound().doubleValue();

		final Series<Number, Number> series = new XYChart.Series<>();
		series.getData().add(new Data<>(lower, 60));
		series.getData().add(new Data<>(upper, 60));
		addSeriesStandard(chart, series);

		setChartYBounds(chart, 100);
		addMarkers(chart, 80, lower, null, upper);
	}

	private void fillChart(AreaChart<Number, Number> chart, DiscreteValueBetaDistribution dist) {
		double maxY = 0.0;

		final double x1 = dist.getLowerBound().doubleValue();
		final double x2 = dist.getUpperBound().doubleValue();
		final double range = x2 - x1;

		if (range > 0) {
			final BetaDistribution mathFunction = new BetaDistribution(null, dist.getAlpha(), dist.getBeta());
			final Series<Number, Number> series = new XYChart.Series<>();

			for (double x = 0.005; x < 1; x=x+0.005) {
				double y = mathFunction.density(x);
				maxY = Math.max(y, maxY);
				series.getData().add(new Data<>(x1 + (x * range), y));
			}
			// add and style series
			addSeriesStandard(chart, series);
		}

		// adopt range and add markers (min, avg, max)
		setChartYBounds(chart, maxY * 1.2);
		addMarkers(chart, maxY * 1.1, x1, dist.getAverage(), x2);
	}

	private String fillChart(AreaChart<Number, Number> chart, DiscreteValueWeibullEstimatorsDistribution dev) {
		double maxY = 0;

		final double lower = dev.getLowerBound().doubleValue();
		final double upper = dev.getUpperBound().doubleValue();
		final double average = dev.getAverage().doubleValue();
		final double remain = dev.getPRemainPromille();

		WeibullUtil.Parameters params = WeibullUtil.findParameters(lower, average, upper, remain);
		double shape = params.shape;
		double scale = params.scale;

		if (lower < upper) {
			if (params.error == null) {
				final WeibullDistribution mathFunction = new WeibullDistribution(null, shape, scale);
				final Series<Number, Number> seriesMain = new XYChart.Series<>();
				final Series<Number, Number> seriesRight = new XYChart.Series<>();

				final double xMax = (chart.getXAxis() instanceof NumberAxis) ?
						((ValueAxis<Number>) chart.getXAxis()).getUpperBound() : upper;
				final double step = (xMax - lower) / 200;

				for (double x = lower; x <= xMax; x = x + step) {
					double y = mathFunction.density(x - lower);
					if (Double.isFinite(y)) {
						maxY = Math.max(y, maxY);
						if (x <= upper) {
							seriesMain.getData().add(new Data<>(x, y));
						} else {
							seriesRight.getData().add(new Data<>(x, y));
						}
					}
				}

				// add and style series
				addSeriesStandard(chart, seriesMain);
				addSeriesOffLimit(chart, seriesRight);
			} else {
				maxY = 100;
			}

			// adopt range
			setChartYBounds(chart, maxY * 1.2);

			// add computed average
			double computedAvg = WeibullUtil.computeAverage(shape, scale, lower, upper);
			final Series<Number, Number> series2 = new XYChart.Series<>();
			chart.getData().add(series2);
			series2.setName("avg");
			series2.getData().add(new XYChart.Data<>(computedAvg, 0));
			series2.getData().add(new XYChart.Data<>(computedAvg, maxY / 2));
			series2.getNode().lookup(".chart-series-area-line").setStyle("-fx-stroke: rgba(5, 150, 5, 1.0); -fx-stroke-dash-array: 3;");
			series2.getData().get(1).getNode().setVisible(false);

			// add markers (min, avg, max)
			setChartYBounds(chart, maxY * 1.2);
			addMarkers(chart, maxY * 1.1, lower, average, upper);

			// add warning if difference between input and actual value of average is too big
			if (Math.abs(average - computedAvg) / (upper - lower) > 0.01) {
				return "Approximation is deviating. Average marker: red" + ARROW + "requested, green" + ARROW + "actual";
			}
		}

		return null;
	}

}
