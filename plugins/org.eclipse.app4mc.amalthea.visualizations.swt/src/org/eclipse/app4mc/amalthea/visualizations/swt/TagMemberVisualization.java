/*********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.swt;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.ITaggable;
import org.eclipse.app4mc.amalthea.model.Tag;
import org.eclipse.app4mc.amalthea.model.provider.AmaltheaDefaultLabelProvider;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerComparator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.osgi.service.component.annotations.Component;

@Component(property = {
		"name=Tag Member Table",
		"description=Shows list of tagged objects"
})
public class TagMemberVisualization implements Visualization {

	@PostConstruct
	public void createVisualization(List<Tag> tags, Composite parent, IEventBroker broker) {
		Composite pane = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().applyTo(pane);

		Label title = new Label(pane, SWT.NONE);
		title.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		title.setText("\nObjects tagged with " + label(tags));

		TableViewer tableViewer = new TableViewer(pane, SWT.READ_ONLY | SWT.V_SCROLL | SWT.BORDER);
		tableViewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));

		// label provider
		tableViewer.setLabelProvider(new AmaltheaDefaultLabelProvider() {
			@Override
			public String getText(Object element) {
				return name(element);
			}
		});

		// content provider
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setInput(taggedObjects(tags));

		// alphabetic order
		tableViewer.setComparator(new ViewerComparator());

		// double click listener
		tableViewer.addDoubleClickListener(event -> {
			IStructuredSelection selection = (IStructuredSelection) event.getSelection();
			Object firstElement = selection.getFirstElement();

			if (firstElement instanceof ITaggable) {
				// select element in the Amalthea model editor
				HashMap<String, Object> data = new HashMap<>();
				data.put("modelElements", List.of(firstElement));
				broker.send("org/eclipse/app4mc/amalthea/editor/SELECT", data);
			}
		});
	}

	private String label(List<Tag> tags) {
		return tags.stream()
				.map(t -> "\"" + t.getQualifiedName() + "\"")
				.sorted()
				.collect(Collectors.joining(" OR "));
	}

	private List<ITaggable> taggedObjects(List<Tag> tags) {
		return tags.stream()
				.flatMap(t -> t.getTaggedObjects().stream())
				.collect(Collectors.toList());
	}

	private String name(final Object object) {
		if (object instanceof INamed) {
			String name = ((INamed) object).getName();
			return (name == null || name.isEmpty()) ? "?" : name;
		}
		return "<undefined>";
	}

}
