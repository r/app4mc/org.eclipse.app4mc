/*********************************************************************************
 * Copyright (c) 2020-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.visualizations.swt;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.ISRAllocation;
import org.eclipse.app4mc.amalthea.model.InterruptController;
import org.eclipse.app4mc.amalthea.model.MappingModel;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.nebula.widgets.nattable.NatTable;
import org.eclipse.nebula.widgets.nattable.data.IColumnPropertyAccessor;
import org.eclipse.nebula.widgets.nattable.data.IDataProvider;
import org.eclipse.nebula.widgets.nattable.data.ListDataProvider;
import org.eclipse.nebula.widgets.nattable.data.ReflectiveColumnPropertyAccessor;
import org.eclipse.nebula.widgets.nattable.extension.glazedlists.tree.GlazedListTreeData;
import org.eclipse.nebula.widgets.nattable.extension.glazedlists.tree.GlazedListTreeRowModel;
import org.eclipse.nebula.widgets.nattable.grid.data.DefaultColumnHeaderDataProvider;
import org.eclipse.nebula.widgets.nattable.grid.data.DefaultCornerDataProvider;
import org.eclipse.nebula.widgets.nattable.grid.data.DefaultRowHeaderDataProvider;
import org.eclipse.nebula.widgets.nattable.grid.layer.ColumnHeaderLayer;
import org.eclipse.nebula.widgets.nattable.grid.layer.CornerLayer;
import org.eclipse.nebula.widgets.nattable.grid.layer.GridLayer;
import org.eclipse.nebula.widgets.nattable.grid.layer.RowHeaderLayer;
import org.eclipse.nebula.widgets.nattable.layer.DataLayer;
import org.eclipse.nebula.widgets.nattable.layer.ILayer;
import org.eclipse.nebula.widgets.nattable.painter.NatTableBorderOverlayPainter;
import org.eclipse.nebula.widgets.nattable.painter.cell.BackgroundPainter;
import org.eclipse.nebula.widgets.nattable.painter.cell.ICellPainter;
import org.eclipse.nebula.widgets.nattable.painter.cell.TextPainter;
import org.eclipse.nebula.widgets.nattable.painter.cell.decorator.PaddingDecorator;
import org.eclipse.nebula.widgets.nattable.resize.AutoResizeRowPaintListener;
import org.eclipse.nebula.widgets.nattable.selection.SelectionLayer;
import org.eclipse.nebula.widgets.nattable.style.HorizontalAlignmentEnum;
import org.eclipse.nebula.widgets.nattable.style.VerticalAlignmentEnum;
import org.eclipse.nebula.widgets.nattable.style.theme.ModernNatTableThemeConfiguration;
import org.eclipse.nebula.widgets.nattable.tree.ITreeRowModel;
import org.eclipse.nebula.widgets.nattable.tree.TreeLayer;
import org.eclipse.nebula.widgets.nattable.tree.painter.IndentedTreeImagePainter;
import org.eclipse.nebula.widgets.nattable.tree.painter.TreeImagePainter;
import org.eclipse.nebula.widgets.nattable.ui.util.CellEdgeEnum;
import org.eclipse.nebula.widgets.nattable.util.GUIHelper;
import org.eclipse.nebula.widgets.nattable.viewport.ViewportLayer;
import org.eclipse.swt.widgets.Composite;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Component;

import com.google.common.base.Strings;

import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.SortedList;
import ca.odell.glazedlists.TransformedList;
import ca.odell.glazedlists.TreeList;

@Component(property = { "name=Scheduler Mapping Table",
		"description=Renders the scheduler mapping in a table based view" })

public class MappingModelVisualization implements Visualization {

	@PostConstruct
	public void createVisualization(MappingModel model, Composite parent) {

		List<TableItem> data = createTableRows(model);
		addMappingTable(parent, data);
	}

	private List<TableItem> createTableRows(MappingModel model) {

		Map<Scheduler, TableItem> internalData = new HashMap<>();

		// ***** get all schedulers and collect table item data

		for (Scheduler scheduler : AmaltheaIndex.getElements(model, Scheduler.class)) {
			TableItem item = new TableItem();
			internalData.put(scheduler, item);

			item.setScheduler(name(scheduler));

			item.setProcessingUnits(scheduler.getSchedulerAllocations().stream()
					.map(SchedulerAllocation::getResponsibility).flatMap(Collection::stream).map(this::name).sorted()
					.collect(Collectors.joining(System.lineSeparator())));

			if (scheduler instanceof TaskScheduler) {
				TaskScheduler ts = (TaskScheduler) scheduler;

				item.setSubSchedulers(ts.getChildSchedulers().stream().map(this::name).sorted()
						.collect(Collectors.joining(System.lineSeparator())));

				item.setProcesses(ts.getTaskAllocations().stream().map(TaskAllocation::getTask).filter(Objects::nonNull)
						.map(this::name).sorted().collect(Collectors.joining(System.lineSeparator())));
			}

			if (scheduler instanceof InterruptController) {
				InterruptController ic = (InterruptController) scheduler;

				item.setProcesses(ic.getIsrAllocations().stream().map(ISRAllocation::getIsr).filter(Objects::nonNull)
						.map(this::name).sorted().collect(Collectors.joining(System.lineSeparator())));
			}
		}

		// ***** link to parent table items (based on model data)

		for (Entry<Scheduler, TableItem> entry : internalData.entrySet()) {
			Scheduler scheduler = entry.getKey();
			TableItem item = entry.getValue();
			if (scheduler instanceof TaskScheduler) {
				TaskScheduler ts = (TaskScheduler) scheduler;
				item.setParent(internalData.get(ts.getParentScheduler()));
			}
		}

		// ***** compute and cache table item path

		for (TableItem item : internalData.values()) {
			// add item as last element of the path
			item.getPath().add(item);

			TableItem parent = item.getParent();

			// add parent items (if available)
			while (parent != null) {
				item.getPath().add(0, parent);
				parent = parent.getParent();
			}
		}

		return new ArrayList<>(internalData.values());
	}

	private String name(final INamed object) {
		if (object == null)
			return "<undefined>";

		if (Strings.isNullOrEmpty(object.getName())) {
			return "?";
		} else {
			return object.getName();
		}
	}

	private void addMappingTable(Composite parent, List<TableItem> data) {
		// property names of the TableItem class
		String[] propertyNames = { "scheduler", "processingUnits", "subSchedulers", "processes" };

		// mapping from property to label, needed for column header labels
		Map<String, String> propertyToLabelMap = new HashMap<>();
		propertyToLabelMap.put("scheduler", "Scheduler");
		propertyToLabelMap.put("processingUnits", "Processing Units");
		propertyToLabelMap.put("subSchedulers", "Sub Schedulers");
		propertyToLabelMap.put("processes", "Processes");

		IColumnPropertyAccessor<TableItem> columnPropertyAccessor = new ReflectiveColumnPropertyAccessor<>(
				propertyNames);

		EventList<TableItem> eventList = GlazedLists.eventList(data);
		TransformedList<TableItem, TableItem> rowObjectsGlazedList = GlazedLists.threadSafeList(eventList);
		SortedList<TableItem> sortedList = new SortedList<>(rowObjectsGlazedList, null);
		TreeList<TableItem> treeList = new TreeList<>(sortedList, new TableItemTreeFormat(),
				TreeList.nodesStartExpanded());

		// create the body layer stack
		IDataProvider bodyDataProvider = new ListDataProvider<>(treeList, columnPropertyAccessor);
		final DataLayer bodyDataLayer = new DataLayer(bodyDataProvider);
		final SelectionLayer selectionLayer = new SelectionLayer(bodyDataLayer);

		GlazedListTreeData<TableItem> treeData = new GlazedListTreeData<>(treeList);
		ITreeRowModel<TableItem> treeRowModel = new GlazedListTreeRowModel<>(treeData);
		TreeLayer treeLayer = new TreeLayer(selectionLayer, treeRowModel);

		ViewportLayer viewportLayer = new ViewportLayer(treeLayer);

		// create the column header layer stack
		IDataProvider columnHeaderDataProvider = new DefaultColumnHeaderDataProvider(propertyNames, propertyToLabelMap);
		ILayer columnHeaderLayer = new ColumnHeaderLayer(new DataLayer(columnHeaderDataProvider), viewportLayer,
				selectionLayer);

		// create the row header layer stack
		IDataProvider rowHeaderDataProvider = new DefaultRowHeaderDataProvider(bodyDataProvider);
		ILayer rowHeaderLayer = new RowHeaderLayer(new DataLayer(rowHeaderDataProvider, 50, 20), viewportLayer,
				selectionLayer);

		// create the corner layer stack
		ILayer cornerLayer = new CornerLayer(
				new DataLayer(new DefaultCornerDataProvider(columnHeaderDataProvider, rowHeaderDataProvider)),
				rowHeaderLayer, columnHeaderLayer);

		// create the grid layer composed with the prior created layer stacks
		GridLayer gridLayer = new GridLayer(viewportLayer, columnHeaderLayer, rowHeaderLayer, cornerLayer);

		NatTable natTable = new NatTable(parent, gridLayer);

		natTable.setBackground(GUIHelper.COLOR_WHITE);

		natTable.setTheme(createTheme());

		natTable.addOverlayPainter(new NatTableBorderOverlayPainter());
		natTable.addPaintListener(new AutoResizeRowPaintListener(natTable, viewportLayer, bodyDataLayer));
	}

	private ModernNatTableThemeConfiguration createTheme() {
		ModernNatTableThemeConfiguration theme = new ModernNatTableThemeConfiguration();
		theme.defaultHAlign = HorizontalAlignmentEnum.LEFT;
		theme.defaultVAlign = VerticalAlignmentEnum.TOP;
		theme.defaultCellPainter = new PaddingDecorator(new TextPainter(true, true, true), 2);

		theme.treeCellPainter = new PaddingDecorator(new TextPainter(false, true, true), 2, 2, 2, 15);

		Bundle bundle = FrameworkUtil.getBundle(this.getClass());
		URL leafImageUrl = FileLocator.find(bundle, new Path("images/right_leaf.png"), null);

		ICellPainter treeImagePainter = new PaddingDecorator(
                new TreeImagePainter(
                        false,
                        GUIHelper.getImage("right"), //$NON-NLS-1$
                        GUIHelper.getImage("right_down"), //$NON-NLS-1$
                        GUIHelper.getImageByURL("right_leaf", leafImageUrl)),
                5, 2, 5, 2);

        IndentedTreeImagePainter treePainter = new IndentedTreeImagePainter(
                15,
                null,
                CellEdgeEnum.TOP_LEFT,
                treeImagePainter,
                false,
                2,
                false);

        theme.treeStructurePainter = new BackgroundPainter(treePainter);

        ICellPainter treeSelectionImagePainter = new PaddingDecorator(
                new TreeImagePainter(
                        false,
                        GUIHelper.getImage("right_inv"), //$NON-NLS-1$
                        GUIHelper.getImage("right_down_inv"), //$NON-NLS-1$
                        GUIHelper.getImageByURL("right_leaf", leafImageUrl)),
                5, 2, 5, 2);

        IndentedTreeImagePainter treeSelectionPainter = new IndentedTreeImagePainter(
                15,
                null,
                CellEdgeEnum.TOP_LEFT,
                treeSelectionImagePainter,
                false,
                2,
                false);

        theme.treeStructureSelectionPainter = new BackgroundPainter(treeSelectionPainter);
		return theme;
	}

	/**
	 * Public class TableItem is accessed by NatTable and contains data of one table
	 * row.
	 */
	public static class TableItem {

		private TableItem parent;
		private List<TableItem> path = new ArrayList<>();

		private String scheduler;
		private String subSchedulers;
		private String processingUnits;
		private String processes;

		public TableItem getParent() {
			return parent;
		}

		public void setParent(TableItem parent) {
			this.parent = parent;
		}

		public List<TableItem> getPath() {
			return path;
		}

		public String getScheduler() {
			return scheduler;
		}

		public void setScheduler(String scheduler) {
			this.scheduler = scheduler;
		}

		public String getSubSchedulers() {
			return subSchedulers;
		}

		public void setSubSchedulers(String subSchedulers) {
			this.subSchedulers = subSchedulers;
		}

		public String getProcessingUnits() {
			return processingUnits;
		}

		public void setProcessingUnits(String cores) {
			processingUnits = cores;
		}

		public String getProcesses() {
			return processes;
		}

		public void setProcesses(String processes) {
			this.processes = processes;
		}
	}

	private static class TableItemTreeFormat implements TreeList.Format<TableItem> {

		/**
		 * Populate path with a list describing the path from a root node to this
		 * element. Upon returning, the list must have size >= 1, where the provided
		 * element is identical to the list's last element.
		 */
		@Override
		public void getPath(List<TableItem> path, TableItem element) {
			path.addAll(element.getPath());
		}

		/**
		 * Simply always return <code>true</code>.
		 *
		 * @return <code>true</code> if this element can have child elements, or
		 *         <code>false</code> if it is always a leaf node.
		 */
		@Override
		public boolean allowsChildren(TableItem element) {
			return true;
		}

		/**
		 * Returns the comparator used to order path elements of the specified depth. If
		 * enforcing order at this level is not intended, this method should return
		 * <code>null</code>. We do a simple sorting of the scheduler names to show so
		 * the tree nodes are sorted in alphabetical order.
		 */
		@Override
		public Comparator<? super TableItem> getComparator(int depth) {
			return Comparator.comparing(i1 -> i1.scheduler);
		}
	}

}
