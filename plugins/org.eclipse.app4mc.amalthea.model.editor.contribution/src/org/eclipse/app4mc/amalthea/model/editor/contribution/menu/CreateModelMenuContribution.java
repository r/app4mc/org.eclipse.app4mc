/*********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.menu;

import java.util.List;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea.model.editor.contribution.registry.CreationServiceRegistry;
import org.eclipse.app4mc.amalthea.model.editor.contribution.registry.RegistryServiceWrapper;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.CreationService;
import org.eclipse.e4.core.di.extensions.Service;
import org.eclipse.e4.ui.di.AboutToShow;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.commands.MCommand;
import org.eclipse.e4.ui.model.application.commands.MCommandsFactory;
import org.eclipse.e4.ui.model.application.commands.MParameter;
import org.eclipse.e4.ui.model.application.ui.menu.MHandledMenuItem;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuElement;
import org.eclipse.e4.ui.model.application.ui.menu.MMenuFactory;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.jface.viewers.IStructuredSelection;

public class CreateModelMenuContribution {

	@AboutToShow
	public void aboutToShow(
			List<MMenuElement> items,
			EModelService modelService,
			MApplication app,
			@Service CreationServiceRegistry registry,
			@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection) {

		List<RegistryServiceWrapper<CreationService>> services = registry.getServices(selection.toList());

		MenuBuilder menuBuilder = new MenuBuilder();
		services.forEach(s -> {
			String path = s.getName();

			MHandledMenuItem menuItem = MMenuFactory.INSTANCE.createHandledMenuItem();
			menuItem.setTooltip(s.getDescription());
			menuItem.setElementId(s.getId());
			menuItem.setContributorURI("platform:/plugin/org.eclipse.app4mc.amalthea.model.editor.contribution");

			List<MCommand> command = modelService.findElements(
					app,
					"org.eclipse.app4mc.amalthea.model.editor.contribution.command.createmodelstructure",
					MCommand.class);
			menuItem.setCommand(command.get(0));

			MParameter parameter = MCommandsFactory.INSTANCE.createParameter();
			parameter.setName("app4mc.creator.id");
			parameter.setValue(s.getId());
			menuItem.getParameters().add(parameter);

			parameter = MCommandsFactory.INSTANCE.createParameter();
			parameter.setName("app4mc.creator.model");
			parameter.setValue(s.getType());
			menuItem.getParameters().add(parameter);

			menuBuilder.addMenuItem(path, menuItem);
		});

		menuBuilder.populateMenuElementsList(items);
	}

}
