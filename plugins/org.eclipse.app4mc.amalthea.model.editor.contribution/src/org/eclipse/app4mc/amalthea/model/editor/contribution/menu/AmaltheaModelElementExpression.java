/*********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.menu;

import java.util.List;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;

/**
 * Imperative expression implementation that checks if the current active selection contains only Amalthea model elements
 *
 */
public class AmaltheaModelElementExpression {

	public boolean evaluate(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection) {
		if (selection == null || selection.isEmpty())
			return false;

		return ((List<?>) selection.toList()).stream().allMatch(e ->
				e instanceof EObject
				&& ((EObject) e).eClass().getEPackage().equals(AmaltheaPackage.eINSTANCE));
	}

}
