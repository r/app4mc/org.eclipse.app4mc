/*********************************************************************************
 * Copyright (c) 2021-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.handler;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea.model.editor.contribution.registry.CreationServiceRegistry;
import org.eclipse.app4mc.amalthea.model.editor.contribution.registry.RegistryServiceWrapper;
import org.eclipse.app4mc.amalthea.model.editor.contribution.service.CreationService;
import org.eclipse.app4mc.amalthea.model.provider.TransientItemProvider;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.core.di.extensions.Service;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;

public class CreateModelStructureHandler {

	@Execute
	public void execute(
			Shell shell,
			@Named("app4mc.creator.model") String modelType,
			@Named("app4mc.creator.id") String creatorId,
			@Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection,
			@Service CreationServiceRegistry registry,
			IEclipseContext context) {

		RegistryServiceWrapper<CreationService> serviceWrapper = registry.getService(modelType, creatorId);

		if (serviceWrapper == null) {
			return;
		}

		List<EObject> objects = getModelObjects(selection);

		if (!objects.isEmpty()) {
			EObject rootContainer = EcoreUtil.getRootContainer(objects.get(0));

			// Use ChangeCommand and CommandStack to execute changes

			EditingDomain editingDomain = AdapterFactoryEditingDomain.getEditingDomainFor(rootContainer);
			ChangeCommandWithStatusResult<CreationService> command = new ChangeCommandWithStatusResult<>(
					rootContainer, context, modelType + " CreateModelStructure", serviceWrapper, objects);

			String[] split = serviceWrapper.getName().split("\\|");
			command.setLabel("Create " + split[split.length-1].trim());

			editingDomain.getCommandStack().execute(command);

			// Display result
			if (command.status != null) {
				MessageDialog.openInformation(shell, "AMALTHEA Structure Creation", command.status.toString());
			}
		}
	}

	private List<EObject> getModelObjects(IStructuredSelection selection) {
		if (selection == null) {
			return Collections.emptyList();
		}

		Object first = selection.getFirstElement();

		// in case of TransientItemProvider: replace with target object
		if (selection.size() == 1
				&& first instanceof TransientItemProvider
				&& ((TransientItemProvider) first).getTarget() instanceof EObject) {
			return List.of((EObject) ((TransientItemProvider) first).getTarget());
		}

		return Arrays.stream(selection.toArray())
				.filter(EObject.class::isInstance)
				.map(EObject.class::cast)
				.collect(Collectors.toList());
	}

}
