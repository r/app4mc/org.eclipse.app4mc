/*********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.registry;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.editor.contribution.service.CreationService;
import org.eclipse.app4mc.amalthea.model.provider.TransientItemProvider;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

@Component(service = CreationServiceRegistry.class)
public class CreationServiceRegistry extends ModelServiceRegistry<CreationService> {

	@Reference(
			cardinality = ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC,
			policyOption = ReferencePolicyOption.GREEDY)
	void bindCreationService(CreationService creationService, Map<String, Object> properties) {
		super.bindService(creationService, properties);
	}

	void unbindCreationService(CreationService creationService, Map<String, Object> properties) {
		super.unbindService(creationService, properties);
	}

	public List<RegistryServiceWrapper<CreationService>> getServices(Object object) {
		return getServices(Collections.singletonList(object));
	}

	@Override
	public List<RegistryServiceWrapper<CreationService>> getServices(List<?> objects) {
		// only single object selections can have creation services
		if (objects == null || objects.isEmpty() || objects.size() > 1)
			return Collections.emptyList();

		// in case of type TransientItemProvider:
		//  - replace selected object with target object
		//  - check supported features
		if (objects.get(0) instanceof TransientItemProvider) {
			TransientItemProvider provider = (TransientItemProvider) objects.get(0);

			return super.getServices(Collections.singletonList(provider.getTarget()))
					.stream()
					.filter(s -> s.getServiceInstance().modelFeature() == null
									|| provider.myFeatures().contains(s.getServiceInstance().modelFeature()))
					.collect(Collectors.toList());
		}

		return super.getServices(objects);
	}

	@Override
	public boolean hasServices(List<?> objects) {
		if (objects == null || objects.isEmpty() || objects.size() > 1)
			return false;
		
		// in case of type TransientItemProvider:
		//  - compute services with standard method
		if (objects.get(0) instanceof TransientItemProvider) {
			return !getServices(objects).isEmpty();
		}

		return super.hasServices(objects);
	}

}
