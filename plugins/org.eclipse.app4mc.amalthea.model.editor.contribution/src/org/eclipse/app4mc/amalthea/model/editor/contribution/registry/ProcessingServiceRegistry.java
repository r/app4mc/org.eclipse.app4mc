/*********************************************************************************
 * Copyright (c) 2021-2023 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.contribution.registry;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.editor.contribution.service.ProcessingService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

@Component(service = ProcessingServiceRegistry.class)
public class ProcessingServiceRegistry extends ModelServiceRegistry<ProcessingService> {

	@Reference(
			cardinality = ReferenceCardinality.MULTIPLE,
			policy = ReferencePolicy.DYNAMIC,
			policyOption = ReferencePolicyOption.GREEDY)
	void bindCreationService(ProcessingService processingService, Map<String, Object> properties) {
		super.bindService(processingService, properties);
	}

	void unbindCreationService(ProcessingService processingService, Map<String, Object> properties) {
		super.unbindService(processingService, properties);
	}

	public List<RegistryServiceWrapper<ProcessingService>> getServices(Object object) {
		return getServices(Collections.singletonList(object));
	}

	@Override
	public List<RegistryServiceWrapper<ProcessingService>> getServices(List<?> objects) {
		if (objects == null || objects.isEmpty())
			return Collections.emptyList();

		return super.getServices(objects).stream()
				.filter(s -> s.getServiceInstance().cardinality() < 0 || s.getServiceInstance().cardinality() == objects.size())
				.collect(Collectors.toList());
	}

}
