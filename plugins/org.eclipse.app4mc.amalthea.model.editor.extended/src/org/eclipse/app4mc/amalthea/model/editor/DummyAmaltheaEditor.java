/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor;

import java.util.HashMap;

import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.app4mc.amalthea.model.util.ModelUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

public class DummyAmaltheaEditor extends EditorPart {

	private static final String AMALTHEA_MODEL_EDITOR = "AMALTHEA Model Editor";

	private Label contents;

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);

		// check model version
		IFile iFile = AmaltheaEditorUtil.getIFile(input);
		boolean isValidVersion = AmaltheaEditorUtil.isNamespaceValid(iFile);

		if (!isValidVersion) {
			// open dialog (message or simple migration) and exit
			showInvalidVersionDialog(iFile);
			closeEditor();
		}

	}

	private void showInvalidVersionDialog(IFile iFile) {

		String versionFromModel = AmaltheaEditorUtil.getModelVersion(iFile);
		String versionFromMetaModel = ModelUtil.MODEL_VERSION;

		String versionMismatchMessage = NLS.bind(
				"Unsupported File ! \r\rEditor could not be opened because of a version mismatch. \r\rFound AMALTHEA model version : {0}\rCurrent editor supports only AMALTHEA model version :  {1}",
				new Object[] { versionFromModel, versionFromMetaModel });

		// *** Check if migration to current version is available ***

		MApplication application = PlatformUI.getWorkbench().getService(MApplication.class);
		if (application != null) {
			IEclipseContext context = application.getContext();
			if (context != null) {
				Object latestSupportedMigrationVersion = context.get("APP4MC_MODEL_MIGRATION_VERSION");
				if (latestSupportedMigrationVersion != null) {
					// the migration component is available in the runtime
					// other APP4MC version detected -> do not open and show migration hint
					MessageDialog dialog = new MessageDialog(
							getSite().getShell(), AMALTHEA_MODEL_EDITOR, null,
							versionMismatchMessage + "\r\r** Start AMALTHEA Model Migration utility to convert the model",
							MessageDialog.QUESTION_WITH_CANCEL, 0,
							"Migrate to " + versionFromMetaModel, "Show Migration Dialog", "Cancel");
					int result = dialog.open();
					// If user choose "Migrate to" option the simple migration is executed (return value == 0)
					// If user choose "Show Migration Dialog" the migration dialog is opened (return value == 1)
					if (result == 0 || result == 1) {
						// send an event to trigger the migration
						// the migration component has an event handler that will then trigger the migration
						IEventBroker broker = getSite().getService(IEventBroker.class);
						if (broker != null) {
							String executionContext = (result == 0) ? "simplemigration" : "dialogmigration";
							HashMap<String, String> args = new HashMap<>();
							args.put("type", executionContext);
							args.put("version", versionFromMetaModel);
							broker.send("org/eclipse/app4mc/amalthea/converter/CONVERT", args);
						}
					}
				} else {
					// migration plugins are not loaded -> show warning
					MessageDialog.openWarning(
							getSite().getShell(), AMALTHEA_MODEL_EDITOR,
							versionMismatchMessage + "\r\r** AMALTHEA Model Migration utility is not available");
				}
			}
		}
	}

	/**
	 * Closes the editor programmatically.
	 */
	private void closeEditor() {
		Display display = getSite().getShell().getDisplay();
		IWorkbenchPage page = getSite().getPage();

		if (display != null && page != null) {
			display.asyncExec(() -> page.closeEditor(this, false));
		}
	}

	@Override
	public void createPartControl(Composite parent) {
		contents = new Label(parent, SWT.NONE);
		contents.setText("Model migration required.");
	}

	@Override
	public void setFocus() {
		if (contents != null)
			contents.setFocus();
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		// do nothing
	}

	@Override
	public void doSaveAs() {
		// do nothing
	}

}
