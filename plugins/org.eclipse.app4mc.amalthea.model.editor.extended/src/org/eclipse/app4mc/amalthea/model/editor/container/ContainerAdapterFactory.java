/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.container;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.runtime.IAdapterFactory;

/**
 * Has to be registered:
 * <pre>{@code
 * <extension
 *    point="org.eclipse.core.runtime.adapters">
 *    <factory
 *       adaptableType="org.eclipse.core.resources.IContainer"
 *          class="org.eclipse.app4mc.amalthea.model.presentation.ContainerAdapterFactory">
 *       <adapter
 *          type="org.eclipse.app4mc.amalthea.model.presentation.AmaltheaModelContainer">
 *       </adapter>
 *    </factory>
 * </extension>
 * }</pre>
 */
public class ContainerAdapterFactory implements IAdapterFactory {

	private Map<IContainer, AmaltheaModelContainer> containerCache = new HashMap<>();

	@Override
	public <T> T getAdapter(Object adaptableObject, Class<T> adapterType) {
		if (adaptableObject instanceof IContainer) {
			IContainer container = (IContainer) adaptableObject;

			if (AmaltheaModelContainer.class.equals(adapterType)) {
				AmaltheaModelContainer amc = containerCache.computeIfAbsent(container, k -> new AmaltheaModelContainer(container) );
				return adapterType.cast(amc);
			}
		}

		return null;
	}

	@Override
	public Class<?>[] getAdapterList() {
		return new Class[] { AmaltheaModelContainer.class };
	}

}
