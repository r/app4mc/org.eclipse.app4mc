/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.decorator;

import java.util.Optional;

import org.eclipse.app4mc.amalthea.model.editor.container.AmaltheaModelContainer;
import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;

public class FileLoadedDecorator extends LabelProvider implements ILightweightLabelDecorator {

	private final Optional<ImageDescriptor> decoratorImage = ResourceLocator.imageDescriptorFromBundle(getClass(),
			"icons/loaded_decorator.gif");

	@Override
	public void decorate(Object element, IDecoration decoration) {

		if (element instanceof IFile) {
			IFile iFile = (IFile) element;

			AmaltheaModelContainer amContainer = AmaltheaEditorUtil.getModelContainer(iFile);

			if (amContainer != null) {
				if (amContainer.getInitialModelFiles().contains(iFile)) {
					// loaded Amalthea file: show decoration icon
					decoratorImage.ifPresent(d -> decoration.addOverlay(d, IDecoration.BOTTOM_RIGHT));
				} else if (amContainer.isInUse() && "amxmi".equals(iFile.getFileExtension())) {
					// other Amalthea file: show version info
					decoration.addSuffix(" - " + AmaltheaEditorUtil.getModelVersion(iFile));
				}
			}
		}
	}

}
