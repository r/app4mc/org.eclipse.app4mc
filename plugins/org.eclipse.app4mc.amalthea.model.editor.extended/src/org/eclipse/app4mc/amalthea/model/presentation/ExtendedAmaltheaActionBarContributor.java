/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.presentation;

import org.eclipse.app4mc.amalthea.model.editor.actions.SearchAction;
import org.eclipse.app4mc.amalthea.model.editor.actions.ShowTypesAction;
import org.eclipse.app4mc.amalthea.model.editor.actions.ValidateAction;
import org.eclipse.jface.action.IToolBarManager;

public class ExtendedAmaltheaActionBarContributor extends AmaltheaActionBarContributor {

	protected ShowTypesAction amaltheaShowTypesAction;
	protected SearchAction amaltheaSearchAction;
	protected ValidateAction amaltheaValidateAction;

	public ExtendedAmaltheaActionBarContributor() {
		super();

		amaltheaShowTypesAction = new ShowTypesAction();
		amaltheaSearchAction = new SearchAction();
		amaltheaValidateAction = new ValidateAction();
	}

	@Override
	public void activate() {
		super.activate();

		amaltheaShowTypesAction.setActiveEditor(activeEditor);
		amaltheaSearchAction.setActiveEditor(activeEditor);
		amaltheaValidateAction.setActiveEditor(activeEditor);
	}

	@Override
	public void deactivate() {
		super.deactivate();

		amaltheaShowTypesAction.setActiveEditor(null);
		amaltheaSearchAction.setActiveEditor(null);
		amaltheaValidateAction.setActiveEditor(null);
	}

	@Override
	public void contributeToToolBar(IToolBarManager toolBarManager) {
		super.contributeToToolBar(toolBarManager);

		toolBarManager.add(amaltheaShowTypesAction);
		toolBarManager.add(amaltheaSearchAction);
		toolBarManager.add(amaltheaValidateAction);
	}

	@Override
	public void dispose() {
		super.dispose();

		amaltheaShowTypesAction = null;
		amaltheaSearchAction = null;
		amaltheaValidateAction = null;
	}

}
