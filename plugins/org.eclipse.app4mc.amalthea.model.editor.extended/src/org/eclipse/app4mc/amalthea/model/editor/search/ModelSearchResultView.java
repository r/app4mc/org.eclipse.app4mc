/**
 ********************************************************************************
 * Copyright (c) 2013-2020 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.search;

import java.util.Collections;
import java.util.Optional;

import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.app4mc.amalthea.model.presentation.AmaltheaUIPlugin;
import org.eclipse.app4mc.amalthea.model.presentation.ExtendedAmaltheaEditor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.jface.viewers.DecoratingStyledCellLabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TreeNode;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.search.ui.IContextMenuConstants;
import org.eclipse.search.ui.text.AbstractTextSearchViewPage;
import org.eclipse.search.ui.text.Match;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class ModelSearchResultView extends AbstractTextSearchViewPage {

	private SearchResultContentProvider contentProvider;
	private SearchResultTreeContentProvider contentTreeProvider;
	private final ToggleGroupViewAction toggleGroupViewAction;

	public ModelSearchResultView() {
		super(FLAG_LAYOUT_FLAT + FLAG_LAYOUT_TREE);
		toggleGroupViewAction = new ToggleGroupViewAction("Group by Type"); //$NON-NLS-1$
		// image taken from
		// https://github.com/eclipse/mylyn.commons/blob/d95886e9e377efa663da5901661d20e5e0da9a49/org.eclipse.mylyn.commons.ui/icons/etool16/presentation.gif
		Optional<ImageDescriptor> imgDesc = ResourceLocator.imageDescriptorFromBundle(AmaltheaUIPlugin.getPluginId(),
				"/icons/full/obj16/presentation.gif"); //$NON-NLS-1$
		if (imgDesc.isPresent()) {
			toggleGroupViewAction.setImageDescriptor(imgDesc.get());
		}
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#elementsChanged(java.lang.Object[])
	 */
	@Override
	protected void elementsChanged(final Object[] objects) {
		if (getLayout() == FLAG_LAYOUT_FLAT) {
			if (null != contentProvider) {
				contentProvider.addElements(objects);
			}
		} else {
			if (null != contentTreeProvider) {
				contentTreeProvider.addElements(objects);
			}
		}
		getViewer().refresh();
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#getDisplayedMatches(java.lang.Object)
	 */
	@Override
	public Match[] getDisplayedMatches(final Object element) {
		if (element instanceof TreeNode) {
			return super.getDisplayedMatches(((TreeNode) element).getValue());
		}
		return super.getDisplayedMatches(element);
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#getDisplayedMatchCount(java.lang.Object)
	 */
	@Override
	public int getDisplayedMatchCount(final Object element) {
		if (element instanceof TreeNode) {
			return super.getDisplayedMatchCount(((TreeNode) element).getValue());
		}
		return super.getDisplayedMatchCount(element);
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#clear()
	 */
	@Override
	protected void clear() {
		if (getLayout() == FLAG_LAYOUT_FLAT) {
			if (null != contentProvider) {
				contentProvider.clear();
			}
		} else {
			if (null != contentTreeProvider) {
				contentTreeProvider.clear();
			}
		}
		getViewer().refresh();
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#configureTreeViewer(org.eclipse.jface.viewers.TreeViewer)
	 */
	@Override
	protected void configureTreeViewer(final TreeViewer viewer) {
		contentTreeProvider = new SearchResultTreeContentProvider();
		viewer.setContentProvider(contentTreeProvider);
		viewer.setLabelProvider(new DecoratingStyledCellLabelProvider(new SearchResultTreeLabelProvider(),
				PlatformUI.getWorkbench().getDecoratorManager().getLabelDecorator(), null));
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#configureTableViewer(org.eclipse.jface.viewers.TableViewer)
	 */
	@Override
	protected void configureTableViewer(final TableViewer viewer) {
		contentProvider = new SearchResultContentProvider();
		viewer.setContentProvider(contentProvider);
		viewer.setLabelProvider(new SearchResultLabelProvider());
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#showMatch(org.eclipse.search.ui.text.Match,
	 *      int, int, boolean)
	 */
	@Override
	protected void showMatch(final Match match, final int currentOffset, final int currentLength,
			final boolean activate) throws PartInitException {

		if (match == null || !(match.getElement() instanceof EObject)) {
			return;
		}

		final EObject eObject = (EObject) match.getElement();

		IEditorInput editorInput = null;
		if (match instanceof SearchMatch) {
			editorInput = ((SearchMatch) match).getEditorInput();
		}
		// fallback
		if (editorInput == null) {
			editorInput = AmaltheaEditorUtil.getIEditorInput(eObject);
		}

		// open ExtendedAmaltheaEditor
		final IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		final IEditorPart editorPart = page.openEditor(editorInput, "org.eclipse.app4mc.amalthea.model.editor.extended", true);

		// show selection
		if (editorPart instanceof ExtendedAmaltheaEditor) {
			((ExtendedAmaltheaEditor) editorPart).setSelectionToViewer(Collections.singleton(eObject));
		}
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#setLayout(int)
	 */
	@Override
	public void setLayout(int layout) {
		if (!isLayoutSupported(layout) || (getLayout() == layout))
			return;

		getViewer().setSelection(StructuredSelection.EMPTY, false);

		super.setLayout(layout);
	}

	/**
	 * @see org.eclipse.search.ui.text.AbstractTextSearchViewPage#fillToolbar(org.eclipse.jface.action.IToolBarManager)
	 */
	@Override
	protected void fillToolbar(final IToolBarManager tbm) {
		super.fillToolbar(tbm);

		tbm.appendToGroup(IContextMenuConstants.GROUP_VIEWER_SETUP, toggleGroupViewAction);
	}

	private class ToggleGroupViewAction extends Action {

		public ToggleGroupViewAction(final String text) {
			super(text, IAction.AS_CHECK_BOX);
			setChecked(getLayout() == FLAG_LAYOUT_TREE);
			setToolTipText(text);
		}

		/**
		 * @see org.eclipse.jface.action.Action#run()
		 */
		@Override
		public void run() {
			if (isChecked()) {
				setLayout(FLAG_LAYOUT_TREE);
			} else {
				setLayout(FLAG_LAYOUT_FLAT);
			}

			getViewer().refresh();
		}
	}

}
