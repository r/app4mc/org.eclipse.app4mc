/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor;

import java.util.List;
import java.util.stream.Stream;

import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IEditorAssociationOverride;

public class AmaltheaEditorAssociationOverride implements IEditorAssociationOverride {

	private static final String AMALTHEA_CONTENT_TYPE = "org.eclipse.app4mc.amalthea.xmi.contentType";
	private static final String AMALTHEA_EDITOR_DUMMY = "org.eclipse.app4mc.amalthea.model.editor.dummy";
	private static final String AMALTHEA_EDITOR_EXTENDED = "org.eclipse.app4mc.amalthea.model.editor.extended";

	private static final List<String> AMALTHEA_EDITORS = List.of(
			AMALTHEA_EDITOR_EXTENDED,
			AMALTHEA_EDITOR_DUMMY);

	private static final List<String> REJECTED_EDITORS = List.of(
			"org.eclipse.emf.ecore.presentation.ReflectiveEditorID",
			"org.eclipse.xtext.builder.trace.TraceEditorID");

	/**
	 * Filter some other editors (for example: Sample Refective Ecore Editor)
	 * and keep only valid Amalthea editors (depending on model version)
	 */
	@Override
	public IEditorDescriptor[] overrideEditors(IEditorInput editorInput, IContentType contentType,
			IEditorDescriptor[] editorDescriptors) {
		// Only override editors for Amalthea content type
		if (!isAmaltheaType(contentType)) {
			return editorDescriptors;
		}

		return Stream.of(editorDescriptors)
				.filter(this::isValidEditor)
				.filter(desc -> isValidAmaltheaEditor(editorInput, desc))
				.toArray(IEditorDescriptor[]::new);
	}

	/**
	 * Filter some other editors (for example: Sample Refective Ecore Editor)
	 */
	@Override
	public IEditorDescriptor[] overrideEditors(String fileName, IContentType contentType,
			IEditorDescriptor[] editorDescriptors) {
		// Only override editors for Amalthea content type
		if (!isAmaltheaType(contentType)) {
			return editorDescriptors;
		}

		return Stream.of(editorDescriptors)
				.filter(this::isValidEditor)
				.toArray(IEditorDescriptor[]::new);
	}

	/**
	 * Use the valid Amalthea editor (depending on model version)
	 */
	@Override
	public IEditorDescriptor overrideDefaultEditor(IEditorInput editorInput, IContentType contentType,
			IEditorDescriptor editorDescriptor) {
		// Only override editors for Amalthea content type
		if (!isAmaltheaType(contentType)) {
			return editorDescriptor;
		}

		return overrideEditorDescriptor(editorInput, editorDescriptor);
	}

	/**
	 * Standard implementation (no modifications)
	 */
	@Override
	public IEditorDescriptor overrideDefaultEditor(String fileName, IContentType contentType,
			IEditorDescriptor editorDescriptor) {
		return editorDescriptor;
	}

	// ---------- internal ----------

	private boolean isAmaltheaType(IContentType contentType) {
		return contentType != null && AMALTHEA_CONTENT_TYPE.equals(contentType.getId());
	}

	private boolean isValidEditor(IEditorDescriptor editorDescriptor) {
		if (editorDescriptor == null)
			return false;

		// filter some generic XMI editors
		return !REJECTED_EDITORS.contains(editorDescriptor.getId());
	}

	private boolean isValidAmaltheaEditor(IEditorInput editorInput, IEditorDescriptor editorDescriptor) {
		if (editorDescriptor == null)
			return false;

		// check model version
		IFile iFile = AmaltheaEditorUtil.getIFile(editorInput);
		boolean isCurrentVersion = AmaltheaEditorUtil.isNamespaceValid(iFile);

		// Amalthea dummy editor is valid for older model versions
		if (AMALTHEA_EDITOR_DUMMY.equals(editorDescriptor.getId()))
			return !isCurrentVersion;

		// Amalthea editor is valid for current model version
		if (AMALTHEA_EDITOR_EXTENDED.equals(editorDescriptor.getId())) {
			return isCurrentVersion;
		}

		// do not filter other editors
		return true;
	}

	private IEditorDescriptor overrideEditorDescriptor(IEditorInput editorInput, IEditorDescriptor editorDescriptor) {
		// only check and override Amalthea editors
		if (editorDescriptor == null || !AMALTHEA_EDITORS.contains(editorDescriptor.getId()))
			return editorDescriptor;

		// check model version
		IFile iFile = AmaltheaEditorUtil.getIFile(editorInput);
		boolean isCurrentVersion = AmaltheaEditorUtil.isNamespaceValid(iFile);

		if (isCurrentVersion) {
			// return the standard Amalthea editor
			return PlatformUI.getWorkbench().getEditorRegistry().findEditor(AMALTHEA_EDITOR_EXTENDED);
		} else {
			// return dummy editor (that triggers migration dialog)
			return PlatformUI.getWorkbench().getEditorRegistry().findEditor(AMALTHEA_EDITOR_DUMMY);
		}
	}

}
