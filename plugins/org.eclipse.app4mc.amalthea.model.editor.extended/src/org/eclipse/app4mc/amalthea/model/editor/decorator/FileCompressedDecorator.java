/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.decorator;

import java.util.Optional;

import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaFileHelper;
import org.eclipse.core.resources.IFile;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;

public class FileCompressedDecorator extends LabelProvider implements ILightweightLabelDecorator {

	private final Optional<ImageDescriptor> decoratorImage = ResourceLocator.imageDescriptorFromBundle(getClass(),
			"icons/compressed_decorator.gif");

	@Override
	public void decorate(Object element, IDecoration decoration) {

		if (element instanceof IFile) {
			IFile iFile = (IFile) element;

			if (AmaltheaFileHelper.isModelFileExtension(iFile.getFileExtension())
					&& AmaltheaEditorUtil.isZipFile(iFile)) {

				// compressed Amalthea file: show decoration icon
				decoratorImage.ifPresent(d -> decoration.addOverlay(d, IDecoration.TOP_RIGHT));
			}
		}
	}

}
