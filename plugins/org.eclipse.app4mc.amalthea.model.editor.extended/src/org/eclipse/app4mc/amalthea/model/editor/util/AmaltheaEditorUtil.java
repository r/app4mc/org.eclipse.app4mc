/**
 ********************************************************************************
 * Copyright (c) 2021-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.editor.container.AmaltheaModelContainer;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaFileHelper;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.part.FileEditorInput;

public class AmaltheaEditorUtil {

	public static final String INVALID = "invalid";
	public static final String MODEL_FILE_EXTENSION = "amxmi";

	// Suppress default constructor
	private AmaltheaEditorUtil() {
		throw new IllegalStateException("Utility class");
	}

	public static IFile getIFile(EObject eObject) {
		if (eObject == null)
			return null;

		return getIFile(eObject.eResource());
	}

	public static IFile getIFile(Resource resource) {
		if (resource == null || resource.getResourceSet() == null)
			return null;

		// see https://www.eclipse.org/forums/index.php/t/128695/

		URI uri = resource.getURI();
		uri = resource.getResourceSet().getURIConverter().normalize(uri);

		if (uri.segmentCount() < 2 || !"platform".equals(uri.scheme()) || !"resource".equals(uri.segment(0)))
			return null;

		StringBuilder platformResourcePath = new StringBuilder();
		for (int j = 1, size = uri.segmentCount(); j < size; ++j) {
			platformResourcePath.append('/');
			platformResourcePath.append(uri.segment(j));
		}
		return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformResourcePath.toString()));
	}

	public static IFile getIFile(IEditorInput editorInput) {
		if (editorInput == null)
			return null;

		// get file
		return editorInput.getAdapter(IFile.class);
	}

	public static IContainer getIContainer(IEditorInput editorInput) {
		if (editorInput == null)
			return null;

		// get file resource
		IResource iResource = editorInput.getAdapter(IResource.class);
		if (iResource == null)
			return null;

		// get container
		return iResource.getParent();
	}

	public static AmaltheaModelContainer getModelContainer(IEditorInput editorInput) {
		return getModelContainer(getIContainer(editorInput));
	}

	public static AmaltheaModelContainer getModelContainer(IFile iFile) {
		if (iFile == null || (! iFile.exists()) || iFile.getParent() == null)
			return null;

		return iFile.getParent().getAdapter(AmaltheaModelContainer.class);
	}

	public static AmaltheaModelContainer getModelContainer(IContainer container) {
		if (container == null)
			return null;

		return container.getAdapter(AmaltheaModelContainer.class);
	}

	public static Resource getResource(IEditorInput editorInput) {
		if (editorInput == null)
			return null;

		AmaltheaModelContainer container = getModelContainer(editorInput);
		URI uri = EditUIUtil.getURI(editorInput);
		if (container == null || container.getResourceSet() == null || uri == null)
			return null;

		return container.getResourceSet().getResource(uri, false);
	}

	public static Amalthea getModel(IEditorInput editorInput) {
		return getModel(getResource(editorInput));
	}

	public static Amalthea getModel(Resource resource) {
		if (resource == null || resource.getContents().isEmpty())
			return null;

		EObject eObject = resource.getContents().get(0);
		if (!(eObject instanceof Amalthea))
			return null;

		return (Amalthea) eObject;
	}

	public static List<Amalthea> getModels(ResourceSet resourceSet) {
		List<Amalthea> models = new ArrayList<>();

		if (resourceSet == null)
			return models;

		for (Resource resource :  resourceSet.getResources()) {
			Amalthea amalthea = getModel(resource);
			if (amalthea != null) {
				models.add(amalthea);
			}
		}

		return models;
	}

	public static List<IFile> getValidModelFiles(IContainer container) {
		List<IFile> files = new ArrayList<>();

		if (container == null || !container.isAccessible())
			return files;

		IResource[] members;
		try {
			members = container.members();
		} catch (CoreException e1) {
			return files;
		}

		return Arrays.stream(members)
				.filter(IFile.class::isInstance)
				.map(IFile.class::cast)
				.filter(AmaltheaEditorUtil::hasModelExtension)
				.filter(AmaltheaEditorUtil::isNamespaceValid)
				.collect(Collectors.toList());
	}

	public static File getLocalFile(final IResource resource) {
		if (resource instanceof IFile && resource.exists()) {
			IPath location = resource.getLocation();
			if (location != null) {
				return location.toFile();
			}
		}

		return null;
	}

	public static boolean isLocalFile(final IResource resource) {
		return resource instanceof IFile && resource.exists() && resource.getLocation() != null;
	}
	
	public static boolean hasModelExtension(final IResource resource) {
		return resource instanceof IFile && MODEL_FILE_EXTENSION.equals(resource.getFileExtension());
	}

	public static boolean isRegularFile(final IFile iFile) {
		return isLocalFile(iFile) && !isZipFile(getLocalFile(iFile));
	}
	
	public static boolean isZipFile(final IFile iFile) {
		return isLocalFile(iFile) && isZipFile(getLocalFile(iFile));
	}

	private static boolean isZipFile(File file) {
		boolean result = false;
		if (file != null) {
			try (ZipFile f = new ZipFile(file)) {
				// zipped file detected
				result = true;
			} catch (IOException e) {
				// IOException includes ZipException -> not a zip file
			}
		}

		return result;
	}

	public static boolean isNamespaceValid(IFile iFile) {
		return isLocalFile(iFile) && AmaltheaFileHelper.isNamespaceValid(getLocalFile(iFile));
	}

	public static String getModelVersion(IFile iFile) {
		if (isLocalFile(iFile)) {
			return AmaltheaFileHelper.getModelVersion(getLocalFile(iFile));
		}

		return INVALID;
	}

	public static IEditorInput getIEditorInput(EObject eObject) {
		// check parameters
		if (eObject == null || eObject.eResource() == null || eObject.eResource().getURI() == null) {
			return null;
		}

		URI uri = eObject.eResource().getURI();

		// try to create FileEditorInput
		if (uri.isPlatformResource()) {
			String path = uri.toPlatformString(true);
			IResource workspaceResource = ResourcesPlugin.getWorkspace().getRoot().findMember(new Path(path));
			if (workspaceResource instanceof IFile) {
				 return new FileEditorInput((IFile) workspaceResource);
			}
		}

		// fallback: create URIEditorInput
		return new URIEditorInput(uri);
	}

	public static boolean isInUse(IFile iFile) {
		AmaltheaModelContainer amContainer = getModelContainer(iFile);
		return amContainer != null && amContainer.getInitialModelFiles().contains(iFile);
	}

	public static boolean isInUse(IContainer iContainer) {
		AmaltheaModelContainer modelContainer = getModelContainer(iContainer);
		return modelContainer != null && modelContainer.isInUse();
	}

}
