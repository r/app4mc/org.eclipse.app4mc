/**
 ********************************************************************************
 * Copyright (c) 2021-2022 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.handler;

import javax.inject.Named;

import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.core.resources.IFile;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;

public class CompressedExpression {

	@Evaluate
	public boolean evaluate(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection) {
		if (selection != null && selection.size() == 1 && selection.getFirstElement() instanceof IFile) {
			IFile iFile = (IFile) selection.getFirstElement();

			return AmaltheaEditorUtil.hasModelExtension(iFile) && AmaltheaEditorUtil.isZipFile(iFile);
		}
		return false;
	}

}
