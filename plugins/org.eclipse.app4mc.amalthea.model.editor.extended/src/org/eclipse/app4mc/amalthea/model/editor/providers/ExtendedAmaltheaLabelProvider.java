/**
 * *******************************************************************************
 *  Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 *  This program and the accompanying materials are made
 *  available under the terms of the Eclipse Public License 2.0
 *  which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 *  SPDX-License-Identifier: EPL-2.0
 *
 * *******************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.providers;

import org.apache.commons.lang.StringUtils;
import org.eclipse.app4mc.amalthea.model.presentation.ExtendedAmaltheaEditor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.DecoratingColumLabelProvider;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.IStructuredSelection;

public class ExtendedAmaltheaLabelProvider extends DecoratingColumLabelProvider {

	private ExtendedAmaltheaEditor editor = null;

	public ExtendedAmaltheaLabelProvider(ILabelProvider labelProvider, ILabelDecorator labelDecorator) {
		super(labelProvider, labelDecorator);
	}

	public ExtendedAmaltheaLabelProvider(ILabelProvider labelProvider, ILabelDecorator labelDecorator, ExtendedAmaltheaEditor editor) {
		super(labelProvider, labelDecorator);
		this.editor = editor;
	}

	@Override
	public String getText(Object element) {
		final String text = super.getText(element);
		if (StringUtils.isBlank(text)) {
			return "???";
		}

		final String type = getTypeName(element);
		if (StringUtils.isBlank(type)) {
			return text;
		}

		final String newText = stripTypePrefix(text, type);
		if (editor != null && editor.isShowTypesEnabled()) {
			return newText + " [" + type + "]";
		} else {
			return newText;
		}
	}

	private String getTypeName(Object element) {
		Object target = element;
		if (element instanceof IStructuredSelection) {
			target = ((IStructuredSelection) element).getFirstElement();
		}

		return (target instanceof EObject) ? ((EObject) target).eClass().getName() : null;
	}

	private String stripTypePrefix(final String text, final String type) {
		final String typeWithBlanks = StringUtils.join(StringUtils.splitByCharacterTypeCamelCase(type), ' ');
		return StringUtils.removeStart(text.trim(), typeWithBlanks + " ").trim();
	}

	@Override
	public void dispose() {
		super.dispose();
		editor = null;
	}

}
