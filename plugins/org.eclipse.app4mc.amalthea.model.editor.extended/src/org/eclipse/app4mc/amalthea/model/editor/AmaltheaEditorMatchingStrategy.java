/**
 ********************************************************************************
 * Copyright (c) 2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor;

import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorMatchingStrategy;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PartInitException;

/**
 * An editor matching strategy allows editor extensions to provide their own
 * algorithm for matching the input of an open editor of that type to a given
 * editor input.
 *
 * This class matches only ExtendedAmaltheaEditors.
 */
public class AmaltheaEditorMatchingStrategy implements IEditorMatchingStrategy {

	private static final String AMALTHEA_EDITOR_EXTENDED = "org.eclipse.app4mc.amalthea.model.editor.extended";

	/**
	 * Returns true if one of the following conditions is true:
	 * <ul>
	 * <li>EditorInputs are equal</li>
	 * <li>Resources are equal</li>
	 * </ul>
	 */
	@Override
	public boolean matches(IEditorReference editorRef, IEditorInput input) {
		if (editorRef == null || input == null)
			return false;

		String editorId = editorRef.getId();
		if (!AMALTHEA_EDITOR_EXTENDED.equals(editorId))
			return false;

		IEditorInput existingEditorInput;
		try {
			existingEditorInput = editorRef.getEditorInput();
		} catch (PartInitException e) {
			return false;
		}

		// If the ExistingEditorInput is same as the passed input,
		// return true
		if (existingEditorInput.equals(input))
			return true;

		Resource inputResource = AmaltheaEditorUtil.getResource(input);
		Resource editorResource = AmaltheaEditorUtil.getResource(existingEditorInput);

		// If the editor resource is same as the passed input resource,
		// return true
		return inputResource != null && editorResource != null && editorResource.equals(inputResource);
	}

}
