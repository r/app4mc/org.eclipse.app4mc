/**
 ********************************************************************************
 * Copyright (c) 2013-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.actions;

import org.eclipse.app4mc.amalthea.model.editor.messages.Messages;
import org.eclipse.app4mc.amalthea.model.presentation.AmaltheaUIPlugin;
import org.eclipse.app4mc.amalthea.model.presentation.ExtendedAmaltheaEditor;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.ui.IEditorPart;

public class ShowTypesAction extends Action {

	private IEditorPart editorPart;

	public ShowTypesAction() {
		super(Messages.ShowTypesAction_title, AS_CHECK_BOX);

		setImageDescriptor(ResourceLocator
				.imageDescriptorFromBundle(AmaltheaUIPlugin.getPluginId(), "/icons/full/obj16/showtypes.png")
				.orElse(null));
		setToolTipText(Messages.ShowTypesAction_tooltip);
		
		InstanceScope.INSTANCE.getNode("org.eclipse.app4mc.platform.ide").addPreferenceChangeListener(event -> {
			if ("app4mc.showTypes".equals(event.getKey())) {
				setChecked(!isChecked());
				run();
			}
		});
	}

	public void setActiveEditor(IEditorPart newEditor) {
		editorPart = newEditor;

		if (editorPart instanceof ExtendedAmaltheaEditor) {
			// set button state according to editor
			setChecked(((ExtendedAmaltheaEditor) editorPart).isShowTypesEnabled());
		}
	}

	/**
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {

		if (editorPart instanceof ExtendedAmaltheaEditor) {
			ExtendedAmaltheaEditor editor = (ExtendedAmaltheaEditor) editorPart;

			editor.enableShowTypes(isChecked()); // toggle

			editor.getViewer().refresh();
		}
	}

}
