/**
 ********************************************************************************
 * Copyright (c) 2013-2021 Robert Bosch GmbH and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.amalthea.model.editor.actions;

import java.util.regex.Pattern;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.editor.messages.Messages;
import org.eclipse.app4mc.amalthea.model.editor.search.ModelHitCollector;
import org.eclipse.app4mc.amalthea.model.editor.search.handlers.SearchDialog;
import org.eclipse.app4mc.amalthea.model.editor.search.handlers.SearchDialogSettings;
import org.eclipse.app4mc.amalthea.model.editor.util.AmaltheaEditorUtil;
import org.eclipse.app4mc.amalthea.model.presentation.AmaltheaUIPlugin;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.resource.ResourceLocator;
import org.eclipse.jface.window.Window;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class SearchAction extends Action {

	private IEditorPart editorPart;

	public SearchAction() {
		super(Messages.SearchAction_title, AS_PUSH_BUTTON);

		// image taken from
		// https://github.com/eclipse/mylyn.commons/blob/d95886e9e377efa663da5901661d20e5e0da9a49/org.eclipse.mylyn.commons.ui/icons/etool16/find.gif
		setImageDescriptor(ResourceLocator
				.imageDescriptorFromBundle(AmaltheaUIPlugin.getPluginId(), "/icons/full/obj16/find.gif").orElse(null)); //$NON-NLS-1$
		setToolTipText(Messages.SearchGUI_title);
	}

	public void setActiveEditor(IEditorPart editor) {
		editorPart = editor;
	}

	/**
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		final SearchDialogSettings settings = new SearchDialogSettings();

		// retrieve stored settings
		final IDialogSettings store = AmaltheaUIPlugin.getDefault().getDialogSettings();
		settings.loadFrom(store);

		final SearchDialog dialog = new SearchDialog(window.getShell(), settings);
		int returnCode = dialog.open();

		if (returnCode == Window.OK) {
			final Pattern searchPattern = settings.computeSearchPattern();
			if (searchPattern == null)
				return;

			// store settings
			settings.saveTo(store);

			// try to get current open model
			Amalthea model = AmaltheaEditorUtil.getModel(editorPart.getEditorInput());
			if (model == null)
				return;

			final ModelHitCollector query = new ModelHitCollector(searchPattern, settings.computeFilterClass(),
					settings.isSearchRestrictedToFile(), model, editorPart.getEditorInput());

			NewSearchUI.runQueryInBackground(query);
		}
	}
}
