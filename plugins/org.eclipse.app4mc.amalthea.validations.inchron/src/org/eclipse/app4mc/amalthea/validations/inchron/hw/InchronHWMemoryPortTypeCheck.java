/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.hw;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.HwPort;
import org.eclipse.app4mc.amalthea.model.Memory;
import org.eclipse.app4mc.amalthea.model.PortType;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(id = "Inchron-HW-Memory-PortTypeResponder",
			checks = {"HW ports of memory should be of type responder" })

public class InchronHWMemoryPortTypeCheck extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getMemory();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof Memory) {
			Memory mem = (Memory) eObject;
			EList<HwPort> listPorts = mem.getPorts();
			for (HwPort port : listPorts) {
				if (port.getPortType() != PortType.RESPONDER) {
					addIssue(results, mem, ePackage.getHwModule_Ports(),
							"Memory " + name(mem) + " has a HW port " + name(port) + " of type not equal to RESPONDER");
				}
			}
		}
	}

}
