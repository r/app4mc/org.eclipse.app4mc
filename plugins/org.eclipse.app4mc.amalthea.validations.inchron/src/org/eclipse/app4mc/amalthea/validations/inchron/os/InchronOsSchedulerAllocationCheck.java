/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.os;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.HwStructure;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;



@Validation(id = "Inchron-OS-Scheduler-Allocation-DifferentCPU",
			checks = {"OS Task scheduler should not be allocated to more "
					+ "than one HwStructure" })

public class InchronOsSchedulerAllocationCheck extends AmaltheaValidation {
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getScheduler();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof Scheduler) {
			Scheduler sched = (Scheduler) eObject;
			Set<SchedulerAllocation> schedAllocations = AmaltheaIndex.getReferringObjects(sched,
					SchedulerAllocation.class);
			Set<ProcessingUnit> setPU = new HashSet<>();

			for (SchedulerAllocation schedAllocation : schedAllocations) {
				EList<ProcessingUnit> listPU = schedAllocation.getResponsibility();
				if (schedAllocation.getExecutingPU() != null) {
					listPU.add(schedAllocation.getExecutingPU());
				}
				setPU.addAll(listPU);
			}

			Set<TaskAllocation> taskAllocations = AmaltheaIndex.getReferringObjects(sched, TaskAllocation.class);

			for (TaskAllocation taskAllocation : taskAllocations) {
				EList<ProcessingUnit> listPU = taskAllocation.getAffinity();
				setPU.addAll(listPU);
			}

			Set<HwStructure> setHWStruct = new HashSet<>();

			for (ProcessingUnit PU : setPU) {
				if (PU.eContainer() instanceof HwStructure) {
					HwStructure hwStruct = (HwStructure) PU.eContainer();
					setHWStruct.add(hwStruct);
				}
			}

			ArrayList<String> ar = new ArrayList<>();
			if (setHWStruct.size() > 1) {
				for (HwStructure hwStruct : setHWStruct) {
					if (hwStruct.getName() != null) {
						ar.add(hwStruct.getName());
					}
				}
				Collections.sort(ar);
				String str = String.join(",", ar);
				addIssue(results, sched, ePackage.getScheduler_SchedulerAllocations(),
						"Task Scheduler " + name(sched) + " allocated to more than one HwStructure: " + str);
			}
		}
	}

}
