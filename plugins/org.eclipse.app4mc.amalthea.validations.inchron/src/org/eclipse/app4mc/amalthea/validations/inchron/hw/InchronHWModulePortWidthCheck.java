/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.hw;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.HwModule;
import org.eclipse.app4mc.amalthea.model.HwPort;
import org.eclipse.app4mc.amalthea.model.PortInterface;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(id = "Inchron-HWModule-InconsistentPortWidths",
			checks = { "HW Module cannot have ports with unequal bitwidth"})

public class InchronHWModulePortWidthCheck extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getHwModule();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof HwModule) {
			HwModule module = (HwModule) eObject;
			EList<HwPort> portList = module.getPorts();
			Set<Integer> portSet = new HashSet<>();

			for (HwPort port : portList) {
				if (port.getPortInterface() == PortInterface.CUSTOM || port.getPortInterface() == PortInterface.AHB
						|| port.getPortInterface() == PortInterface.AXI
						|| port.getPortInterface() == PortInterface.APB) {
					portSet.add(port.getBitWidth());
				}
			}

			if (portSet.size() > 1) {
				addIssue(results, module, ePackage.getHwModule_Ports(),
						"HW Module " + name(module) + " has custom/AHB/APB/AXI ports with unequal bitwidths");
			}
		}
	}

}
