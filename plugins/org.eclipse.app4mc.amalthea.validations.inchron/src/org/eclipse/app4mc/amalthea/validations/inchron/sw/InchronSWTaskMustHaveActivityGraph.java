/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.sw;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(id = "Inchron-SW-Task-MustHaveActivityGraph",
			checks = { "Task must have atleast one ActivityGraph"})

public class InchronSWTaskMustHaveActivityGraph extends AmaltheaValidation  {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getTask();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {

		if (eObject instanceof Task) {
			Task task = (Task) eObject;
			if (task.getActivityGraph() == null) {
				addIssue(results, task, null, "Task " + name(task) + " has no ActivityGraph");
			}
		}
	}

}