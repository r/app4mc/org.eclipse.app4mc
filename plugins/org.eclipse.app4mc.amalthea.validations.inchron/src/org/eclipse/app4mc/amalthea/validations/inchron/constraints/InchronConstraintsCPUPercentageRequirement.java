/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.constraints;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.CPUPercentageRequirementLimit;
import org.eclipse.app4mc.amalthea.model.Requirement;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(
		id = "Inchron-Constraints-LoadRequirementMissingResource",
		checks = { "CPU load requirement must have hardware context" })

public class InchronConstraintsCPUPercentageRequirement extends AmaltheaValidation {
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getCPUPercentageRequirementLimit();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof CPUPercentageRequirementLimit) {
			CPUPercentageRequirementLimit loadReq = (CPUPercentageRequirementLimit) eObject;
			EObject objReq = loadReq.eContainer();
			String str = "";
			if (objReq instanceof Requirement) {
				Requirement req = (Requirement) loadReq.eContainer();
				str = name(req);
			}
			if (loadReq.getHardwareContext() == null) {
				addIssue(results, loadReq, null,
						"CPU Percentage Requirement Limit of Requirement " + str + " is missing Hardware context");
			}
		}
	}

}