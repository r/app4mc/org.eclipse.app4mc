/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.sw;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.Scheduler;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(id = "Inchron-SW-Task-NotAllocated-DifferentSchedulers",
			checks = { "Task cannot be scheduled by more than one OS"})

public class InchronSWInvalidTaskAllocation extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getTask();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {

		if (eObject instanceof Task) {
			Task task = (Task) eObject;
			Set<TaskAllocation> taskAllocations = AmaltheaIndex.getReferringObjects(task, TaskAllocation.class);
			Set<OperatingSystem> osSet = new HashSet<>();
			for (TaskAllocation taskAllocation : taskAllocations) {
				Scheduler sched = taskAllocation.getScheduler();
				if (sched != null) {
					if (sched.eContainer() instanceof OperatingSystem) {
						OperatingSystem os = (OperatingSystem) sched.eContainer();
						osSet.add(os);
					}
				}
			}

			ArrayList<String> ar = new ArrayList<>();
			if (osSet.size() > 1) {
				for (OperatingSystem os2 : osSet) {
					if (os2.getName() != null) {
						ar.add(os2.getName());
					}
				}
				Collections.sort(ar);
				String str = String.join(",", ar);
				addIssue(results, task, null,
						"Task " + name(task) + " is scheduled by more than one operating system: " + str);
			}
		}
	}

}