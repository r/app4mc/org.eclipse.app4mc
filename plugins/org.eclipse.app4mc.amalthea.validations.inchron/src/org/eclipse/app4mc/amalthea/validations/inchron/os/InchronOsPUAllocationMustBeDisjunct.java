/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.os;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.OSModel;
import org.eclipse.app4mc.amalthea.model.OperatingSystem;
import org.eclipse.app4mc.amalthea.model.ProcessingUnit;
import org.eclipse.app4mc.amalthea.model.SchedulerAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;



@Validation(id = "Inchron-OS-PU-Allocation-MustBeDisjunct",
			checks = { "OS Scheduler to core mapping must be distinct"})

public class InchronOsPUAllocationMustBeDisjunct extends AmaltheaValidation {
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getOperatingSystem();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof OperatingSystem) {
			OperatingSystem os = (OperatingSystem) eObject;
			EObject obj = os.eContainer();
			Set<ProcessingUnit> setPUOtherOS = new HashSet<>();
			if (obj instanceof OSModel) {
				OSModel osMod = (OSModel) obj;
				EList<OperatingSystem> listOS = osMod.getOperatingSystems();
				for (OperatingSystem osEntry : listOS) {
					if (!osEntry.equals(os)) {
						EList<TaskScheduler> listSched = osEntry.getTaskSchedulers();
						for (TaskScheduler sched : listSched) {
							EList<SchedulerAllocation> listSchedAlloc = sched.getSchedulerAllocations();
							for (SchedulerAllocation schedAlloc : listSchedAlloc) {
								EList<ProcessingUnit> listPU = schedAlloc.getResponsibility();
								setPUOtherOS.addAll(listPU);
							}
						}
					}
				}
			}

			Set<ProcessingUnit> setPU = new HashSet<>();
			EList<TaskScheduler> listSched = os.getTaskSchedulers();
			for (TaskScheduler sched : listSched) {
				EList<SchedulerAllocation> listSchedAlloc = sched.getSchedulerAllocations();
				for (SchedulerAllocation schedAlloc : listSchedAlloc) {
					EList<ProcessingUnit> listPU = schedAlloc.getResponsibility();
					setPU.addAll(listPU);
				}
			}

			boolean checkAllocation = false;
			ArrayList<String> ar = new ArrayList<>();
			for (ProcessingUnit core : setPU) {
				if (setPUOtherOS.contains(core)) {
					checkAllocation = true;
					ar.add(core.getName());
				}
			}

			if (checkAllocation) {
				Collections.sort(ar);
				String str = String.join(",", ar);
				addIssue(results, os, null,
						"Operating system " + name(os) + " consists of task schedulers allocated to processing units: "
								+ str + " referenced by other operating systems");
			}
		}
	}

}
