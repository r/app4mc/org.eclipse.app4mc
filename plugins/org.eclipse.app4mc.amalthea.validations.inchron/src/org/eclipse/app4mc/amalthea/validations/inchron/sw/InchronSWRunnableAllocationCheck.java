/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.sw;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.RunnableAllocation;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(id = "Inchron-SW-RunnableAllocation-Present",
			checks = { "Runnable allocation is not supported"})

public class InchronSWRunnableAllocationCheck extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getRunnableAllocation();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof RunnableAllocation) {
			RunnableAllocation runAlloc = (RunnableAllocation) eObject;
			addIssue(results, runAlloc, null,
					"RunnableAllocation of runnable " + name(runAlloc.getEntity()) + " is not supported");
		}
	}

}