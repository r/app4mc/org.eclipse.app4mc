/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron;

import org.eclipse.app4mc.amalthea.validations.inchron.stimuli.InchronStimuliTypeCheck;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "Stimuli Validations (INCHRON)")

@ValidationGroup(
		severity = Severity.ERROR,
		validations =  {
				InchronStimuliTypeCheck.class
		}
	)

public class InchronStimuliProfile  implements IProfile {
	// Do nothing
}
