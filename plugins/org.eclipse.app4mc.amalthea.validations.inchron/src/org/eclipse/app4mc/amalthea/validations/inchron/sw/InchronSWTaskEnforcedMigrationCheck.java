/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.sw;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.AmaltheaIndex;
import org.eclipse.app4mc.amalthea.model.EnforcedMigration;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.TaskAllocation;
import org.eclipse.app4mc.amalthea.model.TaskScheduler;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(id = "Inchron-SW-Task-EnforcedMigrationCheck",
			checks = { "Invalid Enforced Migration of a task to a task Scheduler"})

public class InchronSWTaskEnforcedMigrationCheck extends AmaltheaValidation {
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getTask();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof Task) {
			Task task = (Task) eObject;
			Set<TaskScheduler> setScheduler = new HashSet<>();
			if (task.getActivityGraph() == null) {
				return;
			}
			EList<EObject> listItems = task.getActivityGraph().eContents();
			for (EObject item : listItems) {
				if (item instanceof EnforcedMigration) {
					EnforcedMigration emItem = (EnforcedMigration) item;
					TaskScheduler sched = emItem.getResourceOwner();
					if (sched != null) {
						setScheduler.add(emItem.getResourceOwner());
					}
				}
			}

			Set<TaskAllocation> taskAllocations = AmaltheaIndex.getReferringObjects(task, TaskAllocation.class);
			Set<TaskScheduler> setSchedulerAssigned = new HashSet<>();
			for (TaskAllocation taskAllocation : taskAllocations) {
				TaskScheduler sched = taskAllocation.getScheduler();
				if (sched != null) {
					setSchedulerAssigned.add(sched);
				}
			}

			for (TaskScheduler sched : setScheduler) {
				if (!setSchedulerAssigned.contains(sched)) {
					addIssue(results, task, null, "Enforced Migration of task " + name(task) + " to Task Scheduler: "
							+ name(sched) + " that is not part of the list of schedulers allocated to the task");
				}
			}
		}
	}

}