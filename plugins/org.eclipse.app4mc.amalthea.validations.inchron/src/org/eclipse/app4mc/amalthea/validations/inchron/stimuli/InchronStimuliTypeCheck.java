/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.stimuli;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.ArrivalCurveStimulus;
import org.eclipse.app4mc.amalthea.model.CustomStimulus;
import org.eclipse.app4mc.amalthea.model.EventStimulus;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.VariableRateStimulus;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(id = "Inchron-Stimuli-TypeCheck",
			checks = {"Unsupported stimuli types"})

public class InchronStimuliTypeCheck extends AmaltheaValidation {

	@Override
	public EClassifier getEClassifier() {
		return ePackage.getStimulus();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof Stimulus) {
			Stimulus stim = (Stimulus) eObject;
			if (stim instanceof VariableRateStimulus || stim instanceof CustomStimulus || stim instanceof EventStimulus
					|| stim instanceof ArrivalCurveStimulus) {
				addIssue(results, stim, null,
						"Stimulus " + name(stim) + " is of unsupported type: " + stim.eClass().getName());
			}
		}
	}

}