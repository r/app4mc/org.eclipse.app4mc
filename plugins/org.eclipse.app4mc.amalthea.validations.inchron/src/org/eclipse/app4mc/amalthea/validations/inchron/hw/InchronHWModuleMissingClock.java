/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.hw;

import java.util.List;

import org.eclipse.app4mc.amalthea.model.FrequencyDomain;
import org.eclipse.app4mc.amalthea.model.HwModule;
import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;

@Validation(id = "Inchron-HWModule-MissingClockReference",
            checks = { "HW Module must have 'Frequency Domain' reference"})

public class InchronHWModuleMissingClock extends AmaltheaValidation {
	@Override
	public EClassifier getEClassifier() {
		return ePackage.getHwModule();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
		if (eObject instanceof HwModule) {
			HwModule module = (HwModule) eObject;
			FrequencyDomain freq = module.getFrequencyDomain();
			if (freq == null) {
				addIssue(results, module, ePackage.getHwModule_FrequencyDomain(),
						"HW Module " + name(module) + " missing its 'Frequency Domain' reference");
			}
		}
	}

}