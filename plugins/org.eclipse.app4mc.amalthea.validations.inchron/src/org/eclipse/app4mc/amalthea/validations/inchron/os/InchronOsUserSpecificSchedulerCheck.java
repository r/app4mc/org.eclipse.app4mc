/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron.os;

import java.util.List;

import org.eclipse.app4mc.amalthea.validation.core.AmaltheaValidation;
import org.eclipse.app4mc.validation.annotation.Validation;
import org.eclipse.app4mc.validation.core.ValidationDiagnostic;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;



@Validation(id = "Inchron-OS-UserSpecificSchedulerCheck",
			checks = { "User specific task scheduler needs at least one task allocation"})

public class InchronOsUserSpecificSchedulerCheck extends AmaltheaValidation {
	@Override
	public EClassifier getEClassifier() {
// FIXME
//		return ePackage.getUserSpecificSchedulingAlgorithm();
		return ePackage.getAmalthea();
	}

	@Override
	public void validate(EObject eObject, List<ValidationDiagnostic> results) {
// FIXME
//		if (eObject instanceof UserSpecificSchedulingAlgorithm) {
//			UserSpecificSchedulingAlgorithm schedAlg = (UserSpecificSchedulingAlgorithm) eObject;
//			EObject obj = schedAlg.eContainer();
//			if (obj instanceof TaskScheduler) {
//				TaskScheduler sched = (TaskScheduler) obj;
//				EList<TaskAllocation> listTaskAlloc = sched.getTaskAllocations();
//				EList<RunnableAllocation> listRunAlloc = sched.getRunnableAllocations();
//				if (listTaskAlloc.isEmpty() && listRunAlloc.isEmpty()) {
//					addIssue(results, schedAlg, null, "User Specific Task Scheduler Algorithm " + name(sched)
//							+ " needs at least one task/runnable allocation");
//				}
//			}
//		}
	}

}