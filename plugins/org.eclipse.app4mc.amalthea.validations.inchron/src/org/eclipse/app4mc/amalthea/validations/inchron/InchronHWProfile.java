/*******************************************************************************
 * Copyright (c) 2020 INCHRON AG and others.
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     INCHRON AG - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.validations.inchron;

import org.eclipse.app4mc.amalthea.validations.inchron.hw.InchronHWMemoryPortTypeCheck;
import org.eclipse.app4mc.amalthea.validations.inchron.hw.InchronHWModuleMissingClock;
import org.eclipse.app4mc.amalthea.validations.inchron.hw.InchronHWModulePortWidthCheck;
import org.eclipse.app4mc.amalthea.validations.inchron.hw.InchronHWPUPortTypeCheck;
import org.eclipse.app4mc.amalthea.validations.standard.HardwareProfile;
import org.eclipse.app4mc.amalthea.validations.standard.hardware.AmHwPortBitWidth;
import org.eclipse.app4mc.validation.annotation.Profile;
import org.eclipse.app4mc.validation.annotation.ProfileGroup;
import org.eclipse.app4mc.validation.annotation.ValidationGroup;
import org.eclipse.app4mc.validation.core.IProfile;
import org.eclipse.app4mc.validation.core.Severity;

@Profile(name = "Hardware Validations (INCHRON)")

@ProfileGroup(
		// Amalthea standard hardware profile
		profiles = HardwareProfile.class
	)

@ValidationGroup(
		severity = Severity.ERROR,
		validations =  {
				InchronHWModuleMissingClock.class,
				InchronHWModulePortWidthCheck.class,
				InchronHWPUPortTypeCheck.class,
				InchronHWMemoryPortTypeCheck.class,

				// raise severity of standard validation to error
				AmHwPortBitWidth.class
		}
	)

public class InchronHWProfile implements IProfile {
	// Do nothing
}
