/**
 ********************************************************************************
 * Copyright (c) 2020-2022 OFFIS e. V., DLR e. V. and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     OFFIS e. V. - initial API and implementation
 *     DLR e. V.   - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.visualization.runnabledependency;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.app4mc.amalthea.model.ActivityGraphItem;
import org.eclipse.app4mc.amalthea.model.Group;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.amalthea.model.InterProcessStimulus;
import org.eclipse.app4mc.amalthea.model.InterProcessTrigger;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.OsEvent;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableCall;
import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.amalthea.model.SetEvent;
import org.eclipse.app4mc.amalthea.model.Stimulus;
import org.eclipse.app4mc.amalthea.model.Task;
import org.eclipse.app4mc.amalthea.model.WaitEvent;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.app4mc.visualization.util.svg.GraphvizDiagram;
import org.eclipse.emf.ecore.EObject;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Multimaps;

/**
 * Generate Graphviz DOT from software models. 
 * The following elements are considered:
 * <ul>
 *     <li> {@link org.eclipse.app4mc.amalthea.model.Runnable Runnables}  </li>
 *     <li> {@link org.eclipse.app4mc.amalthea.model.Stimulus Stimuli} </li>
 *     <li> {@link org.eclipse.app4mc.amalthea.model.Task Tasks}  </li>
 *     <li> {@link org.eclipse.app4mc.amalthea.model.RunnableCall RunnableCall} (within {@link org.eclipse.app4mc.amalthea.model.Task Tasks}) </li>
 *     <li> {@link org.eclipse.app4mc.amalthea.model.InterProcessTrigger InterProcessTrigger} (within {@link org.eclipse.app4mc.amalthea.model.Task Tasks}) </li>
 *     <li> {@link org.eclipse.app4mc.amalthea.model.SetEvent SetEvent} (within {@link org.eclipse.app4mc.amalthea.model.Task Tasks}) </li>
 *     <li> {@link org.eclipse.app4mc.amalthea.model.WaitEvent WaitEvent} (within {@link org.eclipse.app4mc.amalthea.model.Task Tasks}) </li>
 *     <li> {@link org.eclipse.app4mc.amalthea.model.LabelAccess LabelAccess} (within {@link org.eclipse.app4mc.amalthea.model.Runnable Runnables}) </li>
 * </ul>
 * @author Jan Steffen Becker (jan.becker@dlr.de)
 *
 */
public class RunnableDependencyGenerator {

	private static final int MAX_MODEL_SIZE = 200;

	// Suppress default constructor
	private RunnableDependencyGenerator() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Build the DOT graph
	 * 
	 * @param diagram          the diagram that should be (re)computed
	 * @param swModel          software model
	 * @param config           diagram configuration
	 * @param limitModelSize   only generate if model size is below limit
	 * @param createHyperlinks create hyperlinks for navigation
	 */
	public static void updateDiagram(GraphvizDiagram diagram, SWModel swModel, RunnableDependencyConfig config, boolean limitModelSize, boolean createHyperlinks) {
		// reset old diagram data
		diagram.resetDiagramData();

		// generate new diagram
	
		if (limitModelSize && swModel.getRunnables().size() > MAX_MODEL_SIZE) {
			diagram.append(
					"digraph model {\"The model contains more than " + MAX_MODEL_SIZE
					+ " Runnables and thus is too large to be visualized.\"}");
			return;
		}

		Multimap<Runnable, Label> labelReads = MultimapBuilder.linkedHashKeys().linkedHashSetValues().build();
		Multimap<Runnable, Label> labelWrites = MultimapBuilder.linkedHashKeys().linkedHashSetValues().build();
	
		diagram.append("digraph model {");
		diagram.append(System.lineSeparator());
		diagram.append("node [shape=\"Mrecord\", fontname=\"Helvetica\"];");
		diagram.append(System.lineSeparator());
		diagram.append("edge [fontname=\"Helvetica\"];");
		diagram.append(System.lineSeparator());
		if (config.isHorizontalLayout()) {
			diagram.append("rankdir=LR;");
			diagram.append(System.lineSeparator());
		}
		int i = 0;
	
		if (config.isShowCallDependencies()) {
			buildCallGraph(swModel, diagram);
		}
		if (config.isShowTasks()) {
			buildTaskGroups(swModel, diagram);
		}
	
		// build nodes for Runnables
		diagram.append("node [shape=\"Mrecord\", fontname=\"Helvetica\"];");
		diagram.append(System.lineSeparator());
		for (Runnable runnable : swModel.getRunnables()) {
			Set<Label> reads = SoftwareUtil.getReadLabelSet(runnable, null);
			labelReads.putAll(runnable, reads);
			Set<Label> writes = SoftwareUtil.getWriteLabelSet(runnable, null);
			labelWrites.putAll(runnable, writes);
			buildNode(runnable, reads, writes, i++, diagram, config, createHyperlinks);
		}
	
		// build data dependencies
		if (config.isShowLabelDependencies()) {
			Multimap<Label, Runnable> readers = Multimaps.invertFrom(labelReads,
					MultimapBuilder.linkedHashKeys().linkedHashSetValues().build());
			Set<List<Runnable>> connections = new HashSet<>();
			labelWrites.forEach((writer, label) -> readers.get(label).forEach(reader -> {
				if (writer == reader || !config.isShowLabels() && !connections.add(Arrays.asList(writer, reader))) {
					return;
				}
				diagram.appendId(writer);
				if (config.isShowLabels()) {
					diagram.append(":W");
					diagram.appendId(label);
				}
				diagram.append(config.isHorizontalLayout() ? ":e -> " : ":s -> ");
				diagram.appendId(reader);
				if (config.isShowLabels()) {
					diagram.append(":R");
					diagram.appendId(label);
				}
				diagram.append(config.isHorizontalLayout() ? ":w[color=\"" : ":n[color=\"");
				diagram.append(getColor(swModel.getRunnables().indexOf(writer)));
				diagram.append("\"];");
				diagram.append(System.lineSeparator());
			}));
		}
		diagram.append("}");
	}

	/**
	 * Build a directed graph from a call sequence. In the graph,
	 * {@link org.eclipse.app4mc.amalthea.model.Runable Runnables} are used as
	 * nodes, and {@link org.eclipse.app4mc.amalthea.model.Runable Runnables},
	 * {@link org.eclipse.app4mc.amalthea.model.Stimulus Stimuli}, and
	 * {@link org.eclipse.app4mc.amalthea.model.OsEvent OsEvents}. Edge sources and
	 * targets are added to the {@code sources} and {@code targets} maps. When
	 * returning, the {@code inputs} set contains the outgoing edges for the last
	 * runnable in the sequence.
	 * 
	 * @param lst			list of activity graph items to process
	 * @param prevRunnable	Runnable that comes before this sub-chain
	 * @param inputs		incoming edges to this sub-chain
	 * @param sources		map to collect sources of edges (i.e maps edges to source objects)
	 * @param targets		map to collect targets of edges (i.e maps edges to source objects)
	 * @return the last runnable in this chain
	 */
	private static Runnable buildCallChain(List<ActivityGraphItem> lst, Runnable prevRunnable, Set<EObject> inputs,
			Multimap<Object, Runnable> sources, Multimap<Object, Runnable> targets) {
		Runnable lastRunnable = prevRunnable;
		for (ActivityGraphItem itm : lst) {
			if (itm instanceof RunnableCall) {
				Runnable r = ((RunnableCall) itm).getRunnable();
				for (EObject o : inputs) {
					targets.put(o, r);
				}
				inputs.clear();
				if (lastRunnable != null) {
					sources.put(lastRunnable, lastRunnable);
					targets.put(lastRunnable, r);
				}
				lastRunnable = r;
			} else if (itm instanceof SetEvent) {
				if (lastRunnable != null) {
					for (OsEvent e : ((SetEvent) itm).getEventMask().getEvents()) {
						sources.put(e, lastRunnable);
					}
				}
			} else if (itm instanceof WaitEvent) {
				inputs.addAll(((WaitEvent) itm).getEventMask().getEvents());
			} else if (itm instanceof InterProcessTrigger) {
				if (lastRunnable != null) {
					sources.put(((InterProcessTrigger) itm).getStimulus(), lastRunnable);
				}
			} else if (itm instanceof Group) {
				lastRunnable = buildCallChain(((Group) itm).getItems(), lastRunnable, inputs, sources, targets);
			}
		}
		return lastRunnable;
	}

	/**
	 * Construct Graphviz edges for the call graph
	 * 
	 * @param model		software model
	 * @param diagram	diagram to build
	 */
	private static void buildCallGraph(SWModel model, GraphvizDiagram diagram) {
		Multimap<Object, Runnable> sources = MultimapBuilder.linkedHashKeys().linkedHashSetValues().build();
		Multimap<Object, Runnable> targets = MultimapBuilder.linkedHashKeys().linkedHashSetValues().build();

		// build stimuli
		Set<Stimulus> stims = new LinkedHashSet<>();
		for (Task t : model.getTasks()) {
			stims.addAll(t.getStimuli());
			buildCallChain(t.getActivityGraph().getItems(), null, new LinkedHashSet<>(t.getStimuli()), sources,
					targets);
		}
		diagram.append("node[shape=\"ellipse\"];" + System.lineSeparator());

		for (Stimulus s : stims) {
			if (s instanceof InterProcessStimulus) {
				continue;
			}
			for (Runnable r : targets.get(s)) {
				diagram.append("\"" + getName(s) + "\" -> ");
				diagram.appendId(r);
				diagram.append(System.lineSeparator());
			}
		}

		// build edges between runnables
		for (Entry<Object, Runnable> e : sources.entries()) {
			for (Runnable r : targets.get(e.getKey())) {
				diagram.appendId(e.getValue());
				diagram.append(" -> ");
				diagram.appendId(r);
				if (e.getKey() instanceof Runnable) {
					diagram.append("[style=\"dashed\"]");
				} else if (e.getKey() instanceof INamed) {
					diagram.append("[label=\"" + ((EObject) e.getKey()).eClass().getName() + " "
							+ ((INamed) e.getKey()).getName() + "\"]");
				}
				diagram.append(System.lineSeparator());
			}
		}
	}

	/**
	 * Build a node for a runnable
	 * 
	 * @param runnable	Runnable to build node for
	 * @param reads		Labels read by the runnable
	 * @param writes	Labels written by the runnable
	 * @param idx		Runnable index for the coloring
	 * @param diagram	diagram to build
	 * @param config	diagram configuration
	 * @param addLink	create hyperlink for node
	 */
	private static void buildNode(Runnable runnable, Set<Label> reads, Set<Label> writes, int idx,
			GraphvizDiagram diagram, RunnableDependencyConfig config, boolean addLink) {
		diagram.appendId(runnable);
		diagram.append("[shape=\"Mrecord\", color=\"");
		diagram.append(getColor(idx));
		diagram.append("\",label=\"{");
		if (config.isShowLabels()) {
			buildPortList(reads, "R", diagram);
			diagram.append("|");
			diagram.append(runnable.getName());
			diagram.append("|");
			buildPortList(writes, "W", diagram);
		} else {
			diagram.append(runnable.getName());
		}
		diagram.append("}\"");
		if (addLink) {
			diagram.appendUrl(runnable);
		}
		diagram.append("];");
		diagram.append(System.lineSeparator());
	}

	/**
	 * Build a list of labels in the Record Node Label
	 * 
	 * @param labels	labels in the list
	 * @param dir		access direction for the label ("R" or "W")
	 * @param diagram	diagram to build
	 */
	private static void buildPortList(Set<Label> labels, String dir, GraphvizDiagram diagram) {
		diagram.append("{");
		diagram.append(String.join("|", labels.stream().sorted((a, b) -> getName(a).compareTo(getName(b)))
				.map(label -> "<" + dir + diagram.getOrCreateId(label) + ">" + getName(label)).toArray(len -> new String[len])));
		diagram.append("}");
	}

	/**
	 * Build clusters from {@link org.eclipse.app4mc.amalthea.model.Task Tasks}.
	 * 
	 * @param sw		software model
	 * @param diagram	diagram to build
	 */
	private static void buildTaskGroups(SWModel sw, GraphvizDiagram diagram) {
		int cnt = 0;
		diagram.append("newrank=true");
		diagram.append(System.lineSeparator());
		for (Task t : sw.getTasks()) {
			diagram.append("subgraph cluster");
			diagram.append(Integer.toString(cnt++));
			diagram.append(" {");
			diagram.append(System.lineSeparator());
			diagram.append("fontname=\"Helvetica\"");
			diagram.append(System.lineSeparator());
			diagram.append("label=\"");
			diagram.append(t.getName());
			diagram.append("\"");
			diagram.append(System.lineSeparator());
			for (Runnable r : SoftwareUtil.getRunnableList(t, null)) {
				diagram.appendId(r);
				diagram.append(System.lineSeparator());
			}
			diagram.append("}");
			diagram.append(System.lineSeparator());
		}
	}

	/**
	 * Get a unique color based on an integer
	 * 
	 * @param n		the color list index
	 * @return a HTML color value
	 */
	private static String getColor(int n) {
		// build colors with maximum distance by shuffling the bits
		int colorBits = 0;
		for (int i = 0; i < 24; i++) {
			if ((n & (1 << i)) != 0) {
				colorBits |= 1 << (8 * (i % 3) + 7 - i / 3);
			}
		}
		return String.format("#%06x", colorBits);
	}

	/**
	 * Return the name of object element or {@code <unnamed>}
	 * 
	 * @param obj	an object
	 * @return the name
	 */
	private static String getName(INamed obj) {
		return obj.getName() != null ? obj.getName() : "<unnamed>";
	}

}
