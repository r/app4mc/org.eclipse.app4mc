/**
 ********************************************************************************
 * Copyright (c) 2020-2022 DLR e. V., OFFIS e. V. and others
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     OFFIS e. V. - initial API and implementation
 *     DLR e. V.   - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.amalthea.visualization.runnabledependency;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;

import org.eclipse.app4mc.amalthea.model.SWModel;
import org.eclipse.app4mc.visualization.ui.VisualizationParameters;
import org.eclipse.app4mc.visualization.ui.registry.Visualization;
import org.eclipse.app4mc.visualization.util.svg.GraphvizDiagram;
import org.eclipse.app4mc.visualization.util.svg.SvgUtil;
import org.eclipse.core.runtime.Platform;
import org.eclipse.e4.core.services.events.IEventBroker;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.RowLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationListener;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.osgi.service.component.annotations.Component;

@Component(property = {
		"name=Runnable Data Dependencies",
		"description=Runnable Label Access Visualization for Software Models"
})
public class RunnableDependencyVisualization implements Visualization {

	/**
	 * Entry point for the visualization framework
	 * 
	 * @param model			software model that shall be visualized
	 * @param parameters	visualization parameters
	 * @param parent		parent component
	 * @param broker		event broker for element selection
	 */
	@PostConstruct
	public void createVisualization(
			SWModel swModel,
			VisualizationParameters parameters,
			Composite parent,
			IEventBroker broker) {

		// Create central context object with all relevant inputs
		final Context context = new Context(swModel, parameters);

		Composite pane = new Composite(parent, SWT.NONE);
		GridLayoutFactory.fillDefaults().applyTo(pane);
		Composite buttonArea = new Composite(pane, SWT.NONE);

		addToggleButton(buttonArea, "Horizontal Layout", context.config::setHorizontalLayout, context.config.isHorizontalLayout());
		addToggleButton(buttonArea, "Show Labels", context.config::setShowLabels, context.config.isShowLabels());
		addToggleButton(buttonArea, "Show R/W dependencies", context.config::setShowLabelDependencies, context.config.isShowLabelDependencies());
		addToggleButton(buttonArea, "Show Control Flow", context.config::setShowCallDependencies, context.config.isShowCallDependencies());
		addToggleButton(buttonArea, "Show Tasks", context.config::setShowTasks, context.config.isShowTasks());

		addPushButton(buttonArea, "Export", () -> {
			FileDialog fd = new FileDialog(parent.getShell(), SWT.SAVE);
			fd.setFilterNames(new String[] { "Scalable Vector Graphics (*.svg)", "Portable Document Format (*.pdf)",
					"Portable Network Graphics (*.png)", "all files (*.*)" });
			fd.setFilterExtensions(new String[] { "*.svg", "*.pdf", "*.png", "*.*" });
			String path = fd.open();
			if (path != null) {
				export(path, context);
			}
		});

		addZoomBox(buttonArea, context);

		RowLayoutFactory.swtDefaults().fill(true).applyTo(buttonArea);

		Browser browser = addBrowser(pane, broker, context);
		
		GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(browser);

		// Create and display content
		updateContent(browser, context);
	}

	private Browser addBrowser(Composite pane, IEventBroker broker, final Context context) {
		Browser browser = new Browser(pane, SWT.NONE);

		// Setup navigation to a selected element in the model viewer
		if (broker != null) {
			browser.addLocationListener(LocationListener.changingAdapter(c -> {
				c.doit = true;
			
				Object target = null;
				int idx = c.location.lastIndexOf('#');
				if (idx >= 0) {
					target = context.diagram.getObjectById(c.location.substring(idx + 1));
				}
				if (target != null) {
					HashMap<String, Object> data = new HashMap<>();
					data.put("modelElements", Collections.singletonList(target));
					broker.send("org/eclipse/app4mc/amalthea/editor/SELECT", data);
			
					c.doit = false;
				}
			}));
		}

		// React to configuration parameter changes
		context.config.addChangeListener(e -> {
			if (e.getPropertyName().equals("scale"))
				updateSvgScale(browser, (int) e.getNewValue());
			if (e.getPropertyName().startsWith("parameter"))
				updateContent(browser, context);
		});

		return browser;
	}

	/**
	 * Helper for adding a toggle button
	 * 
	 * @param parent          container element
	 * @param text            label
	 * @param f               button select action; takes the button's selection status as an argument
	 * @param initialSelected initial selection state of the button
	 */
	private void addToggleButton(Composite parent, String text, final Consumer<Boolean> f, boolean initialSelected) {
		final Button btn = new Button(parent, SWT.TOGGLE | SWT.FLAT);
		btn.setText(text);
		btn.setSelection(initialSelected);
		btn.addListener(SWT.Selection, e -> f.accept(btn.getSelection()));
	}

	/**
	 * Helper for adding a push button
	 * 
	 * @param parent container element
	 * @param text   label
	 * @param cmd    button select action
	 */
	private void addPushButton(Composite parent, String text, Runnable cmd) {
		final Button btn = new Button(parent, SWT.PUSH | SWT.FLAT);
		btn.setText(text);
		btn.addListener(SWT.Selection, e -> cmd.run());
	}

	private void addZoomBox(Composite buttonArea, Context context) {
		// Align the box with the other buttons
		final Composite zoomArea = new Composite(buttonArea, SWT.NONE);
		RowLayoutFactory.fillDefaults().margins(1, 1).applyTo(zoomArea);

		final Composite box = new Composite(zoomArea, SWT.BORDER);

		final Button btnLeft = new Button(box, SWT.ARROW | SWT.LEFT | SWT.FLAT);
		btnLeft.addListener(SWT.Selection, e -> context.config.decrementScale());

		final CLabel scaleLabel = new CLabel(box, SWT.FLAT | SWT.CENTER);
		scaleLabel.setText(String.format("%d %%", context.config.getScale())); // set initial label text

		context.config.addChangeListener(e -> {
			if (e.getPropertyName().equals("scale"))
				scaleLabel.setText(String.format("%d %%", (int) e.getNewValue())); // update label text
		});

		final Button btnRight = new Button(box, SWT.ARROW | SWT.RIGHT | SWT.FLAT);
		btnRight.addListener(SWT.Selection, e -> context.config.incrementScale());

		RowLayoutFactory.fillDefaults().fill(true).applyTo(box);
	}

	private void updateSvgScale(Browser browser, int newScale) {
		// Update SVG size in browser via JavaScript/DOM
		if (browser != null) {
			browser.execute(SvgUtil.buildUpdateScaleCommand(newScale));
		}
	}

	/**
	 * Visualizes a given model element in a browser.
	 * <p>
	 * The Graphviz graph is constructed and compiled in a separate thread.
	 * 
	 * @param browser 
	 * @param context 
	 */
	private void updateContent(Browser browser, Context context) {
		new Thread(() -> {
			// Build Graphviz diagram text
			RunnableDependencyGenerator.updateDiagram(context.diagram, context.model, context.config, true, true);

			// Render to SVG
			String result;
			try {
				result = context.diagram.renderToSvg();
			} catch (IOException e) {
				result = "Error invoking PlantUML: \"" + e.getMessage()
				+ "\". Make sure you have configured the path to the dot executable properly in the PlantUML preferences.";
				Platform.getLog(RunnableDependencyVisualization.class).error(result, e);
				return;
			}

			// Apply initial scale and display
			if (result != null && !browser.isDisposed()) {
				final String browserContent = SvgUtil.initiallyApplyScale(result, context.config.getScale());
				browser.getDisplay().asyncExec(() -> {
					if (!browser.isDisposed()) {
						browser.setText(browserContent);
					}
				});
			}

		}).start();
	}

	private String prepareErrorMessage(IOException e) {
		return "Error invoking Graphviz: \"" + e.getMessage()
				+ "\". Make sure you have configured the path to the dot executable properly in the PlantUML preferences.";
	}

	/**
	 * Export the visualization
	 * 
	 * @param path export destination
	 */
	private void export(String path, Context context) {
		Display display = Display.getCurrent();
		final Shell shell = display != null ? display.getActiveShell() : null;
		new Thread(() -> {
			String ext = path.substring(path.lastIndexOf('.') + 1);
			final GraphvizDiagram exportDiagram = new GraphvizDiagram();
			RunnableDependencyGenerator.updateDiagram(exportDiagram, context.model, context.config, !"dot".equals(ext), false);
			try {
				if ("dot".equals(ext)) {
					try (PrintWriter writer = new PrintWriter(path)) {
						writer.append(exportDiagram.getDiagramText());
					}
				} else {
					exportDiagram.runGraphviz("-T" + ext, "-o", path);
				}
			} catch (IOException e) {
				String msg = prepareErrorMessage(e);
				Platform.getLog(RunnableDependencyVisualization.class).error(msg, e);
				MessageDialog.openError(shell, "Error during export", msg);
			}
		}).start();
	}

	static class Context {
		public final SWModel model;
		public final RunnableDependencyConfig config;
		public final GraphvizDiagram diagram = new GraphvizDiagram();

		public Context(SWModel model, VisualizationParameters viewParameters) {
			this.model = model;
			this.config =  new RunnableDependencyConfig(viewParameters);
		}
	}

}
