/**
 ********************************************************************************
 * Copyright (c) 2022 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.emf.visualizations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.app4mc.visualization.util.svg.PlantUmlDiagram;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

public class EObjectRefsGenerator {

	// Suppress default constructor
	private EObjectRefsGenerator() {
		throw new IllegalStateException("Utility class");
	}

	public static void updateDiagram(PlantUmlDiagram diagram, EObject eObject, EObjectRefsConfig config) {
		// reset old diagram data
		diagram.resetDiagramData();

		// generate new diagram
		
		diagram.append("@startuml\n\n");

		diagram.append("' Created by EObjectRefsBuilder (" + timestamp() + ")\n\n");

		diagram.append("hide empty members\n\n");

		if (config.isHorizontalLayout()) {
			diagram.append("left to right direction\n\n");
		} else {
			diagram.append("top to bottom direction\n\n");
		}

		buildContent(diagram, eObject, config);

		diagram.append("\n@enduml");
	}

	private static void buildContent(PlantUmlDiagram diagram, EObject selectedEObj, EObjectRefsConfig config) {

		final Set<Object> nodes = new HashSet<>(); // already created nodes
		final Set<String> connections = new HashSet<>(); // already created connections

		Collection<Setting> references = filterReferences(getAllReferencesTo(selectedEObj), config);

		diagram.append("\n' ===== Main object =====\n\n");

		createClass(selectedEObj, nodes, diagram);

		diagram.append("\n' ===== Referring objects (and their containers) =====\n\n");

		for (final EStructuralFeature.Setting setting : references) {
			EObject eObject = setting.getEObject();
			boolean created = createClass(eObject, nodes, diagram);

			if (created && !isNamePresent(eObject)) {
				/*- As name is not present, generate the hierarchy till an object with Structural Feature name is present */
				createContainer(eObject, config.isShowReferenceLabels(), nodes, connections, diagram);
			}
		}

		diagram.append("\n' ===== References =====\n\n");

		for (final EStructuralFeature.Setting setting : references) {
			createConnection(
					setting.getEObject(),
					selectedEObj,
					setting.getEStructuralFeature(),
					config.isShowReferenceLabels(),
					connections,
					diagram);
		}
	}

	/**
	 * @param selectedEObj
	 * @return
	 */
	private static Collection<Setting> getAllReferencesTo(EObject selectedEObj) {
		Resource eResource = selectedEObj.eResource();
		if (eResource == null) {
			return EcoreUtil.UsageCrossReferencer.find(selectedEObj, EcoreUtil.getRootContainer(selectedEObj));
		} else if (eResource.getResourceSet() == null) {
			return EcoreUtil.UsageCrossReferencer.find(selectedEObj, eResource);
		}
		return EcoreUtil.UsageCrossReferencer.find(selectedEObj, eResource.getResourceSet());
	}

	private static Collection<Setting> filterReferences(final Collection<Setting> settings, final EObjectRefsConfig config) {
		return settings.stream()
				.filter(s -> isVisibleReference(s.getEStructuralFeature(), config))
				.collect(Collectors.toList());
	}

	private static boolean isVisibleReference(final EStructuralFeature feature, final EObjectRefsConfig config) {
		if (feature instanceof EReference) {
			EReference ref = (EReference) feature;
			return !ref.isContainment()
				&& (!ref.isDerived() || config.isShowDerivedReferences());
		}
	
		return false;
	}

	private static boolean createClass(final EObject selectedEObj, final Set<Object> nodes, final PlantUmlDiagram diagram) {
		boolean success = nodes.add(selectedEObj);
		if (success) {
			// selected object has been added by previous line (now marked as created)

			final String name = getName(selectedEObj);
			final String className = selectedEObj.eClass().getName();
			final String id = diagram.getOrCreateId(selectedEObj);

			diagram.append("class \"" + name + "\" as " + id + " << (O,#B4A7E5) " + className + " >>" + " [[#" + id + "]]");
			diagram.append("\n");
		}
		return success;
	}

	private static void createContainer(final EObject eObject, boolean showLabel, final Set<Object> nodes, final Set<String> connections, final PlantUmlDiagram diagram) {
		final EObject eContainerObj = eObject.eContainer();
		if (eContainerObj == null) {
			return;
		}

		// create container
		createClass(eContainerObj, nodes, diagram);

		// create containment reference
		createConnection(eContainerObj, eObject, eObject.eContainmentFeature(), showLabel, connections, diagram);

		if (!isNamePresent(eContainerObj)) {
			createContainer(eContainerObj, showLabel, nodes, connections, diagram);
		}
	}

	private static void createConnection(final EObject from, final EObject to, final EStructuralFeature eStructuralFeature, boolean showLabel, final Set<String> connections, final PlantUmlDiagram diagram) {
		if (! (eStructuralFeature instanceof EReference))
			return;

		EReference reference = (EReference) eStructuralFeature;

		String id1 = diagram.getOrCreateId(from);
		String id2 = diagram.getOrCreateId(to);
		String arrowStyle = reference.isDerived() ? "[#gray,dashed]" : "";
		String arrow = reference.isContainment() ?  " *-- " : " -" + arrowStyle + "-> ";
		String labelSeparator = eStructuralFeature.isDerived() ? " #text:gray : " : " : ";
		String label = showLabel ? labelSeparator + eStructuralFeature.getName() : "";

		String conn = id1 + arrow + id2 + label;
		if (connections.add(conn)) {
			// connection has been added by previous line (now marked as created)

			diagram.append(conn);
			diagram.append("\n");
		}
	}

	private static boolean isNamePresent(final EObject eObject) {
		final EStructuralFeature nameFeature = eObject.eClass().getEStructuralFeature("name");
	
		return nameFeature != null;
	}

	private static String getName(final EObject eObject) {
		final EStructuralFeature eStructuralFeature = eObject.eClass().getEStructuralFeature("name");

		if (eStructuralFeature != null) {
			final Object originalName = eObject.eGet(eStructuralFeature);
			if (originalName != null && originalName.toString().length() > 0) {
				return (String) originalName;
			}
		}

		return eObject.eClass().getName() + "__" + eObject.hashCode();
	}

	static String timestamp() {
		final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		return dateFormat.format(new Date());
	}

}
