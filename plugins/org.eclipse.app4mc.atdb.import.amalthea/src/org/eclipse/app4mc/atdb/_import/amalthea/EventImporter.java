/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.amalthea;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventSet;
import org.eclipse.app4mc.amalthea.model.INamed;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.operation.IRunnableWithProgress;

public class EventImporter implements IRunnableWithProgress {

	private final ATDBConnection con;
	private final Amalthea model;

	public EventImporter(final ATDBConnection con, final Amalthea model) {
		this.con = con;
		this.model = model;
	}

	@Override
	public void run(IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException {
		if (model.getEventModel() != null) {
			final int eventCount = model.getEventModel().getEvents().size();
			final SubMonitor subMon = SubMonitor.convert(progressMonitor, "Importing events from AMALTHEA...", eventCount);
			try {
				con.executeBatchUpdate(atdbCon -> {
					final List<Event> events = model.getEventModel().getEvents();
					for(final Event event:events) {
						// ignore event sets for now
						if (event instanceof EventSet) continue;

						final String eventName = event.getName();
						final Optional<String> optEventTypeName = getEventTypeName(event);
						if (optEventTypeName.isPresent()) {
							atdbCon.insertEventType(optEventTypeName.get());
						}
						final String eventTypeName = optEventTypeName.orElse("");

						final Optional<String> optEntityName = getEntityName(event);
						if (optEntityName.isPresent()) {
							atdbCon.insertEntity(optEntityName.get(), "");
						}
						final String entityName = optEntityName.orElse("");

						final String sourceEntityName = getSourceEntityName(event).orElse("");

						atdbCon.insertEvent(eventName, eventTypeName, entityName, sourceEntityName);
						subMon.worked(1);
					}
				});
			} catch (SQLException e) {
				throw new InvocationTargetException(e);
			}
			subMon.done();
		}
	}

	public static Optional<String> getEventTypeName(final Event event) {
		return Stream.of("eventType").map(p -> getPropertyValue(event, p)).filter(Objects::nonNull)
				.map(p -> p instanceof Enumerator ? ((Enumerator)p).getName() : p.toString()).findFirst();
	}

	public static Optional<String> getEntityName(final Event event) {
		return Stream.of("entity").map(p -> getPropertyValue(event, p)).filter(Objects::nonNull)
				.map(p -> p instanceof INamed ? ((INamed)p).getName() : p.toString()).findFirst();
	}

	public static Optional<String> getSourceEntityName(final Event event) {
		return Stream.of("runnable", "process", "core").map(p -> getPropertyValue(event, p)).filter(Objects::nonNull)
				.map(p -> p instanceof INamed ? ((INamed)p).getName() : p.toString()).findFirst();
	}

	private static Object getPropertyValue(final EObject eo, final String propertyName) {
		if (eo == null || propertyName == null || eo.eClass() == null) {
			return null;
		}
		final EStructuralFeature feature = eo.eClass().getEStructuralFeature(propertyName);
		if (feature == null) {
			return null;
		}
		return eo.eGet(feature);
	}

}
