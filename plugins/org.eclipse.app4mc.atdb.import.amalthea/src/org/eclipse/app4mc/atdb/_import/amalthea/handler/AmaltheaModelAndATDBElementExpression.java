/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.amalthea.handler;

import java.util.List;

import javax.inject.Named;

import org.eclipse.core.resources.IFile;
import org.eclipse.e4.core.di.annotations.Evaluate;
import org.eclipse.e4.core.di.annotations.Optional;
import org.eclipse.e4.ui.services.IServiceConstants;
import org.eclipse.jface.viewers.IStructuredSelection;

public class AmaltheaModelAndATDBElementExpression {

	@Evaluate
	public boolean evaluate(@Optional @Named(IServiceConstants.ACTIVE_SELECTION) IStructuredSelection selection) {
		if (selection != null && selection.size() == 2) {
			final List<?> selectionList = selection.toList();
			if (selectionList.get(0) instanceof IFile && selectionList.get(1) instanceof IFile) {
				final String firstExt = ((IFile)selectionList.get(0)).getFileExtension();
				final String secondExt = ((IFile)selectionList.get(1)).getFileExtension();
				if (firstExt.equalsIgnoreCase("atdb") && secondExt.equalsIgnoreCase("amxmi") ||
					firstExt.equalsIgnoreCase("amxmi") && secondExt.equalsIgnoreCase("atdb")) {
					return true;
				}
			}
		}
		return false;
	}

}
