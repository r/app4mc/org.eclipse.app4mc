/**
 ********************************************************************************
 * Copyright (c) 2020 Eclipse APP4MC contributors.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************
 */

package org.eclipse.app4mc.atdb._import.amalthea;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.app4mc.amalthea.model.AbstractEventChain;
import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventChainItem;
import org.eclipse.app4mc.amalthea.model.EventChainItemType;
import org.eclipse.app4mc.amalthea.model.util.ConstraintsUtil;
import org.eclipse.app4mc.atdb.ATDBConnection;
import org.eclipse.app4mc.atdb.EntityProperty;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

public class EventChainImporter implements IRunnableWithProgress {

	private final ATDBConnection con;
	private final Amalthea model;

	public EventChainImporter(final ATDBConnection con, final Amalthea model) {
		this.con = con;
		this.model = model;
	}

	@Override
	public void run(IProgressMonitor progressMonitor) throws InvocationTargetException, InterruptedException {
		if (model.getConstraintsModel() != null) {
			final List<AbstractEventChain> allECs = ConstraintsUtil.getAllAbstractEventChains(model.getConstraintsModel());
			final int eventChainCount = allECs.size();
			final SubMonitor subMon = SubMonitor.convert(progressMonitor, "Importing event chains from AMALTHEA...", eventChainCount);
			try {
				con.executeBatchUpdate(atdbCon -> {
					for(final EntityProperty ep:EntityProperty.eventChainProperties) {
						atdbCon.insertProperty(ep.camelName, ep.type);
					}

					// first add all ecs as entity, so that we can refer to them in the second step
					for(final AbstractEventChain ec:allECs) {
						final String eventChainName = ec.getName();
						atdbCon.insertEventChain(eventChainName);
					}

					// second set all properties
					for(final AbstractEventChain ec:allECs) {
						final String eventChainName = ec.getName();

						// items
						final List<String> itemNames = ec.getItems().stream().map(EventChainItem::getEventChain)
								.map(AbstractEventChain::getName).collect(Collectors.toList());
						atdbCon.insertEntityRefsPropertyValue(eventChainName, EntityProperty.EC_ITEMS.camelName, itemNames);

						// stimulus/response events
						insertEventRefProp(atdbCon, eventChainName, EntityProperty.EC_STIMULUS.camelName, ec.getStimulus());
						insertEventRefProp(atdbCon, eventChainName, EntityProperty.EC_RESPONSE.camelName, ec.getResponse());

						// min items completed
						if (ec.getItemType() == EventChainItemType.PARALLEL && ec.getMinItemsCompleted() > 0) {
							atdbCon.insertPropertyValue(eventChainName, EntityProperty.EC_MIN_ITEMS_COMPLETED.camelName, ec.getMinItemsCompleted());
						}
						subMon.worked(1);
					}
				});
			} catch (SQLException e) {
				throw new InvocationTargetException(e);
			}
			subMon.done();
		}
	}

	private static void insertEventRefProp(final ATDBConnection atdbCon, final String eventChainName, final String propertyName,
			final Event ev) throws SQLException{
		final String eventName = ev.getName();
		final String eventTypeName = EventImporter.getEventTypeName(ev).orElse("");
		final String subjectEntityName = EventImporter.getEntityName(ev).orElse("");
		final String sourceEntityName = EventImporter.getSourceEntityName(ev).orElse("");
		atdbCon.insertEventRefPropertyValue(eventChainName, propertyName, eventName, eventTypeName, subjectEntityName, sourceEntityName);
	}

}
